package pl.passbox.model.accounts;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TeamMember {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status;

    @Email
    @NotBlank
    private String email;

    @NotBlank
    private String firstname;

    @NotBlank
    private String lastname;

    @NotBlank
    private String password;

    @NotNull
    private Instant created;

    private Instant modified;

    private Instant lastLogin;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "organization_id")
    private Organization organization;

    @NotNull
    @OneToOne
    @JoinColumn(name = "settings_id")
    private Settings settings;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "created_by_id")
    private TeamMember createdBy;

    @ManyToOne
    @JoinColumn(name = "modified_by_id")
    private TeamMember modifiedBy;

    @NotEmpty
    @OneToMany
    @JoinColumn(name = "team_member_id")
    private List<Permission> permissions;

    @OneToMany(mappedBy = "id.teamMember")
    private List<SocialConnection> socialConnections;

    public enum Status {
        UNCONFIRMED,
        UNVERIFIED,
        ACTIVE,
        BLOCKED,
        DELETED
    }

    @Data
    @Entity
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Table(name = "team_member_permission")
    public static class Permission {

        @Id
        @GeneratedValue(generator = "UUID")
        @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
        private UUID id;

        @NotNull
        @Enumerated(EnumType.STRING)
        private Authority authority;

        public enum Authority {

        }

    }

}
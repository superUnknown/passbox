package pl.passbox.model.accounts;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Organization {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status;

    @NotBlank
    private String name;

    @Email
    @NotBlank
    private String email;

    @NotBlank
    private String subdomain;

    @NotNull
    private Instant created;

    private Instant modified;

    private String description;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "owner_id")
    private TeamMember owner;

    @ManyToOne
    @JoinColumn(name = "modified_by_id")
    private TeamMember modifiedBy;

    @OneToOne
    @JoinColumn(name = "social_channels_id")
    private SocialChannels socialChannels;

    @OneToOne
    @JoinColumn(name = "location_id")
    private Address address;

    @OneToOne
    @JoinColumn(name = "facebook_page_id")
    private FacebookPage facebookPage;

    @OneToOne
    @JoinColumn(name = "payments_settings_id")
    private PaymentsSettings paymentsSettings;

    @NotEmpty
    @OneToMany(mappedBy = "organization")
    private List<TeamMember> teamMembers;

    public enum Status {
        UNCONFIRMED,
        ACTIVE,
        BLOCKED,
        DELETED
    }

}
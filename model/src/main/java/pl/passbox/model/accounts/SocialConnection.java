package pl.passbox.model.accounts;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SocialConnection {

    @EmbeddedId
    private SocialConnectionId id;

    @NotNull
    private Integer rank;

    @NotBlank
    private String accessToken;

    @NotBlank
    private String refreshToken;

    @NotNull
    private Long expireTime; // todo czy Instant? lub dodać created
    
    public enum Provider {
        FACEBOOK
    }

    @Data
    @Builder
    @Embeddable
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SocialConnectionId implements Serializable {

        @NotNull
        @ManyToOne
        @JoinColumn(name = "team_member_id")
        private TeamMember teamMember;

        @NotNull
        @Enumerated(EnumType.STRING)
        private Provider provider;

        @NotBlank
        private String providerUserId;

    }

}
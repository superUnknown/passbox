package pl.passbox.model.accounts;

import com.neovisionaries.i18n.CurrencyCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PaymentsSettings {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @NotBlank
    private String bankAccount;

    @NotNull
    @Enumerated(EnumType.STRING)
    private RefundHandling refundHandling;

    @NotNull
    @Enumerated(EnumType.STRING)
    private CurrencyCode currency;

    @OneToOne
    @JoinColumn(name = "invoice_data_id")
    private InvoiceData invoiceData;

    public enum RefundHandling {
        AUTO,
        MANUAL
    }

    @Data
    @Entity
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class InvoiceData {

        @Id
        @GeneratedValue(generator = "UUID")
        @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
        private UUID id;

        @NotBlank
        private String companyName;

        @NotBlank
        private String nip;

        @NotNull
        @OneToOne
        @JoinColumn(name = "location_id")
        private Address address;

    }

}
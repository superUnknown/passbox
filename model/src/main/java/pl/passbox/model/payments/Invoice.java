package pl.passbox.model.payments;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DiscriminatorOptions;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@Inheritance
@NoArgsConstructor
@AllArgsConstructor
@DiscriminatorColumn(name = "type")
@DiscriminatorOptions(force = true)
public abstract class Invoice {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @NotBlank
    private String extInvoiceId;

    public abstract Type getType();

    public enum Type {
        VAT,
        CORRECTION
    }

    @Data
    @Entity
    @NoArgsConstructor
    @DiscriminatorValue("VAT")
    public static class Vat extends Invoice {

        @Enumerated(EnumType.STRING)
        @Column(insertable = false, updatable = false)
        private final Type type = Type.VAT;

        @OneToMany
        @JoinColumn(name = "parent_invoice_id")
        private List<Invoice.Correction> corrections;

    }

    @Data
    @Entity
    @NoArgsConstructor
    @DiscriminatorValue("CORRECTION")
    public static class Correction extends Invoice {

        @Enumerated(EnumType.STRING)
        @Column(insertable = false, updatable = false)
        private final Type type = Type.CORRECTION;

    }

}
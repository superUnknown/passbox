package pl.passbox.model.payments;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TariffDiscount {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status;

    @NotBlank
    private String code;

    @Min(1)
    @NotNull
    private Integer quantity;

    @NotNull
    private Instant validTo;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "tariff_id")
    private Tariff tariff;

    @OneToMany
    private List<Realization> realizations;

    public enum Status {
        ACTIVE,
        DELETED
    }

    @Data
    @Entity
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Table(name = "tariff_discount_realization")
    public static class Realization {

        @Id
        @GeneratedValue(generator = "UUID")
        @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
        private UUID id;

        @NotNull
        private UUID eventId;

        @NotNull
        private Instant realized;

        @NotNull
        @ManyToOne
        @JoinColumn(name = "tariff_discount_id")
        private TariffDiscount tariffDiscount;

    }

}
package pl.passbox.model.payments;

import com.neovisionaries.i18n.CurrencyCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DiscriminatorOptions;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import pl.passbox.model.events.Event;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@Inheritance
@NoArgsConstructor
@AllArgsConstructor
@DiscriminatorColumn(name = "type")
@DiscriminatorOptions(force = true)
public abstract class Transaction {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status;

    @Min(0)
    @NotNull
    private BigDecimal totalAmount;

    @NotNull
    @Enumerated(EnumType.STRING)
    private CurrencyCode currency;

    @NotNull
    private Instant created;

    private Instant completed;

    public abstract Type getType();

    public enum Type {
        PAYMENT,
        REFUND,
        PAYOUT
    }

    public enum Status {
        INITIALIZED,
        PENDING,
        COMPLETED,
        CANCELED
    }

    @Data
    @Entity
    @NoArgsConstructor
    @DiscriminatorValue("PAYMENT")
    public static class Payment extends Transaction {

        @Enumerated(EnumType.STRING)
        @Column(insertable = false, updatable = false)
        private final Type type = Type.PAYMENT;

        @NotNull
        private UUID orderId;

        @NotNull
        @ManyToOne
        @JoinColumn(name = "tariff_id")
        private Tariff tariff;

        @NotNull
        @ManyToOne
        @JoinColumn(name = "buyer_id")
        private Buyer buyer;

        @OneToOne
        @JoinColumn(name = "discount_code_realization_id")
        private DiscountCode.Realization discountCodeRealization;

        @NotEmpty
        @OneToMany
        @JoinColumn(name = "payment_id")
        private List<Payment.Product> products;

        @OneToOne
        @JoinColumn(name = "invoice_id")
        private Invoice invoice;

//        @Builder
//        public Payment(UUID id, Status status, Event.RefundPolicy refundPolicy, Event.FeePolicy feePolicy, BigDecimal totalAmount, CurrencyCode currency, Instant created, Instant completed, UUID orderId, Tariff tariff, Buyer buyer, DiscountCode.Realization discountCodeRealization, List<Product> products, Invoice invoice) {
//            super(id, status, refundPolicy, feePolicy, totalAmount, currency, created, completed);
//            this.orderId = orderId;
//            this.tariff = tariff;
//            this.buyer = buyer;
//            this.discountCodeRealization = discountCodeRealization;
//            this.products = products;
//            this.invoice = invoice;
//        }

        @Data
        @Entity
        @Builder
        @NoArgsConstructor
        @AllArgsConstructor
        @Table(name = "products")
        public static class Product {

            @Id
            @GeneratedValue(generator = "UUID")
            @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
            private UUID id;

            @NotNull
            private UUID ticketId;

            @NotBlank
            private String name;

            @Min(0)
            @NotNull
            private Integer quantity;

            @Min(0)
            @NotNull
            private BigDecimal unitPrice;

        }

    }

    @Data
    @Entity
    @NoArgsConstructor
    @DiscriminatorValue("REFUND")
    public static class Refund extends Transaction {

        @Enumerated(EnumType.STRING)
        @Column(insertable = false, updatable = false)
        private final Type type = Type.REFUND;

        @NotNull
        private UUID orderId;

        @NotEmpty
        @OneToMany
        @JoinColumn(name = "refund_id")
        private List<Refund.Product> products;

        @Builder
        public Refund(UUID id, Status status, BigDecimal totalAmount, CurrencyCode currency, Instant created, Instant completed, UUID orderId, List<Product> products) {
            super(id, status, totalAmount, currency, created, completed);
            this.orderId = orderId;
            this.products = products;
        }

        @Data
        @Entity
        @Builder
        @NoArgsConstructor
        @AllArgsConstructor
        @Table(name = "refund_products")
        public static class Product {

            @Id
            @GeneratedValue(generator = "UUID")
            @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
            private UUID id;

            @Min(0)
            @NotNull
            private Integer quantity;

            @NotNull
            @ManyToOne
            @JoinColumn(name = "product_id")
            private Payment.Product product;

        }

    }

    @Data
    @Entity
    @NoArgsConstructor
    @DiscriminatorValue("PAYOUT")
    public static class Payout extends Transaction {

        @Enumerated(EnumType.STRING)
        @Column(insertable = false, updatable = false)
        private final Type type = Type.PAYOUT;

        @NotEmpty
        @ElementCollection
        @Column(name = "order_id")
        @CollectionTable(name = "payout_orders", joinColumns = @JoinColumn(name = "payout_id"))
        private List<UUID> orderIds;

        @NotBlank
        private String accountNumber;

//        plus reszta danych obowiązkowych dla http://developers.payu.com/pl/payouts.html#payouts_payout_creation_on_account_given

    }

}
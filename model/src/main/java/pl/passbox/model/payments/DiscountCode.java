package pl.passbox.model.payments;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DiscountCode {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status;

    @NotNull
    private UUID eventId;

    @NotNull
    @Enumerated(EnumType.STRING)
    private DiscountType discountType;

    @Min(0)
//    @Max(100) todo pamiętać o tym przy discountType == PERCENTAGE!
    @NotNull
    private BigDecimal discountValue;

    @NotBlank
    private String code;

    @Min(1)
    @NotNull
    private Integer quantity;

    @NotNull
    private Instant validTo;

    @NotNull
    private Instant created;

    @NotNull
    private UUID createdById;

    private Instant modified;

    private UUID modifiedBy;

    @OneToMany(mappedBy = "id.discountCode")
    private List<DiscountCode.AppliedTicket> appliedTickets;

    @OneToMany(mappedBy = "discountCode")
    private List<DiscountCode.Realization> realizations;

    public enum Status {
        ACTIVE,
        DELETED
    }

    public enum DiscountType {
        PERCENTAGE,
        FIXED
    }

    @Data
    @Entity
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Table(name = "discount_code_applied_tickets")
    public static class AppliedTicket {

        @EmbeddedId
        private AppliedTicketDiscountCodeId id;

        @NotNull
        @Enumerated(EnumType.STRING)
        private Status status;

        /**
         * Dzięki statusowi powiązanie discountCodeRealization jest tylko przy transakcji (a nie przy każdym z produktów).
         * Po usunięciu kodu z biletu (żeby nie można było go użyć), jeżeli ten kod był już wcześniej użyty przy zamówieniu
         * danego biletu, nie usuwamy powiązania, tylko przestawiamy status na NOT_APPLIED (a przy ponownym powiązaniu biletu
         * z kodem, status wraca do APPLIED). Dzięki temu nie tracimy powiązania użytej zniżki (podstawy do zmniejszenia należności)
         */
        public enum Status {
            APPLIED,
            NOT_APPLIED
        }

        @Data
        @Builder
        @Embeddable
        @NoArgsConstructor
        @AllArgsConstructor
        public static class AppliedTicketDiscountCodeId implements Serializable {

            @NotNull
            private UUID ticketId;

            @NotNull
            @ManyToOne
            @JoinColumn(name = "discount_code_id")
            private DiscountCode discountCode;

        }

    }

    @Data
    @Entity
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Table(name = "discount_code_realization")
    public static class Realization {

        @Id
        @GeneratedValue(generator = "UUID")
        @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
        private UUID id;

        @NotNull
        private Instant realized;

        @NotNull
        @ManyToOne
        @JoinColumn(name = "discount_code_id")
        private DiscountCode discountCode;

    }

}
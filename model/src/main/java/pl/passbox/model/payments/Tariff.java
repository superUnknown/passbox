package pl.passbox.model.payments;

import com.neovisionaries.i18n.CurrencyCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.UUID;

/**
 * Ta encja nie może być aktualizowana; przy zmianie stawki stara powinna być dezaktywowana i dodana nowa.
 * Dla każdej dostępnej waluty powinna być 1 taryfa DEFAULT.
 */
@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Tariff {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Type type;

    @NotNull
    @Enumerated(EnumType.STRING)
    private CurrencyCode currency;

    @Min(0)
    @Max(100)
    @NotNull
    private BigDecimal percentageFee;

    @Min(0)
    @NotNull
    private BigDecimal fixedFee;

    public enum Status {
        ACTIVE,
        INACTIVE
    }

    public enum Type {
        DEFAULT,
        DISCOUNT
    }

}
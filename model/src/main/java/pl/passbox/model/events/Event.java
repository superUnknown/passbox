package pl.passbox.model.events;

import com.neovisionaries.i18n.CurrencyCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Event {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @NotNull
    private UUID organizationId;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status;

    @NotBlank
    private String name;

    private String description;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Category category;

    @NotNull
    @Enumerated(EnumType.STRING)
    private RefundPolicy refundPolicy;

    @NotNull
    @Enumerated(EnumType.STRING)
    private FeePolicy feePolicy;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(updatable = false)
    private CurrencyCode currency;

    private UUID tariffDiscountRealizationId;

    @OneToOne
    @JoinColumn(name = "location_id")
    private EventLocation eventLocation;

    @NotEmpty
    @OneToMany
    @JoinColumn(name = "event_id")
    private List<EventTerm> eventTerms;

    @NotEmpty
    @OneToMany
    @JoinColumn(name = "event_id")
    private List<Ticket> tickets;

    @OneToMany
    @JoinColumn(name = "event_id")
    private List<CheckoutQuestion> checkoutQuestion;

    @OneToMany(mappedBy = "event")
    private List<Order> orders;

    @OneToMany(mappedBy = "event")
    private List<PayoutRequest> payoutRequests;

    @NotNull
    private Instant created;

    @NotNull
    private UUID createdById;

    private Instant modified;

    private UUID modifiedBy;

    private byte[] background; // todo research do jakiegoś clouda z obrazkami

    public enum Status {
        UNPUBLISHED,
        ACTIVE,
        SUSPENDED,
        FINISHED,
        DELETED
    }

    public enum Category {

    }

    public enum RefundPolicy {
        ONE_DAY_UNTIL_EVENT,
        TWO_DAYS_UNTIL_EVENT,
        THREE_DAYS_UNTIL_EVENT,
        WEEK_UNTIL_EVENT,
        MONTH_UNTIL_EVENT,
        NO_REFUNDS
    }

    public enum FeePolicy {
        INCLUDED_IN_PRICE,
        APPENDED_TO_PRICE
    }

}
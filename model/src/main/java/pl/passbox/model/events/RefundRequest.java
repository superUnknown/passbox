package pl.passbox.model.events;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DiscriminatorOptions;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;
import pl.passbox.model.accounts.PaymentsSettings;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@Inheritance
@NoArgsConstructor
@AllArgsConstructor
@DiscriminatorColumn(name = "refundHandling")
@DiscriminatorOptions(force = true)
public abstract class RefundRequest {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status;

    @NotNull
    private Instant requested;

    private Instant completed;

    @NotEmpty
    @OneToMany(mappedBy = "refundRequest")
    private List<TicketOrder> ticketOrders;

    public abstract PaymentsSettings.RefundHandling getRefundHandling();

    public enum Status {
        UNCONFIRMED,
        PENDING,
        COMPLETED,
//      ... todo
    }

    @Data
    @Entity
    @NoArgsConstructor
    @DiscriminatorValue("AUTO")
    public static class Auto extends RefundRequest {

        @Enumerated(EnumType.STRING)
        @Column(insertable = false, updatable = false)
        private final PaymentsSettings.RefundHandling refundHandling = PaymentsSettings.RefundHandling.AUTO;

    }

    @Data
    @Entity
    @NoArgsConstructor
    @DiscriminatorValue("MANUAL")
    public static class Manual extends RefundRequest {

        @Enumerated(EnumType.STRING)
        @Column(insertable = false, updatable = false)
        private final PaymentsSettings.RefundHandling refundHandling = PaymentsSettings.RefundHandling.MANUAL;

        @NotNull
        private Instant confirmed;

        @NotNull
        private UUID confirmedById;

    }


}
package pl.passbox.model.events;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CheckoutQuestion {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Type type;

    @NotNull
    @Enumerated(EnumType.STRING)
    private RequiredFlag required;

    @NotBlank
    private String name;

    public enum Type {
        PHONE,
        EMAIL,
        TEXT,
        INTEGER,
        FLOAT,
        BOOLEAN
    }

    @Getter
    public enum RequiredFlag {
        YES(true),
        NO(false);

        private final boolean required;

        RequiredFlag(boolean required) {
            this.required = required;
        }

    }

}
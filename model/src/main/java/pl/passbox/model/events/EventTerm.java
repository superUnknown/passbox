package pl.passbox.model.events;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DiscriminatorOptions;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@Inheritance
@NoArgsConstructor
@AllArgsConstructor
@DiscriminatorColumn(name = "type")
@DiscriminatorOptions(force = true)
public abstract class EventTerm {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status;

    public abstract Type getType();

    public enum Status {
        ACTIVE,
        DELETED
    }

    public enum Type {
        SINGLE_DAY,
        MULTI_DAY
    }

    @Data
    @Entity
    @NoArgsConstructor
    @DiscriminatorValue("SINGLE_DAY")
    public static class SingleDay extends EventTerm {

        @Enumerated(EnumType.STRING)
        @Column(insertable = false, updatable = false)
        private final Type type = Type.SINGLE_DAY;

        @OneToOne(mappedBy = "eventTerm")
        private EventTerm.Date date;

        @Builder
        public SingleDay(UUID id, Status status, Date date) {
            super(id, status);
            this.date = date;
        }

    }

    @Data
    @Entity
    @NoArgsConstructor
    @DiscriminatorValue("MULTI_DAY")
    public static class MultiDay extends EventTerm {

        @Enumerated(EnumType.STRING)
        @Column(insertable = false, updatable = false)
        private final Type type = Type.MULTI_DAY;

        @NotEmpty
        @OneToMany(mappedBy = "eventTerm")
        private List<EventTerm.Date> dates;

        @Builder
        public MultiDay(UUID id, Status status, List<Date> dates) {
            super(id, status);
            this.dates = dates;
        }

    }

    @Data
    @Entity
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Table(name = "event_term_date")
    public static class Date {

        @Id
        @GeneratedValue(generator = "UUID")
        @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
        private UUID id;

        @NotNull
        private Instant datetime;

        @NotNull
        @ManyToOne
        @JoinColumn(name = "event_term_id")
        private EventTerm eventTerm;

    }

}
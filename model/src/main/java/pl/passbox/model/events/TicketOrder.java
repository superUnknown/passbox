package pl.passbox.model.events;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TicketOrder {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @NotBlank
    private String number;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status;

    private Instant used;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "ticket_id")
    private Ticket ticket;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "applied_event_term_id")
    private Ticket.AppliedEventTerm appliedEventTerm;

    @NotNull
    @OneToOne
    @JoinColumn(name = "attendee_id")
    private Attendee attendee;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "refund_request_id")
    private RefundRequest refundRequest;

    @OneToMany
    @JoinColumn(name = "ticket_order_id")
    private List<CheckoutAnswer> checkoutAnswers;

    public enum Status {
        /**
         * Order with this ticket created, but not completed.
         */
        REQUESTED,
        /**
         * Order with this ticket successfully paid; ticket is available for use.
         */
        COMPLETED,
        /**
         * Ticket is used; successfully ordered and scanned.
         */
        USED,
        /**
         * Order was requested, but canceled by customer during payment confirmation; all tickets orders are marked the same.
         */
        CANCELED,
        /**
         * Ticket order was completed, but customer returned it before use.
         */
        REFUNDED
    }

    @Data
    @Entity
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CheckoutAnswer {

        @Id
        @GeneratedValue(generator = "UUID")
        @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
        private UUID id;

        @NotNull
        @ManyToOne
        @JoinColumn(name = "checkout_question_id")
        private CheckoutQuestion question;

        @NotBlank
        private String value;

    }

}
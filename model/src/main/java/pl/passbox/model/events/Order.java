package pl.passbox.model.events;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Order {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Event.RefundPolicy refundPolicy;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Event.FeePolicy feePolicy;

    @NotNull
    private Instant started;

    private Instant completed;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "event_id")
    private Event event;

    @NotEmpty
    @OneToMany
    @JoinColumn(name = "order_id")
    private List<TicketOrder> tickets;

    public enum Status {
        /**
         * Order initialized by selecting tickets. Order is not yet requested; customer must fill attendees information and request payment.
         */
        STARTED,
        /**
         * Started order has been fully completed by customer and was redirected to payment gateway for payment confirmation.
         */
        REQUESTED,
        /**
         * Payment confirmed by customer; waiting for finish payment.
         */
        PENDING,
        /**
         * Order successfully paid; payment transaction completed; tickets have been sent.
         */
        COMPLETED,
        /**
         * Order was requested, but canceled by customer during payment confirmation.
         */
        CANCELED,
        /**
         * Payment transaction has failed; order has not been paid.
         */
        FAILED
    }

}
package pl.passbox.model.events;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DiscriminatorOptions;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@Inheritance
@NoArgsConstructor
@AllArgsConstructor
@DiscriminatorColumn(name = "type")
@DiscriminatorOptions(force = true)
public abstract class Ticket {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status;

    @NotBlank
    private String name;

    private String description;

    @Min(1)
    private Integer minPerOrder;

    @Min(1)
    private Integer maxPerOrder;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Restriction restriction;

    @ManyToOne
    @JoinColumn(name = "ticket_access_token_id")
    private AccessToken ticketAccessToken;

    @NotEmpty
    @OneToMany(mappedBy = "id.ticket")
    private List<AppliedEventTerm> appliedEventTerms;

    @OneToMany(mappedBy = "ticket")
    private List<TicketOrder> orders;

    @NotNull
    private Instant created;

    @NotNull
    private UUID createdById;

    private Instant modified;

    private UUID modifiedBy;

    public abstract Type getType();

    public enum Status {
        ACTIVE,
        DELETED
    }

    public enum Type {
        FREE,
        PAID
    }

    public enum Restriction {
        NONE,
        ACCESS_TOKEN
    }

    @Data
    @Entity
    @NoArgsConstructor
    @DiscriminatorValue("FREE")
    public static class Free extends Ticket {

        @Enumerated(EnumType.STRING)
        @Column(insertable = false, updatable = false)
        private final Type type = Type.FREE;

        @Builder
        public Free(UUID id, Status status, String name, String description, Integer minPerOrder, Integer maxPerOrder, Restriction restriction, AccessToken ticketAccessToken, List<AppliedEventTerm> appliedEventTerms, List<TicketOrder> orders, Instant created, UUID createdById, Instant modified, UUID modifiedBy) {
            super(id, status, name, description, minPerOrder, maxPerOrder, restriction, ticketAccessToken, appliedEventTerms, orders, created, createdById, modified, modifiedBy);
        }

    }

    @Data
    @Entity
    @NoArgsConstructor
    @DiscriminatorValue("PAID")
    public static class Paid extends Ticket {

        @Enumerated(EnumType.STRING)
        @Column(insertable = false, updatable = false)
        private final Type type = Type.PAID;

        @NotNull
        private BigDecimal price;

        @Builder
        public Paid(UUID id, Status status, String name, String description, Integer minPerOrder, Integer maxPerOrder, Restriction restriction, AccessToken ticketAccessToken, List<AppliedEventTerm> appliedEventTerms, List<TicketOrder> orders, Instant created, UUID createdById, Instant modified, UUID modifiedBy, BigDecimal price) {
            super(id, status, name, description, minPerOrder, maxPerOrder, restriction, ticketAccessToken, appliedEventTerms, orders, created, createdById, modified, modifiedBy);
            this.price = price;
        }

    }

    @Data
    @Entity
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Table(name = "ticket_access_token")
    public static class AccessToken {

        @Id
        @GeneratedValue(generator = "UUID")
        @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
        private UUID id;

        @NotBlank
        private String value;

        @ManyToOne
        @JoinColumn(name = "event_id")
        private Event event;

    }

    @Data
    @Entity
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Table(name = "event_term_applied_tickets")
    public static class AppliedEventTerm {

        @Id
        @GeneratedValue(generator = "UUID")
        @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
        private UUID id;

        @NotNull
        @Enumerated(EnumType.STRING)
        private Status status;

        @NotNull
        private Instant saleStart;

        @NotNull
        private Instant saleEnd;

        @Min(1)
        @NotNull
        private Integer quantity;

        @NotNull
        @ManyToOne
        @JoinColumn(name = "ticket_id")
        private Ticket ticket;

        @NotEmpty
        @ManyToMany
        private List<EventTerm.Date> dates;

        /**
         * Status zostaje przestawiony na NOT_APPLIED w przypadku, gdy na dany termin zostały już zamówione
         * bilety ({@link TicketOrder}.appliedEventTerm). W ten sposób zmiana jest widoczna dla kolejnych
         * zamówień (termin dla tego biletu będzie niedostępny), ale zostaje zachowana informacja historyczna.
         * Przy ponownym powiązaniu biletu z terminem, status wraca do APPLIED).
         * Jeżeli na dany termin nie ma biletów, encja jest po prostu usuwana z bazy.
         */
        public enum Status {
            APPLIED,
            NOT_APPLIED
        }

    }

}
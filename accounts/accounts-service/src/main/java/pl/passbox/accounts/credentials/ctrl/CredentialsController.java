package pl.passbox.accounts.credentials.ctrl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.passbox.accounts.credentials.service.CredentialsService;
import pl.passbox.api.accounts.credentials.CredentialsApi;
import pl.passbox.api.accounts.credentials.model.ChangePasswordCommand;
import pl.passbox.api.accounts.credentials.model.ConfirmPasswordResetCommand;
import pl.passbox.api.accounts.credentials.model.RequestEmailChangeCommand;
import pl.passbox.api.accounts.credentials.model.UserDetails;
import pl.passbox.api.accounts.accounts.exceptions.IllegalAccountStatusException;
import pl.passbox.api.accounts.accounts.exceptions.AccountNotFoundException;
import pl.passbox.api.accounts.team.exceptions.IllegalUserStatusException;
import pl.passbox.api.accounts.team.exceptions.UserAlreadyExistsException;
import pl.passbox.api.accounts.team.exceptions.UserNotFoundException;
import pl.passbox.api.accounts.tokens.exceptions.TokenInvalidException;
import pl.passbox.commons.identity.UserIdentityResolver;
import pl.passbox.commons.identity.exception.UserIdentityMissingException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Slf4j
@RestController
@AllArgsConstructor
public class CredentialsController implements CredentialsApi {

    private final CredentialsService credentialsService;

    private final HttpServletRequest request;

    @Override
    public UserDetails getUserDetails(@NotNull @PathVariable(ACCOUNT_ID_PARAM) UUID accountId, @NotBlank @Email @RequestParam(EMAIL_PARAM) String email) throws UserNotFoundException, IllegalUserStatusException, IllegalAccountStatusException, AccountNotFoundException {
        log.trace("Handling request for user details with { email: {}, accountId: {} }.", email, accountId);
        UserDetails result = credentialsService.getUserDetails(accountId, email);
        log.trace("Request for user details with { email: {}, accountId: {} } handled successfully: {}.", email, accountId, result);
        return result;
    }

    @Override
    public void requestPasswordReset(@NotNull @PathVariable(ACCOUNT_ID_PARAM) UUID accountId, @NotBlank @Email @RequestParam(EMAIL_PARAM) String email) throws UserNotFoundException, IllegalUserStatusException, AccountNotFoundException, IllegalAccountStatusException {
        log.trace("Handling request for password reset of { email: {}, accountId: {} }.", email, accountId);
        credentialsService.requestPasswordReset(accountId, email);
        log.trace("Request for password reset of { email: {}, accountId: {} } handled successfully.", email, accountId);
    }

    @Override
    public void confirmPasswordReset(@NotNull @Valid @RequestBody ConfirmPasswordResetCommand command) throws TokenInvalidException {
        log.trace("Handling request for password reset confirmation: {}.", command);
        credentialsService.confirmPasswordReset(command);
        log.trace("Request for password reset confirmation with command {} handled successfully.", command);
    }

    @Override
    public void changePassword(@NotNull @Valid @RequestBody ChangePasswordCommand command) throws UserIdentityMissingException, UserNotFoundException, IllegalUserStatusException, AccountNotFoundException, IllegalAccountStatusException {
        log.trace("Handling request for password change.");
        credentialsService.changePassword(UserIdentityResolver.resolve(request), command);
        log.trace("Request for password change handled successfully.");
    }

    @Override
    public void requestEmailChange(@NotNull @Valid @RequestBody RequestEmailChangeCommand command) throws UserIdentityMissingException, UserAlreadyExistsException, IllegalUserStatusException, UserNotFoundException, AccountNotFoundException, IllegalAccountStatusException {
        log.trace("Handling request for email change.");
        credentialsService.requestEmailChange(UserIdentityResolver.resolve(request), command);
        log.trace("Request for email change handled successfully.");
    }

    @Override
    public void confirmEmailChange(@NotBlank @RequestParam(TOKEN_PARAM) String token) throws TokenInvalidException, UserAlreadyExistsException {
        log.trace("Handling request for email change confirmation [ token: {} ].", token);
        credentialsService.confirmEmailChange(token);
        log.trace("Request for email change confirmation handled successfully.");
    }

}
package pl.passbox.accounts.tokens;

import org.hibernate.validator.constraints.NotBlank;
import pl.passbox.api.accounts.tokens.exceptions.TokenInvalidException;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public interface VerificationTokenSupport {

    <T extends VerificationToken> String encode(@NotNull @Valid T spec);

    <T extends VerificationToken> T decode(@NotBlank String value, @NotNull Class<T> clazz) throws TokenInvalidException;

}
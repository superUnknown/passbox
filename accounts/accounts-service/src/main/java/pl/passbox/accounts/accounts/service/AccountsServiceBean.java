package pl.passbox.accounts.accounts.service;

import com.neovisionaries.i18n.CurrencyCode;
import com.neovisionaries.i18n.LanguageCode;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import pl.passbox.accounts.accounts.model.Account;
import pl.passbox.accounts.accounts.model.PaymentsSettings;
import pl.passbox.accounts.accounts.repository.AccountsRepository;
import pl.passbox.accounts.accounts.utils.SubdomainGenerationSupport;
import pl.passbox.accounts.configuration.ConfirmationLinksProperties;
import pl.passbox.accounts.team.model.Settings;
import pl.passbox.accounts.team.model.User;
import pl.passbox.accounts.tokens.VerificationToken;
import pl.passbox.accounts.tokens.VerificationTokenSupport;
import pl.passbox.api.accounts.accounts.exceptions.AccountAlreadyExistsException;
import pl.passbox.api.accounts.accounts.exceptions.AccountNotFoundException;
import pl.passbox.api.accounts.accounts.exceptions.IllegalAccountStatusException;
import pl.passbox.api.accounts.accounts.model.AccountDto;
import pl.passbox.api.accounts.accounts.model.CreateAccountCommand;
import pl.passbox.api.accounts.accounts.model.PaymentsSettingsDto;
import pl.passbox.api.accounts.accounts.model.Status;
import pl.passbox.api.accounts.accounts.model.UpdateAccountCommand;
import pl.passbox.api.accounts.team.exceptions.IllegalUserStatusException;
import pl.passbox.api.accounts.team.exceptions.UserNotFoundException;
import pl.passbox.api.accounts.tokens.exceptions.TokenInvalidException;
import pl.passbox.api.messages.MessagesApi;
import pl.passbox.api.messages.model.RegistrationConfirmationEmail;
import pl.passbox.commons.identity.model.UserIdentity;

import java.time.Instant;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Service
@Validated
@AllArgsConstructor
public class AccountsServiceBean implements AccountsService {

    private final ConfirmationLinksProperties confirmationLinksProperties;

    private final VerificationTokenSupport verificationTokenSupport;

    private final AccountsRepository accountsRepository;

    private final PasswordEncoder passwordEncoder;

    private final MessagesApi messagesApi;

    private final ModelMapper mapper;

    @Override
    public AccountDto get(UUID accountId) throws AccountNotFoundException {
        log.debug("Getting account with id {}.", accountId);
        AccountDto account = mapper.map(accountsRepository.findOne(accountId).orElseThrow(
                () -> AccountNotFoundException.byId(accountId)
        ), AccountDto.class);
        log.debug("Account with id {} retrieved: {}.", accountId, account);
        return account;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public AccountDto create(CreateAccountCommand command, LanguageCode language) throws AccountAlreadyExistsException {
        log.info("Creating account: { command: {}, locale: {} }.", command, language);

        if (accountsRepository.findByName(command.getName()).isPresent()) {
            throw AccountAlreadyExistsException.byName(command.getName());
        }

        if (accountsRepository.findByEmail(command.getEmail()).isPresent()) {
            throw AccountAlreadyExistsException.byEmail(command.getEmail());
        }

        Account account = accountsRepository.save(Account.builder()
                .subdomain(SubdomainGenerationSupport.assemble(command.getName(), accountsRepository))
                .founder(User.builder()
                        .settings(Settings.builder().language(language).build())
                        .password(passwordEncoder.encode(command.getFounder().getPassword1()))
                        .status(pl.passbox.api.accounts.team.model.Status.UNVERIFIED)
                        .firstname(command.getFounder().getFirstname())
                        .lastname(command.getFounder().getLastname())
                        .email(command.getEmail())
                        .build()
                )
                .paymentsSettings(PaymentsSettings.builder()
                        .currency(CurrencyCode.PLN)
                        .build()
                )
                .email(command.getEmail())
                .name(command.getName())
                .build()
        );

        log.debug("Sending account registration confirmation email { email: {}, accountId: {} }.", account.getEmail(), account.getId());
        messagesApi.send(Collections.singletonList(RegistrationConfirmationEmail.builder()
                .link(confirmationLinksProperties.getRegistrationConfirmation().assembleConfirmationLink(verificationTokenSupport.encode(VerificationToken.AccountRegistrationConfirmation.builder()
                        .expires(confirmationLinksProperties.getRegistrationConfirmation().assembleLinkExpiration())
                        .accountId(account.getId())
                        .build()
                )))
                .firstname(account.getFounder().getFirstname())
                .accountName(account.getName())
                .locale(language.toLocale())
                .email(account.getEmail())
                .build()
        ));

        AccountDto result = mapper.map(account, AccountDto.class);
        log.info("Account created successfully: {}.", result);

        return result;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public AccountDto confirm(String sToken) throws TokenInvalidException {
        log.info("Confirming registration of account with token '{}'.", sToken);
        Account account = getByToken(verificationTokenSupport.decode(sToken, VerificationToken.AccountRegistrationConfirmation.class));
        log.debug("Account from token resolved { id: {}, name: {}, founderId: {} }. Activating.", account.getId(), account.getName(), account.getFounder().getId());
        account.getFounder().setStatus(pl.passbox.api.accounts.team.model.Status.ACTIVE);
        account.setStatus(Status.ACTIVE);
        accountsRepository.save(account);
        log.info("Registration of account with id {} confirmed successfully.", account.getId());
        return mapper.map(account, AccountDto.class);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public AccountDto update(UserIdentity identity, UpdateAccountCommand command) throws UserNotFoundException, IllegalUserStatusException, IllegalAccountStatusException, AccountNotFoundException, AccountAlreadyExistsException {
        log.info("Updating account with id {} by user with id {}: {}", identity.getAccountId(), identity.getUserId(), command);

        Account account = accountsRepository.findOne(identity.getAccountId())
                .orElseThrow(() -> AccountNotFoundException.byId(identity.getAccountId()));
        User user = account.findActiveUser(User.filterById(identity.getUserId()));

        if (accountsRepository.findByName(command.getName()).filter(o -> !o.getId().equals(account.getId())).isPresent()) {
            throw AccountAlreadyExistsException.byName(command.getName());
        }

        if (accountsRepository.findBySubdomain(command.getSubdomain()).filter(o -> !o.getId().equals(account.getId())).isPresent()) {
            throw AccountAlreadyExistsException.bySubdomain(command.getSubdomain());
        }

        mapper.map(command, account);
        account.setModified(Instant.now());
        account.setModifiedBy(user);

        AccountDto result = mapper.map(account, AccountDto.class);
        log.info("Account updated successfully: {}.", result);
        return result;
    }

    @Override
    public PaymentsSettingsDto getPaymentsSettings(UUID accountId) throws AccountNotFoundException {
        log.debug("Getting payment settings of account with id {}.", accountId);
        PaymentsSettingsDto result = mapper.map(accountsRepository.findOne(accountId)
                .orElseThrow(() -> AccountNotFoundException.byId(accountId))
                .getPaymentsSettings(), PaymentsSettingsDto.class);
        log.debug("Payment settings of account with id {} retrieved: {}.", accountId, result);
        return result;
    }

    private Account getByToken(VerificationToken.AccountRegistrationConfirmation token) throws TokenInvalidException {
        Optional<Account> account = accountsRepository.findOne(token.getAccountId());
        if (!account.map(o -> o.getStatus().equals(Status.UNCONFIRMED)).orElse(false)) {
            log.debug("Decoded account id from token ({}) don't belong to any account that requires confirmation (account by id: {}).", account.map(Account::getId).orElse(null), account.map(o -> mapper.map(o, AccountDto.class)).orElse(null));
            throw new TokenInvalidException();
        }
        return account.get();
    }

}
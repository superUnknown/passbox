package pl.passbox.accounts.accounts.model;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import pl.passbox.accounts.team.model.User;
import pl.passbox.api.accounts.accounts.exceptions.IllegalAccountStatusException;
import pl.passbox.api.accounts.accounts.model.Status;
import pl.passbox.api.accounts.team.exceptions.IllegalUserStatusException;
import pl.passbox.api.accounts.team.exceptions.UserNotFoundException;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.function.Predicate;

@Getter
@Setter
@Entity
@NoArgsConstructor
public class Account {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @NotNull
    @Builder.Default
    @Enumerated(EnumType.STRING)
    private Status status = Status.UNCONFIRMED;

    @NotBlank
    private String name;

    @Email
    @NotBlank
    private String email;

    @NotBlank
    private String subdomain;

    private String description;

    @NotNull
    @Builder.Default
    private Instant created = Instant.now();

    @NotNull
    @JoinColumn(name = "founder_id")
    @OneToOne(cascade = CascadeType.PERSIST)
    private User founder;

    private Instant modified;

    @OneToOne
    @JoinColumn(name = "modified_by")
    private User modifiedBy;

    @JoinColumn(name = "social_channels_id")
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private SocialChannels socialChannels;

    @JoinColumn(name = "location_id")
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private Address location;

    @NotNull
    @JoinColumn(name = "payments_settings_id")
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private PaymentsSettings paymentsSettings;

    @NotEmpty
    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<User> users = new ArrayList<>();

    @Builder
    public Account(String name, String email, String subdomain, User founder, PaymentsSettings paymentsSettings) {
        this.name = name;
        this.email = email;
        this.subdomain = subdomain;
        this.founder = founder;
        this.paymentsSettings = paymentsSettings;
        addUser(founder);
    }

    @Transient
    public void addUser(User user) {
        user.setAccount(this);
        this.users.add(user);
    }

    @Transient
    public boolean hasStatus(Status... statuses) {
        return Arrays.asList(statuses).contains(status);
    }

    public User findActiveUser(Predicate<User> filter) throws UserNotFoundException, IllegalUserStatusException, IllegalAccountStatusException {
        if (!hasStatus(pl.passbox.api.accounts.accounts.model.Status.ACTIVE, pl.passbox.api.accounts.accounts.model.Status.UNCONFIRMED)) {
            throw IllegalAccountStatusException.status(id, status);
        }

        User user = users.stream().filter(filter).findFirst().orElseThrow(() -> UserNotFoundException.byFilter(id, filter));
        if (!user.hasStatus(pl.passbox.api.accounts.team.model.Status.UNVERIFIED, pl.passbox.api.accounts.team.model.Status.ACTIVE)) {
            throw IllegalUserStatusException.status(user.getId(), user.getStatus());
        }

        return user;
    }

}
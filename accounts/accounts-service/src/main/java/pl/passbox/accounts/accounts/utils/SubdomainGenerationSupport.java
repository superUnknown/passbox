package pl.passbox.accounts.accounts.utils;

import lombok.extern.slf4j.Slf4j;
import pl.passbox.accounts.accounts.repository.AccountsRepository;

@Slf4j
public class SubdomainGenerationSupport {

    private static final String ILLEGAL_CHARACTERS_PATTERN = "[^A-Za-z0-9]";

    public static String assemble(String accountName, AccountsRepository repository) {
        log.trace("Generating subdomain from '{}'.", accountName);
        String subdomain = accountName.replaceAll(ILLEGAL_CHARACTERS_PATTERN, "");

        int i = 0;
        String tmp = subdomain;
        while (repository.findBySubdomain(tmp).isPresent()) {
            log.trace("Subdomain '{}' is used. Trying another.", tmp);
            tmp = subdomain + i;
            i++;
        }
        subdomain = tmp;
        log.trace("Subdomain from '{}' generated: {}.", accountName, subdomain);
        return subdomain;
    }

}
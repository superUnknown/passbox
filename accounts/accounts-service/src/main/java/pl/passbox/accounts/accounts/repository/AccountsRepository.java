package pl.passbox.accounts.accounts.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import pl.passbox.accounts.accounts.model.Account;

import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

public interface AccountsRepository extends Repository<Account, UUID> {

    Optional<Account> findOne(UUID id);

    @Query("SELECT o FROM Account o WHERE o.status != 'DELETED' AND UPPER(o.name) = UPPER(:name)")
    Optional<Account> findByName(@Param("name") String name);

    @Query("SELECT o FROM Account o WHERE o.status != 'DELETED' AND UPPER(o.email) = UPPER(:email)")
    Optional<Account> findByEmail(@Param("email") String email);

    @Query("SELECT o FROM Account o WHERE o.status != 'DELETED' AND UPPER(o.subdomain) = UPPER(:subdomain)")
    Optional<Account> findBySubdomain(@Param("subdomain") String subdomain);

    Account save(Account account);

    Stream<Account> findAll();

    void delete(Account account);

    void deleteAll();

}
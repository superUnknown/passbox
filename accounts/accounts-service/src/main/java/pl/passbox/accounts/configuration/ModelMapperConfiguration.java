package pl.passbox.accounts.configuration;

import com.github.jmnarloch.spring.boot.modelmapper.ModelMapperConfigurer;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.convention.MatchingStrategies;
import org.modelmapper.spi.MappingContext;
import org.modelmapper.spi.MatchingStrategy;
import org.springframework.context.annotation.Configuration;
import pl.passbox.accounts.accounts.model.Account;
import pl.passbox.accounts.accounts.model.Address;
import pl.passbox.accounts.accounts.model.SocialChannels;
import pl.passbox.api.accounts.accounts.model.AccountDto;
import pl.passbox.api.accounts.accounts.model.AddressDto;
import pl.passbox.api.accounts.accounts.model.SocialChannelsDto;
import pl.passbox.api.accounts.accounts.model.UpdateAccountCommand;

import java.util.Optional;

@Configuration
public class ModelMapperConfiguration implements ModelMapperConfigurer {

    private static final MatchingStrategy STRATEGY = MatchingStrategies.STRICT;

    private static final PropertyMap<UpdateAccountCommand, Account> UPDATE_ACCOUNT_MAPPING = new PropertyMap<UpdateAccountCommand, Account>() {
        @Override
        protected void configure() {
            using(new AbstractConverter<String, String>() {
                @Override
                protected String convert(String source) {
                    return Optional.ofNullable(source).map(String::toLowerCase).orElse(null);
                }
            }).map().setSubdomain(source.getSubdomain());
            using(SOCIAL_CHANNELS_CONVERTER).map().setSocialChannels(null);
            using(ADDRESS_CONVERTER).map().setLocation(null);
        }
    };

    private static final PropertyMap<Account, AccountDto> ACCOUNT_MAPPING = new PropertyMap<Account, AccountDto>() {
        @Override
        protected void configure() {
            map().setFounderId(source.getFounder().getId());
            map().setModifiedById(source.getModifiedBy().getId());
        }
    };

    private static final Converter<SocialChannelsDto, SocialChannels> SOCIAL_CHANNELS_CONVERTER = new Converter<SocialChannelsDto, SocialChannels>() {
        @Override
        public SocialChannels convert(MappingContext<SocialChannelsDto, SocialChannels> ctx) {
            SocialChannelsDto source = ((UpdateAccountCommand) ctx.getParent().getSource()).getSocialChannels();
            if (source == null) {
                return null;
            }

            SocialChannels destination = Optional.ofNullable(((Account) ctx.getParent().getDestination()).getSocialChannels()).orElse(new SocialChannels());
            destination.setEmail(source.getEmail());
            destination.setFacebookLink(source.getFacebookLink());
            destination.setInstagramLink(source.getInstagramLink());
            destination.setPhone(source.getPhone());
            destination.setTwitterLink(source.getTwitterLink());
            destination.setWebPageLink(source.getWebPageLink());

            return Optional.of(destination).filter(d -> !d.isEmpty()).orElse(null);
        }
    };

    private static final Converter<AddressDto, Address> ADDRESS_CONVERTER = new Converter<AddressDto, Address>() {
        @Override
        public Address convert(MappingContext<AddressDto, Address> ctx) {
            AddressDto source = ((UpdateAccountCommand) ctx.getParent().getSource()).getLocation();
            if (source == null) {
                return null;
            }

            Address destination = Optional.ofNullable(((Account) ctx.getParent().getDestination()).getLocation()).orElse(new Address());
            destination.setCountry(source.getCountry());
            destination.setCity(source.getCity());
            destination.setStreet(source.getStreet());
            destination.setHouse(source.getHouse());
            destination.setFlat(source.getFlat());
            destination.setPostalCode(source.getPostalCode());
            return destination;
        }
    };

    @Override
    public void configure(ModelMapper mapper) {
        doConfigure(mapper);
    }

    public static ModelMapper create() {
        ModelMapper mapper = new ModelMapper();
        doConfigure(mapper);
        return mapper;
    }

    private static void doConfigure(ModelMapper mapper) {
        mapper.getConfiguration().setMatchingStrategy(STRATEGY);
        mapper.addConverter(SOCIAL_CHANNELS_CONVERTER);
        mapper.addConverter(ADDRESS_CONVERTER);
        mapper.addMappings(UPDATE_ACCOUNT_MAPPING);
        mapper.addMappings(ACCOUNT_MAPPING);
    }

}
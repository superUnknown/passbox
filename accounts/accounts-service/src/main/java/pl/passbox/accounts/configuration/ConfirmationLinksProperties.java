package pl.passbox.accounts.configuration;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.Duration;
import java.time.Instant;

@Data
@Builder
@Configuration
@NoArgsConstructor
@AllArgsConstructor
@ConfigurationProperties("accounts.confirmation_links")
public class ConfirmationLinksProperties {

    @Valid
    @NotNull
    private Link emailChange;

    @Valid
    @NotNull
    private Link passwordReset;

    @Valid
    @NotNull
    private Link registrationConfirmation;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Link {

        private static final String CONFIRMATION_TOKEN_PLACEHOLDER = "{TOKEN}";

        @Min(1)
        @NotNull
        private Long linkExpirationInHours;

        @NotBlank
        private String confirmationLink;

        public Instant assembleLinkExpiration() {
            return Instant.now().plus(Duration.ofHours(linkExpirationInHours));
        }

        public String assembleConfirmationLink(String token) {
            return confirmationLink.replace(CONFIRMATION_TOKEN_PLACEHOLDER, token);
        }

    }

}
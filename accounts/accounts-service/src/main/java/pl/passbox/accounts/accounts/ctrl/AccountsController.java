package pl.passbox.accounts.accounts.ctrl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.passbox.accounts.accounts.service.AccountsService;
import pl.passbox.api.accounts.accounts.AccountsApi;
import pl.passbox.api.accounts.accounts.exceptions.IllegalAccountStatusException;
import pl.passbox.api.accounts.accounts.exceptions.AccountAlreadyExistsException;
import pl.passbox.api.accounts.accounts.exceptions.AccountNotFoundException;
import pl.passbox.api.accounts.accounts.model.CreateAccountCommand;
import pl.passbox.api.accounts.accounts.model.AccountDto;
import pl.passbox.api.accounts.accounts.model.PaymentsSettingsDto;
import pl.passbox.api.accounts.accounts.model.UpdateAccountCommand;
import pl.passbox.api.accounts.team.exceptions.IllegalUserStatusException;
import pl.passbox.api.accounts.team.exceptions.UserNotFoundException;
import pl.passbox.api.accounts.tokens.exceptions.TokenInvalidException;
import pl.passbox.commons.identity.UserIdentityResolver;
import pl.passbox.commons.identity.exception.UserIdentityMissingException;
import pl.passbox.commons.utils.LanguageSupport;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Slf4j
@RestController
@AllArgsConstructor
public class AccountsController implements AccountsApi {

    private final AccountsService accountsService;

    private final HttpServletRequest request;

    @Override
    public AccountDto get(@NotNull @PathVariable(ACCOUNT_ID_PATH_VARIABLE) UUID accountId) throws AccountNotFoundException {
        log.trace("Handling request for account with id {}.", accountId);
        AccountDto result = accountsService.get(accountId);
        log.trace("Request for account with id {} handled successfully: {}.", accountId, result);
        return result;
    }

    @Override
    public AccountDto create(@Valid @NotNull @RequestBody CreateAccountCommand command) throws AccountAlreadyExistsException {
        log.trace("Handling request for account creation: {}.", command);
        AccountDto result = accountsService.create(command, LanguageSupport.resolve(request));
        log.trace("Request for account creation with command {} handled successfully: {}.", command, result);
        return result;
    }

    @Override
    public AccountDto confirm(@NotBlank @RequestParam(TOKEN_REQUEST_PARAM) String token) throws TokenInvalidException {
        log.trace("Handling request for account confirmation [ token: {} ].", token);
        AccountDto result = accountsService.confirm(token);
        log.trace("Request for account confirmation handled successfully: {}.", result);
        return result;
    }

    @Override
    public AccountDto update(@Valid @NotNull @RequestBody UpdateAccountCommand command) throws UserIdentityMissingException, UserNotFoundException, AccountAlreadyExistsException, AccountNotFoundException, IllegalUserStatusException, IllegalAccountStatusException {
        log.trace("Handling request for account update: {}.", command);
        AccountDto result = accountsService.update(UserIdentityResolver.resolve(request), command);
        log.trace("Request for account update handled successfully: {}.", result);
        return result;
    }

    @Override
    public PaymentsSettingsDto getPaymentsSettings(@NotNull @PathVariable(ACCOUNT_ID_PATH_VARIABLE) UUID accountId) throws AccountNotFoundException {
        log.trace("Handling request for payment settings of account with id {}.", accountId);
        PaymentsSettingsDto result = accountsService.getPaymentsSettings(accountId);
        log.trace("Request for payment settings of account with id {} handled successfully: {}.", accountId, result);
        return result;
    }

}
package pl.passbox.accounts.accounts.service;

import com.neovisionaries.i18n.LanguageCode;
import org.hibernate.validator.constraints.NotBlank;
import pl.passbox.api.accounts.accounts.exceptions.AccountAlreadyExistsException;
import pl.passbox.api.accounts.accounts.exceptions.AccountNotFoundException;
import pl.passbox.api.accounts.accounts.exceptions.IllegalAccountStatusException;
import pl.passbox.api.accounts.accounts.model.AccountDto;
import pl.passbox.api.accounts.accounts.model.CreateAccountCommand;
import pl.passbox.api.accounts.accounts.model.PaymentsSettingsDto;
import pl.passbox.api.accounts.accounts.model.UpdateAccountCommand;
import pl.passbox.api.accounts.team.exceptions.IllegalUserStatusException;
import pl.passbox.api.accounts.team.exceptions.UserNotFoundException;
import pl.passbox.api.accounts.tokens.exceptions.TokenInvalidException;
import pl.passbox.commons.identity.model.UserIdentity;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

public interface AccountsService {

    AccountDto get(@NotNull UUID accountId) throws AccountNotFoundException;

    AccountDto create(@Valid @NotNull CreateAccountCommand command, @NotNull LanguageCode language) throws AccountAlreadyExistsException;

    AccountDto confirm(@NotBlank String sToken) throws TokenInvalidException;

    AccountDto update(@NotNull @Valid UserIdentity identity, @NotNull @Valid UpdateAccountCommand command) throws UserNotFoundException, IllegalUserStatusException, IllegalAccountStatusException, AccountNotFoundException, AccountAlreadyExistsException;

    PaymentsSettingsDto getPaymentsSettings(@NotNull UUID accountId) throws AccountNotFoundException;

}
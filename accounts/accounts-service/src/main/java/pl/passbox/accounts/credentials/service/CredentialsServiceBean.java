package pl.passbox.accounts.credentials.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import pl.passbox.accounts.accounts.model.Account;
import pl.passbox.accounts.accounts.repository.AccountsRepository;
import pl.passbox.accounts.configuration.ConfirmationLinksProperties;
import pl.passbox.accounts.team.model.User;
import pl.passbox.accounts.team.repository.UsersRepository;
import pl.passbox.accounts.tokens.VerificationToken;
import pl.passbox.accounts.tokens.VerificationTokenSupport;
import pl.passbox.api.accounts.accounts.exceptions.AccountNotFoundException;
import pl.passbox.api.accounts.accounts.exceptions.IllegalAccountStatusException;
import pl.passbox.api.accounts.credentials.model.ChangePasswordCommand;
import pl.passbox.api.accounts.credentials.model.ConfirmPasswordResetCommand;
import pl.passbox.api.accounts.credentials.model.RequestEmailChangeCommand;
import pl.passbox.api.accounts.credentials.model.UserDetails;
import pl.passbox.api.accounts.team.exceptions.IllegalUserStatusException;
import pl.passbox.api.accounts.team.exceptions.UserAlreadyExistsException;
import pl.passbox.api.accounts.team.exceptions.UserNotFoundException;
import pl.passbox.api.accounts.tokens.exceptions.TokenInvalidException;
import pl.passbox.api.messages.MessagesApi;
import pl.passbox.api.messages.model.EmailChangeConfirmationEmail;
import pl.passbox.api.messages.model.PasswordResetConfirmationEmail;
import pl.passbox.commons.identity.model.UserIdentity;

import java.util.Collections;
import java.util.UUID;

@Slf4j
@Service
@Validated
@AllArgsConstructor
public class CredentialsServiceBean implements CredentialsService {

    private final ConfirmationLinksProperties confirmationLinksProperties;

    private final VerificationTokenSupport verificationTokenSupport;

    private final AccountsRepository accountsRepository;

    private final UsersRepository usersRepository;

    private final PasswordEncoder passwordEncoder;

    private final MessagesApi messagesApi;

    @Override
    public UserDetails getUserDetails(UUID accountId, String email) throws UserNotFoundException, IllegalUserStatusException, IllegalAccountStatusException, AccountNotFoundException {
        log.debug("Getting user details for { email: {}, accountId: {} }.", email, accountId);
        User user = accountsRepository.findOne(accountId)
                .orElseThrow(() -> AccountNotFoundException.byId(accountId))
                .findActiveUser(User.filterByEmail(email));
        UserDetails result = UserDetails.builder()
                .authorities(Collections.emptyList()) // todo
                .accountId(accountId)
                .password(user.getPassword())
                .username(user.getEmail())
                .userId(user.getId())
                .enabled(true)
                .build();
        log.debug("User details for { email: {}, accountId: {} } retrieved successfully: {}.", email, accountId, result);
        return result;
    }

    @Override
    public void requestPasswordReset(UUID accountId, String email) throws UserNotFoundException, IllegalUserStatusException, AccountNotFoundException, IllegalAccountStatusException {
        log.info("Requesting password reset for { accountId: {}, email: {} }.", accountId, email);
        Account account = accountsRepository.findOne(accountId)
                .orElseThrow(() -> AccountNotFoundException.byId(accountId));
        User user = account.findActiveUser(User.filterByEmail(email));
        messagesApi.send(Collections.singletonList(PasswordResetConfirmationEmail.builder()
                .link(confirmationLinksProperties.getPasswordReset().assembleConfirmationLink(verificationTokenSupport.encode(VerificationToken.PasswordResetConfirmation.builder()
                        .expires(confirmationLinksProperties.getPasswordReset().assembleLinkExpiration())
                        .hash(DigestUtils.sha256Hex(user.getPassword()))
                        .userId(user.getId())
                        .build()
                )))
                .locale(user.getSettings().getLanguage().toLocale())
                .accountName(account.getName())
                .firstname(user.getFirstname())
                .email(user.getEmail())
                .build()
        ));
        log.info("Password reset for { accountId: {}, email: {} } requested successfully. Password reset confirmation email sent.", accountId, email);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void confirmPasswordReset(ConfirmPasswordResetCommand command) throws TokenInvalidException {
        log.info("Confirm password reset: {}.", command);
        User user = getByToken(verificationTokenSupport.decode(command.getToken(), VerificationToken.PasswordResetConfirmation.class));
        log.debug("User by decoded token retrieved, userId: {}.", user.getId());
        user.setPassword(passwordEncoder.encode(command.getPassword1()));
        usersRepository.save(user);
        log.info("Password reset for user with id {} confirmed successfully.", user.getId());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void changePassword(UserIdentity identity, ChangePasswordCommand command) throws UserNotFoundException, IllegalUserStatusException, IllegalAccountStatusException, AccountNotFoundException {
        log.info("Changing password for user identity {}.", identity);
        User user = accountsRepository.findOne(identity.getAccountId())
                .orElseThrow(() -> AccountNotFoundException.byId(identity.getAccountId()))
                .findActiveUser(User.filterById(identity.getUserId()));
        user.setPassword(passwordEncoder.encode(command.getPassword1()));
        usersRepository.save(user);
        log.info("Password for user with id {} changed successfully.", user.getId());
    }

    @Override
    public void requestEmailChange(UserIdentity identity, RequestEmailChangeCommand command) throws UserNotFoundException, IllegalUserStatusException, UserAlreadyExistsException, IllegalAccountStatusException, AccountNotFoundException {
        log.info("Requesting email change for identity {}: {}.", identity, command);

        Account account = accountsRepository.findOne(identity.getAccountId())
                .orElseThrow(() -> AccountNotFoundException.byId(identity.getAccountId()));

        User user = account.findActiveUser(User.filterById(identity.getUserId()));
        if (user.getEmail().equals(command.getEmail())) {
            log.info("Given email ({}) to change to for identity {} is equal to actual. Ignoring.", command.getEmail(), identity);
            return;
        }

        if (usersRepository.isUserExists(account.getId(), command.getEmail())) {
            throw UserAlreadyExistsException.byEmail(command.getEmail(), account.getId());
        }

        messagesApi.send(Collections.singletonList(EmailChangeConfirmationEmail.builder()
                .link(confirmationLinksProperties.getEmailChange().assembleConfirmationLink(verificationTokenSupport.encode(VerificationToken.EmailChangeConfirmation.builder()
                        .expires(confirmationLinksProperties.getEmailChange().assembleLinkExpiration())
                        .hash(DigestUtils.sha256Hex(user.getEmail()))
                        .email(command.getEmail())
                        .userId(user.getId())
                        .build()
                )))
                .locale(user.getSettings().getLanguage().toLocale())
                .accountName(account.getName())
                .firstname(user.getFirstname())
                .email(command.getEmail())
                .build()
        ));

        log.info("Email change for user with id {} requested successfully. Confirmation email sent to email '{}'.", user.getId(), command.getEmail());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void confirmEmailChange(String token) throws TokenInvalidException, UserAlreadyExistsException {
        log.debug("Confirm email change for token {}.", token);
        VerificationToken.EmailChangeConfirmation decoded = verificationTokenSupport.decode(token, VerificationToken.EmailChangeConfirmation.class);
        User user = getByToken(decoded);
        if (usersRepository.isUserExists(user.getAccount().getId(), decoded.getEmail())) {
            throw UserAlreadyExistsException.byEmail(decoded.getEmail(), user.getAccount().getId());
        }
        log.debug("User by decoded token retrieved, userId: {}. Changing email [{} -> {}].", user.getId(), user.getEmail(), decoded.getEmail());
        user.setEmail(decoded.getEmail());
        usersRepository.save(user);
        log.info("Email change for user with id {} confirmed successfully.", user.getId());
    }

    private User getByToken(VerificationToken.PasswordResetConfirmation token) throws TokenInvalidException {
        log.debug("Getting user by decoded token {}.", token);
        try {
            User user = usersRepository.findOne(token.getUserId()).orElseThrow(() -> UserNotFoundException.byId(token.getUserId()));
            if (!DigestUtils.sha256Hex(user.getPassword()).equals(token.getHash())) {
                log.debug("Old password hash doesn't match with decoded hash from token. Token is probably already used.");
                throw new TokenInvalidException();
            }
            return user;
        } catch (UserNotFoundException e) {
            throw new TokenInvalidException(e);
        }
    }

    private User getByToken(VerificationToken.EmailChangeConfirmation token) throws TokenInvalidException {
        log.debug("Getting user by decoded token {}.", token);
        try {
            User user = usersRepository.findOne(token.getUserId()).orElseThrow(() -> UserNotFoundException.byId(token.getUserId()));
            if (!DigestUtils.sha256Hex(user.getEmail()).equals(token.getHash())) {
                log.debug("Old email hash doesn't match with decoded hash from token. Token is probably already used.");
                throw new TokenInvalidException();
            }
            return user;
        } catch (UserNotFoundException e) {
            throw new TokenInvalidException(e);
        }
    }

}
package pl.passbox.accounts.tokens;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.google.common.collect.ImmutableMap;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.impl.DefaultClaims;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import pl.passbox.commons.utils.DateTimeUtils;

import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Map;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonSubTypes({
        @JsonSubTypes.Type(value = VerificationToken.AccountRegistrationConfirmation.class, name = "REGISTRATION_CONFIRMATION"),
        @JsonSubTypes.Type(value = VerificationToken.PasswordResetConfirmation.class, name = "PASSWORD_RESET_CONFIRMATION"),
        @JsonSubTypes.Type(value = VerificationToken.EmailChangeConfirmation.class, name = "EMAIL_CHANGE_CONFIRMATION")
})
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
public abstract class VerificationToken {

    private static final String EXPIRES_PROPERTY = "expires";

    private static final String TYPE_PROPERTY = "type";

    @NotNull
    private Instant expires;

    @JsonIgnore
    public abstract Type getType();

    @JsonIgnore
    public abstract Map<String, Object> assembleProperties();

    @JsonIgnore
    public final Claims getClaims() {
        return new DefaultClaims(ImmutableMap.<String, Object>builder()
                .put(EXPIRES_PROPERTY, DateTimeUtils.format(expires))
                .put(TYPE_PROPERTY, getType())
                .putAll(assembleProperties())
                .build()
        );
    }

    @JsonIgnore
    public final boolean isExpired() {
        return expires.isBefore(Instant.now());
    }

    public enum Type {
        REGISTRATION_CONFIRMATION,
        PASSWORD_RESET_CONFIRMATION,
        EMAIL_CHANGE_CONFIRMATION
    }

    @Data
    @NoArgsConstructor
    @ToString(callSuper = true)
    public static class AccountRegistrationConfirmation extends VerificationToken {

        private static final String ACCOUNT_ID_PROPERTY = "accountId";

        private final Type type = Type.REGISTRATION_CONFIRMATION;

        @NotNull
        private UUID accountId;

        @Builder
        public AccountRegistrationConfirmation(@JsonProperty(EXPIRES_PROPERTY) Instant expires, @JsonProperty(ACCOUNT_ID_PROPERTY) UUID accountId) {
            super(expires);
            this.accountId = accountId;
        }

        @Override
        public Map<String, Object> assembleProperties() {
            return ImmutableMap.of(
                    ACCOUNT_ID_PROPERTY, accountId
            );
        }

    }

    @Data
    @NoArgsConstructor
    @ToString(callSuper = true)
    public static class PasswordResetConfirmation extends VerificationToken {

        private static final String PASSWORD_HASH_PROPERTY = "hash";

        private static final String USER_ID_PROPERTY = "userId";

        private final Type type = Type.PASSWORD_RESET_CONFIRMATION;

        @NotNull
        private UUID userId;

        @NotBlank
        private String hash;

        @Builder
        public PasswordResetConfirmation(@JsonProperty(EXPIRES_PROPERTY) Instant expires, @JsonProperty(USER_ID_PROPERTY) UUID userId, @JsonProperty(PASSWORD_HASH_PROPERTY) String hash) {
            super(expires);
            this.userId = userId;
            this.hash = hash;
        }

        @Override
        public Map<String, Object> assembleProperties() {
            return ImmutableMap.of(
                    PASSWORD_HASH_PROPERTY, hash,
                    USER_ID_PROPERTY, userId
            );
        }

    }

    @Data
    @NoArgsConstructor
    @ToString(callSuper = true)
    public static class EmailChangeConfirmation extends VerificationToken {

        private static final String OLD_EMAIL_HASH_PROPERTY = "hash";

        private static final String NEW_EMAIL_PROPERTY = "email";

        private static final String USER_ID_PROPERTY = "userId";

        private final Type type = Type.EMAIL_CHANGE_CONFIRMATION;

        @NotNull
        private UUID userId;

        @Email
        @NotBlank
        private String email;

        @NotBlank
        private String hash;

        @Builder
        public EmailChangeConfirmation(@JsonProperty(EXPIRES_PROPERTY) Instant expires, @JsonProperty(USER_ID_PROPERTY) UUID userId, @JsonProperty(NEW_EMAIL_PROPERTY) String email, @JsonProperty(OLD_EMAIL_HASH_PROPERTY) String hash) {
            super(expires);
            this.userId = userId;
            this.email = email;
            this.hash = hash;
        }

        @Override
        public Map<String, Object> assembleProperties() {
            return ImmutableMap.of(
                    OLD_EMAIL_HASH_PROPERTY, hash,
                    USER_ID_PROPERTY, userId,
                    NEW_EMAIL_PROPERTY, email
            );
        }

    }

}
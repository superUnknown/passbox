package pl.passbox.accounts.tokens;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.CompressionCodecs;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import pl.passbox.api.accounts.tokens.exceptions.TokenInvalidException;

import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@Validated
@AllArgsConstructor
public class VerificationTokenSupportBean implements VerificationTokenSupport {

    private static final String SALT = "4TBAF8kThcbV2DxkVHTWlDEmZvRzgNaM";

    private static final String SIGNATURE_HEADER = "signature";

    private final ObjectMapper mapper;

    @Override
    public <T extends VerificationToken> String encode(T spec) {
        return Jwts.builder()
                .setHeaderParam(SIGNATURE_HEADER, assembleSignature(spec.getClaims()))
                .signWith(SignatureAlgorithm.HS256, SALT)
                .compressWith(CompressionCodecs.DEFLATE)
                .setClaims(spec.getClaims())
                .compact();
    }

    @Override
    public <T extends VerificationToken> T decode(String value, Class<T> clazz) throws TokenInvalidException {
        try {
            Jws<Claims> decoded = Jwts.parser().setSigningKey(SALT).parseClaimsJws(value);
            if (!isSignatureValid(decoded)) {
                throw new SignatureException("Given token is invalid.");
            }
            T token = mapper.convertValue(decoded.getBody(), clazz);
            if (token.isExpired()) {
                throw new SignatureException("Given token is expired.");
            }
            return token;
        } catch (Exception e) {
            throw new TokenInvalidException(e);
        }
    }

    private static String assembleSignature(Claims claims) {
        return DigestUtils.sha256Hex(claims.values().stream().map(Object::toString).collect(Collectors.joining()) + SALT);
    }

    private static boolean isSignatureValid(Jws<Claims> token) {
        return Optional.ofNullable(token.getHeader().get(SIGNATURE_HEADER))
                .map(signature -> signature.equals(assembleSignature(token.getBody())))
                .orElse(false);
    }

}
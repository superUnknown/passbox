package pl.passbox.accounts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import pl.passbox.api.messages.configuration.MessagesApiConfiguration;
import pl.passbox.commons.configuration.CommonApplicationConfiguration;

@SpringBootApplication
@Import({CommonApplicationConfiguration.class, MessagesApiConfiguration.class})
public class AccountsApplication {

    public static void main(String[] args) {
        SpringApplication.run(AccountsApplication.class, args);
    }

}
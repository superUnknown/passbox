package pl.passbox.accounts.credentials.service;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import pl.passbox.api.accounts.credentials.model.ChangePasswordCommand;
import pl.passbox.api.accounts.credentials.model.ConfirmPasswordResetCommand;
import pl.passbox.api.accounts.credentials.model.RequestEmailChangeCommand;
import pl.passbox.api.accounts.credentials.model.UserDetails;
import pl.passbox.api.accounts.accounts.exceptions.IllegalAccountStatusException;
import pl.passbox.api.accounts.accounts.exceptions.AccountNotFoundException;
import pl.passbox.api.accounts.team.exceptions.IllegalUserStatusException;
import pl.passbox.api.accounts.team.exceptions.UserAlreadyExistsException;
import pl.passbox.api.accounts.team.exceptions.UserNotFoundException;
import pl.passbox.api.accounts.tokens.exceptions.TokenInvalidException;
import pl.passbox.commons.identity.model.UserIdentity;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

public interface CredentialsService {

    UserDetails getUserDetails(@NotNull UUID accountId, @NotBlank @Email String email) throws UserNotFoundException, IllegalUserStatusException, IllegalAccountStatusException, AccountNotFoundException;

    void requestPasswordReset(@NotNull UUID accountId, @NotBlank @Email String email) throws UserNotFoundException, IllegalUserStatusException, AccountNotFoundException, IllegalAccountStatusException;

    void confirmPasswordReset(@NotNull @Valid ConfirmPasswordResetCommand command) throws TokenInvalidException;

    void changePassword(@NotNull @Valid UserIdentity identity, @NotNull @Valid ChangePasswordCommand command) throws UserNotFoundException, IllegalUserStatusException, IllegalAccountStatusException, AccountNotFoundException;

    void requestEmailChange(@NotNull @Valid UserIdentity identity, @NotNull @Valid RequestEmailChangeCommand command) throws UserNotFoundException, IllegalUserStatusException, UserAlreadyExistsException, IllegalAccountStatusException, AccountNotFoundException;

    void confirmEmailChange(@NotBlank String token) throws TokenInvalidException, UserAlreadyExistsException;

}
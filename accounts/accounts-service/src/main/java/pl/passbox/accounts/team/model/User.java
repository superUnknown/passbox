package pl.passbox.accounts.team.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import pl.passbox.accounts.accounts.model.Account;
import pl.passbox.api.accounts.team.model.Status;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Arrays;
import java.util.UUID;
import java.util.function.Predicate;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @NotNull
    @Builder.Default
    @Enumerated(EnumType.STRING)
    private Status status = Status.UNCONFIRMED;

    @Email
    @NotBlank
    private String email;

    @NotBlank
    private String firstname;

    @NotBlank
    private String lastname;

    private String password;

    @NotNull
    @JoinColumn(name = "settings_id")
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private Settings settings;

    @NotNull
    @Builder.Default
    private Instant created = Instant.now();

    @ManyToOne
    @JoinColumn(name = "created_by")
    private User createdBy;

    private Instant modified;

    @ManyToOne
    @JoinColumn(name = "modified_by")
    private User modifiedBy;

    private Instant lastLogin;

    @ManyToOne
    @JoinColumn(name = "account_id")
    private Account account;

    @Transient
    public boolean hasStatus(Status... statuses) {
        return Arrays.asList(statuses).contains(status);
    }

    @Transient
    public static Predicate<User> filterByEmail(String email) {
        return new FilterByEmail(email);
    }

    @Transient
    public static Predicate<User> filterById(UUID userId) {
        return new FilterById(userId);
    }

    @ToString
    @AllArgsConstructor
    private static class FilterByEmail implements Predicate<User> {

        @Email
        @NotBlank
        private final String email;

        @Override
        public boolean test(User user) {
            return user.getEmail().equals(email);
        }

    }

    @ToString
    @AllArgsConstructor
    private static class FilterById implements Predicate<User> {

        @NotNull
        private final UUID userId;

        @Override
        public boolean test(User user) {
            return user.getId().equals(userId);
        }

    }

}
package pl.passbox.accounts.accounts.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Email;
import org.springframework.util.StringUtils;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;
import java.util.stream.Stream;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SocialChannels {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @Email
    private String email;

    private String phone;

    private String facebookLink;

    private String twitterLink;

    private String instagramLink;

    private String webPageLink;

    public boolean isEmpty() {
        return Stream.of(email, phone, facebookLink, twitterLink, instagramLink, webPageLink)
                .allMatch(StringUtils::isEmpty);
    }

}
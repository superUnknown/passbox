package pl.passbox.accounts.team.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import pl.passbox.accounts.team.model.User;

import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

public interface UsersRepository extends Repository<User, UUID> {

    @Query("" +
            "SELECT count(u)>0 FROM Account o " +
            "   JOIN o.users u " +
            "WHERE " +
            "   o.id = :accountId AND " +
            "   u.email = :email AND u.status != 'DELETED'" +
            ""
    )
    boolean isUserExists(@Param("accountId") UUID accountId, @Param("email") String email);

    Optional<? extends User> findOne(UUID id);

    User save(User user);

    Stream<? extends User> findAll();

    void delete(User user);

    void deleteAll();

}
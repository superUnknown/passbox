package pl.passbox.accounts

import com.fasterxml.jackson.databind.ObjectMapper
import com.neovisionaries.i18n.LanguageCode
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.mock.web.MockHttpServletResponse
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder
import pl.passbox.accounts.accounts.repository.AccountsRepository
import pl.passbox.accounts.team.repository.UsersRepository
import pl.passbox.accounts.tokens.VerificationTokenSupport
import pl.passbox.api.accounts.credentials.CredentialsApi
import pl.passbox.api.accounts.credentials.model.ChangePasswordCommand
import pl.passbox.api.accounts.credentials.model.ConfirmPasswordResetCommand
import pl.passbox.api.accounts.credentials.model.RequestEmailChangeCommand
import pl.passbox.api.accounts.accounts.AccountsApi
import pl.passbox.api.accounts.accounts.model.AddressDto
import pl.passbox.api.accounts.accounts.model.CreateAccountCommand
import pl.passbox.api.accounts.accounts.model.AccountDto
import pl.passbox.api.accounts.accounts.model.SocialChannelsDto
import pl.passbox.api.accounts.accounts.model.UpdateAccountCommand
import pl.passbox.api.messages.model.EmailChangeConfirmationEmail
import pl.passbox.api.messages.model.PasswordResetConfirmationEmail
import pl.passbox.api.messages.model.RegistrationConfirmationEmail
import pl.passbox.api.messages.model.Type
import pl.passbox.commons.configuration.ApplicationHeader
import pl.passbox.commons.identity.model.UserIdentity
import spock.lang.Specification

import java.time.Instant

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = RANDOM_PORT, classes = [MessagesApiMockConfiguration])
abstract class AccountsSpecIT extends Specification {

	@Autowired
	protected MessagesApiMockConfiguration.MessagesRegistry messagesRegistry

	@Autowired
	protected VerificationTokenSupport verificationTokenSupport

	@Autowired
	protected AccountsRepository accountsRepository

	@Autowired
	protected UsersRepository usersRepository

	@Autowired
	protected PasswordEncoder passwordEncoder

	@Autowired
	protected ObjectMapper mapper

	@Autowired
	protected MockMvc mockMvc

	def setup() {
		clean()
	}

	def cleanup() {
		clean()
	}

	void clean() {
		usersRepository.findAll().each {
			it.setAccount(null)
			usersRepository.save(it)
		}
		accountsRepository.findAll().each {
			it.setFounder(null)
			accountsRepository.delete(it)
		}
		usersRepository.deleteAll()
		messagesRegistry.clean()
	}

	protected static boolean isCloseBeforeNow(Instant time) {
		def diff = Instant.now().toEpochMilli() - time.toEpochMilli()
		diff >= 0 && diff <= 5000
	}

	protected <T> T createAccount(Class<T> clazz, String name = "test account", String email = "dev@null.com", String firstname = "Jan", String lastname = "Nowak", Locale locale = LanguageCode.pl.toLocale(), String password1 = "secret", String password2 = "secret") {
		CreateAccountCommand command = CreateAccountCommand.builder()
				.founder(CreateAccountCommand.Founder.builder().firstname(firstname).lastname(lastname).password1(password1).password2(password2).build())
				.email(email)
				.name(name)
				.build()
		return mapper.readValue(mockMvc.perform(post("${AccountsApi.ACCOUNTS_PATH}")
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.content(mapper.writeValueAsString(command))
				.locale(locale)
		).andReturn().getResponse().getContentAsString(), clazz)
	}

	protected <T> T confirmAccount(Class<T> clazz, String token) {
		return mapper.readValue(mockMvc.perform(get("${AccountsApi.CONFIRM_REGISTRATION_PATH}")
				.param(AccountsApi.TOKEN_REQUEST_PARAM, token)
		).andReturn().getResponse().getContentAsString(), clazz)
	}

	protected AccountDto createAndConfirmAccount(String name = "test account", String email = "dev@null.com", String firstname = "Jan", String lastname = "Nowak", Locale locale = LanguageCode.pl.toLocale(), String password1 = "secret", String password2 = "secret") {
		createAccount(AccountDto, name, email, firstname, lastname, locale, password1, password2)
		String link = (messagesRegistry.getMessages(Type.REGISTRATION_CONFIRMATION).last() as RegistrationConfirmationEmail).link
		return confirmAccount(AccountDto, link.substring(link.indexOf("token=") + 6))
	}

	protected <T> T updateAccount(Class<T> clazz, UserIdentity identity, String name = "test account", String description = "test description", String subdomain = "dev", AddressDto location = null, SocialChannelsDto socialChannels = null) {
		UpdateAccountCommand command = UpdateAccountCommand.builder()
				.socialChannels(socialChannels)
				.description(description)
				.subdomain(subdomain)
				.location(location)
				.name(name)
				.build()
		MockHttpServletRequestBuilder builder = put("${AccountsApi.ACCOUNTS_PATH}")
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.content(mapper.writeValueAsString(command))
		if (identity != null) {
			HttpHeaders headers = new HttpHeaders()
			headers.put(ApplicationHeader.USER_IDENTITY.value, [identity.encode()])
			builder.headers(headers)
		}
		return mapper.readValue(mockMvc.perform(builder).andReturn().getResponse().getContentAsString(), clazz)
	}

	protected <T> T getUserDetails(Class<T> clazz, String email, UUID accountId) {
		return mapper.readValue(mockMvc.perform(get("${CredentialsApi.USER_ACCOUNT_CREDENTIALS_PATH}", accountId)
				.param(CredentialsApi.EMAIL_PARAM, email)
		).andReturn().getResponse().getContentAsString(), clazz)
	}

	protected <T> T requestPasswordReset(Class<T> clazz, String email, UUID accountId) {
		MockHttpServletResponse response = mockMvc.perform(patch("${CredentialsApi.USER_ACCOUNT_CREDENTIALS_PASSWORD_RESET_PATH}", accountId)
				.param(CredentialsApi.EMAIL_PARAM, email)
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
		).andReturn().getResponse()
		return clazz == Void ? null : mapper.readValue(response.getContentAsString(), clazz)
	}

	protected <T> T confirmPasswordReset(Class<T> clazz, String password1, String password2 = password1) {
		String link = (messagesRegistry.getMessages(Type.PASSWORD_RESET_CONFIRMATION).last() as PasswordResetConfirmationEmail).link
		MockHttpServletResponse response = mockMvc.perform(patch("${CredentialsApi.CREDENTIALS_CONFIRM_PASSWORD_RESET_PATH}")
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.content(mapper.writeValueAsString(new ConfirmPasswordResetCommand(link.substring(link.indexOf("token=") + 6), password1, password2)))
		).andReturn().getResponse()
		return clazz == Void ? null : mapper.readValue(response.getContentAsString(), clazz)
	}

	protected <T> T changePassword(Class<T> clazz, UserIdentity identity, String password1, String password2 = password1) {
		MockHttpServletRequestBuilder builder = patch("${CredentialsApi.CREDENTIALS_PASSWORD_CHANGE_PATH}")
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.content(mapper.writeValueAsString(new ChangePasswordCommand(password1, password2)))

		if (identity != null) {
			HttpHeaders headers = new HttpHeaders()
			headers.put(ApplicationHeader.USER_IDENTITY.value, [identity.encode()])
			builder.headers(headers)
		}
		MockHttpServletResponse response = mockMvc.perform(builder).andReturn().getResponse()
		return clazz == Void ? null : mapper.readValue(response.getContentAsString(), clazz)
	}

	protected <T> T requestEmailChange(Class<T> clazz, String email, UserIdentity identity) {
		MockHttpServletRequestBuilder builder = patch("${CredentialsApi.CREDENTIALS_EMAIL_CHANGE_PATH}")
				.param(CredentialsApi.EMAIL_PARAM, email)
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.content(mapper.writeValueAsString(new RequestEmailChangeCommand(email)))
		if (identity != null) {
			HttpHeaders headers = new HttpHeaders()
			headers.put(ApplicationHeader.USER_IDENTITY.value, [identity.encode()])
			builder.headers(headers)
		}
		MockHttpServletResponse response = mockMvc.perform(builder).andReturn().getResponse()
		return clazz == Void ? null : mapper.readValue(response.getContentAsString(), clazz)
	}

	protected <T> T confirmEmailChange(Class<T> clazz) {
		String link = (messagesRegistry.getMessages(Type.EMAIL_CHANGE_CONFIRMATION).last() as EmailChangeConfirmationEmail).link
		MockHttpServletResponse response = mockMvc.perform(get("${CredentialsApi.CREDENTIALS_CONFIRM_EMAIL_CHANGE_PATH}")
				.param(CredentialsApi.TOKEN_PARAM, link.substring(link.indexOf("token=") + 6))
		).andReturn().getResponse()
		return clazz == Void ? null : mapper.readValue(response.getContentAsString(), clazz)
	}

}
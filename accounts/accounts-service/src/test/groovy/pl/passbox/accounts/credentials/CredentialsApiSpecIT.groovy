package pl.passbox.accounts.credentials

import com.neovisionaries.i18n.LanguageCode
import org.apache.commons.codec.digest.DigestUtils
import org.springframework.http.MediaType
import org.springframework.test.context.TestPropertySource
import pl.passbox.accounts.AccountsSpecIT
import pl.passbox.accounts.accounts.model.Account
import pl.passbox.accounts.team.model.User
import pl.passbox.accounts.tokens.VerificationToken
import pl.passbox.api.accounts.configuration.AccountsErrorCode
import pl.passbox.api.accounts.credentials.CredentialsApi
import pl.passbox.api.accounts.credentials.model.ConfirmPasswordResetCommand
import pl.passbox.api.accounts.credentials.model.UserDetails
import pl.passbox.api.accounts.accounts.model.AccountDto
import pl.passbox.api.accounts.team.model.Status
import pl.passbox.api.messages.model.Channel
import pl.passbox.api.messages.model.EmailChangeConfirmationEmail
import pl.passbox.api.messages.model.PasswordResetConfirmationEmail
import pl.passbox.api.messages.model.Type
import pl.passbox.commons.configuration.ApiError
import pl.passbox.commons.configuration.ApplicationException
import pl.passbox.commons.identity.model.UserIdentity
import spock.lang.Ignore

import java.time.Instant

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch

@TestPropertySource(properties = [
		"accounts.confirmation_links.password_reset.confirmation_link=http://127.0.0.1/credentials/password-reset/confirm?token={TOKEN}",
		"accounts.confirmation_links.email_change.confirmation_link=http://127.0.0.1/credentials/email/confirm?token={TOKEN}"
])
class CredentialsApiSpecIT extends AccountsSpecIT {

	def "Should retrieve user details for active member of active account successfully"() {
		given:
		AccountDto account = createAndConfirmAccount()
		User user = usersRepository.findOne(account.founderId).get()

		when:
		UserDetails result = getUserDetails(UserDetails, account.email, account.id)

		then:
		0 * _

		result.accountId == account.id
		result.password == user.password
		result.username == user.email
		result.credentialsNonExpired
		result.authorities.isEmpty() // todo
		result.accountNonExpired
		result.accountNonLocked
		result.enabled

	}

	def "Should retrieve user details for unverified member of unconfirmed account successfully"() {
		given:
		AccountDto account = createAccount(AccountDto)
		User user = usersRepository.findOne(account.founderId).get()

		when:
		UserDetails result = getUserDetails(UserDetails, account.email, account.id)

		then:
		0 * _

		result.accountId == account.id
		result.password == user.password
		result.username == user.email
		result.credentialsNonExpired
		result.authorities.isEmpty() // todo
		result.accountNonExpired
		result.accountNonLocked
		result.enabled

	}

	def "Should not retrieve user details when account with given id not exists"() {
		when:
		ApiError result = getUserDetails(ApiError, "dev@null.com", UUID.randomUUID())
		then:
		0 * _
		result.code == AccountsErrorCode.ACCOUNT_NOT_FOUND.name()
	}

	def "Should not retrieve user details for active member when account is deleted"() {
		given:
		AccountDto account = createAndConfirmAccount()
		with(accountsRepository.findOne(account.id).get()) { Account o ->
			o.setStatus(pl.passbox.api.accounts.accounts.model.Status.DELETED)
			accountsRepository.save(o)
		}

		when:
		ApiError result = getUserDetails(ApiError, account.email, account.id)

		then:
		0 * _
		result.code == AccountsErrorCode.ACCOUNT_DELETED.name()

	}

	def "Should not retrieve user details for active member when account is blocked"() {
		given:
		AccountDto account = createAndConfirmAccount()
		with(accountsRepository.findOne(account.id).get()) { Account o ->
			o.setStatus(pl.passbox.api.accounts.accounts.model.Status.BLOCKED)
			accountsRepository.save(o)
		}

		when:
		ApiError result = getUserDetails(ApiError, account.email, account.id)

		then:
		0 * _
		result.code == AccountsErrorCode.ACCOUNT_BLOCKED.name()

	}

	def "Should not retrieve user details when account doesn't contain member with given email"() {
		given:
		AccountDto account = createAndConfirmAccount()

		when:
		ApiError result = getUserDetails(ApiError, "unknown@null.com", account.id)

		then:
		0 * _

		result.code == AccountsErrorCode.USER_NOT_FOUND.name()

	}

	def "Should not retrieve user details when member of given account is blocked"() {
		given:
		AccountDto account = createAndConfirmAccount()
		with(usersRepository.findOne(account.founderId).get() as User) { User user ->
			user.setStatus(Status.BLOCKED)
			usersRepository.save(user)
		}

		when:
		ApiError result = getUserDetails(ApiError, account.email, account.id)

		then:
		0 * _

		result.code == AccountsErrorCode.USER_BLOCKED.name()

	}

	def "Should not retrieve user details when member of given account is deleted"() {
		given:
		AccountDto account = createAndConfirmAccount()
		with(usersRepository.findOne(account.founderId).get() as User) { User user ->
			user.setStatus(Status.DELETED)
			usersRepository.save(user)
		}

		when:
		ApiError result = getUserDetails(ApiError, account.email, account.id)

		then:
		0 * _

		result.code == AccountsErrorCode.USER_DELETED.name()

	}

	def "Should not retrieve user details when member of given account is unconfirmed"() {
		given:
		AccountDto account = createAndConfirmAccount()
		with(usersRepository.findOne(account.founderId).get() as User) { User user ->
			user.setStatus(Status.UNCONFIRMED)
			usersRepository.save(user)
		}

		when:
		ApiError result = getUserDetails(ApiError, account.email, account.id)

		then:
		0 * _

		result.code == AccountsErrorCode.USER_UNCONFIRMED.name()

	}

	def "Should request and confirm password reset successfully when account and user are active"() {
		given:
		AccountDto account = createAndConfirmAccount()
		with(usersRepository.findOne(account.founderId).get() as User) { User member ->
			member.setEmail("teamMember@email.com")
			usersRepository.save(member)
		}
		User user = usersRepository.findOne(account.founderId).get()

		assert passwordEncoder.matches("secret", user.password)

		when:

		requestPasswordReset(Void, user.email, account.id)
		confirmPasswordReset(Void, "updated_password")

		then:
		0 * _
		noExceptionThrown()

		with(usersRepository.findOne(user.id).get() as User) {
			passwordEncoder.matches("updated_password", password)
		}

		messagesRegistry.getMessages(Type.PASSWORD_RESET_CONFIRMATION).size() == 1
		with(messagesRegistry.getMessages(Type.PASSWORD_RESET_CONFIRMATION).first() as PasswordResetConfirmationEmail) {
			channel == Channel.EMAIL
			locale == LanguageCode.pl.toLocale()
			email == "teamMember@email.com"
			accountName == "test account"
			firstname == "Jan"
			link ==~ /http:\/\/127\.0\.0\.1\/credentials\/password-reset\/confirm\?token=.+/
		}

	}

	def "Should request and confirm password reset successfully when account is unconfirmed and user is unverified"() {
		given:
		AccountDto account = createAccount(AccountDto)
		with(usersRepository.findOne(account.founderId).get() as User) { User member ->
			member.setEmail("teamMember@email.com")
			usersRepository.save(member)
		}
		User user = usersRepository.findOne(account.founderId).get()

		assert passwordEncoder.matches("secret", user.password)

		when:

		requestPasswordReset(Void, user.email, account.id)
		confirmPasswordReset(Void, "updated_password")

		then:
		0 * _
		noExceptionThrown()

		with(usersRepository.findOne(user.id).get() as User) {
			passwordEncoder.matches("updated_password", password)
		}

		messagesRegistry.getMessages(Type.PASSWORD_RESET_CONFIRMATION).size() == 1
		with(messagesRegistry.getMessages(Type.PASSWORD_RESET_CONFIRMATION).first() as PasswordResetConfirmationEmail) {
			channel == Channel.EMAIL
			locale == LanguageCode.pl.toLocale()
			email == "teamMember@email.com"
			accountName == "test account"
			firstname == "Jan"
			link ==~ /http:\/\/127\.0\.0\.1\/credentials\/password-reset\/confirm\?token=.+/
		}

	}

	def "Should request and confirm password reset successfully when user was deactivated after password reset request"() {
		given:
		AccountDto account = createAndConfirmAccount()
		User user = usersRepository.findOne(account.founderId).get()

		assert passwordEncoder.matches("secret", user.password)

		requestPasswordReset(Void, user.email, account.id)
		with(usersRepository.findOne(user.id).get() as User) { User member ->
			member.setStatus(Status.BLOCKED)
			usersRepository.save(member)
		}

		when:

		confirmPasswordReset(Void, "updated_password")

		then:
		0 * _
		noExceptionThrown()

		with(usersRepository.findOne(user.id).get() as User) {
			passwordEncoder.matches("updated_password", password)
		}

		messagesRegistry.getMessages(Type.PASSWORD_RESET_CONFIRMATION).size() == 1
		with(messagesRegistry.getMessages(Type.PASSWORD_RESET_CONFIRMATION).first() as PasswordResetConfirmationEmail) {
			channel == Channel.EMAIL
			locale == LanguageCode.pl.toLocale()
			email == "dev@null.com"
			accountName == "test account"
			firstname == "Jan"
			link ==~ /http:\/\/127\.0\.0\.1\/credentials\/password-reset\/confirm\?token=.+/
		}

	}

	def "Should not request password reset when user is blocked"() {
		given:
		AccountDto account = createAndConfirmAccount()
		with(usersRepository.findOne(account.founderId).get() as User) { User member ->
			member.setStatus(Status.BLOCKED)
			usersRepository.save(member)
		}

		User user = usersRepository.findOne(account.founderId).get()

		assert passwordEncoder.matches("secret", user.password)

		when:

		ApiError response = requestPasswordReset(ApiError, user.email, account.id)

		then:
		0 * _
		response.code == AccountsErrorCode.USER_BLOCKED.name()

		with(usersRepository.findOne(user.id).get() as User) {
			passwordEncoder.matches("secret", password)
		}

		messagesRegistry.getMessages(Type.PASSWORD_RESET_CONFIRMATION).isEmpty()

	}

	def "Should not request password reset when user is unconfirmed"() {
		given:
		AccountDto account = createAndConfirmAccount()
		with(usersRepository.findOne(account.founderId).get() as User) { User member ->
			member.setStatus(Status.UNCONFIRMED)
			usersRepository.save(member)
		}

		User user = usersRepository.findOne(account.founderId).get()

		assert passwordEncoder.matches("secret", user.password)

		when:

		ApiError response = requestPasswordReset(ApiError, user.email, account.id)

		then:
		0 * _
		response.code == AccountsErrorCode.USER_UNCONFIRMED.name()

		with(usersRepository.findOne(user.id).get() as User) {
			passwordEncoder.matches("secret", password)
		}

		messagesRegistry.getMessages(Type.PASSWORD_RESET_CONFIRMATION).isEmpty()

	}

	def "Should not request password reset when user is deleted"() {
		given:
		AccountDto account = createAndConfirmAccount()
		with(usersRepository.findOne(account.founderId).get() as User) { User member ->
			member.setStatus(Status.DELETED)
			usersRepository.save(member)
		}

		User user = usersRepository.findOne(account.founderId).get()

		assert passwordEncoder.matches("secret", user.password)

		when:

		ApiError response = requestPasswordReset(ApiError, user.email, account.id)

		then:
		0 * _
		response.code == AccountsErrorCode.USER_DELETED.name()

		with(usersRepository.findOne(user.id).get() as User) {
			passwordEncoder.matches("secret", password)
		}

		messagesRegistry.getMessages(Type.PASSWORD_RESET_CONFIRMATION).isEmpty()

	}

	def "Should not request password reset when account not exists"() {
		given:
		AccountDto account = createAndConfirmAccount()

		when:

		ApiError response = requestPasswordReset(ApiError, "dev@null", UUID.randomUUID())

		then:
		0 * _
		response.code == AccountsErrorCode.ACCOUNT_NOT_FOUND.name()

		with(usersRepository.findOne(account.founderId).get() as User) {
			passwordEncoder.matches("secret", password)
		}

		messagesRegistry.getMessages(Type.PASSWORD_RESET_CONFIRMATION).isEmpty()

	}

	def "Should not confirm password reset when given passwords doesn't match"() {
		given:
		AccountDto account = createAndConfirmAccount()
		User user = usersRepository.findOne(account.founderId).get()

		assert passwordEncoder.matches("secret", user.password)
		requestPasswordReset(Void, user.email, account.id)

		when:

		ApiError error = confirmPasswordReset(ApiError, "updated_password", "other")

		then:
		0 * _
		error.code == ApplicationException.CommonsErrorCode.BAD_REQUEST.name()

		with(usersRepository.findOne(user.id).get() as User) {
			passwordEncoder.matches("secret", password)
		}

	}

	def "Should not confirm password reset when given token is already used"() {
		given:
		AccountDto account = createAndConfirmAccount()
		User user = usersRepository.findOne(account.founderId).get()

		assert passwordEncoder.matches("secret", user.password)

		requestPasswordReset(Void, user.email, account.id)
		confirmPasswordReset(Void, "updated_password")

		when:

		ApiError error = confirmPasswordReset(ApiError, "test")

		then:
		0 * _
		error.code == AccountsErrorCode.INVALID_TOKEN.name()

		with(usersRepository.findOne(user.id).get() as User) {
			passwordEncoder.matches("updated_password", password)
		}

	}

	def "Should not confirm password reset when token is expired"() {
		given:
		AccountDto account = createAndConfirmAccount()
		User user = usersRepository.findOne(account.founderId).get()

		assert passwordEncoder.matches("secret", user.password)

		requestPasswordReset(Void, user.email, account.id)

		when:

		ApiError error = mapper.readValue(mockMvc.perform(patch("${CredentialsApi.CREDENTIALS_CONFIRM_PASSWORD_RESET_PATH}")
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.content(mapper.writeValueAsString(new ConfirmPasswordResetCommand(verificationTokenSupport.encode(VerificationToken.PasswordResetConfirmation.builder()
				.expires(Instant.now().minusSeconds(60))
				.userId(user.getId())
				.hash(DigestUtils.sha256Hex(user.getPassword()))
				.build()
		), "updated_password", "updated_password")))
		).andReturn().getResponse().getContentAsString(), ApiError)

		then:
		0 * _
		error.code == AccountsErrorCode.INVALID_TOKEN.name()

		with(usersRepository.findOne(user.id).get() as User) {
			passwordEncoder.matches("secret", password)
		}

	}

	def "Should not confirm password reset when token signature doesn't match"() {
		given:
		AccountDto account = createAndConfirmAccount()
		User user = usersRepository.findOne(account.founderId).get()

		assert passwordEncoder.matches("secret", user.password)

		requestPasswordReset(Void, user.email, account.id)

		when:

		ApiError error = mapper.readValue(mockMvc.perform(patch("${CredentialsApi.CREDENTIALS_CONFIRM_PASSWORD_RESET_PATH}")
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.content(mapper.writeValueAsString(new ConfirmPasswordResetCommand(verificationTokenSupport.encode(VerificationToken.PasswordResetConfirmation.builder()
				.expires(Instant.now().plusSeconds(60))
				.userId(user.getId())
				.hash(DigestUtils.sha256Hex("invalid_password_hash"))
				.build()
		), "updated_password", "updated_password")))
		).andReturn().getResponse().getContentAsString(), ApiError)

		then:
		0 * _
		error.code == AccountsErrorCode.INVALID_TOKEN.name()

		with(usersRepository.findOne(user.id).get() as User) {
			passwordEncoder.matches("secret", password)
		}

	}

	def "Should not confirm password reset when token is invalid"() {
		given:
		AccountDto account = createAndConfirmAccount()
		User user = usersRepository.findOne(account.founderId).get()

		assert passwordEncoder.matches("secret", user.password)

		requestPasswordReset(Void, user.email, account.id)

		when:

		ApiError error = mapper.readValue(mockMvc.perform(patch("${CredentialsApi.CREDENTIALS_CONFIRM_PASSWORD_RESET_PATH}")
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.content(mapper.writeValueAsString(new ConfirmPasswordResetCommand("invalid", "updated_password", "updated_password")))
		).andReturn().getResponse().getContentAsString(), ApiError)

		then:
		0 * _
		error.code == AccountsErrorCode.INVALID_TOKEN.name()

		with(usersRepository.findOne(user.id).get() as User) {
			passwordEncoder.matches("secret", password)
		}

	}

	def "Should change password successfully when account and user are active"() {
		given:
		AccountDto account = createAndConfirmAccount()
		User user = usersRepository.findOne(account.founderId).get()

		assert passwordEncoder.matches("secret", user.password)

		when:

		changePassword(Void, UserIdentity.builder().accountId(account.id).userId(account.founderId).build(), "changed")

		then:
		0 * _
		noExceptionThrown()

		with(usersRepository.findOne(user.id).get() as User) {
			passwordEncoder.matches("changed", password)
		}

	}

	def "Should change password successfully when account is unconfirmed and user is unverified"() {
		given:
		AccountDto account = createAccount(AccountDto)
		User user = usersRepository.findOne(account.founderId).get()

		assert passwordEncoder.matches("secret", user.password)

		when:

		changePassword(Void, UserIdentity.builder().accountId(account.id).userId(account.founderId).build(), "changed")

		then:
		0 * _
		noExceptionThrown()

		with(usersRepository.findOne(user.id).get() as User) {
			passwordEncoder.matches("changed", password)
		}

	}

	def "Should not change password when user is blocked"() {
		given:
		AccountDto account = createAndConfirmAccount()
		with(usersRepository.findOne(account.founderId).get() as User) { User member ->
			member.setStatus(Status.BLOCKED)
			usersRepository.save(member)
		}
		User user = usersRepository.findOne(account.founderId).get()

		assert passwordEncoder.matches("secret", user.password)

		when:

		ApiError response = changePassword(ApiError, UserIdentity.builder().accountId(account.id).userId(account.founderId).build(), "changed")

		then:
		0 * _

		response.code == AccountsErrorCode.USER_BLOCKED.name()
		with(usersRepository.findOne(user.id).get() as User) {
			passwordEncoder.matches("secret", password)
		}

	}

	def "Should not change password when user is unconfirmed"() {
		given:
		AccountDto account = createAndConfirmAccount()
		with(usersRepository.findOne(account.founderId).get() as User) { User member ->
			member.setStatus(Status.UNCONFIRMED)
			usersRepository.save(member)
		}
		User user = usersRepository.findOne(account.founderId).get()

		assert passwordEncoder.matches("secret", user.password)

		when:

		ApiError response = changePassword(ApiError, UserIdentity.builder().accountId(account.id).userId(account.founderId).build(), "changed")

		then:
		0 * _

		response.code == AccountsErrorCode.USER_UNCONFIRMED.name()
		with(usersRepository.findOne(user.id).get() as User) {
			passwordEncoder.matches("secret", password)
		}

	}

	def "Should not change password when user is deleted"() {
		given:
		AccountDto account = createAndConfirmAccount()
		with(usersRepository.findOne(account.founderId).get() as User) { User member ->
			member.setStatus(Status.DELETED)
			usersRepository.save(member)
		}
		User user = usersRepository.findOne(account.founderId).get()

		assert passwordEncoder.matches("secret", user.password)

		when:

		ApiError response = changePassword(ApiError, UserIdentity.builder().accountId(account.id).userId(account.founderId).build(), "changed")

		then:
		0 * _

		response.code == AccountsErrorCode.USER_DELETED.name()
		with(usersRepository.findOne(user.id).get() as User) {
			passwordEncoder.matches("secret", password)
		}

	}

	def "Should not change password when user not exists"() {
		given:
		AccountDto account = createAndConfirmAccount()
		User user = usersRepository.findOne(account.founderId).get()

		assert passwordEncoder.matches("secret", user.password)

		when:

		ApiError response = changePassword(ApiError, UserIdentity.builder().accountId(account.id).userId(UUID.randomUUID()).build(), "changed")

		then:
		0 * _

		response.code == AccountsErrorCode.USER_NOT_FOUND.name()
		with(usersRepository.findOne(user.id).get() as User) {
			passwordEncoder.matches("secret", password)
		}

	}

	def "Should not change password when account not exists"() {
		given:
		AccountDto account = createAndConfirmAccount()
		User user = usersRepository.findOne(account.founderId).get()

		assert passwordEncoder.matches("secret", user.password)

		when:

		ApiError response = changePassword(ApiError, UserIdentity.builder().accountId(UUID.randomUUID()).userId(account.founderId).build(), "changed")

		then:
		0 * _

		response.code == AccountsErrorCode.ACCOUNT_NOT_FOUND.name()
		with(usersRepository.findOne(user.id).get() as User) {
			passwordEncoder.matches("secret", password)
		}

	}

	def "Should not change password when given passwords doesn't match"() {
		given:
		AccountDto account = createAndConfirmAccount()
		User user = usersRepository.findOne(account.founderId).get()

		assert passwordEncoder.matches("secret", user.password)

		when:

		ApiError response = changePassword(ApiError, UserIdentity.builder().accountId(account.id).userId(account.founderId).build(), "changed", "invalid")

		then:
		0 * _

		response.code == ApplicationException.CommonsErrorCode.BAD_REQUEST.name()
		with(usersRepository.findOne(user.id).get() as User) {
			passwordEncoder.matches("secret", password)
		}

	}

	def "Should not change password when user identity in header is invalid"() {
		given:
		AccountDto account = createAndConfirmAccount()
		User user = usersRepository.findOne(account.founderId).get()

		assert passwordEncoder.matches("secret", user.password)

		when:

		ApiError response = changePassword(ApiError, UserIdentity.builder().userId(account.founderId).build(), "changed")

		then:
		0 * _

		response.code == ApplicationException.CommonsErrorCode.USER_IDENTITY_MISSING.name()
		with(usersRepository.findOne(user.id).get() as User) {
			passwordEncoder.matches("secret", password)
		}

	}

	def "Should not change password when user identity in header is missing"() {
		given:
		AccountDto account = createAndConfirmAccount()
		User user = usersRepository.findOne(account.founderId).get()

		assert passwordEncoder.matches("secret", user.password)

		when:

		ApiError response = changePassword(ApiError, null, "changed")

		then:
		0 * _

		response.code == ApplicationException.CommonsErrorCode.USER_IDENTITY_MISSING.name()
		with(usersRepository.findOne(user.id).get() as User) {
			passwordEncoder.matches("secret", password)
		}

	}

	def "Should request and confirm email change successfully when account and user are active"() {
		given:
		AccountDto account = createAndConfirmAccount()
		User user = usersRepository.findOne(account.founderId).get()

		assert user.email == "dev@null.com"

		when:
		requestEmailChange(Void, "modified@null.com", new UserIdentity(user.id, account.id, Collections.emptySet()))
		confirmEmailChange(Void)

		then:
		0 * _
		noExceptionThrown()

		with(usersRepository.findOne(user.id).get() as User) {
			email == "modified@null.com"
		}

		messagesRegistry.getMessages(Type.EMAIL_CHANGE_CONFIRMATION).size() == 1
		with(messagesRegistry.getMessages(Type.EMAIL_CHANGE_CONFIRMATION).first() as EmailChangeConfirmationEmail) {
			channel == Channel.EMAIL
			locale == LanguageCode.pl.toLocale()
			email == "modified@null.com"
			accountName == "test account"
			firstname == "Jan"
			link ==~ /http:\/\/127\.0\.0\.1\/credentials\/email\/confirm\?token=.+/
		}

	}

	def "Should request and confirm email change successfully when account is unconfirmed and user is unverified"() {
		given:
		AccountDto account = createAccount(AccountDto)
		User user = usersRepository.findOne(account.founderId).get()

		assert user.email == "dev@null.com"

		when:
		requestEmailChange(Void, "modified@null.com", new UserIdentity(user.id, account.id, Collections.emptySet()))
		confirmEmailChange(Void)

		then:
		0 * _
		noExceptionThrown()

		with(usersRepository.findOne(user.id).get() as User) {
			email == "modified@null.com"
		}

		messagesRegistry.getMessages(Type.EMAIL_CHANGE_CONFIRMATION).size() == 1
		with(messagesRegistry.getMessages(Type.EMAIL_CHANGE_CONFIRMATION).first() as EmailChangeConfirmationEmail) {
			channel == Channel.EMAIL
			locale == LanguageCode.pl.toLocale()
			email == "modified@null.com"
			accountName == "test account"
			firstname == "Jan"
			link ==~ /http:\/\/127\.0\.0\.1\/credentials\/email\/confirm\?token=.+/
		}

	}

	def "Should request and confirm email change successfully when user was deactivated after email change request"() {
		given:
		AccountDto account = createAndConfirmAccount()
		User user = usersRepository.findOne(account.founderId).get()

		assert user.email == "dev@null.com"

		requestEmailChange(Void, "modified@null.com", new UserIdentity(user.id, account.id, Collections.emptySet()))
		with(usersRepository.findOne(user.id).get() as User) { User member ->
			member.setStatus(Status.BLOCKED)
			usersRepository.save(member)
		}

		when:
		confirmEmailChange(Void)

		then:
		0 * _
		noExceptionThrown()

		with(usersRepository.findOne(user.id).get() as User) {
			email == "modified@null.com"
		}

		messagesRegistry.getMessages(Type.EMAIL_CHANGE_CONFIRMATION).size() == 1
		with(messagesRegistry.getMessages(Type.EMAIL_CHANGE_CONFIRMATION).first() as EmailChangeConfirmationEmail) {
			channel == Channel.EMAIL
			locale == LanguageCode.pl.toLocale()
			email == "modified@null.com"
			accountName == "test account"
			firstname == "Jan"
			link ==~ /http:\/\/127\.0\.0\.1\/credentials\/email\/confirm\?token=.+/
		}

	}

	def "Should not request email change when current user email is equal to given one"() {
		given:
		AccountDto account = createAndConfirmAccount()
		User user = usersRepository.findOne(account.founderId).get()

		assert user.email == "dev@null.com"

		when:
		requestEmailChange(Void, "dev@null.com", new UserIdentity(user.id, account.id, Collections.emptySet()))

		then:
		0 * _
		noExceptionThrown()

		with(usersRepository.findOne(user.id).get() as User) {
			email == "dev@null.com"
		}

		messagesRegistry.getMessages(Type.EMAIL_CHANGE_CONFIRMATION).isEmpty()

	}

	def "Should not confirm email change when token has been already used"() {
		given:
		AccountDto account = createAndConfirmAccount()
		User user = usersRepository.findOne(account.founderId).get()
		assert user.email == "dev@null.com"
		requestEmailChange(Void, "modified@null.com", new UserIdentity(user.id, account.id, Collections.emptySet()))
		confirmEmailChange(Void)

		when:

		ApiError response = confirmEmailChange(ApiError)

		then:
		0 * _
		response.code == AccountsErrorCode.INVALID_TOKEN.name()

		with(usersRepository.findOne(user.id).get() as User) {
			email == "modified@null.com"
		}

		messagesRegistry.getMessages(Type.EMAIL_CHANGE_CONFIRMATION).size() == 1
		with(messagesRegistry.getMessages(Type.EMAIL_CHANGE_CONFIRMATION).first() as EmailChangeConfirmationEmail) {
			channel == Channel.EMAIL
			locale == LanguageCode.pl.toLocale()
			email == "modified@null.com"
			accountName == "test account"
			firstname == "Jan"
			link ==~ /http:\/\/127\.0\.0\.1\/credentials\/email\/confirm\?token=.+/
		}

	}

	@Ignore("Nie ma logiki dodawania user'ów do teamu.")
	def "Should request and confirm email change when other deleted user in account has equal email"() {

	}

	@Ignore("Nie ma logiki dodawania user'ów do teamu.")
	def "Should not request email change when email used by other user in account"() {

	}

	@Ignore("Nie ma logiki dodawania user'ów do teamu.")
	def "Should not request email change when other user in account requested change to equal email and has confirmed change earlier"() {

	}

}
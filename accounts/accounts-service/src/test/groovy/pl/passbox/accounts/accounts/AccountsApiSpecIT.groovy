package pl.passbox.accounts.accounts

import com.neovisionaries.i18n.CountryCode
import com.neovisionaries.i18n.CurrencyCode
import com.neovisionaries.i18n.LanguageCode
import org.springframework.test.context.TestPropertySource
import pl.passbox.accounts.AccountsSpecIT
import pl.passbox.accounts.accounts.model.Account
import pl.passbox.accounts.tokens.VerificationToken
import pl.passbox.api.accounts.accounts.model.AccountDto
import pl.passbox.api.accounts.accounts.model.AddressDto
import pl.passbox.api.accounts.accounts.model.RefundHandling
import pl.passbox.api.accounts.accounts.model.SocialChannelsDto
import pl.passbox.api.accounts.accounts.model.Status
import pl.passbox.api.accounts.configuration.AccountsErrorCode
import pl.passbox.api.messages.model.Channel
import pl.passbox.api.messages.model.RegistrationConfirmationEmail
import pl.passbox.api.messages.model.Type
import pl.passbox.commons.configuration.ApiError
import pl.passbox.commons.configuration.ApplicationException
import pl.passbox.commons.identity.model.UserIdentity

import java.time.Instant

@TestPropertySource(properties = [
		"accounts.confirmation_links.registration_confirmation.confirmation_link=http://127.0.0.1/accounts/confirm?token={TOKEN}"
])
class AccountsApiSpecIT extends AccountsSpecIT {

	def "Should create account successfully and send registration confirmation email"() {
		when:
		AccountDto result = createAccount(
				AccountDto,
				"test account",
				"dev@null.com",
				"Jan",
				"Nowak",
				LanguageCode.pl.toLocale(),
				"secret",
				"secret"
		)

		then:
		0 * _

		result.subdomain == "testaccount"
		result.status == Status.UNCONFIRMED
		result.name == "test account"
		isCloseBeforeNow(result.created)
		result.email == "dev@null.com"
		result.modifiedById == null
		result.description == null
		result.modified == null
		result.founderId != null
		result.id != null
		result.location == null
		result.socialChannels == null
		result.paymentsSettings.currency == CurrencyCode.PLN
		result.paymentsSettings.refundHandling == RefundHandling.AUTO
		result.paymentsSettings.bankAccount == null

		accountsRepository.findAll().count() == 1
		with(accountsRepository.findOne(result.id).get()) {
			status == result.status
			name == result.name
			email == result.email
			subdomain == result.subdomain
			description == null
			created == result.created
			founder.id == result.founderId
			modified == null
			modifiedBy == null
			location == null
			socialChannels == null
			paymentsSettings.currency == CurrencyCode.PLN
			paymentsSettings.refundHandling == RefundHandling.AUTO
			paymentsSettings.bankAccount == null
		}

		usersRepository.findAll().count() == 1
		with(usersRepository.findOne(result.founderId).get()) {
			status == pl.passbox.api.accounts.team.model.Status.UNVERIFIED
			email == result.email
			firstname == "Jan"
			lastname == "Nowak"
			passwordEncoder.matches("secret", password)
			isCloseBeforeNow(created)
			modified == null
			modifiedBy == null
			lastLogin == null
			settings.id != null
			settings.language == LanguageCode.pl
			createdBy == null
			account.id == result.id
		}

		messagesRegistry.getMessages(Type.REGISTRATION_CONFIRMATION).size() == 1
		with(messagesRegistry.getMessages(Type.REGISTRATION_CONFIRMATION).first() as RegistrationConfirmationEmail) {
			channel == Channel.EMAIL
			locale == LanguageCode.pl.toLocale()
			email == "dev@null.com"
			accountName == "test account"
			firstname == "Jan"
			link ==~ /http:\/\/127\.0\.0\.1\/accounts\/confirm\?token=.+/
		}

	}

	def "Should create account successfully and send registration confirmation email even when given email or name is used in other (deleted) account"() {
		given:
		AccountDto deletedWithEqualName = createAccount(AccountDto, "account_1", "email@email1.com", "Jan", "Nowak", LanguageCode.pl.toLocale(), "secret", "secret")
		AccountDto deletedWithEqualEmail = createAccount(AccountDto, "account_2", "email@email2.com", "Jan", "Nowak", LanguageCode.en.toLocale(), "secret", "secret")

		with(accountsRepository.findOne(deletedWithEqualName.id).get()) { Account o ->
			o.setStatus(Status.DELETED)
			accountsRepository.save(o)
		}
		with(accountsRepository.findOne(deletedWithEqualEmail.id).get()) { Account o ->
			o.setStatus(Status.DELETED)
			accountsRepository.save(o)
		}

		when:

		AccountDto result = createAccount(AccountDto, name, email, "Jan", "Nowak", LanguageCode.de.toLocale(), "secret", "secret")

		then:
		0 * _

		result.id != null

		accountsRepository.findAll().count() == savedAccountsSize
		with(accountsRepository.findOne(result.id).get()) {
			getName() == name
			getEmail() == email
		}

		messagesRegistry.getMessages(Type.REGISTRATION_CONFIRMATION).size() == 3
		(messagesRegistry.getMessages(Type.REGISTRATION_CONFIRMATION) as List<RegistrationConfirmationEmail>).collect {
			it.locale
		}.containsAll(LanguageCode.pl.toLocale(), LanguageCode.en.toLocale(), LanguageCode.de.toLocale())

		where:
		name        | email              | savedAccountsSize
		"account_1" | "dev@null.com"     | 3
		"test"      | "email@email2.com" | 3

	}

	def "Should not create account when there are constraint validation"() {
		when:
		ApiError result = createAccount(ApiError, name, email, firstname, lastname, locale, password1, password2)
		then:
		0 * _

		result.code == code.name()

		accountsRepository.findAll().count() == 0
		usersRepository.findAll().count() == 0
		messagesRegistry.getMessages(Type.REGISTRATION_CONFIRMATION).isEmpty()

		where:
		name           | email          | firstname | lastname | locale                     | password1 | password2    | code
		""             | "dev@null.com" | "Jan"     | "Nowak"  | LanguageCode.pl.toLocale() | "secret"  | "secret"     | ApplicationException.CommonsErrorCode.BAD_REQUEST
		" "            | "dev@null.com" | "Jan"     | "Nowak"  | LanguageCode.pl.toLocale() | "secret"  | "secret"     | ApplicationException.CommonsErrorCode.BAD_REQUEST
		"test account" | ""             | "Jan"     | "Nowak"  | LanguageCode.pl.toLocale() | "secret"  | "secret"     | ApplicationException.CommonsErrorCode.BAD_REQUEST
		"test account" | " "            | "Jan"     | "Nowak"  | LanguageCode.pl.toLocale() | "secret"  | "secret"     | ApplicationException.CommonsErrorCode.BAD_REQUEST
		"test account" | "dev@null.com" | ""        | "Nowak"  | LanguageCode.pl.toLocale() | "secret"  | "secret"     | ApplicationException.CommonsErrorCode.BAD_REQUEST
		"test account" | "dev@null.com" | " "       | "Nowak"  | LanguageCode.pl.toLocale() | "secret"  | "secret"     | ApplicationException.CommonsErrorCode.BAD_REQUEST
		"test account" | "dev@null.com" | "Jan"     | ""       | LanguageCode.pl.toLocale() | "secret"  | "secret"     | ApplicationException.CommonsErrorCode.BAD_REQUEST
		"test account" | "dev@null.com" | "Jan"     | " "      | LanguageCode.pl.toLocale() | "secret"  | "secret"     | ApplicationException.CommonsErrorCode.BAD_REQUEST
		"test account" | "dev@null.com" | "Jan"     | "Nowak"  | LanguageCode.pl.toLocale() | ""        | "secret"     | ApplicationException.CommonsErrorCode.BAD_REQUEST
		"test account" | "dev@null.com" | "Jan"     | "Nowak"  | LanguageCode.pl.toLocale() | " "       | "secret"     | ApplicationException.CommonsErrorCode.BAD_REQUEST
		"test account" | "dev@null.com" | "Jan"     | "Nowak"  | LanguageCode.pl.toLocale() | "secret"  | ""           | ApplicationException.CommonsErrorCode.BAD_REQUEST
		"test account" | "dev@null.com" | "Jan"     | "Nowak"  | LanguageCode.pl.toLocale() | "secret"  | " "          | ApplicationException.CommonsErrorCode.BAD_REQUEST
		"test account" | "dev@null.com" | "Jan"     | "Nowak"  | LanguageCode.pl.toLocale() | "secret"  | "not_secret" | ApplicationException.CommonsErrorCode.BAD_REQUEST

	}

	def "Should not create account when given email is used in other not deleted account"() {
		given:
		AccountDto account1 = createAccount(AccountDto, "account_1", "dev@null.com", "Jan 1", "Nowak", LanguageCode.pl.toLocale(), "secret", "secret")

		when:

		ApiError result = createAccount(ApiError, "account_2", "dev@null.com", "Jan 2", "Nowak", LanguageCode.en.toLocale(), "secret", "secret")

		then:
		0 * _

		result.code == AccountsErrorCode.ACCOUNT_ALREADY_EXISTS.name()
		accountsRepository.findAll().count() == 1
		accountsRepository.findOne(account1.id).isPresent()
		messagesRegistry.getMessages(Type.REGISTRATION_CONFIRMATION).size() == 1
		(messagesRegistry.getMessages(Type.REGISTRATION_CONFIRMATION).first() as RegistrationConfirmationEmail).locale == LanguageCode.pl.toLocale()

	}

	def "Should not create account when given name is used in other not deleted account"() {
		given:
		AccountDto account1 = createAccount(AccountDto, "account", "dev@null.com", "Jan 1", "Nowak", LanguageCode.pl.toLocale(), "secret", "secret")

		when:

		ApiError result = createAccount(ApiError, "account", "other@null.com", "Jan 2", "Nowak", LanguageCode.en.toLocale(), "secret", "secret")

		then:
		0 * _

		result.code == AccountsErrorCode.ACCOUNT_ALREADY_EXISTS.name()
		accountsRepository.findAll().count() == 1
		accountsRepository.findOne(account1.id).isPresent()

		messagesRegistry.getMessages(Type.REGISTRATION_CONFIRMATION).size() == 1
		(messagesRegistry.getMessages(Type.REGISTRATION_CONFIRMATION).first() as RegistrationConfirmationEmail).locale == LanguageCode.pl.toLocale()

	}

	def "Should confirm account registration successfully"() {
		given:
		createAccount(AccountDto)
		String link = (messagesRegistry.getMessages(Type.REGISTRATION_CONFIRMATION).first() as RegistrationConfirmationEmail).link

		when:

		AccountDto result = confirmAccount(AccountDto, link.substring(link.indexOf("token=") + 6))

		then:
		0 * _

		result.subdomain == "testaccount"
		result.status == Status.ACTIVE
		result.name == "test account"
		isCloseBeforeNow(result.created)
		result.email == "dev@null.com"
		result.modifiedById == null
		result.description == null
		result.modified == null
		result.founderId != null
		result.id != null
		result.location == null
		result.socialChannels == null

		accountsRepository.findAll().count() == 1
		with(accountsRepository.findOne(result.id).get()) {
			status == result.status
			name == result.name
			email == result.email
			subdomain == result.subdomain
			description == null
			created == result.created
			founder.id == result.founderId
			modified == null
			modifiedBy == null
			location == null
			socialChannels == null
		}

		usersRepository.findAll().count() == 1
		with(usersRepository.findOne(result.founderId).get()) {
			status == pl.passbox.api.accounts.team.model.Status.ACTIVE
			email == result.email
			firstname == "Jan"
			lastname == "Nowak"
			passwordEncoder.matches("secret", password)
			isCloseBeforeNow(created)
			modified == null
			modifiedBy == null
			lastLogin == null
			settings.id != null
			settings.language == LanguageCode.pl
			createdBy == null
		}

	}

	def "Should not confirm account registration when account is already confirmed"() {
		given:
		createAccount(AccountDto)
		String link = (messagesRegistry.getMessages(Type.REGISTRATION_CONFIRMATION).first() as RegistrationConfirmationEmail).link
		confirmAccount(AccountDto, link.substring(link.indexOf("token=") + 6))

		when:

		ApiError result = confirmAccount(ApiError, link.substring(link.indexOf("token=") + 6))

		then:
		0 * _

		result.code == AccountsErrorCode.INVALID_TOKEN.name()
		accountsRepository.findAll().count() == 1

	}

	def "Should not confirm account registration when given token is invalid"() {
		given:
		createAccount(AccountDto)

		when:

		ApiError result = confirmAccount(ApiError, "invalid")

		then:
		0 * _

		result.code == AccountsErrorCode.INVALID_TOKEN.name()
		accountsRepository.findAll().count() == 1

	}

	def "Should not confirm account registration when account not exists for decoded id from token"() {
		given:
		createAccount(AccountDto)

		when:

		ApiError result = confirmAccount(ApiError, verificationTokenSupport.encode(VerificationToken.AccountRegistrationConfirmation.builder()
				.expires(Instant.now().plusSeconds(10))
				.accountId(UUID.randomUUID())
				.build()))

		then:
		0 * _

		result.code == AccountsErrorCode.INVALID_TOKEN.name()
		accountsRepository.findAll().count() == 1

	}

	def "Should not confirm account registration when given token is expired"() {
		given:
		createAccount(AccountDto)

		when:

		ApiError result = confirmAccount(ApiError, verificationTokenSupport.encode(VerificationToken.AccountRegistrationConfirmation.builder()
				.expires(Instant.now().minusSeconds(10))
				.accountId(UUID.randomUUID())
				.build()))

		then:
		0 * _

		result.code == AccountsErrorCode.INVALID_TOKEN.name()
		accountsRepository.findAll().count() == 1

	}

	def "Should update account successfully when account and user are active"() {
		given:
		AccountDto account = createAndConfirmAccount()

		when:
		AccountDto result = updateAccount(
				AccountDto,
				new UserIdentity(account.founderId, account.id, Collections.emptySet()),
				"updated name",
				"updated desc",
				"UPDATEDsubdomain1"
		)

		then:
		0 * _

		result.subdomain == "updatedsubdomain1"
		result.status == Status.ACTIVE
		result.name == "updated name"
		result.email == "dev@null.com"
		result.modifiedById == account.founderId
		result.description == "updated desc"
		isCloseBeforeNow(result.modified)
		result.founderId == account.founderId
		result.id == account.id
		result.location == null
		result.socialChannels == null

		accountsRepository.findAll().count() == 1
		with(accountsRepository.findOne(result.id).get()) {
			status == result.status
			name == result.name
			email == result.email
			subdomain == result.subdomain
			description == result.description
			created == result.created
			founder.id == result.founderId
			modified == result.modified
			modifiedBy.id == result.founderId
			location == null
			socialChannels == null
		}

	}

	def "Should update account successfully when account is unconfirmed and user is unverified"() {
		given:
		AccountDto account = createAccount(AccountDto)

		when:
		AccountDto result = updateAccount(
				AccountDto,
				new UserIdentity(account.founderId, account.id, Collections.emptySet()),
				"updated name",
				"updated desc",
				"UPDATEDsubdomain1"
		)

		then:
		0 * _

		result.subdomain == "updatedsubdomain1"
		result.status == Status.UNCONFIRMED
		result.name == "updated name"
		result.email == "dev@null.com"
		result.modifiedById == account.founderId
		result.description == "updated desc"
		isCloseBeforeNow(result.modified)
		result.founderId == account.founderId
		result.id == account.id
		result.location == null
		result.socialChannels == null

		accountsRepository.findAll().count() == 1
		with(accountsRepository.findOne(result.id).get()) {
			status == result.status
			name == result.name
			email == result.email
			subdomain == result.subdomain
			description == result.description
			created == result.created
			founder.id == result.founderId
			modified == result.modified
			modifiedBy.id == result.founderId
			location == null
			socialChannels == null
		}

	}

	def "Should update account successfully when current name and subdomain are given and description is not given"() {
		given:
		AccountDto account = createAccount(AccountDto)

		when:
		AccountDto result = updateAccount(
				AccountDto,
				new UserIdentity(account.founderId, account.id, Collections.emptySet()),
				account.name,
				null,
				account.subdomain
		)

		then:
		0 * _

		result.subdomain == account.subdomain
		result.status == Status.UNCONFIRMED
		result.name == account.name
		result.email == "dev@null.com"
		result.modifiedById == account.founderId
		result.description == null
		isCloseBeforeNow(result.modified)
		result.founderId == account.founderId
		result.id == account.id
		result.location == null
		result.socialChannels == null

		accountsRepository.findAll().count() == 1
		with(accountsRepository.findOne(result.id).get()) {
			status == result.status
			name == result.name
			email == result.email
			subdomain == result.subdomain
			description == result.description
			created == result.created
			founder.id == result.founderId
			modified == result.modified
			modifiedBy.id == result.founderId
			location == null
			socialChannels == null
		}

	}

	def "Should update account successfully when location and social channels are given and then cleared"() {
		given:
		AccountDto account = createAndConfirmAccount()

		when:
		AccountDto result = updateAccount(
				AccountDto,
				new UserIdentity(account.founderId, account.id, Collections.emptySet()),
				"updated name",
				"updated desc",
				"UPDATEDsubdomain1",
				AddressDto.builder().country(CountryCode.PL).city("Gdańsk").street("Długa").build(),
				SocialChannelsDto.builder().email("social@null.com").build()
		)

		then:
		0 * _

		result.subdomain == "updatedsubdomain1"
		result.status == Status.ACTIVE
		result.name == "updated name"
		result.email == "dev@null.com"
		result.modifiedById == account.founderId
		result.description == "updated desc"
		isCloseBeforeNow(result.modified)
		result.founderId == account.founderId
		result.id == account.id
		with(result.location) {
			country == CountryCode.PL
			city == "Gdańsk"
			street == "Długa"
			house == null
			flat == null
			postalCode == null
		}
		with(result.socialChannels) {
			email == "social@null.com"
			phone == null
			facebookLink == null
			twitterLink == null
			instagramLink == null
			webPageLink == null
			phone == null
		}

		accountsRepository.findAll().count() == 1
		with(accountsRepository.findOne(result.id).get()) {
			status == result.status
			name == result.name
			email == result.email
			subdomain == result.subdomain
			description == result.description
			created == result.created
			founder.id == result.founderId
			modified == result.modified
			modifiedBy.id == result.founderId

			location.country == result.location.country
			location.city == result.location.city
			location.street == result.location.street
			location.house == result.location.house
			location.flat == result.location.flat
			location.postalCode == result.location.postalCode

			socialChannels.email == result.socialChannels.email
			socialChannels.phone == result.socialChannels.phone
			socialChannels.facebookLink == result.socialChannels.facebookLink
			socialChannels.twitterLink == result.socialChannels.twitterLink
			socialChannels.instagramLink == result.socialChannels.instagramLink
			socialChannels.webPageLink == result.socialChannels.webPageLink
			socialChannels.phone == result.socialChannels.phone

		}

		when:
		result = updateAccount(
				AccountDto,
				new UserIdentity(account.founderId, account.id, Collections.emptySet()),
				"updated name",
				"updated desc",
				"UPDATEDsubdomain1",
				null,
				SocialChannelsDto.builder().email("").build()
		)

		then:
		0 * _

		result.subdomain == "updatedsubdomain1"
		result.status == Status.ACTIVE
		result.name == "updated name"
		result.email == "dev@null.com"
		result.modifiedById == account.founderId
		result.description == "updated desc"
		isCloseBeforeNow(result.modified)
		result.founderId == account.founderId
		result.id == account.id
		result.location == null
		result.socialChannels == null

		accountsRepository.findAll().count() == 1
		with(accountsRepository.findOne(result.id).get()) {
			status == result.status
			name == result.name
			email == result.email
			subdomain == result.subdomain
			description == result.description
			created == result.created
			founder.id == result.founderId
			modified == result.modified
			modifiedBy.id == result.founderId
			location == null
			socialChannels == null
		}

	}

	def "Should not update account when validation fails"() {
		given:
		AccountDto account = createAccount(AccountDto, "test account", "dev@null.com")
		AccountDto account2 = createAccount(AccountDto, "test account 2", "dev2@null.com")
		AccountDto accountDeleted = createAccount(AccountDto, "test account 3", "dev3@null.com")
		with(accountsRepository.findOne(accountDeleted.id).get()) { Account o ->
			o.setStatus(Status.DELETED)
			accountsRepository.save(o)
		}

		Map<Integer, UUID> accountIds = [
				1: account.id,
				2: account2.id,
				3: accountDeleted.id,
				4: UUID.randomUUID()
		]

		Map<Integer, UUID> userIds = [
				1: account.founderId,
				2: account2.founderId,
				3: accountDeleted.founderId,
				4: UUID.randomUUID()
		]

		when:
		ApiError result = updateAccount(
				ApiError,
				new UserIdentity(userIds[userId], accountIds[accountId], Collections.emptySet()),
				name,
				null,
				subdomain,
				location,
				socialChannels
		)

		then:
		0 * _
		result.code == code.name()

		accountsRepository.findAll().count() == 3
		with(accountsRepository.findOne(account.id).get()) {
			status == account.status
			getName() == account.name
			email == account.email
			getSubdomain() == account.subdomain
			description == account.description
			created == account.created
			founder.id == account.founderId
			modified == null
			modifiedBy == null
		}

		where:
		code                                              | userId | accountId | name             | subdomain         | location                                             | socialChannels
		ApplicationException.CommonsErrorCode.BAD_REQUEST | 1      | 1         | null             | "valid"           | null                                                 | null
		ApplicationException.CommonsErrorCode.BAD_REQUEST | 1      | 1         | ""               | "valid"           | null                                                 | null
		ApplicationException.CommonsErrorCode.BAD_REQUEST | 1      | 1         | " "              | "valid"           | null                                                 | null
		ApplicationException.CommonsErrorCode.BAD_REQUEST | 1      | 1         | "valid"          | null              | null                                                 | null
		ApplicationException.CommonsErrorCode.BAD_REQUEST | 1      | 1         | "valid"          | ""                | null                                                 | null
		ApplicationException.CommonsErrorCode.BAD_REQUEST | 1      | 1         | "valid"          | " "               | null                                                 | null
		ApplicationException.CommonsErrorCode.BAD_REQUEST | 1      | 1         | "valid"          | "with whitespace" | null                                                 | null
		ApplicationException.CommonsErrorCode.BAD_REQUEST | 1      | 1         | "valid"          | "+withplus"       | null                                                 | null
		ApplicationException.CommonsErrorCode.BAD_REQUEST | 1      | 1         | "valid"          | "valid"           | AddressDto.builder().country(CountryCode.PL).build() | null
		ApplicationException.CommonsErrorCode.BAD_REQUEST | 1      | 1         | "valid"          | "valid"           | AddressDto.builder().city("Gdynia").build()          | null
		ApplicationException.CommonsErrorCode.BAD_REQUEST | 1      | 1         | "valid"          | "valid"           | null                                                 | SocialChannelsDto.builder().email("invalid").build()
		ApplicationException.CommonsErrorCode.BAD_REQUEST | 1      | 1         | "valid"          | "+/%4#2"          | null                                                 | null
		AccountsErrorCode.USER_NOT_FOUND                  | 4      | 1         | "valid"          | "valid"           | null                                                 | null
		AccountsErrorCode.USER_NOT_FOUND                  | 1      | 2         | "valid"          | "valid"           | null                                                 | null
		AccountsErrorCode.ACCOUNT_NOT_FOUND               | 1      | 4         | "valid"          | "valid"           | null                                                 | null
		AccountsErrorCode.ACCOUNT_ALREADY_EXISTS          | 1      | 1         | "test account 2" | "valid"           | null                                                 | null
		AccountsErrorCode.ACCOUNT_DELETED                 | 3      | 3         | "valid"          | "valid"           | null                                                 | null

	}

	def "Should not update account when user identity is missing"() {
		given:
		AccountDto account = createAccount(AccountDto, "test account", "dev@null.com")

		when:
		ApiError result = updateAccount(ApiError, null, "test account", "dev@null.com")

		then:
		0 * _
		result.code == ApplicationException.CommonsErrorCode.USER_IDENTITY_MISSING.name()

		accountsRepository.findAll().count() == 1
		with(accountsRepository.findOne(account.id).get()) {
			status == account.status
			getName() == account.name
			email == account.email
			getSubdomain() == account.subdomain
			description == account.description
			created == account.created
			founder.id == account.founderId
			modified == null
			modifiedBy == null
		}

	}

}
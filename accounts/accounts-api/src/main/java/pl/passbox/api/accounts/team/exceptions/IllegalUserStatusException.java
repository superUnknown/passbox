package pl.passbox.api.accounts.team.exceptions;

import lombok.Getter;
import pl.passbox.api.accounts.configuration.AccountsApplicationException;
import pl.passbox.api.accounts.configuration.AccountsErrorCode;
import pl.passbox.api.accounts.team.model.Status;

import java.util.UUID;

@Getter
public class IllegalUserStatusException extends AccountsApplicationException {

    private final AccountsErrorCode code;

    private IllegalUserStatusException(String message, AccountsErrorCode code) {
        super(message);
        this.code = code;
    }

    public static IllegalUserStatusException status(UUID userId, Status status) {
        switch (status) {
            case DELETED:
                return deleted(userId);
            case UNCONFIRMED:
                return unconfirmed(userId);
            case BLOCKED:
                return blocked(userId);
            case UNVERIFIED:
                return unverified(userId);
            default:
                return new IllegalUserStatusException(String.format("User with id %s has illegal status to execute this operation.", userId), AccountsErrorCode.ILLEGAL_USER_STATUS);
        }
    }

    private static IllegalUserStatusException deleted(UUID userId) {
        return new IllegalUserStatusException(String.format("User with id %s is deleted.", userId), AccountsErrorCode.USER_DELETED);
    }

    private static IllegalUserStatusException unconfirmed(UUID userId) {
        return new IllegalUserStatusException(String.format("User with id %s is unconfirmed.", userId), AccountsErrorCode.USER_UNCONFIRMED);
    }

    private static IllegalUserStatusException blocked(UUID userId) {
        return new IllegalUserStatusException(String.format("User with id %s is blocked.", userId), AccountsErrorCode.USER_BLOCKED);
    }

    private static IllegalUserStatusException unverified(UUID userId) {
        return new IllegalUserStatusException(String.format("User with id %s is unverified.", userId), AccountsErrorCode.USER_UNVERIFIED);
    }

}
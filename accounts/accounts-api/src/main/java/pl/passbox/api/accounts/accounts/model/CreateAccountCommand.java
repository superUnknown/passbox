package pl.passbox.api.accounts.accounts.model;

import lombok.Builder;
import lombok.ToString;
import lombok.Value;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import pl.passbox.commons.validation.EqualProperties;
import pl.passbox.commons.validation.Password;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Value
@Builder
public class CreateAccountCommand {

    @NotBlank
    private String name;

    @Email
    @NotBlank
    private String email;

    @Valid
    @NotNull
    private Founder founder;

    @Value
    @Builder
    @EqualProperties({"password1", "password2"})
    @ToString(exclude = {"password1", "password2"})
    public static class Founder {

        @NotBlank
        private String firstname;

        @NotBlank
        private String lastname;

        @NotBlank
        @Password
        private String password1;

        @NotBlank
        private String password2;

    }

}
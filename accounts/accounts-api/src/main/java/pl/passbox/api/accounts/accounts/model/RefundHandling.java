package pl.passbox.api.accounts.accounts.model;

public enum RefundHandling {
    AUTO,
    MANUAL
}
package pl.passbox.api.accounts.accounts;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.passbox.api.accounts.accounts.exceptions.IllegalAccountStatusException;
import pl.passbox.api.accounts.accounts.exceptions.AccountAlreadyExistsException;
import pl.passbox.api.accounts.accounts.exceptions.AccountNotFoundException;
import pl.passbox.api.accounts.accounts.model.CreateAccountCommand;
import pl.passbox.api.accounts.accounts.model.AccountDto;
import pl.passbox.api.accounts.accounts.model.PaymentsSettingsDto;
import pl.passbox.api.accounts.accounts.model.UpdateAccountCommand;
import pl.passbox.api.accounts.team.exceptions.IllegalUserStatusException;
import pl.passbox.api.accounts.team.exceptions.UserNotFoundException;
import pl.passbox.api.accounts.tokens.exceptions.TokenInvalidException;
import pl.passbox.commons.identity.exception.UserIdentityMissingException;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@FeignClient(AccountsApi.NAME)
@RequestMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface AccountsApi {

    String NAME = "accounts-api";

    String ACCOUNT_ID_PATH_VARIABLE = "id";

    String TOKEN_REQUEST_PARAM = "token";

    String ACCOUNTS_PATH = "/accounts";

    String ACCOUNT_PATH = ACCOUNTS_PATH + "/{" + ACCOUNT_ID_PATH_VARIABLE + "}";

    String ACCOUNT_PAYMENT_SETTINGS_PATH = ACCOUNT_PATH + "/payments-settings";

    String CONFIRM_REGISTRATION_PATH = ACCOUNTS_PATH + "/confirm";

    @GetMapping(ACCOUNT_PATH)
    AccountDto get(@NotNull @PathVariable(ACCOUNT_ID_PATH_VARIABLE) UUID accountId) throws AccountNotFoundException;

    @PostMapping(path = ACCOUNTS_PATH, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    AccountDto create(@Valid @NotNull @RequestBody CreateAccountCommand command) throws AccountAlreadyExistsException;

    @GetMapping(CONFIRM_REGISTRATION_PATH)
    AccountDto confirm(@NotBlank @RequestParam(TOKEN_REQUEST_PARAM) String token) throws TokenInvalidException;

    @PutMapping(path = ACCOUNTS_PATH, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    AccountDto update(@Valid @NotNull @RequestBody UpdateAccountCommand command) throws UserIdentityMissingException, UserNotFoundException, AccountAlreadyExistsException, AccountNotFoundException, IllegalUserStatusException, IllegalAccountStatusException;

    @GetMapping(ACCOUNT_PAYMENT_SETTINGS_PATH)
    PaymentsSettingsDto getPaymentsSettings(@NotNull @PathVariable(ACCOUNT_ID_PATH_VARIABLE) UUID accountId) throws AccountNotFoundException;

}
package pl.passbox.api.accounts.team.exceptions;

import lombok.Getter;
import pl.passbox.api.accounts.configuration.AccountsApplicationException;
import pl.passbox.api.accounts.configuration.AccountsErrorCode;

import java.util.UUID;
import java.util.function.Predicate;

@Getter
public class UserNotFoundException extends AccountsApplicationException {

    private final AccountsErrorCode code;

    private UserNotFoundException(String message, AccountsErrorCode code) {
        super(message);
        this.code = code;
    }

    public static UserNotFoundException byId(UUID userId) {
        return new UserNotFoundException(String.format("User with id %s not found.", userId), AccountsErrorCode.USER_NOT_FOUND);
    }

    public static UserNotFoundException byEmail(UUID accountId, String email) {
        return new UserNotFoundException(String.format("User with email '%s' not found in account with id %s.", email, accountId), AccountsErrorCode.USER_NOT_FOUND);
    }

    public static UserNotFoundException byFilter(UUID accountId, Predicate<?> filter) {
        return new UserNotFoundException(String.format("User not found in account with id %s by filter %s.", accountId, filter), AccountsErrorCode.USER_NOT_FOUND);
    }

}
package pl.passbox.api.accounts.credentials;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.passbox.api.accounts.credentials.model.ChangePasswordCommand;
import pl.passbox.api.accounts.credentials.model.ConfirmPasswordResetCommand;
import pl.passbox.api.accounts.credentials.model.RequestEmailChangeCommand;
import pl.passbox.api.accounts.credentials.model.UserDetails;
import pl.passbox.api.accounts.accounts.exceptions.IllegalAccountStatusException;
import pl.passbox.api.accounts.accounts.exceptions.AccountNotFoundException;
import pl.passbox.api.accounts.team.exceptions.IllegalUserStatusException;
import pl.passbox.api.accounts.team.exceptions.UserAlreadyExistsException;
import pl.passbox.api.accounts.team.exceptions.UserNotFoundException;
import pl.passbox.api.accounts.tokens.exceptions.TokenInvalidException;
import pl.passbox.commons.identity.exception.UserIdentityMissingException;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@FeignClient(CredentialsApi.NAME)
@RequestMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface CredentialsApi {

    String NAME = "credentials-api";

    String ACCOUNT_ID_PARAM = "accountId";

    String TOKEN_PARAM = "token";

    String EMAIL_PARAM = "email";

    String ACCOUNT_CREDENTIALS_PATH = "/accounts/{" + ACCOUNT_ID_PARAM + "}/credentials";

    String USER_ACCOUNT_CREDENTIALS_PATH = ACCOUNT_CREDENTIALS_PATH;

    String USER_ACCOUNT_CREDENTIALS_PASSWORD_RESET_PATH = USER_ACCOUNT_CREDENTIALS_PATH + "/password/reset";

    String CREDENTIALS_PATH = "/credentials";

    String CREDENTIALS_PASSWORD_CHANGE_PATH = CREDENTIALS_PATH + "/password";

    String CREDENTIALS_EMAIL_CHANGE_PATH = CREDENTIALS_PATH + "/email";

    String CREDENTIALS_CONFIRM_PASSWORD_RESET_PATH = CREDENTIALS_PATH + "/password-reset/confirm";

    String CREDENTIALS_CONFIRM_EMAIL_CHANGE_PATH = CREDENTIALS_PATH + "/email-change/confirm";

    @GetMapping(USER_ACCOUNT_CREDENTIALS_PATH)
    UserDetails getUserDetails(@NotNull @PathVariable(ACCOUNT_ID_PARAM) UUID accountId, @NotBlank @Email @RequestParam(EMAIL_PARAM) String email) throws UserNotFoundException, IllegalUserStatusException, IllegalAccountStatusException, AccountNotFoundException;

    @PatchMapping(USER_ACCOUNT_CREDENTIALS_PASSWORD_RESET_PATH)
    void requestPasswordReset(@NotNull @PathVariable(ACCOUNT_ID_PARAM) UUID accountId, @NotBlank @Email @RequestParam(EMAIL_PARAM) String email) throws UserNotFoundException, IllegalUserStatusException, AccountNotFoundException, IllegalAccountStatusException;

    @PatchMapping(path = CREDENTIALS_CONFIRM_PASSWORD_RESET_PATH, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    void confirmPasswordReset(@NotNull @Valid @RequestBody ConfirmPasswordResetCommand command) throws TokenInvalidException;

    @PatchMapping(path = CREDENTIALS_PASSWORD_CHANGE_PATH, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    void changePassword(@NotNull @Valid @RequestBody ChangePasswordCommand command) throws UserIdentityMissingException, UserNotFoundException, IllegalUserStatusException, AccountNotFoundException, IllegalAccountStatusException;

    @PatchMapping(path = CREDENTIALS_EMAIL_CHANGE_PATH, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    void requestEmailChange(@NotNull @Valid @RequestBody RequestEmailChangeCommand command) throws UserIdentityMissingException, UserAlreadyExistsException, IllegalUserStatusException, UserNotFoundException, AccountNotFoundException, IllegalAccountStatusException;

    @GetMapping(CREDENTIALS_CONFIRM_EMAIL_CHANGE_PATH)
    void confirmEmailChange(@NotBlank @RequestParam(TOKEN_PARAM) String token) throws TokenInvalidException, UserAlreadyExistsException;

}
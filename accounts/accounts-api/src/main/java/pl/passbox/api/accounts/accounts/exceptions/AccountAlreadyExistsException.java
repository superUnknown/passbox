package pl.passbox.api.accounts.accounts.exceptions;

import lombok.Getter;
import pl.passbox.api.accounts.configuration.AccountsApplicationException;
import pl.passbox.api.accounts.configuration.AccountsErrorCode;

@Getter
public class AccountAlreadyExistsException extends AccountsApplicationException {

    private final AccountsErrorCode code = AccountsErrorCode.ACCOUNT_ALREADY_EXISTS;

    private AccountAlreadyExistsException(String message) {
        super(message);
    }

    public static AccountAlreadyExistsException byName(String name) {
        return new AccountAlreadyExistsException(String.format("Account with name '%s' already exists.", name));
    }

    public static AccountAlreadyExistsException byEmail(String email) {
        return new AccountAlreadyExistsException(String.format("Account with email '%s' already exists.", email));
    }

    public static AccountAlreadyExistsException bySubdomain(String subdomain) {
        return new AccountAlreadyExistsException(String.format("Account with subdomain '%s' already exists.", subdomain));
    }

}
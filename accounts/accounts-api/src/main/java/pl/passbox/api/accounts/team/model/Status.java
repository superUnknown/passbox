package pl.passbox.api.accounts.team.model;

public enum Status {
    /**
     * User added to team before confirming and setting password by confirmation by link; can't login
     */
    UNCONFIRMED,
    /**
     * Owner of account before confirming registration by confirmation by link; can login
     */
    UNVERIFIED,
    /**
     * Active user; can login
     */
    ACTIVE,
    /**
     * Blocked previously active user; can't login
     */
    BLOCKED,
    /**
     * Deleted previously active user; can't login
     */
    DELETED
}
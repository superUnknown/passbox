package pl.passbox.api.accounts.accounts.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccountDto {

    @NotNull
    private UUID id;

    @NotNull
    private Status status;

    @NotBlank
    private String name;

    @Email
    @NotBlank
    private String email;

    @NotBlank
    private String subdomain;

    @NotNull
    private UUID founderId;

    private String description;

    @NotNull
    private Instant created;

    private Instant modified;

    private UUID modifiedById;

    @Valid
    private AddressDto location;

    @Valid
    private SocialChannelsDto socialChannels;

    @Valid
    @NotNull
    private PaymentsSettingsDto paymentsSettings;

}
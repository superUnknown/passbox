package pl.passbox.api.accounts.accounts.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Email;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SocialChannelsDto {

    @Email
    private String email;

    private String phone;

    private String facebookLink;

    private String twitterLink;

    private String instagramLink;

    private String webPageLink;

}
package pl.passbox.api.accounts.credentials.model;

import lombok.Builder;
import lombok.Value;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

@Value
@Builder
public class RequestEmailChangeCommand {

    @Email
    @NotBlank
    private String email;

}
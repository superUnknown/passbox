package pl.passbox.api.accounts.accounts.model;

public enum Status {
    /**
     * Newly created account before confirmation by link.
     */
    UNCONFIRMED,
    /**
     * Active account.
     */
    ACTIVE,
    /**
     * Blocked account.
     */
    BLOCKED,
    /**
     * Deleted account before hard delete is executed.
     */
    DELETED
}
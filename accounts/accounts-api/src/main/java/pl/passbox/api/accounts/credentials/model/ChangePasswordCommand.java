package pl.passbox.api.accounts.credentials.model;

import lombok.Builder;
import lombok.ToString;
import lombok.Value;
import org.hibernate.validator.constraints.NotBlank;
import pl.passbox.commons.validation.EqualProperties;
import pl.passbox.commons.validation.Password;

@Value
@Builder
@EqualProperties({"password1", "password2"})
@ToString(exclude = {"password1", "password2"})
public class ChangePasswordCommand {

    @NotBlank
    @Password
    private String password1;

    @NotBlank
    private String password2;

}
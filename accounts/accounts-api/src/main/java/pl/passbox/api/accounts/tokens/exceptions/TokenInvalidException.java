package pl.passbox.api.accounts.tokens.exceptions;

import lombok.Getter;
import pl.passbox.api.accounts.configuration.AccountsApplicationException;
import pl.passbox.api.accounts.configuration.AccountsErrorCode;

@Getter
public class TokenInvalidException extends AccountsApplicationException {

    private static final String MESSAGE = "Given token is invalid.";

    private final AccountsErrorCode code = AccountsErrorCode.INVALID_TOKEN;

    public TokenInvalidException(Exception cause) {
        super(MESSAGE, cause);
    }

    public TokenInvalidException() {
        super(MESSAGE);
    }

}
package pl.passbox.api.accounts.accounts.exceptions;

import lombok.Getter;
import pl.passbox.api.accounts.configuration.AccountsApplicationException;
import pl.passbox.api.accounts.configuration.AccountsErrorCode;

import java.util.UUID;

@Getter
public class AccountNotFoundException extends AccountsApplicationException {

    private final AccountsErrorCode code = AccountsErrorCode.ACCOUNT_NOT_FOUND;

    private AccountNotFoundException(String message) {
        super(message);
    }

    public static AccountNotFoundException byId(UUID accountId) {
        return new AccountNotFoundException(String.format("Account with id %s not found.", accountId));
    }

}
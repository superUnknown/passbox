package pl.passbox.api.accounts.team.exceptions;

import lombok.Getter;
import pl.passbox.api.accounts.configuration.AccountsApplicationException;
import pl.passbox.api.accounts.configuration.AccountsErrorCode;

import java.util.UUID;

@Getter
public class UserAlreadyExistsException extends AccountsApplicationException {

    private final AccountsErrorCode code;

    private UserAlreadyExistsException(String message, AccountsErrorCode code) {
        super(message);
        this.code = code;
    }

    public static UserAlreadyExistsException byEmail(String email, UUID accountId) {
        return new UserAlreadyExistsException(String.format("User with email '%s' in account with id %s already exists.", email, accountId), AccountsErrorCode.USER_ALREADY_EXISTS);
    }

}
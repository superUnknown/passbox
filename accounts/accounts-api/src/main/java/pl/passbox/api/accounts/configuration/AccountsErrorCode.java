package pl.passbox.api.accounts.configuration;

import lombok.Getter;
import pl.passbox.api.accounts.accounts.exceptions.IllegalAccountStatusException;
import pl.passbox.api.accounts.accounts.exceptions.AccountAlreadyExistsException;
import pl.passbox.api.accounts.accounts.exceptions.AccountNotFoundException;
import pl.passbox.api.accounts.team.exceptions.IllegalUserStatusException;
import pl.passbox.api.accounts.team.exceptions.UserAlreadyExistsException;
import pl.passbox.api.accounts.team.exceptions.UserNotFoundException;
import pl.passbox.api.accounts.tokens.exceptions.TokenInvalidException;
import pl.passbox.commons.configuration.ApplicationException;

@Getter
public enum AccountsErrorCode implements ApplicationException.ErrorCode {
    ACCOUNT_ALREADY_EXISTS(AccountAlreadyExistsException.class),
    ACCOUNT_NOT_FOUND(AccountNotFoundException.class),
    USER_NOT_FOUND(UserNotFoundException.class),
    USER_DELETED(IllegalUserStatusException.class),
    USER_UNCONFIRMED(IllegalUserStatusException.class),
    USER_BLOCKED(IllegalUserStatusException.class),
    USER_UNVERIFIED(IllegalUserStatusException.class),
    ILLEGAL_USER_STATUS(IllegalUserStatusException.class),
    ACCOUNT_DELETED(IllegalAccountStatusException.class),
    ACCOUNT_UNCONFIRMED(IllegalAccountStatusException.class),
    ACCOUNT_BLOCKED(IllegalAccountStatusException.class),
    ILLEGAL_ACCOUNT_STATUS(IllegalAccountStatusException.class),
    USER_ALREADY_EXISTS(UserAlreadyExistsException.class),
    INVALID_TOKEN(TokenInvalidException.class);

    private final Class<? extends AccountsApplicationException> type;

    AccountsErrorCode(Class<? extends AccountsApplicationException> type) {
        this.type = type;
    }

}
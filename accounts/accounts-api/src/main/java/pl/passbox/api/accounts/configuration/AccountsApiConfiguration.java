package pl.passbox.api.accounts.configuration;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.passbox.api.accounts.accounts.AccountsApi;
import pl.passbox.commons.configuration.FeignApplicationErrorDecoder;
import pl.passbox.commons.configuration.FeignClientConfigurationSupport;

@Configuration
@EnableConfigurationProperties(AccountsApiConfiguration.Properties.class)
public class AccountsApiConfiguration {

    @Bean
    public AccountsApi accountsApi(AccountsApiConfiguration.Properties properties, FeignApplicationErrorDecoder feignApplicationErrorDecoder) {
        return FeignClientConfigurationSupport.build(AccountsApi.class, properties.getServiceUrl(), feignApplicationErrorDecoder);
    }

    @Bean
    public FeignApplicationErrorDecoder feignApplicationErrorDecoder() {
        return new FeignApplicationErrorDecoder(AccountsErrorCode.values());
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @ConfigurationProperties("accounts.api")
    public static class Properties {

        @NotBlank
        private String serviceUrl;

    }

}

package pl.passbox.api.accounts.accounts.exceptions;

import lombok.Getter;
import pl.passbox.api.accounts.configuration.AccountsApplicationException;
import pl.passbox.api.accounts.configuration.AccountsErrorCode;
import pl.passbox.api.accounts.accounts.model.Status;

import java.util.UUID;

@Getter
public class IllegalAccountStatusException extends AccountsApplicationException {

    private final AccountsErrorCode code;

    private IllegalAccountStatusException(String message, AccountsErrorCode code) {
        super(message);
        this.code = code;
    }

    public static IllegalAccountStatusException status(UUID accountId, Status status) {
        switch (status) {
            case DELETED:
                return deleted(accountId);
            case UNCONFIRMED:
                return unconfirmed(accountId);
            case BLOCKED:
                return blocked(accountId);
            default:
                return new IllegalAccountStatusException(String.format("Account with id %s has illegal status to execute this operation.", accountId), AccountsErrorCode.ILLEGAL_ACCOUNT_STATUS);
        }
    }

    private static IllegalAccountStatusException deleted(UUID accountId) {
        return new IllegalAccountStatusException(String.format("Account with id %s is deleted.", accountId), AccountsErrorCode.ACCOUNT_DELETED);
    }

    private static IllegalAccountStatusException unconfirmed(UUID accountId) {
        return new IllegalAccountStatusException(String.format("Account with id %s is unconfirmed.", accountId), AccountsErrorCode.ACCOUNT_UNCONFIRMED);
    }

    private static IllegalAccountStatusException blocked(UUID accountId) {
        return new IllegalAccountStatusException(String.format("Account with id %s is blocked.", accountId), AccountsErrorCode.ACCOUNT_BLOCKED);
    }

}
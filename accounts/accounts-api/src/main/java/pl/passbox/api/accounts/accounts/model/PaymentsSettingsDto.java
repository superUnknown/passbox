package pl.passbox.api.accounts.accounts.model;

import com.neovisionaries.i18n.CurrencyCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PaymentsSettingsDto {

    private String bankAccount;

    @NotNull
    private RefundHandling refundHandling;

    @NotNull
    private CurrencyCode currency;

}
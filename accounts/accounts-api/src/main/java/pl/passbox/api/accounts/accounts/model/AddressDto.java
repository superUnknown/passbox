package pl.passbox.api.accounts.accounts.model;

import com.neovisionaries.i18n.CountryCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AddressDto {

    @NotNull
    private CountryCode country;

    @NotBlank
    private String city;

    private String street;

    private String house;

    private String flat;

    private String postalCode;

}
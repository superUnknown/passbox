package pl.passbox.api.accounts.configuration;

import pl.passbox.commons.configuration.ApplicationException;

public abstract class AccountsApplicationException extends ApplicationException {

    public AccountsApplicationException(String message) {
        super(message);
    }

    public AccountsApplicationException(String message, Exception cause) {
        super(message, cause);
    }

    @Override
    public abstract AccountsErrorCode getCode();

}
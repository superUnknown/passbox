package pl.passbox.events.events.model;

import com.neovisionaries.i18n.CurrencyCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Where;
import org.hibernate.validator.constraints.NotBlank;
import pl.passbox.api.events.events.model.types.EventCategory;
import pl.passbox.api.events.events.model.types.EventStatus;
import pl.passbox.api.events.events.model.types.RefundPolicy;
import pl.passbox.api.events.tickets.model.types.TicketClassType;
import pl.passbox.api.payments.charges.model.FeePolicy;
import pl.passbox.events.schedules.model.Schedule;
import pl.passbox.events.tickets.model.TicketClass;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityNotFoundException;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Event {

    @Id
    @Builder.Default
    private UUID id = UUID.randomUUID();

    @NotNull
    private UUID accountId;

    @NotNull
    @Builder.Default
    @Enumerated(EnumType.STRING)
    private EventStatus status = EventStatus.UNPUBLISHED;

    @NotBlank
    private String name;

    private String description;

    @NotNull
    @Enumerated(EnumType.STRING)
    private EventCategory category;

    @NotNull
    @Enumerated(EnumType.STRING)
    private RefundPolicy refundPolicy;

    @NotNull
    @Enumerated(EnumType.STRING)
    private FeePolicy feePolicy;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(updatable = false)
    private CurrencyCode currency;

    @Min(1)
    private Integer totalCapacity;

    @NotNull
    @JoinColumn(name = "location_id")
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private EventLocation location;

    @NotNull
    @JoinColumn(name = "schedule_id")
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private Schedule schedule;

    @Builder.Default
    @JoinColumn(name = "event_id")
    @Where(clause = "status != 'DELETED'")
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<TicketClass> tickets = new ArrayList<>();

    @NotNull
    @Builder.Default
    private Instant created = Instant.now();

    @NotNull
    private UUID createdBy;

    private Instant modified;

    private UUID modifiedBy;

    private byte[] background; // todo research do jakiegoś clouda z obrazkami

    @Transient
    public boolean isPublishPossible() {
        return EventStatus.isPublishPossible(status);
    }

    @Transient
    public boolean isUpdatePossible() {
        return EventStatus.isUpdatePossible(status);
    }

    @Transient
    public boolean isPublished() {
        return EventStatus.PUBLISHED.equals(status);
    }

    @Transient
    public TicketClass getTicket(UUID ticketClassId) {
        return tickets.stream().filter(ticketClass -> ticketClass.getId().equals(ticketClassId)).findAny()
                .orElseThrow(() -> new EntityNotFoundException(String.format("Ticket class with id %s not found.", ticketClassId)));
    }

    @Transient
    public TicketClass getTicket(UUID ticketClassId, TicketClassType type) {
        return tickets.stream().filter(ticket -> ticket.getId().equals(ticketClassId) && ticket.getType().equals(type)).findAny()
                .orElseThrow(() -> new EntityNotFoundException(String.format("Ticket class of type %s with id %s not found.", type, ticketClassId)));
    }

}
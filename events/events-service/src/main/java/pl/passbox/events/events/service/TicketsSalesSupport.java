package pl.passbox.events.events.service;

import pl.passbox.api.events.events.exceptions.EventNotFoundException;
import pl.passbox.events.events.model.TicketsSalesInfoDto;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public interface TicketsSalesSupport {

    TicketsSalesInfoDto getTicketsSalesInfo(@NotNull UUID eventId) throws EventNotFoundException;

}
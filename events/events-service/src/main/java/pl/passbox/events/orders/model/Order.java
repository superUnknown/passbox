package pl.passbox.events.orders.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;
import pl.passbox.api.events.events.model.types.RefundPolicy;
import pl.passbox.api.events.orders.model.OrderStatus;
import pl.passbox.api.payments.charges.model.FeePolicy;
import pl.passbox.events.events.model.Event;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "orders")
public class Order {

    @Id
    @Builder.Default
    private UUID id = UUID.randomUUID();

    @NotNull
    @Builder.Default
    @Enumerated(EnumType.STRING)
    private OrderStatus status = OrderStatus.REQUESTED;

    @NotNull
    @Enumerated(EnumType.STRING)
    private RefundPolicy refundPolicy;

    @NotNull
    @Enumerated(EnumType.STRING)
    private FeePolicy feePolicy;

    @NotNull
    private Instant started;

    @NotNull
    @Builder.Default
    private Instant requested = Instant.now();

    private Instant completed;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "event_id")
    private Event event;

    @NotNull
    @JoinColumn(name = "customer_id")
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private Customer customer;

    @NotEmpty
    @JoinColumn(name = "order_id")
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Ticket> tickets;

}
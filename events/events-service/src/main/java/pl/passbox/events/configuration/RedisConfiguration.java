package pl.passbox.events.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.codec.JsonJacksonCodec;
import org.redisson.config.Config;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Configuration
@EnableConfigurationProperties(RedisConfiguration.Properties.class)
public class RedisConfiguration {

    @Bean
    public RedissonClient redisson(Properties properties, ObjectMapper mapper) {
        Config config = new Config();
        config.setCodec(new JsonJacksonCodec(mapper))
                .useSingleServer()
                .setAddress(String.format("%s:%d", properties.getHost(), properties.getPort()))
                .setTimeout(properties.getTimeoutInMillis())
                .setPassword(properties.getPassword());
        return Redisson.create(config);
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @ConfigurationProperties("events.redis")
    public static class Properties {

        private static final String DEFAULT_HOST = "redis://localhost";

        private static final int DEFAULT_PORT = 6379;

        private static final int DEFAULT_TIMEOUT_IN_MILLIS = 2000;

        @NotBlank
        private String host = DEFAULT_HOST;

        @Min(0)
        @NotNull
        private Integer port = DEFAULT_PORT;

        @Min(0)
        @NotNull
        private Integer timeoutInMillis = DEFAULT_TIMEOUT_IN_MILLIS;

        private String password;

    }

}
package pl.passbox.events.tickets.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import pl.passbox.events.tickets.model.TicketClass;

import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

public interface TicketClassesRepository extends Repository<TicketClass, UUID> {

    @Query("SELECT t FROM TicketClass t WHERE t.status != 'DELETED' AND t.id = :id")
    Optional<TicketClass> findOne(@Param("id") UUID id);

    TicketClass save(TicketClass ticketClass);

    Stream<TicketClass> findAll();

    void delete(TicketClass ticketClass);

    void deleteAll();

}
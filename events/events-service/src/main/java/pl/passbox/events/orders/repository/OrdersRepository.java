package pl.passbox.events.orders.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import pl.passbox.events.events.model.Event;
import pl.passbox.events.orders.model.Order;

import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

public interface OrdersRepository extends Repository<Order, UUID> {

    @Query("SELECT o FROM Order o WHERE o.status in ('REQUESTED', 'PENDING', 'COMPLETED') AND o.id = :id")
    Optional<Order> findOne(@Param("id") UUID id);

    @Query("" +
            "SELECT o FROM Order o " +
            "   JOIN o.event e " +
            "WHERE " +
            "   o.status in ('REQUESTED', 'PENDING', 'COMPLETED') AND " +
            "   e = :event ")
    Stream<Order> findByEvent(@Param("event") Event event);

    Order save(Order event);

    Stream<Order> findAll();

    void delete(Order event);

    void deleteAll();

}
package pl.passbox.events;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import pl.passbox.api.accounts.configuration.AccountsApiConfiguration;
import pl.passbox.api.messages.configuration.MessagesApiConfiguration;
import pl.passbox.commons.configuration.CommonApplicationConfiguration;
import pl.passbox.commons.configuration.DistributedLockConfiguration;

@SpringBootApplication
@Import({
        CommonApplicationConfiguration.class,
        MessagesApiConfiguration.class,
        AccountsApiConfiguration.class,
        DistributedLockConfiguration.class
})
public class EventsApplication {

    public static void main(String[] args) {
        SpringApplication.run(EventsApplication.class, args);
    }

}
package pl.passbox.events.tickets.utils;

import pl.passbox.api.events.schedules.model.ScheduleDto;
import pl.passbox.api.events.tickets.model.CreateTicketClassCommand;
import pl.passbox.api.events.tickets.model.TicketClassDto;
import pl.passbox.events.tickets.model.TicketClass;
import pl.passbox.events.utils.Mapper;

import java.util.stream.Collectors;

public class TicketClassMapper {

    public static TicketClass map(CreateTicketClassCommand src) {
        TicketClass dest;
        switch (src.getType()) {
            case FREE:
                dest = TicketClass.Free.builder().build();
                break;
            case PAID:
                dest = TicketClass.Paid.builder()
                        .price(((CreateTicketClassCommand.Paid) src).getPrice())
                        .build();
                break;
            default:
                throw new RuntimeException(String.format("Unsupported ticket class of type %s.", src.getType()));
        }
        Mapper.map(src, dest);
        dest.getAppliedDates().clear();
        return dest;
    }

    public static TicketClassDto map(TicketClass src) {
        TicketClassDto dest;
        switch (src.getType()) {
            case FREE:
                dest = TicketClassDto.Free.builder().build();
                break;
            case PAID:
                dest = TicketClassDto.Paid.builder()
                        .price(src.getPrice())
                        .build();
                break;
            default:
                throw new RuntimeException(String.format("Unsupported ticket class of type %s.", src.getType()));
        }
        Mapper.map(src, dest);
        dest.setAppliedDates(src.getAppliedDates().stream().map(appliedSchedule -> ScheduleDto.Date.builder()
                .date(appliedSchedule.getDate().getDate())
                .time(appliedSchedule.getDate().getTime())
                .build()
        ).collect(Collectors.toSet()));
        return dest;
    }

}
package pl.passbox.events.tickets.ctrl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.passbox.api.events.events.exceptions.EventNotFoundException;
import pl.passbox.api.events.events.exceptions.IllegalEventStatusException;
import pl.passbox.api.events.tickets.TicketClassesApi;
import pl.passbox.api.events.tickets.exceptions.AppliedDateNotDefinedException;
import pl.passbox.api.events.tickets.exceptions.OrdersForTicketExistsException;
import pl.passbox.api.events.tickets.exceptions.TicketClassNotFoundException;
import pl.passbox.api.events.tickets.exceptions.TicketPriceInvalidException;
import pl.passbox.api.events.tickets.model.ApplyScheduleDatesCommand;
import pl.passbox.api.events.tickets.model.CreateTicketClassCommand;
import pl.passbox.api.events.tickets.model.TicketClassDto;
import pl.passbox.commons.exception.UnauthorizedAccessException;
import pl.passbox.commons.identity.UserIdentityResolver;
import pl.passbox.commons.identity.exception.UserIdentityMissingException;
import pl.passbox.events.tickets.service.TicketClassesService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Slf4j
@RestController
@AllArgsConstructor
public class TicketClassesController implements TicketClassesApi {

    private final HttpServletRequest request;

    private final TicketClassesService ticketClassesService;

    @Override
    public TicketClassDto create(@NotNull @PathVariable(EVENT_ID_VARIABLE) UUID eventId, @Valid @NotNull @RequestBody CreateTicketClassCommand command) throws UserIdentityMissingException, EventNotFoundException, TicketPriceInvalidException, UnauthorizedAccessException, AppliedDateNotDefinedException {
        log.trace("Handling request for ticket class creation in event with id {}: {}.", eventId, command);
        TicketClassDto result = ticketClassesService.create(eventId, command, UserIdentityResolver.resolve(request));
        log.trace("Request for ticket class creation in event with id {} with command {} handled successfully: {}.", eventId, command, result);
        return result;
    }

    @Override
    public TicketClassDto applyScheduleDates(@NotNull @PathVariable(EVENT_ID_VARIABLE) UUID eventId, @NotNull @PathVariable(TICKET_CLASS_ID_VARIABLE) UUID ticketClassId, @Valid @NotNull @RequestBody ApplyScheduleDatesCommand command) throws UserIdentityMissingException, AppliedDateNotDefinedException, IllegalEventStatusException, TicketClassNotFoundException, EventNotFoundException, UnauthorizedAccessException, OrdersForTicketExistsException {
        log.trace("Handling request for applying schedule dates to ticket class with id {} in event with id {}: {}.", ticketClassId, eventId, command);
        TicketClassDto result = ticketClassesService.applyScheduleDates(eventId, ticketClassId, command, UserIdentityResolver.resolve(request));
        log.trace("Request for applying schedule dates to ticket class with id {} in event with id {} with command {} handled successfully: {}.", ticketClassId, eventId, command, result);
        return result;
    }
    
}
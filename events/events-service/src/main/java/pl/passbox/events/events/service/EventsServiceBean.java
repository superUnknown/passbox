package pl.passbox.events.events.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import pl.passbox.api.accounts.accounts.AccountsApi;
import pl.passbox.api.accounts.accounts.exceptions.AccountNotFoundException;
import pl.passbox.api.accounts.accounts.exceptions.IllegalAccountStatusException;
import pl.passbox.api.events.events.exceptions.BankAccountNumberMissingException;
import pl.passbox.api.events.events.exceptions.EventNotFoundException;
import pl.passbox.api.events.events.exceptions.IllegalEventStatusException;
import pl.passbox.api.events.events.model.CreateEventCommand;
import pl.passbox.api.events.events.model.EventDto;
import pl.passbox.api.events.events.model.UpdateEventCommand;
import pl.passbox.api.events.events.model.types.EventStatus;
import pl.passbox.api.events.schedules.model.ScheduleDto;
import pl.passbox.api.events.tickets.exceptions.AppliedDateNotDefinedException;
import pl.passbox.api.events.tickets.exceptions.TicketClassNotFoundException;
import pl.passbox.api.events.tickets.exceptions.TicketPriceInvalidException;
import pl.passbox.api.events.tickets.model.CreateTicketClassCommand;
import pl.passbox.api.messages.MessagesApi;
import pl.passbox.api.messages.model.EventLocationChangedNotificationEmail;
import pl.passbox.api.messages.model.EventLocationChangedNotificationWebSocket;
import pl.passbox.api.messages.model.EventScheduleChangedNotificationEmail;
import pl.passbox.api.messages.model.EventScheduleChangedNotificationWebSocket;
import pl.passbox.commons.exception.UnauthorizedAccessException;
import pl.passbox.commons.identity.model.UserIdentity;
import pl.passbox.commons.locking.model.Lock;
import pl.passbox.commons.locking.model.LockType;
import pl.passbox.commons.locking.service.DistributedLockService;
import pl.passbox.commons.utils.StreamUtils;
import pl.passbox.events.events.model.Event;
import pl.passbox.events.events.model.EventLocation;
import pl.passbox.events.events.model.TicketsSalesInfoDto;
import pl.passbox.events.events.repository.EventsRepository;
import pl.passbox.events.orders.model.Customer;
import pl.passbox.events.orders.model.Order;
import pl.passbox.events.orders.repository.OrdersRepository;
import pl.passbox.events.orders.session.service.OrdersSessionSupport;
import pl.passbox.events.schedules.model.Schedule;
import pl.passbox.events.schedules.utils.ScheduleMapper;
import pl.passbox.events.tickets.service.TicketClassesService;
import pl.passbox.events.utils.Mapper;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
@Validated
@AllArgsConstructor
public class EventsServiceBean implements EventsService {

    private final DistributedLockService distributedLockService;

    private final TicketClassesService ticketClassesService;

    private final OrdersSessionSupport ordersSessionSupport;

    private final TicketsSalesSupport ticketsSalesSupport;

    private final OrdersRepository ordersRepository;

    private final EventsRepository eventsRepository;

    private final MessagesApi messagesApi;

    private final AccountsApi accountsApi;

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = {EventNotFoundException.class, TicketPriceInvalidException.class, AppliedDateNotDefinedException.class, UnauthorizedAccessException.class})
    public EventDto create(UserIdentity identity, CreateEventCommand command) throws AccountNotFoundException, AppliedDateNotDefinedException, TicketPriceInvalidException, EventNotFoundException, UnauthorizedAccessException {
        log.info("Creating event: { command: {}, identity: {} }.", command, identity);
        Assert.isTrue(command.getSchedule().getDates().stream().noneMatch(d -> d.getDate().isBefore(LocalDate.now())), "Schedule dates cannot be past.");

        Event event = eventsRepository.save(Event.builder()
                .currency(accountsApi.get(identity.getAccountId()).getPaymentsSettings().getCurrency())
                .location(Mapper.map(command.getLocation(), EventLocation.builder().build()))
                .schedule(ScheduleMapper.map(command.getSchedule()))
                .totalCapacity(command.getTotalCapacity())
                .refundPolicy(command.getRefundPolicy())
                .description(command.getDescription())
                .accountId(identity.getAccountId())
                .feePolicy(command.getFeePolicy())
                .category(command.getCategory())
                .createdBy(identity.getUserId())
                .name(command.getName())
                .build()
        );
        for (CreateTicketClassCommand ticket : command.getTickets()) {
            ticketClassesService.create(event.getId(), ticket, identity);
        }
        EventDto result = Mapper.map(eventsRepository.save(event), EventDto.class);
        log.info("Event created successfully: {}.", result);
        return result;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public EventDto update(UUID eventId, UpdateEventCommand command, UserIdentity identity) throws EventNotFoundException, UnauthorizedAccessException, TicketPriceInvalidException, TicketClassNotFoundException, IllegalEventStatusException {
        log.info("Updating event with id {}: { command: {}, identity: {} }.", eventId, command, identity);

        try (Lock ignored = distributedLockService.acquireLockOrWait(LockType.EVENT_ACCESS_LOCK, eventId)) {
            Event event = eventsRepository.findOne(eventId).orElseThrow(() -> EventNotFoundException.byId(eventId));
            if (!event.getAccountId().equals(identity.getAccountId())) {
                throw new UnauthorizedAccessException();
            }
            if (!event.isUpdatePossible()) {
                throw IllegalEventStatusException.status(event.getId(), event.getStatus());
            }

            Assert.isTrue(event.getSchedule().getType().equals(command.getSchedule().getType()), "Schedule type cannot differ from current.");
            Assert.isTrue(event.getSchedule().getDates().stream()
                    .map(Schedule.Date::getDate).sorted(Comparator.comparing(d -> d))
                    .collect(Collectors.toSet())
                    .equals(command.getSchedule().getDates().stream()
                            .map(ScheduleDto.Date::getDate).sorted(Comparator.comparing(d -> d))
                            .collect(Collectors.toSet())
                    ), "Schedule days cannot differ from current.");

            if (Optional.ofNullable(command.getTotalCapacity()).map(capacity -> !capacity.equals(event.getTotalCapacity())).orElse(false)) {
                TicketsSalesInfoDto ticketsSalesInfo = ticketsSalesSupport.getTicketsSalesInfo(eventId);
                Assert.isTrue(ticketsSalesInfo.getTotalQuantityPerDate().entrySet().stream().noneMatch(e -> e.getValue() > command.getTotalCapacity()),
                        "Total capacity cannot be less than total sales quantity."
                );
            }

            for (UpdateEventCommand.Ticket ticket : command.getTickets()) {
                ticketClassesService.update(eventId, ticket.getTicketClassId(), ticket.getTicket(), identity);
            }

            sendEventUpdatedNotifications(event, command);
            Mapper.map(command, event);
            event.setModifiedBy(identity.getUserId());
            event.setModified(Instant.now());

            EventDto result = Mapper.map(eventsRepository.save(event), EventDto.class);
            log.info("Event with id {} updated successfully: {}.", eventId, result);
            return result;
        }

    }

    private void sendEventUpdatedNotifications(Event event, UpdateEventCommand command) {
        List<Customer> customers = ordersRepository.findByEvent(event).map(Order::getCustomer)
                .filter(StreamUtils.distinct(Customer::getEmail))
                .collect(Collectors.toList());

        if (!event.getLocation().equals(Mapper.map(command.getLocation(), EventLocation.builder().build()))) {
            log.debug("Event location is changed. Sending email notifications.");
            messagesApi.send(customers.stream().map(customer -> EventLocationChangedNotificationEmail.builder()
                    .location(Mapper.map(command.getLocation(), EventLocationChangedNotificationEmail.Location.class))
                    .customerFirstname(customer.getFirstname())
                    .locale(customer.getLanguage().toLocale())
                    .email(customer.getEmail())
                    .eventName(event.getName())
                    .build()
            ).collect(Collectors.toList()));
            messagesApi.send(Collections.singletonList(EventLocationChangedNotificationWebSocket.builder()
                    .location(Mapper.map(command.getLocation(), EventLocationChangedNotificationWebSocket.Location.class))
                    .eventId(event.getId())
                    .build()
            ));
        }

        if (!event.getSchedule().equals(ScheduleMapper.map(command.getSchedule()))) {
            log.debug("Event schedule is changed. Sending email notifications.");
            messagesApi.send(customers.stream().map(customer -> EventScheduleChangedNotificationEmail.builder()
                    .scheduleDates(command.getSchedule().getDates().stream()
                            .map(ScheduleDto.Date::asDateTime).sorted(Comparator.comparing(d -> d))
                            .collect(Collectors.toList())
                    )
                    .customerFirstname(customer.getFirstname())
                    .locale(customer.getLanguage().toLocale())
                    .email(customer.getEmail())
                    .eventName(event.getName())
                    .build()
            ).collect(Collectors.toList()));
            messagesApi.send(Collections.singletonList(EventScheduleChangedNotificationWebSocket.builder()
                    .scheduleDates(command.getSchedule().getDates().stream()
                            .map(ScheduleDto.Date::asDateTime).sorted(Comparator.comparing(d -> d))
                            .collect(Collectors.toList())
                    )
                    .eventId(event.getId())
                    .build()
            ));
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public EventDto publish(UUID eventId, UserIdentity identity) throws EventNotFoundException, UnauthorizedAccessException, AccountNotFoundException, IllegalAccountStatusException, BankAccountNumberMissingException, IllegalEventStatusException {
        log.info("Publishing event with id {} { identity: {} }.", eventId, identity);

        Event event = eventsRepository.findOne(eventId).orElseThrow(() -> EventNotFoundException.byId(eventId));
        if (!event.isPublishPossible()) {
            throw IllegalEventStatusException.status(event.getId(), event.getStatus());
        }

        if (!event.getAccountId().equals(identity.getAccountId())) {
            throw new UnauthorizedAccessException();
        }

        if (StringUtils.isEmpty(accountsApi.get(identity.getAccountId()).getPaymentsSettings().getBankAccount())) {
            throw new BankAccountNumberMissingException();
        }

        event.setStatus(EventStatus.PUBLISHED);
        event.setModifiedBy(identity.getUserId());
        event.setModified(Instant.now());
        EventDto result = Mapper.map(eventsRepository.save(event), EventDto.class);
        log.info("Event with id {} published successfully: {}.", eventId, result);
        return result;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public EventDto suspend(UUID eventId, UserIdentity identity) throws EventNotFoundException, IllegalEventStatusException, UnauthorizedAccessException, AccountNotFoundException, IllegalAccountStatusException {
        log.info("Suspending event with id {} { identity: {} }.", eventId, identity);

        try (Lock ignored = distributedLockService.acquireLockOrWait(LockType.EVENT_ACCESS_LOCK, eventId)) {
            Event event = eventsRepository.findOne(eventId).orElseThrow(() -> EventNotFoundException.byId(eventId));
            if (!event.isPublished()) {
                throw IllegalEventStatusException.status(event.getId(), event.getStatus());
            }

            if (!event.getAccountId().equals(identity.getAccountId())) {
                throw new UnauthorizedAccessException();
            }

            ordersSessionSupport.clearEventOrderSessions(eventId);

            event.setStatus(EventStatus.SUSPENDED);
            event.setModifiedBy(identity.getUserId());
            event.setModified(Instant.now());
            EventDto result = Mapper.map(eventsRepository.save(event), EventDto.class);
            log.info("Event with id {} suspended successfully: {}.", eventId, result);
            return result;
        }
        
    }

}
package pl.passbox.events.tickets.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import pl.passbox.api.events.events.exceptions.EventNotFoundException;
import pl.passbox.api.events.events.exceptions.IllegalEventStatusException;
import pl.passbox.api.events.schedules.model.ScheduleDto;
import pl.passbox.api.events.tickets.exceptions.AppliedDateNotDefinedException;
import pl.passbox.api.events.tickets.exceptions.OrdersForTicketExistsException;
import pl.passbox.api.events.tickets.exceptions.TicketClassNotFoundException;
import pl.passbox.api.events.tickets.exceptions.TicketPriceInvalidException;
import pl.passbox.api.events.tickets.model.ApplyScheduleDatesCommand;
import pl.passbox.api.events.tickets.model.CreateTicketClassCommand;
import pl.passbox.api.events.tickets.model.TicketAvailabilityResult;
import pl.passbox.api.events.tickets.model.TicketClassDto;
import pl.passbox.api.events.tickets.model.UpdateTicketClassCommand;
import pl.passbox.api.events.tickets.model.types.TicketClassType;
import pl.passbox.commons.exception.UnauthorizedAccessException;
import pl.passbox.commons.identity.model.UserIdentity;
import pl.passbox.commons.locking.model.Lock;
import pl.passbox.commons.locking.model.LockType;
import pl.passbox.commons.locking.service.DistributedLockService;
import pl.passbox.events.configuration.TicketsSpecificationProperties;
import pl.passbox.events.events.model.Event;
import pl.passbox.events.events.model.TicketsSalesInfoDto;
import pl.passbox.events.events.repository.EventsRepository;
import pl.passbox.events.events.service.TicketsSalesSupport;
import pl.passbox.events.orders.session.service.OrdersSessionSupport;
import pl.passbox.events.tickets.model.CheckTicketAvailabilityCommand;
import pl.passbox.events.tickets.model.TicketClass;
import pl.passbox.events.tickets.utils.TicketClassMapper;
import pl.passbox.events.utils.Mapper;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
@Validated
@AllArgsConstructor
public class TicketClassesServiceBean implements TicketClassesService {

    private final TicketsSpecificationProperties ticketsSpecificationProperties;

    private final DistributedLockService distributedLockService;

    private final OrdersSessionSupport ordersSessionSupport;

    private final TicketsSalesSupport ticketsSalesSupport;

    private final EventsRepository eventsRepository;

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public TicketClassDto create(UUID eventId, CreateTicketClassCommand command, UserIdentity identity) throws EventNotFoundException, TicketPriceInvalidException, AppliedDateNotDefinedException, UnauthorizedAccessException {
        log.info("Creating ticket class for event with id {} (identity: {}): {}.", eventId, identity, command);

        if (command.getType().equals(TicketClassType.PAID) && ticketsSpecificationProperties.getMinTicketPrice().compareTo(((CreateTicketClassCommand.Paid) command).getPrice()) > 0) {
            throw TicketPriceInvalidException.gtMin(command.getName(), ((CreateTicketClassCommand.Paid) command).getPrice(), ticketsSpecificationProperties.getMinTicketPrice());
        }

        Event event = eventsRepository.findOne(eventId).orElseThrow(() -> EventNotFoundException.byId(eventId));
        if (!event.getAccountId().equals(identity.getAccountId())) {
            throw new UnauthorizedAccessException();
        }

        TicketClass ticket = TicketClassMapper.map(command);
        ticket.setCreatedBy(identity.getUserId());
        ticket.setCreated(Instant.now());
        applyScheduleDates(event, ticket, command.getAppliedDates());

        event.getTickets().add(ticket);
        eventsRepository.save(event);

        TicketClassDto result = TicketClassMapper.map(ticket);
        log.info("Ticket class for event with id {} created successfully: {}.", eventId, result);
        return result;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public TicketClassDto update(UUID eventId, UUID ticketClassId, UpdateTicketClassCommand command, UserIdentity identity) throws TicketClassNotFoundException, EventNotFoundException, UnauthorizedAccessException, TicketPriceInvalidException, IllegalEventStatusException {
        log.info("Updating ticket class with id {} for event with id {} (identity: {}): {}.", ticketClassId, eventId, identity, command);

        try (Lock ignored = distributedLockService.acquireLockOrWait(LockType.EVENT_ACCESS_LOCK, eventId)) {
            Event event = eventsRepository.findOne(eventId).orElseThrow(() -> EventNotFoundException.byId(eventId));
            if (!event.getAccountId().equals(identity.getAccountId())) {
                throw new UnauthorizedAccessException();
            }
            if (!event.isUpdatePossible()) {
                throw IllegalEventStatusException.status(event.getId(), event.getStatus());
            }

            TicketClass ticketClass;
            try {
                ticketClass = event.getTicket(ticketClassId, command.getType());
            } catch (EntityNotFoundException e) {
                log.trace(e.getMessage(), e);
                throw TicketClassNotFoundException.byIdAndType(ticketClassId, command.getType());
            }

            if (command.getType().equals(TicketClassType.PAID)) {
                BigDecimal givenPrice = ((UpdateTicketClassCommand.Paid) command).getPrice();
                if (!givenPrice.equals(ticketClass.getPrice()) && ticketsSpecificationProperties.getMinTicketPrice().compareTo(givenPrice) > 0) {
                    throw TicketPriceInvalidException.gtMin(command.getName(), givenPrice, ticketsSpecificationProperties.getMinTicketPrice());
                }
            }

            if (Optional.ofNullable(command.getQuantity()).map(quantity -> !quantity.equals(ticketClass.getQuantity())).orElse(false)) {
                TicketsSalesInfoDto ticketsSalesInfo = ticketsSalesSupport.getTicketsSalesInfo(event.getId());
                Assert.isTrue(ticketsSalesInfo.getTotalQuantityForTicket(ticketClassId) <= command.getQuantity(),
                        "Ticket quantity cannot be less than its total sales quantity."
                );
            }

            ticketClass.setModifiedBy(identity.getUserId());
            ticketClass.setModified(Instant.now());
            Mapper.map(command, ticketClass);
            eventsRepository.save(event);

            TicketClassDto result = TicketClassMapper.map(ticketClass);
            log.info("Ticket class with id {} for event with id {} updated successfully: {}.", ticketClassId, eventId, result);
            return result;
        }

    }

    @Override
    @Transactional(readOnly = true)
    public TicketAvailabilityResult checkTicketAvailability(CheckTicketAvailabilityCommand command) throws EventNotFoundException, TicketClassNotFoundException {
        log.debug("Checking ticket availability: {}.", command);

        try (Lock ignored = distributedLockService.acquireLockOrWait(LockType.EVENT_ACCESS_LOCK, command.getEventId())) {
            Event event = eventsRepository.findOne(command.getEventId()).orElseThrow(() -> EventNotFoundException.byId(command.getEventId()));

            TicketClass ticket;
            try {
                ticket = event.getTicket(command.getTicketClassId());
            } catch (EntityNotFoundException e) {
                log.trace(e.getMessage(), e);
                throw TicketClassNotFoundException.byId(command.getTicketClassId());
            }

            if (isQuantityOutOfTicketLimits(ticket, command.getQuantity())) {
                log.debug("Ticket not available (given quantity out of defined limits).");
                return TicketAvailabilityResult.TICKET_QUANTITY_OUT_OF_DEFINED_LIMITS;
            }

            if (!isTicketSaleStarted(ticket)) {
                log.debug("Ticket not available (sell hasn't started).");
                return TicketAvailabilityResult.TICKET_SALE_NOT_STARTED;
            }

            if (isTicketSaleEnded(ticket)) {
                log.debug("Ticket not available (sell ended).");
                return TicketAvailabilityResult.TICKET_SALE_ENDED;
            }

            TicketsSalesInfoDto ticketsSalesInfo = ticketsSalesSupport.getTicketsSalesInfo(event.getId());
            log.debug("Checking if ticket sales reached limits: {}", ticketsSalesInfo);

            if (Optional.ofNullable(ticket.getQuantity()).map(quantity -> ticketsSalesInfo.getTotalQuantityForTicket(ticket.getId()) + command.getQuantity() > quantity).orElse(false)) {
                log.debug("Ticket not available (total ticket quantity reached).");
                return TicketAvailabilityResult.TOTAL_TICKET_QUANTITY_REACHED;
            }
            if (Optional.ofNullable(event.getTotalCapacity()).map(totalCapacity -> ticketsSalesInfo.getTotalQuantityPerDate().entrySet().stream().anyMatch(e -> e.getValue() + command.getQuantity() > totalCapacity)).orElse(false)) {
                log.debug("Ticket not available (total event capacity reached).");
                return TicketAvailabilityResult.TOTAL_EVENT_CAPACITY_REACHED;
            }

            log.debug("Ticket available.");

            return TicketAvailabilityResult.AVAILABLE;
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public TicketClassDto applyScheduleDates(UUID eventId, UUID ticketClassId, ApplyScheduleDatesCommand command, UserIdentity identity) throws EventNotFoundException, UnauthorizedAccessException, TicketClassNotFoundException, IllegalEventStatusException, OrdersForTicketExistsException, AppliedDateNotDefinedException {
        log.info("Applying schedule dates to ticket class with id {} in event with id {} (identity: {}): {}.", ticketClassId, eventId, identity, command);

        try (Lock ignored = distributedLockService.acquireLockOrWait(LockType.EVENT_ACCESS_LOCK, eventId)) {
            Event event = eventsRepository.findOne(eventId).orElseThrow(() -> EventNotFoundException.byId(eventId));
            if (!event.getAccountId().equals(identity.getAccountId())) {
                throw new UnauthorizedAccessException();
            }
            if (!event.isUpdatePossible()) {
                throw IllegalEventStatusException.status(event.getId(), event.getStatus());
            }

            TicketClass ticket;
            try {
                ticket = event.getTicket(ticketClassId);
            } catch (EntityNotFoundException e) {
                log.trace(e.getMessage(), e);
                throw TicketClassNotFoundException.byId(ticketClassId);
            }

            if (ticket.getAppliedDates().stream().map(d -> d.getDate().asDateTime()).collect(Collectors.toSet()).equals(command.getDates().stream().map(ScheduleDto.Date::asDateTime).collect(Collectors.toSet()))) {
                log.info("Given schedule dates are equal to current. Ignoring.");
                return TicketClassMapper.map(ticket);
            }

            if (ticketsSalesSupport.getTicketsSalesInfo(event.getId()).getSoldQuantityForTicket(ticketClassId) > 0) {
                throw new OrdersForTicketExistsException(ticketClassId);
            }

            applyScheduleDates(event, ticket, command.getDates());
            ticket.setModifiedBy(identity.getUserId());
            ticket.setModified(Instant.now());
            eventsRepository.save(event);

            ordersSessionSupport.clearEventOrderSessions(eventId);

            TicketClassDto result = TicketClassMapper.map(ticket);
            log.info("Applied schedule dates to ticket class with id {} in event with id {} successfully: {}.", ticketClassId, eventId, result);
            return result;
        }

    }

    private void applyScheduleDates(Event event, TicketClass ticket, Set<ScheduleDto.Date> dates) throws AppliedDateNotDefinedException {
        ticket.getAppliedDates().clear();
        for (ScheduleDto.Date date : dates) {
            ticket.getAppliedDates().add(TicketClass.AppliedDate.builder()
                    .date(event.getSchedule().getDates().stream().filter(d -> d.asDateTime().equals(date.asDateTime())).findFirst()
                            .orElseThrow(() -> new AppliedDateNotDefinedException(date.asDateTime(), ticket.getId()))
                    )
                    .ticket(ticket)
                    .build()
            );
        }
    }

    private boolean isQuantityOutOfTicketLimits(TicketClass ticket, Integer quantity) {
        log.debug("Checking ticket order limits: { min: {}, max: {}, quantity: {} }", ticket.getMinPerOrder(), ticket.getMaxPerOrder(), quantity);
        return Optional.ofNullable(ticket.getMinPerOrder()).orElse(Integer.MIN_VALUE) > quantity || Optional.ofNullable(ticket.getMaxPerOrder()).orElse(Integer.MAX_VALUE) < quantity;
    }

    private boolean isTicketSaleStarted(TicketClass ticket) {
        log.debug("Checking if ticket sell started: { saleStart: {} }", ticket.getSaleStart());
        return !Optional.ofNullable(ticket.getSaleStart()).orElse(Instant.MIN).isAfter(Instant.now());
    }

    private boolean isTicketSaleEnded(TicketClass ticket) {
        log.debug("Checking if ticket sell ended: { saleEnd: {} }", ticket.getSaleEnd());
        return !Optional.ofNullable(ticket.getSaleEnd()).orElse(Instant.MAX).isAfter(Instant.now());
    }

}
package pl.passbox.events.orders.service;

import pl.passbox.api.events.events.exceptions.EventNotFoundException;
import pl.passbox.api.events.events.exceptions.IllegalEventStatusException;
import pl.passbox.api.events.orders.exception.OrderSessionNotFoundException;
import pl.passbox.api.events.orders.model.CheckoutOrderCommand;
import pl.passbox.api.events.orders.model.ConfirmOrderCommand;
import pl.passbox.api.events.orders.model.ConfirmOrderResponseDto;
import pl.passbox.api.events.orders.model.OrderSessionDto;
import pl.passbox.api.events.tickets.exceptions.TicketClassNotFoundException;
import pl.passbox.api.events.tickets.exceptions.TicketNotAvailableException;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public interface OrdersService {

    OrderSessionDto checkout(@NotNull @Valid CheckoutOrderCommand command) throws EventNotFoundException, IllegalEventStatusException, TicketClassNotFoundException, TicketNotAvailableException;

    ConfirmOrderResponseDto confirm(@NotNull @Valid ConfirmOrderCommand command) throws OrderSessionNotFoundException, EventNotFoundException, IllegalEventStatusException;

}
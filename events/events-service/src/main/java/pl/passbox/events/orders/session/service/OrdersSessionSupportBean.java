package pl.passbox.events.orders.session.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RMapCache;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import pl.passbox.api.events.orders.exception.OrderSessionNotFoundException;
import pl.passbox.api.events.orders.model.OrderSessionDto;
import pl.passbox.api.messages.MessagesApi;
import pl.passbox.api.messages.model.OrderSessionInvalidatedWebSocket;
import pl.passbox.events.configuration.OrdersConfiguration;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Slf4j
@Service
@Validated
@AllArgsConstructor
public class OrdersSessionSupportBean implements OrdersSessionSupport {

    private static final String ORDERS_SESSIONS_REGISTRY_KEY_PATTERN = "%s_%s";

    @Qualifier(OrdersConfiguration.ORDERS_SESSIONS_REGISTRY_NAME)
    private final RMapCache<String, OrderSessionDto> ordersSessionsRegistry;

    private final OrdersConfiguration.Properties properties;

    private final MessagesApi messagesApi;

    @Override
    public OrderSessionDto get(UUID sessionId) throws OrderSessionNotFoundException {
        log.debug("Getting session with id {}.", sessionId);
        OrderSessionDto result = findOne(sessionId).orElseThrow(() -> OrderSessionNotFoundException.byId(sessionId));
        log.debug("Retrieved session with id {} successfully: {}.", result);
        return result;
    }

    @Override
    public OrderSessionDto create(OrderSessionDto session) {
        log.debug("Creating order session for event [ ttl: {} sec ]: {}.", properties.getSession().getTtlInSeconds(), session);
        save(session);
        log.debug("Order session for event created successfully: {}.", session);
        return session;
    }

    @Override
    public void refresh(UUID sessionId) throws OrderSessionNotFoundException {
        log.debug("Refreshing order session with id {}.", sessionId);
        save(findOne(sessionId).orElseThrow(() -> OrderSessionNotFoundException.byId(sessionId)));
        log.debug("Order session with id {} refreshed successfully.", sessionId);
    }

    @Override
    public void remove(UUID sessionId) throws OrderSessionNotFoundException {
        log.debug("Removing order session with id {}.", sessionId);
        remove(findOne(sessionId).orElseThrow(() -> OrderSessionNotFoundException.byId(sessionId)).getEventId(), Collections.singletonList(sessionId));
        log.debug("Order session with id {} removed successfully.", sessionId);
    }

    @Override
    public void clearEventOrderSessions(UUID eventId) {
        log.debug("Clearing order sessions for event with id {}.", eventId);

        List<UUID> sessions = findByEventId(eventId).stream().map(OrderSessionDto::getId).collect(Collectors.toList());
        if (sessions.isEmpty()) {
            log.debug("No order sessions for event with id {}. Ignoring.", eventId);
            return;
        }
        log.debug("Retrieved {} order sessions for event with id {}: {}", sessions.size(), eventId, sessions);

        messagesApi.send(sessions.stream().map(sessionId -> OrderSessionInvalidatedWebSocket.builder()
                .reason(OrderSessionInvalidatedWebSocket.Reason.EVENT_SUSPENDED)
                .sessionId(sessionId)
                .build()
        ).collect(Collectors.toList()));
        log.debug("Order session invalidated web sockets sent.");

        remove(eventId, sessions);
        log.debug("Order sessions for event with id {} cleared successfully.", eventId);
    }

    @Override
    public List<OrderSessionDto> findByEventId(UUID eventId) {
        log.debug("Getting order sessions for event with id {}.", eventId);
        List<OrderSessionDto> sessions = ordersSessionsRegistry.keySet().stream()
                .filter(key -> key.matches(assembleKey(eventId.toString(), ".*")))
                .map(ordersSessionsRegistry::get)
                .collect(Collectors.toList());
        log.debug("Order sessions for event with id {} retrieved successfully ({}).", eventId, sessions.size());
        return sessions;
    }

    private Optional<OrderSessionDto> findOne(UUID sessionId) {
        return ordersSessionsRegistry.keySet().stream()
                .filter(key -> key.matches(assembleKey(".*", sessionId.toString())))
                .findFirst().map(ordersSessionsRegistry::get);
    }

    private void remove(UUID eventId, List<UUID> sessionIds) {
        sessionIds.forEach(sessionId -> ordersSessionsRegistry
                .remove(assembleKey(eventId.toString(), sessionId.toString()))
        );
    }

    private void save(OrderSessionDto session) {
        ordersSessionsRegistry.put(assembleKey(session.getEventId().toString(), session.getId().toString()), session, properties.getSession().getTtlInSeconds(), TimeUnit.SECONDS);
    }

    private String assembleKey(String eventId, String sessionId) {
        return String.format(ORDERS_SESSIONS_REGISTRY_KEY_PATTERN, eventId, sessionId);
    }

}
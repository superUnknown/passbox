package pl.passbox.events.schedules.utils;

import pl.passbox.api.events.schedules.model.ScheduleDto;
import pl.passbox.events.schedules.model.Schedule;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class ScheduleMapper {

    public static Schedule map(ScheduleDto src) {
        if (Objects.isNull(src)) {
            return null;
        }
        List<Schedule.Date> dates = src.getDates().stream().map(d -> Schedule.Date.builder()
                .date(d.getDate())
                .time(d.getTime())
                .build()
        ).collect(Collectors.toList());
        switch (src.getType()) {
            case SINGLE_DAY:
                return Schedule.SingleDay.builder().dates(dates).build();
            case MULTI_DAY:
                return Schedule.MultiDay.builder().dates(dates).build();
            default:
                throw new RuntimeException(String.format("Unsupported schedule of type %s.", src.getType()));
        }
    }

    public static Schedule map(ScheduleDto src, Schedule dest) {
        if (Objects.isNull(src)) {
            return dest;
        }

        if (Objects.isNull(dest)) {
            return map(src);
        }

        dest.getDates().forEach(date -> date.setTime(src.getDates().stream()
                .filter(d -> d.getDate().equals(date.getDate()))
                .findAny().get().getTime()
        ));

        return dest;
    }

    public static ScheduleDto map(Schedule src) {
        if (Objects.isNull(src)) {
            return null;
        }
        Set<ScheduleDto.Date> dates = src.getDates().stream().map(d -> ScheduleDto.Date.builder()
                .date(d.getDate())
                .time(d.getTime())
                .build()
        ).collect(Collectors.toSet());
        switch (src.getType()) {
            case SINGLE_DAY:
                return ScheduleDto.SingleDay.builder().dates(dates).build();
            case MULTI_DAY:
                return ScheduleDto.MultiDay.builder().dates(dates).build();
            default:
                throw new RuntimeException(String.format("Unsupported schedule of type %s.", src.getType()));
        }
    }

}
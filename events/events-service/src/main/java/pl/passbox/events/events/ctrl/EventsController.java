package pl.passbox.events.events.ctrl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.passbox.api.accounts.accounts.exceptions.AccountNotFoundException;
import pl.passbox.api.accounts.accounts.exceptions.IllegalAccountStatusException;
import pl.passbox.api.events.events.EventsApi;
import pl.passbox.api.events.events.exceptions.BankAccountNumberMissingException;
import pl.passbox.api.events.events.exceptions.EventNotFoundException;
import pl.passbox.api.events.events.exceptions.IllegalEventStatusException;
import pl.passbox.api.events.events.model.CreateEventCommand;
import pl.passbox.api.events.events.model.EventDto;
import pl.passbox.api.events.events.model.UpdateEventCommand;
import pl.passbox.api.events.tickets.exceptions.AppliedDateNotDefinedException;
import pl.passbox.api.events.tickets.exceptions.TicketClassNotFoundException;
import pl.passbox.api.events.tickets.exceptions.TicketPriceInvalidException;
import pl.passbox.commons.exception.UnauthorizedAccessException;
import pl.passbox.commons.identity.UserIdentityResolver;
import pl.passbox.commons.identity.exception.UserIdentityMissingException;
import pl.passbox.events.events.service.EventsService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Slf4j
@RestController
@AllArgsConstructor
public class EventsController implements EventsApi {

    private final HttpServletRequest request;

    private final EventsService eventsService;

    @Override
    public EventDto create(@Valid @NotNull @RequestBody CreateEventCommand command) throws UserIdentityMissingException, AccountNotFoundException, AppliedDateNotDefinedException, TicketPriceInvalidException, EventNotFoundException, UnauthorizedAccessException {
        log.trace("Handling request for event creation: {}.", command);
        EventDto result = eventsService.create(UserIdentityResolver.resolve(request), command);
        log.trace("Request for event creation with command {} handled successfully: {}.", command, result);
        return result;
    }

    @Override
    public EventDto update(@NotNull @PathVariable(EVENT_ID_VARIABLE) UUID eventId, @Valid @NotNull @RequestBody UpdateEventCommand command) throws UserIdentityMissingException, EventNotFoundException, UnauthorizedAccessException, TicketPriceInvalidException, TicketClassNotFoundException, IllegalEventStatusException {
        log.trace("Handling request for update event with id {}: {}.", eventId, command);
        EventDto result = eventsService.update(eventId, command, UserIdentityResolver.resolve(request));
        log.trace("Request for update event with id {} with command {} handled successfully: {}.", eventId, command, result);
        return result;
    }

    @Override
    public EventDto publish(@NotNull @PathVariable(EVENT_ID_VARIABLE) UUID eventId) throws UserIdentityMissingException, EventNotFoundException, AccountNotFoundException, BankAccountNumberMissingException, UnauthorizedAccessException, IllegalAccountStatusException, IllegalEventStatusException {
        log.trace("Handling request for publish event with id {}.", eventId);
        EventDto result = eventsService.publish(eventId, UserIdentityResolver.resolve(request));
        log.trace("Request for publish event with id {} handled successfully: {}.", eventId, result);
        return result;
    }

    @Override
    public EventDto suspend(@NotNull @PathVariable(EVENT_ID_VARIABLE) UUID eventId) throws UserIdentityMissingException, EventNotFoundException, AccountNotFoundException, UnauthorizedAccessException, IllegalAccountStatusException, IllegalEventStatusException {
        log.trace("Handling request for suspend event with id {}.", eventId);
        EventDto result = eventsService.suspend(eventId, UserIdentityResolver.resolve(request));
        log.trace("Request for suspend event with id {} handled successfully: {}.", eventId, result);
        return result;
    }

}
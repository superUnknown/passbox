package pl.passbox.events.orders.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import pl.passbox.api.events.events.exceptions.EventNotFoundException;
import pl.passbox.api.events.events.exceptions.IllegalEventStatusException;
import pl.passbox.api.events.orders.exception.OrderSessionNotFoundException;
import pl.passbox.api.events.orders.model.CheckoutOrderCommand;
import pl.passbox.api.events.orders.model.ConfirmOrderCommand;
import pl.passbox.api.events.orders.model.ConfirmOrderResponseDto;
import pl.passbox.api.events.orders.model.OrderSessionDto;
import pl.passbox.api.events.tickets.exceptions.TicketClassNotFoundException;
import pl.passbox.api.events.tickets.exceptions.TicketNotAvailableException;
import pl.passbox.api.events.tickets.model.TicketAvailabilityResult;
import pl.passbox.api.payments.charges.ChargesApi;
import pl.passbox.api.payments.charges.model.EstimateChargeCommand;
import pl.passbox.api.payments.payments.PaymentsApi;
import pl.passbox.api.payments.payments.model.RequestPaymentCommand;
import pl.passbox.api.payments.payments.model.RequestPaymentResponseDto;
import pl.passbox.commons.locking.model.Lock;
import pl.passbox.commons.locking.model.LockType;
import pl.passbox.commons.locking.service.DistributedLockService;
import pl.passbox.events.events.model.Event;
import pl.passbox.events.events.repository.EventsRepository;
import pl.passbox.events.orders.model.Attendee;
import pl.passbox.events.orders.model.Customer;
import pl.passbox.events.orders.model.Order;
import pl.passbox.events.orders.model.Ticket;
import pl.passbox.events.orders.repository.OrdersRepository;
import pl.passbox.events.orders.session.service.OrdersSessionSupport;
import pl.passbox.events.tickets.model.CheckTicketAvailabilityCommand;
import pl.passbox.events.tickets.model.TicketClass;
import pl.passbox.events.tickets.service.TicketClassesService;
import pl.passbox.events.utils.Mapper;

import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
@Validated
@AllArgsConstructor
public class OrdersServiceBean implements OrdersService {

    private final DistributedLockService distributedLockService;

    private final OrdersSessionSupport ordersSessionSupport;

    private final TicketClassesService ticketClassesService;

    private final OrdersRepository ordersRepository;

    private final EventsRepository eventsRepository;

    private final PaymentsApi paymentsApi;

    private final ChargesApi chargesApi;

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public OrderSessionDto checkout(CheckoutOrderCommand command) throws EventNotFoundException, IllegalEventStatusException, TicketClassNotFoundException, TicketNotAvailableException {
        log.info("Handling order checkout: {}.", command);

        try (Lock ignored = distributedLockService.acquireLockOrWait(LockType.EVENT_ACCESS_LOCK, command.getEventId())) {
            Event event = eventsRepository.findOne(command.getEventId()).orElseThrow(() -> EventNotFoundException.byId(command.getEventId()));
            if (!event.isPublished()) {
                throw IllegalEventStatusException.status(command.getEventId(), event.getStatus());
            }
            for (CheckoutOrderCommand.Ticket ticket : command.getTickets()) {
                TicketAvailabilityResult availability = ticketClassesService.checkTicketAvailability(CheckTicketAvailabilityCommand.builder()
                        .ticketClassId(ticket.getTicketClassId())
                        .quantity(ticket.getQuantity())
                        .eventId(event.getId())
                        .build()
                );
                if (!availability.equals(TicketAvailabilityResult.AVAILABLE)) {
                    throw TicketNotAvailableException.byResult(ticket.getTicketClassId(), availability);
                }
            }

            OrderSessionDto session = ordersSessionSupport.create(OrderSessionDto.builder()
                    .charge(chargesApi.estimate(EstimateChargeCommand.builder()
                            .tickets(command.getTickets().stream().map(t -> {
                                        TicketClass ticketClass = event.getTicket(t.getTicketClassId());
                                        return EstimateChargeCommand.Ticket.builder()
                                                .ticketClassId(t.getTicketClassId())
                                                .ticketName(ticketClass.getName())
                                                .unitPrice(ticketClass.getPrice())
                                                .quantity(t.getQuantity())
                                                .build();
                                    }).collect(Collectors.toList())
                            )
                            .feePolicy(event.getFeePolicy())
                            .currency(event.getCurrency())
                            .build()
                    ))
                    .tickets(command.getTickets().stream().map(t -> Mapper.map(t, OrderSessionDto.Ticket.class)).collect(Collectors.toList()))
                    .eventId(event.getId())
                    .build()
            );

            log.info("Order checkout for command {} handled successfully: {}.", command, session);
            return session;
        }

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public ConfirmOrderResponseDto confirm(ConfirmOrderCommand command) throws OrderSessionNotFoundException, EventNotFoundException, IllegalEventStatusException {
        log.info("Confirming order: {}.", command);

        try (Lock ignored = distributedLockService.acquireLockOrWait(LockType.EVENT_ACCESS_LOCK, command.getEventId())) {
            OrderSessionDto session = ordersSessionSupport.get(command.getSessionId());
            Assert.isTrue(session.getEventId().equals(command.getEventId()), "Given event id doesn't match with session's event id.");
            Assert.isTrue(confirmedTicketsMatchWithCheckedOut(command, session), "Requested tickets doesn't match with checkout tickets.");

            Event event = eventsRepository.findOne(session.getEventId()).orElseThrow(() -> EventNotFoundException.byId(session.getEventId()));
            log.debug("Resolved event id from session: {}.", event.getId());
            if (!event.isPublished()) {
                ordersSessionSupport.remove(session.getId());
                throw IllegalEventStatusException.status(event.getId(), event.getStatus());
            }


            Order order = ordersRepository.save(Order.builder()
                    .tickets(command.getTickets().stream().map(t -> Ticket.builder()
                                    .attendee(Mapper.map(t.getAttendee(), Attendee.builder().build()))
                                    .ticketClass(event.getTicket(t.getTicketClassId()))
                                    .build()
                            ).collect(Collectors.toList())
                    )
                    .customer(Mapper.map(command.getCustomer(), Customer.builder().build()))
                    .refundPolicy(event.getRefundPolicy())
                    .feePolicy(event.getFeePolicy())
                    .started(session.getStarted())
                    .event(event)
                    .build()
            );

            RequestPaymentResponseDto payment = paymentsApi.request(RequestPaymentCommand.builder()
                    .customer(Mapper.map(order.getCustomer(), RequestPaymentCommand.Customer.class))
                    .customerIp(command.getCustomerIp())
                    .charge(session.getCharge())
                    .orderId(order.getId())
                    .build()
            );

            ordersSessionSupport.remove(session.getId());

            ConfirmOrderResponseDto result = ConfirmOrderResponseDto.builder()
                    .paymentUrl(payment.getPaymentUrl())
                    .orderId(order.getId())
                    .build();
            log.info("Order for event with id {} confirmed successfully: {}.", event.getId(), result);

            return result;
        }

    }

    private boolean confirmedTicketsMatchWithCheckedOut(ConfirmOrderCommand command, OrderSessionDto session) {
        Map<UUID, Long> checkedOut = session.getTickets().stream().collect(Collectors
                .groupingBy(OrderSessionDto.Ticket::getTicketClassId, Collectors.summingLong(OrderSessionDto.Ticket::getQuantity))
        );
        Map<UUID, Long> confirmed = command.getTickets().stream().collect(Collectors
                .groupingBy(ConfirmOrderCommand.Ticket::getTicketClassId, Collectors.counting())
        );
        return checkedOut.equals(confirmed);
    }

}
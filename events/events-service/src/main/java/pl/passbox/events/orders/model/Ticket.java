package pl.passbox.events.orders.model;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;
import pl.passbox.api.events.tickets.model.types.TicketStatus;
import pl.passbox.events.tickets.model.TicketClass;
import pl.passbox.events.tickets.utils.TicketNumberSupport;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
public class Ticket {

    @Id
    @Builder.Default
    private UUID id = UUID.randomUUID();

    @NotNull
    @Builder.Default
    @Enumerated(EnumType.STRING)
    private TicketStatus status = TicketStatus.REQUESTED;

    @NotBlank
    private String number;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "ticket_class_id")
    private TicketClass ticketClass;

    @NotNull
    @JoinColumn(name = "attendee_id")
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private Attendee attendee;

    @JoinColumn(name = "checkin_id")
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private Ticket.CheckIn checkIn;

    @Builder
    public Ticket(TicketClass ticketClass, Attendee attendee) {
        this.number = TicketNumberSupport.generate(this.id);
        this.ticketClass = ticketClass;
        this.attendee = attendee;
    }

    @Getter
    @Setter
    @Entity
    @NoArgsConstructor
    @Table(name = "ticket_checkins")
    public static class CheckIn {

        @Id
        @Builder.Default
        private UUID id = UUID.randomUUID();

        @NotNull
        @Builder.Default
        private Instant datetime = Instant.now();

        @NotNull
        private UUID checkedBy;

    }

}
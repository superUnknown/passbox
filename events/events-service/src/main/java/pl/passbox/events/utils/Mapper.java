package pl.passbox.events.utils;

import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.convention.MatchingStrategies;
import org.modelmapper.spi.MappingContext;
import org.modelmapper.spi.MatchingStrategy;
import org.springframework.util.CollectionUtils;
import pl.passbox.api.events.events.model.UpdateEventCommand;
import pl.passbox.api.events.schedules.model.ScheduleDto;
import pl.passbox.api.events.tickets.model.TicketClassDto;
import pl.passbox.events.events.model.Event;
import pl.passbox.events.schedules.model.Schedule;
import pl.passbox.events.schedules.utils.ScheduleMapper;
import pl.passbox.events.tickets.model.TicketClass;
import pl.passbox.events.tickets.utils.TicketClassMapper;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Mapper {

    private static final ModelMapper INSTANCE;

    private static final MatchingStrategy STRATEGY = MatchingStrategies.STRICT;

    private static final PropertyMap<UpdateEventCommand, Event> UPDATE_EVENT_TO_ENTITY_MAPPING = new PropertyMap<UpdateEventCommand, Event>() {
        @Override
        protected void configure() {
            skip().setTickets(null);
        }
    };

    private static final Converter<Schedule, ScheduleDto> SCHEDULE_TO_DTO_CONVERTER = new AbstractConverter<Schedule, ScheduleDto>() {
        @Override
        protected ScheduleDto convert(Schedule source) {
            return ScheduleMapper.map(source);
        }
    };

    private static final Converter<ScheduleDto, Schedule> SCHEDULE_TO_ENTITY_CONVERTER = new Converter<ScheduleDto, Schedule>() {
        @Override
        public Schedule convert(MappingContext<ScheduleDto, Schedule> ctx) {
            return ScheduleMapper.map(ctx.getSource(), ctx.getDestination());
        }
    };

    private static final Converter<List<TicketClass>, List<TicketClassDto>> TICKET_CLASSES_TO_DTO_CONVERTER = new AbstractConverter<List<TicketClass>, List<TicketClassDto>>() {
        @Override
        public List<TicketClassDto> convert(List<TicketClass> source) {
            if (CollectionUtils.isEmpty(source)) {
                return Collections.emptyList();
            }
            return source.stream().map(TicketClassMapper::map).collect(Collectors.toList());
        }
    };

    static {
        INSTANCE = new ModelMapper();
        INSTANCE.getConfiguration().setMatchingStrategy(STRATEGY);
        INSTANCE.addConverter(TICKET_CLASSES_TO_DTO_CONVERTER);
        INSTANCE.addConverter(SCHEDULE_TO_ENTITY_CONVERTER);
        INSTANCE.addConverter(SCHEDULE_TO_DTO_CONVERTER);
        INSTANCE.addMappings(UPDATE_EVENT_TO_ENTITY_MAPPING);
    }

    public static <D> D map(Object src, Class<D> clazz) {
        return INSTANCE.map(src, clazz);
    }

    public static <D> D map(Object src, D dest) {
        INSTANCE.map(src, dest);
        return dest;
    }

}
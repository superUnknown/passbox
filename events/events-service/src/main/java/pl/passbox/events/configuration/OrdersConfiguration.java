package pl.passbox.events.configuration;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.redisson.api.RMapCache;
import org.redisson.api.RedissonClient;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.passbox.api.events.orders.model.OrderSessionDto;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Configuration
@EnableConfigurationProperties(OrdersConfiguration.Properties.class)
public class OrdersConfiguration {

    public static final String ORDERS_SESSIONS_REGISTRY_NAME = "events.orders.orders_sessions_registry";

    @Bean(ORDERS_SESSIONS_REGISTRY_NAME)
    public RMapCache<String, OrderSessionDto> ordersSessionsRegistry(RedissonClient redisson) {
        return redisson.getMapCache(ORDERS_SESSIONS_REGISTRY_NAME);
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @ConfigurationProperties("events.orders")
    public static class Properties {

        @Valid
        @NotNull
        private Session session;

        @Data
        @Builder
        @NoArgsConstructor
        @AllArgsConstructor
        public static class Session {

            @Min(1)
            @Max(3600)
            @NotNull
            private Long ttlInSeconds;

        }

    }

}
package pl.passbox.events.tickets.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import pl.passbox.events.events.model.Event;
import pl.passbox.events.orders.model.Ticket;
import pl.passbox.events.tickets.model.TicketClass;

import java.util.UUID;

public interface TicketsRepository extends Repository<Ticket, UUID> {

    @Query("" +
            "SELECT count(t) FROM Order o " +
            "   JOIN o.event e " +
            "   JOIN o.tickets t " +
            "   JOIN t.ticketClass c " +
            "WHERE " +
            "   o.status IN ('REQUESTED', 'PENDING', 'COMPLETED') AND " +
            "   t.status IN ('REQUESTED', 'AVAILABLE', 'CHECKED_IN') AND " +
            "   c = :ticketClass" +
            "")
    Long getTicketSalesQuantity(@Param("ticketClass") TicketClass ticketClass);

}
package pl.passbox.events.tickets.model;

import lombok.Builder;
import lombok.Value;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Value
@Builder
public class CheckTicketAvailabilityCommand {

    @NotNull
    private UUID eventId;

    @NotNull
    private UUID ticketClassId;

    @Min(1)
    @NotNull
    private Integer quantity;

}
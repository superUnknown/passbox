package pl.passbox.events.events.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import pl.passbox.events.events.model.Event;

import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

public interface EventsRepository extends Repository<Event, UUID> {

    @Query("SELECT e FROM Event e WHERE e.status != 'DELETED' AND e.id = :id")
    Optional<Event> findOne(@Param("id") UUID id);

    @Query("" +
            "SELECT e FROM Event e " +
            "   JOIN e.tickets t " +
            "WHERE " +
            "   e.status != 'DELETED' AND " +
            "   t.id = :ticketClassId"
    )
    Optional<Event> findByTicketClassId(@Param("ticketClassId") UUID ticketClassId);

    Event save(Event event);

    Stream<Event> findAll();

    void delete(Event event);

    void deleteAll();

}
package pl.passbox.events.orders.ctrl;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.passbox.api.events.events.exceptions.EventNotFoundException;
import pl.passbox.api.events.events.exceptions.IllegalEventStatusException;
import pl.passbox.api.events.orders.OrdersApi;
import pl.passbox.api.events.orders.exception.OrderSessionNotFoundException;
import pl.passbox.api.events.orders.model.CheckoutOrderCommand;
import pl.passbox.api.events.orders.model.ConfirmOrderCommand;
import pl.passbox.api.events.orders.model.ConfirmOrderResponseDto;
import pl.passbox.api.events.orders.model.OrderSessionDto;
import pl.passbox.api.events.schedules.exceptions.ScheduleNotFoundException;
import pl.passbox.api.events.tickets.exceptions.TicketClassNotFoundException;
import pl.passbox.api.events.tickets.exceptions.TicketNotAvailableException;
import pl.passbox.commons.configuration.ApplicationHeader;
import pl.passbox.commons.utils.ApplicationHeaderResolver;
import pl.passbox.commons.utils.LanguageSupport;
import pl.passbox.events.orders.service.OrdersService;
import pl.passbox.events.orders.session.service.OrdersSessionSupport;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Slf4j
@RestController
@AllArgsConstructor
public class OrdersController implements OrdersApi {

    private final OrdersSessionSupport ordersSessionSupport;

    private final OrdersService ordersService;

    private final HttpServletRequest request;

    @Override
    public OrderSessionDto checkout(@NotNull @Valid @RequestBody CheckoutOrderCommand command) throws IllegalEventStatusException, EventNotFoundException, ScheduleNotFoundException, TicketNotAvailableException, TicketClassNotFoundException {
        log.trace("Handling request for order checkout: {}.", command);
        OrderSessionDto result = ordersService.checkout(command);
        log.trace("Request for order checkout with command {} handled successfully: {}.", command, result);
        return result;
    }

    @Override
    public ConfirmOrderResponseDto confirm(@NotNull @Valid @RequestBody ConfirmOrderCommand command) throws EventNotFoundException, IllegalEventStatusException, OrderSessionNotFoundException {
        log.trace("Handling request for order confirmation: {}.", command);
        command.getCustomer().setLanguage(LanguageSupport.resolve(command.getCustomer().getLanguage(), request));
        command.setCustomerIp(ApplicationHeaderResolver.resolve(request, ApplicationHeader.X_FORWARDED_FOR)
                .orElseThrow(() -> new RuntimeException(String.format("Application header '%s' is missing.", ApplicationHeader.X_FORWARDED_FOR)))
        );
        ConfirmOrderResponseDto result = ordersService.confirm(command);
        log.trace("Request for order confirmation with command {} handled successfully: {}.", command, result);
        return result;
    }

    @Override
    public void refreshSession(@NotNull @PathVariable(SESSION_ID_VARIABLE) UUID sessionId) throws OrderSessionNotFoundException {
        log.trace("Handling request for refresh order session with id {}.", sessionId);
        ordersSessionSupport.refresh(sessionId);
        log.trace("Request for refresh order session with id {} handled successfully.", sessionId);
    }

    @Override
    public void removeSession(@NotNull @PathVariable(SESSION_ID_VARIABLE) UUID sessionId) throws OrderSessionNotFoundException {
        log.trace("Handling request for remove order session with id {}.", sessionId);
        ordersSessionSupport.remove(sessionId);
        log.trace("Request for remove order session with id {} handled successfully.", sessionId);
    }

}
package pl.passbox.events.tickets.utils;

import java.util.UUID;

public class TicketNumberSupport {

    public static String generate(UUID uuid) {
        return uuid.toString().split("-")[0];
    }

}
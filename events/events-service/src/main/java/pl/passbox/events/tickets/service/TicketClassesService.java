package pl.passbox.events.tickets.service;

import pl.passbox.api.events.events.exceptions.EventNotFoundException;
import pl.passbox.api.events.events.exceptions.IllegalEventStatusException;
import pl.passbox.api.events.tickets.exceptions.AppliedDateNotDefinedException;
import pl.passbox.api.events.tickets.exceptions.OrdersForTicketExistsException;
import pl.passbox.api.events.tickets.exceptions.TicketClassNotFoundException;
import pl.passbox.api.events.tickets.exceptions.TicketPriceInvalidException;
import pl.passbox.api.events.tickets.model.ApplyScheduleDatesCommand;
import pl.passbox.api.events.tickets.model.CreateTicketClassCommand;
import pl.passbox.api.events.tickets.model.TicketAvailabilityResult;
import pl.passbox.api.events.tickets.model.TicketClassDto;
import pl.passbox.api.events.tickets.model.UpdateTicketClassCommand;
import pl.passbox.commons.exception.UnauthorizedAccessException;
import pl.passbox.commons.identity.model.UserIdentity;
import pl.passbox.events.tickets.model.CheckTicketAvailabilityCommand;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

public interface TicketClassesService {

    TicketClassDto create(@NotNull UUID eventId, @NotNull @Valid CreateTicketClassCommand command, @NotNull @Valid UserIdentity identity) throws EventNotFoundException, TicketPriceInvalidException, AppliedDateNotDefinedException, UnauthorizedAccessException;

    TicketClassDto update(@NotNull UUID eventId, @NotNull UUID ticketClassId, @Valid @NotNull UpdateTicketClassCommand command, @NotNull @Valid UserIdentity identity) throws TicketClassNotFoundException, EventNotFoundException, UnauthorizedAccessException, TicketPriceInvalidException, IllegalEventStatusException;

    TicketAvailabilityResult checkTicketAvailability(@NotNull @Valid CheckTicketAvailabilityCommand command) throws EventNotFoundException, TicketClassNotFoundException;

    TicketClassDto applyScheduleDates(@NotNull UUID eventId, @NotNull UUID ticketClassId, @NotNull @Valid ApplyScheduleDatesCommand command, @NotNull @Valid UserIdentity identity) throws EventNotFoundException, UnauthorizedAccessException, TicketClassNotFoundException, IllegalEventStatusException, OrdersForTicketExistsException, AppliedDateNotDefinedException;

}
package pl.passbox.events.schedules.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DiscriminatorOptions;
import org.hibernate.validator.constraints.NotEmpty;
import pl.passbox.api.events.schedules.model.ScheduleDateStatus;
import pl.passbox.api.events.schedules.model.ScheduleType;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Getter
@Setter
@Entity
@Inheritance
@NoArgsConstructor
@DiscriminatorColumn(name = "type")
@DiscriminatorOptions(force = true)
public abstract class Schedule {

    @Id
    @Builder.Default
    private UUID id = UUID.randomUUID();

    public abstract ScheduleType getType();

    public abstract List<Schedule.Date> getDates();

    public abstract void setDates(List<Schedule.Date> dates);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Schedule)) return false;
        Schedule that = (Schedule) o;
        return this.getType().equals(that.getType()) &&
                this.getDates().stream()
                        .map(Schedule.Date::asDateTime).sorted(Comparator.comparing(d -> d))
                        .collect(Collectors.toSet()).equals(that.getDates().stream()
                        .map(Schedule.Date::asDateTime).sorted(Comparator.comparing(d -> d))
                        .collect(Collectors.toSet()));
    }

    @Getter
    @Setter
    @Entity
    @NoArgsConstructor
    @DiscriminatorValue("SINGLE_DAY")
    public static class SingleDay extends Schedule {

        @Enumerated(EnumType.STRING)
        @Column(insertable = false, updatable = false)
        private final ScheduleType type = ScheduleType.SINGLE_DAY;

        @NotEmpty
        @Size(min = 1, max = 1)
        @JoinColumn(name = "schedule_id")
        @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
        private List<Schedule.Date> dates = new ArrayList<>();

        @Builder
        public SingleDay(List<Date> dates) {
            this.dates = dates;
        }

    }

    @Getter
    @Setter
    @Entity
    @NoArgsConstructor
    @DiscriminatorValue("MULTI_DAY")
    public static class MultiDay extends Schedule {

        @Enumerated(EnumType.STRING)
        @Column(insertable = false, updatable = false)
        private final ScheduleType type = ScheduleType.MULTI_DAY;

        @NotEmpty
        @Size(min = 2)
        @JoinColumn(name = "schedule_id")
        @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
        private List<Schedule.Date> dates = new ArrayList<>();

        @Builder
        public MultiDay(List<Date> dates) {
            this.dates = dates;
        }

    }

    @Getter
    @Setter
    @Entity
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @Table(name = "schedule_date")
    public static class Date {

        @Id
        @Builder.Default
        private UUID id = UUID.randomUUID();

        @NotNull
        @Builder.Default
        @Enumerated(EnumType.STRING)
        private ScheduleDateStatus status = ScheduleDateStatus.ACTIVE;

        @NotNull
        private LocalDate date;

        @NotNull
        private LocalTime time;

        @Transient
        public LocalDateTime asDateTime() {
            return date.atTime(time).truncatedTo(ChronoUnit.MINUTES);
        }

    }

    public boolean overlapsWith(Schedule other) {
        LinkedList<LocalDate> thisSorted = this.getDates().stream().map(Date::getDate)
                .sorted(Comparator.comparing(d -> d))
                .collect(Collectors.toCollection(LinkedList::new));
        LinkedList<LocalDate> otherSorted = other.getDates().stream().map(Date::getDate)
                .sorted(Comparator.comparing(d -> d))
                .collect(Collectors.toCollection(LinkedList::new));

        LocalDateTime thisFrom = thisSorted.getFirst().atTime(LocalTime.MIN);
        LocalDateTime thisTo = thisSorted.getLast().atTime(LocalTime.MAX);
        LocalDateTime otherFrom = otherSorted.getFirst().atTime(LocalTime.MIN);
        LocalDateTime otherTo = otherSorted.getLast().atTime(LocalTime.MAX);
        return (thisFrom.isBefore(otherTo) || thisFrom.equals(otherTo)) && (thisTo.isAfter(otherFrom) || thisTo.equals(otherFrom));
    }

}
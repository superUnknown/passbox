package pl.passbox.events.orders.session.service;

import pl.passbox.api.events.orders.exception.OrderSessionNotFoundException;
import pl.passbox.api.events.orders.model.OrderSessionDto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

public interface OrdersSessionSupport {

    OrderSessionDto get(@NotNull @Valid UUID sessionId) throws OrderSessionNotFoundException;

    OrderSessionDto create(@NotNull @Valid OrderSessionDto command);

    void refresh(@NotNull UUID sessionId) throws OrderSessionNotFoundException;

    void remove(@NotNull UUID sessionId) throws OrderSessionNotFoundException;

    void clearEventOrderSessions(@NotNull UUID eventId);

    List<OrderSessionDto> findByEventId(@NotNull UUID eventId);

}
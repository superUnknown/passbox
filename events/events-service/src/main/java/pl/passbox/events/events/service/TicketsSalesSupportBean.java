package pl.passbox.events.events.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import pl.passbox.api.events.events.exceptions.EventNotFoundException;
import pl.passbox.api.events.orders.model.OrderSessionDto;
import pl.passbox.api.events.schedules.model.ScheduleDto;
import pl.passbox.events.events.model.Event;
import pl.passbox.events.events.model.TicketsSalesInfoDto;
import pl.passbox.events.events.repository.EventsRepository;
import pl.passbox.events.orders.session.service.OrdersSessionSupport;
import pl.passbox.events.tickets.repository.TicketsRepository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
@Validated
@AllArgsConstructor
public class TicketsSalesSupportBean implements TicketsSalesSupport {

    private final OrdersSessionSupport ordersSessionSupport;

    private final TicketsRepository ticketsRepository;

    private final EventsRepository eventsRepository;

    @Override
    @Transactional(readOnly = true)
    public TicketsSalesInfoDto getTicketsSalesInfo(UUID eventId) throws EventNotFoundException {
        log.debug("Getting tickets sales information for event with id {}.", eventId);
        Event event = eventsRepository.findOne(eventId).orElseThrow(() -> EventNotFoundException.byId(eventId));
        List<OrderSessionDto> pendingSessions = ordersSessionSupport.findByEventId(event.getId());
        TicketsSalesInfoDto result = TicketsSalesInfoDto.builder()
                .tickets(event.getTickets().stream().map(ticket -> TicketsSalesInfoDto.Ticket.builder()
                        .dates(ticket.getAppliedDates().stream().map(d -> ScheduleDto.Date.fromDateTime(d.getDate().asDateTime())).collect(Collectors.toSet()))
                        .ticketClassId(ticket.getId())
                        .sold(ticketsRepository.getTicketSalesQuantity(ticket))
                        .pending(getPendingOrdersQuantity(pendingSessions, ticket.getId()))
                        .build()
                ).collect(Collectors.toList()))
                .build();
        log.debug("Tickets sales information for event with id {} retrieved successfully: {}.", eventId, result);
        return result;
    }

    private Long getPendingOrdersQuantity(List<OrderSessionDto> sessions, UUID ticketClassId) {
        return sessions.stream().map(OrderSessionDto::getTickets).flatMap(List::stream)
                .filter(t -> t.getTicketClassId().equals(ticketClassId))
                .mapToLong(OrderSessionDto.Ticket::getQuantity).sum();
    }

}
package pl.passbox.events.tickets.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DiscriminatorOptions;
import org.hibernate.annotations.Where;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import pl.passbox.api.events.tickets.model.types.TicketClassStatus;
import pl.passbox.api.events.tickets.model.types.TicketClassType;
import pl.passbox.events.schedules.model.Schedule;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity
@Inheritance
@NoArgsConstructor
@DiscriminatorColumn(name = "type")
@DiscriminatorOptions(force = true)
public abstract class TicketClass {

    @Id
    @Builder.Default
    private UUID id = UUID.randomUUID();

    @NotNull
    @Enumerated(EnumType.STRING)
    private TicketClassStatus status = TicketClassStatus.ACTIVE;

    @NotBlank
    private String name;

    private String description;

    @Min(1)
    private Integer quantity;

    @Min(1)
    private Integer minPerOrder;

    @Min(1)
    private Integer maxPerOrder;

    private Instant saleStart;

    private Instant saleEnd;

    @NotEmpty
    @OneToMany(mappedBy = "id.ticket", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<AppliedDate> appliedDates = new ArrayList<>();

    @NotNull
    private Instant created;

    @NotNull
    private UUID createdBy;

    private Instant modified;

    private UUID modifiedBy;

    public TicketClass(String name, String description, Integer quantity, Integer minPerOrder, Integer maxPerOrder, Instant saleStart, Instant saleEnd, Instant created, UUID createdBy) {
        this.name = name;
        this.description = description;
        this.quantity = quantity;
        this.minPerOrder = minPerOrder;
        this.maxPerOrder = maxPerOrder;
        this.saleStart = saleStart;
        this.saleEnd = saleEnd;
        this.created = created;
        this.createdBy = createdBy;
    }

    public abstract TicketClassType getType();

    public abstract BigDecimal getPrice();

    @Getter
    @Setter
    @Entity
    @NoArgsConstructor
    @DiscriminatorValue("FREE")
    public static class Free extends TicketClass {

        @Enumerated(EnumType.STRING)
        @Column(insertable = false, updatable = false)
        private final TicketClassType type = TicketClassType.FREE;

        @Builder
        public Free(String name, String description, Integer quantity, Integer minPerOrder, Integer maxPerOrder, Instant saleStart, Instant saleEnd, Instant created, UUID createdBy) {
            super(name, description, quantity, minPerOrder, maxPerOrder, saleStart, saleEnd, created, createdBy);
        }

        @Override
        @Transient
        public BigDecimal getPrice() {
            return BigDecimal.ZERO;
        }

    }

    @Getter
    @Setter
    @Entity
    @NoArgsConstructor
    @DiscriminatorValue("PAID")
    public static class Paid extends TicketClass {

        @Enumerated(EnumType.STRING)
        @Column(insertable = false, updatable = false)
        private final TicketClassType type = TicketClassType.PAID;

        @NotNull
        @DecimalMin("0.01")
        private BigDecimal price;

        @Builder
        public Paid(String name, String description, Integer quantity, Integer minPerOrder, Integer maxPerOrder, Instant saleStart, Instant saleEnd, Instant created, UUID createdBy, BigDecimal price) {
            super(name, description, quantity, minPerOrder, maxPerOrder, saleStart, saleEnd, created, createdBy);
            this.price = price;
        }

    }

    @Getter
    @Setter
    @Entity
    @NoArgsConstructor
    @Table(name = "ticket_applied_dates")
    public static class AppliedDate {

        @EmbeddedId
        private AppliedDate.Id id;

        @Builder
        public AppliedDate(TicketClass ticket, Schedule.Date date) {
            this.id = new Id(ticket, date);
        }

        @Transient
        public Schedule.Date getDate() {
            return id.date;
        }

        @Getter
        @Setter
        @Builder
        @Embeddable
        @NoArgsConstructor
        @AllArgsConstructor
        public static class Id implements Serializable {

            @NotNull
            @ManyToOne
            @JoinColumn(name = "ticket_class_id")
            private TicketClass ticket;

            @NotNull
            @ManyToOne
            @JoinColumn(name = "date_id")
            private Schedule.Date date;

        }

    }

}
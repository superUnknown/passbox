package pl.passbox.events.events.model;

import lombok.Builder;
import lombok.Value;
import org.hibernate.validator.constraints.NotEmpty;
import pl.passbox.api.events.schedules.model.ScheduleDto;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Value
@Builder
public class TicketsSalesInfoDto {

    @Valid
    @NotEmpty
    private List<Ticket> tickets;

    public Map<ScheduleDto.Date, Long> getTotalQuantityPerDate() {
        Map<ScheduleDto.Date, Long> result = new HashMap<>();
        tickets.forEach(ticket -> ticket.getDates().forEach(date ->
                result.put(date, Optional.ofNullable(result.get(date)).orElse(0L) + ticket.getSold() + ticket.getPending())
        ));
        return result;
    }

    public Long getTotalQuantityForTicket(UUID ticketClassId) {
        return tickets.stream().filter(t -> t.getTicketClassId().equals(ticketClassId))
                .mapToLong(t -> t.getSold() + t.getPending())
                .sum();
    }

    public Long getSoldQuantityForTicket(UUID ticketClassId) {
        return tickets.stream().filter(t -> t.getTicketClassId().equals(ticketClassId))
                .mapToLong(Ticket::getSold)
                .sum();
    }

    @Value
    @Builder
    public static class Ticket {

        @NotNull
        private UUID ticketClassId;

        @Valid
        @NotEmpty
        private Set<ScheduleDto.Date> dates;

        @Min(0)
        @NotNull
        private Long sold;

        @Min(0)
        @NotNull
        private Long pending;

    }

}
package pl.passbox.events.events.service;

import pl.passbox.api.accounts.accounts.exceptions.AccountNotFoundException;
import pl.passbox.api.accounts.accounts.exceptions.IllegalAccountStatusException;
import pl.passbox.api.events.events.exceptions.BankAccountNumberMissingException;
import pl.passbox.api.events.events.exceptions.EventNotFoundException;
import pl.passbox.api.events.events.exceptions.IllegalEventStatusException;
import pl.passbox.api.events.events.model.CreateEventCommand;
import pl.passbox.api.events.events.model.EventDto;
import pl.passbox.api.events.events.model.UpdateEventCommand;
import pl.passbox.api.events.tickets.exceptions.AppliedDateNotDefinedException;
import pl.passbox.api.events.tickets.exceptions.TicketClassNotFoundException;
import pl.passbox.api.events.tickets.exceptions.TicketPriceInvalidException;
import pl.passbox.commons.exception.UnauthorizedAccessException;
import pl.passbox.commons.identity.model.UserIdentity;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

public interface EventsService {

    EventDto create(@NotNull @Valid UserIdentity identity, @NotNull @Valid CreateEventCommand command) throws AccountNotFoundException, AppliedDateNotDefinedException, TicketPriceInvalidException, EventNotFoundException, UnauthorizedAccessException;

    EventDto update(@NotNull UUID eventId, @NotNull @Valid UpdateEventCommand command, @NotNull @Valid UserIdentity identity) throws EventNotFoundException, UnauthorizedAccessException, TicketPriceInvalidException, TicketClassNotFoundException, IllegalEventStatusException;

    EventDto publish(@NotNull UUID eventId, @NotNull @Valid UserIdentity identity) throws EventNotFoundException, UnauthorizedAccessException, AccountNotFoundException, IllegalAccountStatusException, BankAccountNumberMissingException, IllegalEventStatusException;

    EventDto suspend(@NotNull UUID eventId, @NotNull @Valid UserIdentity identity) throws EventNotFoundException, IllegalEventStatusException, UnauthorizedAccessException, AccountNotFoundException, IllegalAccountStatusException;

}
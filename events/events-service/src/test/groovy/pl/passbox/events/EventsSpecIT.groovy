package pl.passbox.events

import com.fasterxml.jackson.databind.ObjectMapper
import com.neovisionaries.i18n.CountryCode
import com.neovisionaries.i18n.LanguageCode
import org.redisson.api.RMapCache
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.mock.web.MockHttpServletResponse
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder
import pl.passbox.api.events.events.EventsApi
import pl.passbox.api.events.events.model.CreateEventCommand
import pl.passbox.api.events.events.model.EventDto
import pl.passbox.api.events.events.model.EventLocationDto
import pl.passbox.api.events.events.model.UpdateEventCommand
import pl.passbox.api.events.events.model.types.EventCategory
import pl.passbox.api.events.events.model.types.RefundPolicy
import pl.passbox.api.events.orders.OrdersApi
import pl.passbox.api.events.orders.model.AttendeeDto
import pl.passbox.api.events.orders.model.CheckoutOrderCommand
import pl.passbox.api.events.orders.model.ConfirmOrderCommand
import pl.passbox.api.events.orders.model.ConfirmOrderResponseDto
import pl.passbox.api.events.orders.model.OrderSessionDto
import pl.passbox.api.events.schedules.model.ScheduleDto
import pl.passbox.api.events.tickets.TicketClassesApi
import pl.passbox.api.events.tickets.model.ApplyScheduleDatesCommand
import pl.passbox.api.events.tickets.model.CreateTicketClassCommand
import pl.passbox.api.events.tickets.model.UpdateTicketClassCommand
import pl.passbox.api.payments.charges.model.FeePolicy
import pl.passbox.commons.configuration.ApplicationHeader
import pl.passbox.commons.identity.model.UserIdentity
import pl.passbox.events.configuration.OrdersConfiguration
import pl.passbox.events.events.repository.EventsRepository
import pl.passbox.events.events.service.TicketsSalesSupport
import pl.passbox.events.orders.repository.OrdersRepository
import pl.passbox.events.tickets.repository.TicketClassesRepository
import redis.embedded.RedisServer
import spock.lang.Specification

import java.time.Instant
import java.time.LocalDateTime
import java.time.Year

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = RANDOM_PORT, classes = [MessagesApiMockConfiguration, AccountsApiMockConfiguration, PaymentsApiMockConfiguration])
abstract class EventsSpecIT extends Specification {

	static RedisServer redisServer = new RedisServer(6379)

	@Autowired
	protected PaymentsApiMockConfiguration.PaymentRequestsRegistry paymentRequestsRegistry

	@Autowired
	protected MessagesApiMockConfiguration.MessagesRegistry messagesRegistry

	@Autowired
	@Qualifier(OrdersConfiguration.ORDERS_SESSIONS_REGISTRY_NAME)
	protected RMapCache<String, OrderSessionDto> ordersSessionsRegistry

	@Autowired
	protected TicketClassesRepository ticketClassesRepository

	@Autowired
	protected TicketsSalesSupport ticketsSalesSupport

	@Autowired
	protected EventsRepository eventsRepository

	@Autowired
	protected OrdersRepository ordersRepository

	@Autowired
	protected ObjectMapper mapper

	@Autowired
	protected MockMvc mockMvc

	def setupSpec() {
		if (!redisServer.isActive()) {
			redisServer.start()
		}
	}

	def cleanupSpec() {
		if (redisServer.isActive()) {
			redisServer.stop()
		}
	}

	def setup() {
		clean()
	}

	def cleanup() {
		clean()
	}

	void clean() {
		paymentRequestsRegistry.clear()
		ordersSessionsRegistry.clear()
		messagesRegistry.clean()
		ordersRepository.deleteAll()
	}

	protected static boolean isCloseBeforeNow(Instant time) {
		def diff = Instant.now().toEpochMilli() - time.toEpochMilli()
		diff >= 0 && diff <= 5000
	}

	protected <T> T createEvent(Class<T> clazz, UserIdentity identity, CreateEventCommand command) {
		MockHttpServletRequestBuilder builder = post("${EventsApi.EVENTS_PATH}")
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.content(mapper.writeValueAsString(command))
		if (identity != null) {
			HttpHeaders headers = new HttpHeaders()
			headers.put(ApplicationHeader.USER_IDENTITY.value, [identity.encode()])
			builder.headers(headers)
		}
		return mapper.readValue(mockMvc.perform(builder).andReturn().getResponse().getContentAsString(), clazz)
	}

	protected EventDto createAndPublishEvent(UserIdentity identity, CreateEventCommand command) {
		return publishEvent(EventDto, identity, createEvent(EventDto, identity, command).id)
	}

	protected <T> T updateEvent(Class<T> clazz, UserIdentity identity, UUID eventId, String name, String description, EventCategory category, RefundPolicy refundPolicy, FeePolicy feePolicy, ScheduleDto schedule, Integer totalCapacity = null, List<UpdateEventCommand.Ticket> tickets = [], EventLocationDto location = assembleEventLocationDto()) {
		UpdateEventCommand command = UpdateEventCommand.builder()
				.totalCapacity(totalCapacity)
				.refundPolicy(refundPolicy)
				.description(description)
				.feePolicy(feePolicy)
				.tickets(tickets)
				.category(category)
				.schedule(schedule)
				.location(location)
				.name(name)
				.build()
		MockHttpServletRequestBuilder builder = put("${EventsApi.EVENT_PATH}", eventId)
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.content(mapper.writeValueAsString(command))
		if (identity != null) {
			HttpHeaders headers = new HttpHeaders()
			headers.put(ApplicationHeader.USER_IDENTITY.value, [identity.encode()])
			builder.headers(headers)
		}
		return mapper.readValue(mockMvc.perform(builder).andReturn().getResponse().getContentAsString(), clazz)
	}

	protected <T> T publishEvent(Class<T> clazz, UserIdentity identity, UUID eventId) {
		MockHttpServletRequestBuilder builder = patch("${EventsApi.PUBLISH_EVENT_PATH}", eventId)
		if (identity != null) {
			HttpHeaders headers = new HttpHeaders()
			headers.put(ApplicationHeader.USER_IDENTITY.value, [identity.encode()])
			builder.headers(headers)
		}
		return mapper.readValue(mockMvc.perform(builder).andReturn().getResponse().getContentAsString(), clazz)
	}

	protected <T> T suspendEvent(Class<T> clazz, UserIdentity identity, UUID eventId) {
		MockHttpServletRequestBuilder builder = patch("${EventsApi.SUSPEND_EVENT_PATH}", eventId)
		if (identity != null) {
			HttpHeaders headers = new HttpHeaders()
			headers.put(ApplicationHeader.USER_IDENTITY.value, [identity.encode()])
			builder.headers(headers)
		}
		return mapper.readValue(mockMvc.perform(builder).andReturn().getResponse().getContentAsString(), clazz)
	}

	protected <T> T checkoutOrder(Class<T> clazz, UUID eventId, List<CheckoutOrderCommand.Ticket> tickets) {
		CheckoutOrderCommand command = CheckoutOrderCommand.builder()
				.tickets(tickets)
				.eventId(eventId)
				.build()
		return mapper.readValue(mockMvc.perform(post("${OrdersApi.CHECKOUT_ORDER_PATH}")
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.content(mapper.writeValueAsString(command))
		).andReturn().getResponse().getContentAsString(), clazz)
	}

	protected <T> T confirmOrder(Class<T> clazz, String customerIp, Locale locale, ConfirmOrderCommand command) {
		return mapper.readValue(mockMvc.perform(post("${OrdersApi.CONFIRM_ORDER_PATH}")
				.headers(headers([(ApplicationHeader.X_FORWARDED_FOR.value): customerIp]))
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.content(mapper.writeValueAsString(command))
				.locale(locale as Locale)
		).andReturn().getResponse().getContentAsString(), clazz)
	}

	protected ConfirmOrderResponseDto checkoutAndConfirmOrder(UUID eventId, List<CheckoutOrderCommand.Ticket> checkoutTickets, ConfirmOrderCommand.Customer customer = new ConfirmOrderCommand.Customer("customer@dev.null", "customer@dev.null", "Jan", "Nowak", null)) {
		OrderSessionDto session = checkoutOrder(OrderSessionDto, eventId, checkoutTickets)
		List<ConfirmOrderCommand.Ticket> tickets = []
		checkoutTickets.each {
			def i = it.quantity
			while (i > 0) {
				tickets.add(ConfirmOrderCommand.Ticket.builder()
						.attendee(new AttendeeDto("attendee@dev.null", "Jan", "Nowak"))
						.ticketClassId(it.ticketClassId)
						.build()
				)
				i--
			}
		}
		return confirmOrder(ConfirmOrderResponseDto, "127.0.0.1", LanguageCode.pl.toLocale(), ConfirmOrderCommand.builder()
				.sessionId(session.id)
				.customer(customer)
				.eventId(eventId)
				.tickets(tickets)
				.build()
		)
	}

	protected <T> T refreshOrderSession(Class<T> clazz, UUID sessionId) {
		MockHttpServletResponse response = mockMvc.perform(put("${OrdersApi.ORDER_SESSION_PATH}", sessionId))
				.andReturn().getResponse()
		return clazz == Void ? null : mapper.readValue(response.getContentAsString(), clazz)
	}

	protected <T> T removeOrderSession(Class<T> clazz, UUID sessionId) {
		MockHttpServletResponse response = mockMvc.perform(delete("${OrdersApi.ORDER_SESSION_PATH}", sessionId))
				.andReturn().getResponse()
		return clazz == Void ? null : mapper.readValue(response.getContentAsString(), clazz)
	}

	protected <T> T createTicketClass(Class<T> clazz, UserIdentity identity, UUID eventId, CreateTicketClassCommand command) {
		MockHttpServletRequestBuilder builder = post("${TicketClassesApi.CREATE_TICKET_CLASS_PATH}", eventId)
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.content(mapper.writeValueAsString(command))
		if (identity != null) {
			HttpHeaders headers = new HttpHeaders()
			headers.put(ApplicationHeader.USER_IDENTITY.value, [identity.encode()])
			builder.headers(headers)
		}
		return mapper.readValue(mockMvc.perform(builder).andReturn().getResponse().getContentAsString(), clazz)
	}

	protected <T> T applyTicketScheduleDates(Class<T> clazz, UserIdentity identity, UUID eventId, UUID ticketClassId, List<ScheduleDto.Date> dates) {
		MockHttpServletRequestBuilder builder = put("${TicketClassesApi.APPLY_SCHEDULE_DATES_PATH}", eventId, ticketClassId)
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
				.content(mapper.writeValueAsString(ApplyScheduleDatesCommand.builder().dates(new HashSet<ScheduleDto.Date>(dates)).build()))
		if (identity != null) {
			HttpHeaders headers = new HttpHeaders()
			headers.put(ApplicationHeader.USER_IDENTITY.value, [identity.encode()])
			builder.headers(headers)
		}
		return mapper.readValue(mockMvc.perform(builder).andReturn().getResponse().getContentAsString(), clazz)
	}

	protected static EventLocationDto assembleEventLocationDto(
			String name = "Browar Gdański",
			CountryCode country = CountryCode.PL,
			String city = "Gdańsk",
			String street = "Długa",
			String house = "15A",
			String flat = "10",
			String postalCode = "80-333"
	) {
		return EventLocationDto.builder()
				.postalCode(postalCode)
				.country(country)
				.street(street)
				.house(house)
				.city(city)
				.flat(flat)
				.name(name)
				.build()
	}

	protected static CreateTicketClassCommand.Free assembleCreateFreeTicketCommand(
			List<ScheduleDto.Date> appliedDates,
			String name = "Darmowy bilet",
			String description = "bez piwa",
			Integer minPerOrder = null,
			Integer maxPerOrder = null,
			Instant saleStart = null,
			Instant saleEnd = null,
			Integer quantity = null
	) {
		return CreateTicketClassCommand.Free.builder()
				.appliedDates(new HashSet<ScheduleDto.Date>(appliedDates))
				.description(description)
				.minPerOrder(minPerOrder)
				.maxPerOrder(maxPerOrder)
				.saleStart(saleStart)
				.quantity(quantity)
				.saleEnd(saleEnd)
				.name(name)
				.build()
	}

	protected static CreateTicketClassCommand.Paid assembleCreatePaidTicketCommand(
			List<ScheduleDto.Date> appliedDates,
			String name = "Darmowy bilet",
			String description = "bez piwa",
			BigDecimal price = BigDecimal.TEN,
			Integer minPerOrder = null,
			Integer maxPerOrder = null,
			Instant saleStart = null,
			Instant saleEnd = null,
			Integer quantity = null
	) {
		return CreateTicketClassCommand.Paid.builder()
				.appliedDates(new HashSet<ScheduleDto.Date>(appliedDates))
				.description(description)
				.minPerOrder(minPerOrder)
				.maxPerOrder(maxPerOrder)
				.saleStart(saleStart)
				.quantity(quantity)
				.saleEnd(saleEnd)
				.price(price)
				.name(name)
				.build()
	}

	protected static UpdateTicketClassCommand.Free assembleUpdateFreeTicketCommand(
			String name = "Darmowy bilet",
			String description = "bez piwa",
			Integer minPerOrder = null,
			Integer maxPerOrder = null,
			Instant saleStart = null,
			Instant saleEnd = null,
			Integer quantity = null
	) {
		return UpdateTicketClassCommand.Free.builder()
				.description(description)
				.minPerOrder(minPerOrder)
				.maxPerOrder(maxPerOrder)
				.saleStart(saleStart)
				.quantity(quantity)
				.saleEnd(saleEnd)
				.name(name)
				.build()
	}

	protected static UpdateTicketClassCommand.Paid assembleUpdatePaidTicketCommand(
			String name = "Darmowy bilet",
			String description = "bez piwa",
			BigDecimal price = BigDecimal.TEN,
			Integer minPerOrder = null,
			Integer maxPerOrder = null,
			Instant saleStart = null,
			Instant saleEnd = null,
			Integer quantity = null
	) {
		return UpdateTicketClassCommand.Paid.builder()
				.description(description)
				.minPerOrder(minPerOrder)
				.maxPerOrder(maxPerOrder)
				.saleStart(saleStart)
				.quantity(quantity)
				.saleEnd(saleEnd)
				.price(price)
				.name(name)
				.build()
	}

	protected static ScheduleDto.Date assembleScheduleDate(LocalDateTime dateTime) {
		return ScheduleDto.Date.builder()
				.date(dateTime.toLocalDate())
				.time(dateTime.toLocalTime())
				.build()
	}

	protected static CreateEventCommand assembleValidCreateEventCommand(
			String name = "Beer fest",
			String description = "test description",
			EventCategory category = EventCategory.MUSIC,
			RefundPolicy refundPolicy = RefundPolicy.NO_REFUNDS,
			FeePolicy feePolicy = FeePolicy.INCLUDED_IN_PRICE,
			EventLocationDto location = assembleEventLocationDto(),
			List<CreateTicketClassCommand> tickets = [assembleCreateFreeTicketCommand([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 39))], "Darmowy bilet", "bez piwa", null, null, null, null)],
			Integer totalCapacity = null,
			ScheduleDto schedule = ScheduleDto.SingleDay.builder().dates([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 45))] as Set).build()
	) {
		return CreateEventCommand.builder()
				.name(name)
				.totalCapacity(totalCapacity)
				.description(description)
				.category(category)
				.refundPolicy(refundPolicy)
				.feePolicy(feePolicy)
				.location(location)
				.schedule(schedule)
				.tickets(tickets)
				.build()
	}

	private HttpHeaders headers(Map<String, String> map) {
		HttpHeaders headers = new HttpHeaders()
		map.find { it.value != null }.each { headers.add(it.key, it.value) }
		return headers
	}

}
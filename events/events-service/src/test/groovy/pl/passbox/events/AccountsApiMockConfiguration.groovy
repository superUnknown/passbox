package pl.passbox.events

import com.neovisionaries.i18n.CurrencyCode
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Primary
import pl.passbox.api.accounts.accounts.AccountsApi
import pl.passbox.api.accounts.accounts.exceptions.AccountAlreadyExistsException
import pl.passbox.api.accounts.accounts.exceptions.AccountNotFoundException
import pl.passbox.api.accounts.accounts.exceptions.IllegalAccountStatusException
import pl.passbox.api.accounts.accounts.model.AccountDto
import pl.passbox.api.accounts.accounts.model.CreateAccountCommand
import pl.passbox.api.accounts.accounts.model.PaymentsSettingsDto
import pl.passbox.api.accounts.accounts.model.RefundHandling
import pl.passbox.api.accounts.accounts.model.Status
import pl.passbox.api.accounts.accounts.model.UpdateAccountCommand
import pl.passbox.api.accounts.team.exceptions.IllegalUserStatusException
import pl.passbox.api.accounts.team.exceptions.UserNotFoundException
import pl.passbox.api.accounts.tokens.exceptions.TokenInvalidException
import pl.passbox.commons.identity.exception.UserIdentityMissingException

@TestConfiguration
class AccountsApiMockConfiguration {

	public static final UUID ACCOUNT_ID = UUID.randomUUID()

	public static final UUID MISSING_BANK_ACCOUNT_NO_ACCOUNT_ID = UUID.randomUUID()

	@Bean
	@Primary
	AccountsApi accountsApi() {
		return new AccountsApi() {

			private static final Map<UUID, AccountDto> ACCOUNTS = [
					(ACCOUNT_ID)                        : AccountDto.builder()
							.paymentsSettings(PaymentsSettingsDto.builder().bankAccount("4395847573748857857").refundHandling(RefundHandling.AUTO).currency(CurrencyCode.PLN).build())
							.id(ACCOUNT_ID)
							.status(Status.ACTIVE)
							.build(),
					(MISSING_BANK_ACCOUNT_NO_ACCOUNT_ID): AccountDto.builder()
							.paymentsSettings(PaymentsSettingsDto.builder().refundHandling(RefundHandling.AUTO).currency(CurrencyCode.PLN).build())
							.id(MISSING_BANK_ACCOUNT_NO_ACCOUNT_ID)
							.status(Status.ACTIVE)
							.build()
			]

			@Override
			AccountDto get(UUID accountId) throws AccountNotFoundException {
				AccountDto account = ACCOUNTS[accountId]
				if (account == null) {
					throw AccountNotFoundException.byId(accountId)
				}
				return account
			}

			@Override
			AccountDto create(CreateAccountCommand command) throws AccountAlreadyExistsException {
				return null
			}

			@Override
			AccountDto confirm(String token) throws TokenInvalidException {
				return null
			}

			@Override
			AccountDto update(UpdateAccountCommand command) throws UserIdentityMissingException, UserNotFoundException, AccountAlreadyExistsException, AccountNotFoundException, IllegalUserStatusException, IllegalAccountStatusException {
				return null
			}

			@Override
			PaymentsSettingsDto getPaymentsSettings(UUID accountId) throws AccountNotFoundException {
				return null
			}
		}
	}

}
package pl.passbox.events.orders

import com.neovisionaries.i18n.LanguageCode
import org.springframework.test.context.TestPropertySource
import org.springframework.transaction.annotation.Transactional
import pl.passbox.api.events.configuration.EventsErrorCode
import pl.passbox.api.events.events.model.CreateEventCommand
import pl.passbox.api.events.events.model.EventDto
import pl.passbox.api.events.events.model.types.EventCategory
import pl.passbox.api.events.events.model.types.EventStatus
import pl.passbox.api.events.events.model.types.RefundPolicy
import pl.passbox.api.events.orders.model.AttendeeDto
import pl.passbox.api.events.orders.model.CheckoutOrderCommand
import pl.passbox.api.events.orders.model.ConfirmOrderCommand
import pl.passbox.api.events.orders.model.ConfirmOrderResponseDto
import pl.passbox.api.events.orders.model.OrderSessionDto
import pl.passbox.api.events.orders.model.OrderStatus
import pl.passbox.api.events.schedules.model.ScheduleDto
import pl.passbox.api.events.tickets.model.TicketAvailabilityResult
import pl.passbox.api.events.tickets.model.types.TicketClassType
import pl.passbox.api.events.tickets.model.types.TicketStatus
import pl.passbox.api.payments.charges.model.ChargeDto
import pl.passbox.api.payments.charges.model.FeePolicy
import pl.passbox.api.payments.payments.model.RequestPaymentCommand
import pl.passbox.api.payments.payments.model.RequestPaymentResponseDto
import pl.passbox.commons.configuration.ApiError
import pl.passbox.commons.configuration.ApplicationException
import pl.passbox.commons.identity.model.UserIdentity
import pl.passbox.events.AccountsApiMockConfiguration
import pl.passbox.events.EventsSpecIT
import pl.passbox.events.events.model.Event

import java.time.Instant
import java.time.LocalDateTime
import java.time.Year

@TestPropertySource(properties = [
		"events.orders.session.ttl_in_seconds=5"
])
class OrdersApiSpecIT extends EventsSpecIT {

	def "Should checkout order for event with one ticket successfully when event capacity and ticket quantity is unlimited"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()

		when:
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand("Beer fest", null, EventCategory.MUSIC, RefundPolicy.NO_REFUNDS, FeePolicy.INCLUDED_IN_PRICE))
		publishEvent(EventDto, identity, event.id)
		OrderSessionDto response = checkoutOrder(OrderSessionDto, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.first().id, 5)])

		then:
		response.eventId == event.id
		isCloseBeforeNow(response.started)
		response.charge != null
		response.tickets.size() == 1
		response.tickets.first().ticketClassId == event.tickets.first().id
		response.tickets.first().quantity == 5
		response.id != null

		response.charge.feePolicy == FeePolicy.INCLUDED_IN_PRICE
		response.charge.products.size() == 1
		response.charge.products.first().ticketClassId == event.tickets.first().id
		response.charge.products.first().unitPrice == BigDecimal.ZERO
		response.charge.products.first().quantity == 5

		ordersSessionsRegistry.size() == 1
		with(ordersSessionsRegistry.get("${event.id}_${response.id}" as String)) {
			started == response.started
			tickets == response.tickets
			eventId == response.eventId
			charge == response.charge
			id == response.id
		}

		Thread.sleep(10000)
		ordersSessionsRegistry.isEmpty()

	}

	def "Should checkout order for event with many tickets successfully when event capacity and ticket quantity is unlimited"() {
		given:
		UUID tariffId = UUID.randomUUID()
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()

		when:
		EventDto event = createEvent(EventDto, identity, CreateEventCommand.builder()
				.name("Beer fest")
				.category(EventCategory.MUSIC)
				.refundPolicy(RefundPolicy.NO_REFUNDS)
				.feePolicy(FeePolicy.INCLUDED_IN_PRICE)
				.location(assembleEventLocationDto())
				.schedule(ScheduleDto.MultiDay.builder().dates([
				assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 45)),
				assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 28, 15, 25, 45)),
				assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 29, 19, 30, 45))
		] as Set).build()
		)
				.tickets(
				[
						assembleCreateFreeTicketCommand([
								assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 39))
						], "Darmowy bilet", "bez piwa", 1, 5, null, null),
						assembleCreatePaidTicketCommand([
								assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 39)),
								assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 28, 15, 25, 45)),
								assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 29, 19, 30, 45)),
						], "Płatny bilet", "z piwem", BigDecimal.ONE, null, 5, null, null)
				]
		)
				.build()
		)
		publishEvent(EventDto, identity, event.id)
		OrderSessionDto response = checkoutOrder(
				OrderSessionDto,
				event.id,
				[
						new CheckoutOrderCommand.Ticket(event.tickets.find { it.type == TicketClassType.FREE }.id, 5),
						new CheckoutOrderCommand.Ticket(event.tickets.find { it.type == TicketClassType.PAID }.id, 2)
				]
		)

		then:
		response.eventId == event.id
		isCloseBeforeNow(response.started)
		response.tickets.size() == 2
		response.tickets.find { it.ticketClassId == event.tickets.find { it.type == TicketClassType.FREE }.id }.quantity == 5
		response.tickets.find { it.ticketClassId == event.tickets.find { it.type == TicketClassType.PAID }.id }.quantity == 2
		response.id != null

		response.charge.feePolicy == FeePolicy.INCLUDED_IN_PRICE
		response.charge.products.size() == 2
		with(response.charge.products.find { it.ticketClassId == event.tickets.find { it.type == TicketClassType.FREE }.id }) {
			unitPrice == BigDecimal.ZERO
			quantity == 5
		}
		with(response.charge.products.find { it.ticketClassId == event.tickets.find { it.type == TicketClassType.PAID }.id }) {
			unitPrice == BigDecimal.ONE
			quantity == 2
		}

		ordersSessionsRegistry.size() == 1
		with(ordersSessionsRegistry.get("${event.id}_${response.id}" as String)) {
			started == response.started
			tickets == response.tickets
			eventId == response.eventId
			charge == response.charge
			id == response.id
		}

		Thread.sleep(10000)
		ordersSessionsRegistry.isEmpty()

	}

	def "Should checkout order for event with many tickets successfully when event capacity and ticket quantity is defined and there are sold tickets and pending orders"() {
//		totalCapacity: 15 qty
//		Schedule: 10 MAY, 11 MAY, 12 MAY
//		TICKET X: [ 10, 11 ]		8 qty | 5 sold | 2 pending | FREE
//		TICKET Y: [ 10 ]			6 qty | 1 sold | 3 pending | PAID
//		TICKET Z: [ 10, 11, 12 ]	5 qty | 0 sold | 2 pending | PAID
//
//		ORDER:
//		TICKET X: 1
//		TICKET Y: 2
//		TICKET Z: 2
//
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()

		ScheduleDto.Date day1 = assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 5, 10, 15, 0))
		ScheduleDto.Date day2 = assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 5, 11, 15, 0))
		ScheduleDto.Date day3 = assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 5, 12, 15, 0))
		when:
		EventDto event = createEvent(EventDto, identity, CreateEventCommand.builder()
				.name("Beer fest")
				.totalCapacity(15)
				.category(EventCategory.MUSIC)
				.refundPolicy(RefundPolicy.NO_REFUNDS)
				.feePolicy(FeePolicy.INCLUDED_IN_PRICE)
				.location(assembleEventLocationDto())
				.schedule(ScheduleDto.MultiDay.builder().dates([day1, day2, day3] as Set).build())
				.tickets(
				[
						assembleCreateFreeTicketCommand([day1, day2], "TICKET X", null, 1, 5, null, null, 8),
						assembleCreatePaidTicketCommand([day1], "TICKET Y", null, BigDecimal.TEN, null, null, Instant.now(), Instant.now().plusSeconds(60), 6),
						assembleCreatePaidTicketCommand([day1, day2, day3], "TICKET Z", null, BigDecimal.ONE, null, null, null, null, 5)
				])
				.build()
		)
		publishEvent(EventDto, identity, event.id)
		checkoutAndConfirmOrder(event.id, [
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET X" }.id, 2),
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET Y" }.id, 1)
		])
		checkoutAndConfirmOrder(event.id, [
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET X" }.id, 3)
		])
		checkoutOrder(OrderSessionDto, event.id, [
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET X" }.id, 2),
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET Y" }.id, 2)
		])
		checkoutOrder(OrderSessionDto, event.id, [
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET Y" }.id, 1),
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET Z" }.id, 2)
		])

		OrderSessionDto response = checkoutOrder(OrderSessionDto, event.id, [
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET X" }.id, 1),
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET Y" }.id, 2),
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET Z" }.id, 2)
		])

		then:

		response.eventId == event.id
		isCloseBeforeNow(response.started)
		response.tickets.size() == 3
		response.tickets.find { it.ticketClassId == event.tickets.find { it.name == "TICKET X" }.id }.quantity == 1
		response.tickets.find { it.ticketClassId == event.tickets.find { it.name == "TICKET Y" }.id }.quantity == 2
		response.tickets.find { it.ticketClassId == event.tickets.find { it.name == "TICKET Z" }.id }.quantity == 2
		response.id != null

		response.charge.feePolicy == FeePolicy.INCLUDED_IN_PRICE
		response.charge.products.size() == 3
		with(response.charge.products.find { it.ticketClassId == event.tickets.find { it.name == "TICKET X" }.id }) {
			unitPrice == BigDecimal.ZERO
			quantity == 1
		}
		with(response.charge.products.find { it.ticketClassId == event.tickets.find { it.name == "TICKET Y" }.id }) {
			unitPrice == BigDecimal.TEN
			quantity == 2
		}
		with(response.charge.products.find { it.ticketClassId == event.tickets.find { it.name == "TICKET Z" }.id }) {
			unitPrice == BigDecimal.ONE
			quantity == 2
		}

		ordersSessionsRegistry.size() == 3
		with(ordersSessionsRegistry.get("${event.id}_${response.id}" as String)) {
			started == response.started
			tickets == response.tickets
			eventId == response.eventId
			charge == response.charge
			id == response.id
		}

		with(ticketsSalesSupport.getTicketsSalesInfo(event.id)) {
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET X" }.id }.pending == 3
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET X" }.id }.sold == 5
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET Y" }.id }.pending == 5
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET Y" }.id }.sold == 1
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET Z" }.id }.pending == 4
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET Z" }.id }.sold == 0
		}
		Thread.sleep(10000)
		ordersSessionsRegistry.isEmpty()

	}

	def "Should not checkout order when redis is down"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()

		when:
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand("Beer fest", null, EventCategory.MUSIC, RefundPolicy.NO_REFUNDS, FeePolicy.INCLUDED_IN_PRICE))
		publishEvent(EventDto, identity, event.id)

		redisServer.stop()
		ApiError response = checkoutOrder(ApiError, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.first().id, 5)])
		redisServer.start()

		then:
		response.code == ApplicationException.CommonsErrorCode.INTERNAL_SERVER_ERROR.name()
		Thread.sleep(2000)
		ordersSessionsRegistry.isEmpty()

	}

	def "Should not checkout order when given ticket not belongs to event"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()

		when:
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand())
		publishEvent(EventDto, identity, event.id)

		ApiError response = checkoutOrder(ApiError, event.id, [new CheckoutOrderCommand.Ticket(UUID.randomUUID(), 1)])

		then:
		response.code == EventsErrorCode.TICKET_CLASS_NOT_FOUND.name()
		ordersSessionsRegistry.isEmpty()
	}

	def "Should not checkout order when sale not started or already ended"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()

		CreateEventCommand command = assembleValidCreateEventCommand()
		command.tickets.first().setSaleStart(saleStart)
		command.tickets.first().setSaleEnd(saleEnd)

		when:
		EventDto event = createEvent(EventDto, identity, command)
		publishEvent(EventDto, identity, event.id)
		ApiError response = checkoutOrder(ApiError, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.first().id, 5)])

		then:
		response.code == code.name()
		ordersSessionsRegistry.isEmpty()

		where:
		code                                 | saleStart                       | saleEnd
		EventsErrorCode.TICKET_NOT_AVAILABLE | Instant.now().plusSeconds(60)   | null
		EventsErrorCode.TICKET_NOT_AVAILABLE | Instant.now().plusSeconds(60)   | Instant.now().plusSeconds(120)
		EventsErrorCode.TICKET_NOT_AVAILABLE | null                            | Instant.now().minusSeconds(60)
		EventsErrorCode.TICKET_NOT_AVAILABLE | Instant.now().minusSeconds(120) | Instant.now().minusSeconds(60)

	}

	def "Should not checkout order when given quantity is out of order limits"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()

		CreateEventCommand command = assembleValidCreateEventCommand()
		command.tickets.first().setMinPerOrder(minPerOrder)
		command.tickets.first().setMaxPerOrder(maxPerOrder)

		when:
		EventDto event = createEvent(EventDto, identity, command)
		publishEvent(EventDto, identity, event.id)
		ApiError response = checkoutOrder(ApiError, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.first().id, 5)])

		then:
		response.code == code.name()
		ordersSessionsRegistry.isEmpty()

		where:
		code                                 | minPerOrder | maxPerOrder
		EventsErrorCode.TICKET_NOT_AVAILABLE | 6           | null
		EventsErrorCode.TICKET_NOT_AVAILABLE | 6           | 10
		EventsErrorCode.TICKET_NOT_AVAILABLE | null        | 4
		EventsErrorCode.TICKET_NOT_AVAILABLE | 2           | 4

	}

	def "Should not checkout order when ticket left quantity is smaller then ordered"() {
//		totalCapacity: 15 qty
//		Schedule: 10 MAY, 11 MAY, 12 MAY
//		TICKET X: [ 10, 11 ]		8 qty | 5 sold | 2 pending | FREE
//		TICKET Y: [ 10 ]			6 qty | 1 sold | 3 pending | PAID
//		TICKET Z: [ 10, 11, 12 ]	5 qty | 0 sold | 2 pending | PAID
//
//		ORDER:
//		TICKET X: 2 - TO MUCH
//		TICKET Y: 2
//		TICKET Z: 2
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()

		ScheduleDto.Date day1 = assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 5, 10, 15, 0))
		ScheduleDto.Date day2 = assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 5, 11, 15, 0))
		ScheduleDto.Date day3 = assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 5, 12, 15, 0))
		when:
		EventDto event = createEvent(EventDto, identity, CreateEventCommand.builder()
				.name("Beer fest")
				.totalCapacity(15)
				.category(EventCategory.MUSIC)
				.refundPolicy(RefundPolicy.NO_REFUNDS)
				.feePolicy(FeePolicy.INCLUDED_IN_PRICE)
				.location(assembleEventLocationDto())
				.schedule(ScheduleDto.MultiDay.builder().dates([day1, day2, day3] as Set).build())
				.tickets(
				[
						assembleCreateFreeTicketCommand([day1, day2], "TICKET X", null, 1, 5, null, null, 8),
						assembleCreatePaidTicketCommand([day1], "TICKET Y", null, BigDecimal.TEN, null, null, Instant.now(), Instant.now().plusSeconds(60), 6),
						assembleCreatePaidTicketCommand([day1, day2, day3], "TICKET Z", null, BigDecimal.ONE, null, null, null, null, 5)
				])
				.build()
		)
		publishEvent(EventDto, identity, event.id)
		checkoutAndConfirmOrder(event.id, [
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET X" }.id, 2),
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET Y" }.id, 1)
		])
		checkoutAndConfirmOrder(event.id, [
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET X" }.id, 3)
		])
		checkoutOrder(OrderSessionDto, event.id, [
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET X" }.id, 2),
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET Y" }.id, 2)
		])
		checkoutOrder(OrderSessionDto, event.id, [
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET Y" }.id, 1),
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET Z" }.id, 2)
		])

		ApiError response = checkoutOrder(ApiError, event.id, [
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET X" }.id, 2),
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET Y" }.id, 2),
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET Z" }.id, 2)
		])

		then:

		response.code == EventsErrorCode.TICKET_NOT_AVAILABLE.name()
		response.message.contains("$TicketAvailabilityResult.TOTAL_TICKET_QUANTITY_REACHED")

		ordersSessionsRegistry.size() == 2

		with(ticketsSalesSupport.getTicketsSalesInfo(event.id)) {
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET X" }.id }.pending == 2
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET X" }.id }.sold == 5
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET Y" }.id }.pending == 3
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET Y" }.id }.sold == 1
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET Z" }.id }.pending == 2
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET Z" }.id }.sold == 0
		}

	}

	def "Should not checkout order when event capacity has reached"() {
//		totalCapacity: 15 qty
//		Schedule: 10 MAY, 11 MAY, 12 MAY
//		TICKET X: [ 10, 11 ]		30 qty | 5 sold | 2 pending | FREE
//		TICKET Y: [ 10 ]			6 qty  | 1 sold | 3 pending | PAID
//		TICKET Z: [ 10, 11, 12 ]	5 qty  | 0 sold | 2 pending | PAID
//
//		ORDER:
//		TICKET X: 2
//		TICKET Y: 2
//		TICKET Z: 2
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()

		ScheduleDto.Date day1 = assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 5, 10, 15, 0))
		ScheduleDto.Date day2 = assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 5, 11, 15, 0))
		ScheduleDto.Date day3 = assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 5, 12, 15, 0))
		when:
		EventDto event = createEvent(EventDto, identity, CreateEventCommand.builder()
				.name("Beer fest")
				.totalCapacity(15)
				.category(EventCategory.MUSIC)
				.refundPolicy(RefundPolicy.NO_REFUNDS)
				.feePolicy(FeePolicy.INCLUDED_IN_PRICE)
				.location(assembleEventLocationDto())
				.schedule(ScheduleDto.MultiDay.builder().dates([day1, day2, day3] as Set).build())
				.tickets(
				[
						assembleCreateFreeTicketCommand([day1, day2], "TICKET X", null, 1, null, null, null, 30),
						assembleCreatePaidTicketCommand([day1], "TICKET Y", null, BigDecimal.TEN, null, null, Instant.now(), Instant.now().plusSeconds(60), 6),
						assembleCreatePaidTicketCommand([day1, day2, day3], "TICKET Z", null, BigDecimal.ONE, null, null, null, null, 5)
				])
				.build()
		)
		publishEvent(EventDto, identity, event.id)
		checkoutAndConfirmOrder(event.id, [
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET X" }.id, 2),
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET Y" }.id, 1)
		])
		checkoutAndConfirmOrder(event.id, [
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET X" }.id, 3)
		])
		checkoutOrder(OrderSessionDto, event.id, [
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET X" }.id, 2),
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET Y" }.id, 2)
		])
		checkoutOrder(OrderSessionDto, event.id, [
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET Y" }.id, 1),
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET Z" }.id, 2)
		])

		ApiError response = checkoutOrder(ApiError, event.id, [
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET X" }.id, 10),
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET Y" }.id, 2),
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET Z" }.id, 2)
		])

		then:

		response.code == EventsErrorCode.TICKET_NOT_AVAILABLE.name()
		response.message.contains("$TicketAvailabilityResult.TOTAL_EVENT_CAPACITY_REACHED")

		ordersSessionsRegistry.size() == 2

		with(ticketsSalesSupport.getTicketsSalesInfo(event.id)) {
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET X" }.id }.pending == 2
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET X" }.id }.sold == 5
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET Y" }.id }.pending == 3
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET Y" }.id }.sold == 1
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET Z" }.id }.pending == 2
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET Z" }.id }.sold == 0
		}

	}

	def "Should not checkout order when event has illegal status"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()

		when:
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand("Beer fest", null, EventCategory.MUSIC, RefundPolicy.NO_REFUNDS, FeePolicy.INCLUDED_IN_PRICE))
		with(eventsRepository.findOne(event.id).get()) { Event e ->
			e.setStatus(eventStatus)
			eventsRepository.save(e)
		}
		ApiError response = checkoutOrder(ApiError, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.first().id, 5)])

		then:

		response.code == code.name()
		ordersSessionsRegistry.isEmpty()

		where:
		eventStatus             | code
		EventStatus.UNPUBLISHED | EventsErrorCode.EVENT_UNPUBLISHED
		EventStatus.SUSPENDED   | EventsErrorCode.EVENT_SUSPENDED
		EventStatus.FINISHED    | EventsErrorCode.EVENT_FINISHED
		EventStatus.DELETED     | EventsErrorCode.EVENT_NOT_FOUND

	}

	def "Should not checkout order when event not exists"() {
		when:
		ApiError response = checkoutOrder(ApiError, UUID.randomUUID(), [new CheckoutOrderCommand.Ticket(UUID.randomUUID(), 5)])

		then:
		response.code == EventsErrorCode.EVENT_NOT_FOUND.name()
		ordersSessionsRegistry.isEmpty()

	}

	def "Should refresh order session successfully"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()

		when:
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand("Beer fest", null, EventCategory.MUSIC, RefundPolicy.NO_REFUNDS, FeePolicy.INCLUDED_IN_PRICE))
		publishEvent(EventDto, identity, event.id)
		OrderSessionDto session = checkoutOrder(OrderSessionDto, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.first().id, 5)])
		Thread.sleep(3000)
		refreshOrderSession(Void, session.id)
		Thread.sleep(3000)

		then:
		ordersSessionsRegistry.size() == 1
		ordersSessionsRegistry.get("${event.id}_${session.id}" as String).eventId == event.id

		Thread.sleep(10000)
		ordersSessionsRegistry.isEmpty()

	}

	def "Should not refresh order session when redis is down"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()

		when:
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand("Beer fest", null, EventCategory.MUSIC, RefundPolicy.NO_REFUNDS, FeePolicy.INCLUDED_IN_PRICE))
		publishEvent(EventDto, identity, event.id)
		OrderSessionDto session = checkoutOrder(OrderSessionDto, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.first().id, 5)])

		redisServer.stop()
		ApiError response = refreshOrderSession(ApiError, session.id)
		redisServer.start()

		then:
		response.code == ApplicationException.CommonsErrorCode.INTERNAL_SERVER_ERROR.name()
		ordersSessionsRegistry.isEmpty()

	}

	def "Should not refresh order session when not exists"() {
		when:
		ApiError response = refreshOrderSession(ApiError, UUID.randomUUID())

		then:
		response.code == EventsErrorCode.ORDER_SESSION_NOT_FOUND.name()
		ordersSessionsRegistry.isEmpty()

	}

	def "Should remove order session successfully"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()

		when:
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand("Beer fest", null, EventCategory.MUSIC, RefundPolicy.NO_REFUNDS, FeePolicy.INCLUDED_IN_PRICE))
		publishEvent(EventDto, identity, event.id)
		OrderSessionDto session = checkoutOrder(OrderSessionDto, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.first().id, 5)])
		assert ordersSessionsRegistry.size() == 1

		removeOrderSession(Void, session.id)

		then:

		ordersSessionsRegistry.isEmpty()

	}

	def "Should not remove order session when not exists"() {
		when:
		ApiError response = removeOrderSession(ApiError, UUID.randomUUID())

		then:
		response.code == EventsErrorCode.ORDER_SESSION_NOT_FOUND.name()
		ordersSessionsRegistry.isEmpty()

	}

	@Transactional
	def "Should confirm previously checked out order successfully"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()
		ScheduleDto.Date day1 = assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 5, 10, 15, 0))
		ScheduleDto.Date day2 = assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 5, 11, 15, 0))
		ScheduleDto.Date day3 = assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 5, 12, 15, 0))

		when:
		EventDto event = createAndPublishEvent(identity, CreateEventCommand.builder()
				.name("Beer fest")
				.totalCapacity(15)
				.category(EventCategory.MUSIC)
				.refundPolicy(RefundPolicy.NO_REFUNDS)
				.feePolicy(FeePolicy.INCLUDED_IN_PRICE)
				.location(assembleEventLocationDto())
				.schedule(ScheduleDto.MultiDay.builder().dates([day1, day2, day3] as Set).build())
				.tickets(
				[
						assembleCreateFreeTicketCommand([day1, day2], "TICKET X", null, 1, 2, null, null, 8),
						assembleCreatePaidTicketCommand([day1], "TICKET Y", null, BigDecimal.TEN, null, null, Instant.now(), Instant.now().plusSeconds(60), 6),
						assembleCreatePaidTicketCommand([day1, day2, day3], "TICKET Z", null, BigDecimal.ONE, null, null, null, null, 5)
				])
				.build()
		)
		OrderSessionDto session = checkoutOrder(OrderSessionDto, event.id, [
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET X" }.id, 1),
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET Y" }.id, 2),
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET Z" }.id, 2)
		])

		ConfirmOrderCommand.Ticket[] tickets = [
				ConfirmOrderCommand.Ticket.builder()
						.attendee(new AttendeeDto("attendee@dev.null", "Jan", "Nowak"))
						.ticketClassId(event.tickets.find { it.name == "TICKET X" }.id)
						.build(),
				ConfirmOrderCommand.Ticket.builder()
						.attendee(new AttendeeDto("attendee@dev.null", "Jan", "Nowak"))
						.ticketClassId(event.tickets.find { it.name == "TICKET Y" }.id)
						.build(),
				ConfirmOrderCommand.Ticket.builder()
						.attendee(new AttendeeDto("attendee1@dev.null", "Jan", "Nowak"))
						.ticketClassId(event.tickets.find { it.name == "TICKET Y" }.id)
						.build(),
				ConfirmOrderCommand.Ticket.builder()
						.attendee(new AttendeeDto("attendee1@dev.null", "Jan", "Nowak"))
						.ticketClassId(event.tickets.find { it.name == "TICKET Z" }.id)
						.build(),
				ConfirmOrderCommand.Ticket.builder()
						.attendee(new AttendeeDto("attendee2@dev.null", "Jan", "Nowak"))
						.ticketClassId(event.tickets.find { it.name == "TICKET Z" }.id)
						.build()
		]

		ConfirmOrderResponseDto response = confirmOrder(ConfirmOrderResponseDto, "127.0.0.1", null, ConfirmOrderCommand.builder()
				.customer(new ConfirmOrderCommand.Customer("email@dev.null", "email@dev.null", "Jan", "Nowak", LanguageCode.en))
				.tickets(tickets as List)
				.sessionId(session.id)
				.eventId(event.id)
				.build()
		)

		then:

		response.paymentUrl == "http://example.com/payment"
		with(paymentRequestsRegistry.last()) {
			orderId == response.orderId
			customerIp == "127.0.0.1"
			customer.email == "email@dev.null"
			customer.firstname == "Jan"
			customer.lastname == "Nowak"
			customer.language == LanguageCode.en
		}

		ordersSessionsRegistry.isEmpty()

		with(ordersRepository.findOne(response.orderId).get()) {
			status == OrderStatus.REQUESTED
			refundPolicy == event.refundPolicy
			feePolicy == event.feePolicy
			started == session.started
			isCloseBeforeNow(requested)
			completed == null
			getEvent().id == event.id
			customer.email == "email@dev.null"
			customer.firstname == "Jan"
			customer.lastname == "Nowak"
			customer.language == LanguageCode.en

			getTickets().size() == 5
			getTickets().findAll { it.ticketClass.name == "TICKET X" }.size() == 1
			getTickets().findAll { it.ticketClass.name == "TICKET Y" }.size() == 2
			getTickets().findAll { it.ticketClass.name == "TICKET Z" }.size() == 2
			getTickets().find { it.ticketClass.name == "TICKET X" }.attendee.email == "attendee@dev.null"
			getTickets().each {
				it.status == TicketStatus.REQUESTED
				it.number == it.id.toString().substring(it.id.toString().indexOf("-") - 1)
				it.attendee.firstname == "Jan"
				it.attendee.lastname == "Nowak"
				it.checkIn == null
			}

		}

		with(ticketsSalesSupport.getTicketsSalesInfo(event.id)) {
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET X" }.id }.pending == 0
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET X" }.id }.sold == 1
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET Y" }.id }.pending == 0
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET Y" }.id }.sold == 2
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET Z" }.id }.pending == 0
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET Z" }.id }.sold == 2
		}

	}

	def "Should not confirm previously checked out order when confirmed tickets doesn't match with checked out"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()
		ScheduleDto.Date day1 = assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 5, 10, 15, 0))
		ScheduleDto.Date day2 = assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 5, 11, 15, 0))
		ScheduleDto.Date day3 = assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 5, 12, 15, 0))

		when:
		EventDto event = createAndPublishEvent(identity, CreateEventCommand.builder()
				.name("Beer fest")
				.totalCapacity(15)
				.category(EventCategory.MUSIC)
				.refundPolicy(RefundPolicy.NO_REFUNDS)
				.feePolicy(FeePolicy.INCLUDED_IN_PRICE)
				.location(assembleEventLocationDto())
				.schedule(ScheduleDto.MultiDay.builder().dates([day1, day2, day3] as Set).build())
				.tickets(
				[
						assembleCreateFreeTicketCommand([day1, day2], "TICKET X", null, 1, 2, null, null, 8),
						assembleCreatePaidTicketCommand([day1], "TICKET Y", null, BigDecimal.TEN, null, null, Instant.now(), Instant.now().plusSeconds(60), 6),
						assembleCreatePaidTicketCommand([day1, day2, day3], "TICKET Z", null, BigDecimal.ONE, null, null, null, null, 5)
				])
				.build()
		)
		OrderSessionDto session = checkoutOrder(OrderSessionDto, event.id, [
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET X" }.id, 1),
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET Y" }.id, 2),
				new CheckoutOrderCommand.Ticket(event.tickets.find { it.name == "TICKET Z" }.id, 2)
		])

		ConfirmOrderCommand.Ticket[] tickets = confirmedTickets.collect { String name ->
			ConfirmOrderCommand.Ticket.builder()
					.attendee(new AttendeeDto("attendee@dev.null", "Jan", "Nowak"))
					.ticketClassId(event.tickets.find { it.name == name }.id)
					.build()
		}

		ApiError response = confirmOrder(ApiError, "127.0.0.1", null, ConfirmOrderCommand.builder()
				.customer(new ConfirmOrderCommand.Customer("email@dev.null", "email@dev.null", "Jan", "Nowak", LanguageCode.en))
				.tickets(tickets as List)
				.sessionId(session.id)
				.eventId(event.id)
				.build()
		)

		then:
		response.code == code.name()

		ordersSessionsRegistry.get("${event.id}_${session.id}" as String).eventId == event.id
		ordersRepository.findAll().count() == 0

		with(ticketsSalesSupport.getTicketsSalesInfo(event.id)) {
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET X" }.id }.pending == 1
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET X" }.id }.sold == 0
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET Y" }.id }.pending == 2
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET Y" }.id }.sold == 0
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET Z" }.id }.pending == 2
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET Z" }.id }.sold == 0
		}

		where:
		code                                              | confirmedTickets
		ApplicationException.CommonsErrorCode.BAD_REQUEST | []
		ApplicationException.CommonsErrorCode.BAD_REQUEST | ["TICKET X"]
		ApplicationException.CommonsErrorCode.BAD_REQUEST | ["TICKET X", "TICKET Y", "TICKET Y"]
		ApplicationException.CommonsErrorCode.BAD_REQUEST | ["TICKET X", "TICKET Y", "TICKET Y", "TICKET Z"]
		ApplicationException.CommonsErrorCode.BAD_REQUEST | ["TICKET X", "TICKET Z", "TICKET Z"]
		ApplicationException.CommonsErrorCode.BAD_REQUEST | ["TICKET X", "TICKET Y", "TICKET Z", "TICKET Z"]
		ApplicationException.CommonsErrorCode.BAD_REQUEST | ["TICKET Y", "TICKET Y", "TICKET Z", "TICKET Z"]

	}

	def "Should not confirm order when given session not exists"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()
		ScheduleDto.Date day1 = assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 5, 10, 15, 0))
		ScheduleDto.Date day2 = assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 5, 11, 15, 0))
		ScheduleDto.Date day3 = assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 5, 12, 15, 0))

		when:
		EventDto event = createAndPublishEvent(identity, CreateEventCommand.builder()
				.name("Beer fest")
				.totalCapacity(15)
				.category(EventCategory.MUSIC)
				.refundPolicy(RefundPolicy.NO_REFUNDS)
				.feePolicy(FeePolicy.INCLUDED_IN_PRICE)
				.location(assembleEventLocationDto())
				.schedule(ScheduleDto.MultiDay.builder().dates([day1, day2, day3] as Set).build())
				.tickets(
				[
						assembleCreateFreeTicketCommand([day1, day2], "TICKET X", null, 1, 2, null, null, 8),
						assembleCreatePaidTicketCommand([day1], "TICKET Y", null, BigDecimal.TEN, null, null, Instant.now(), Instant.now().plusSeconds(60), 6),
						assembleCreatePaidTicketCommand([day1, day2, day3], "TICKET Z", null, BigDecimal.ONE, null, null, null, null, 5)
				])
				.build()
		)

		ConfirmOrderCommand.Ticket[] tickets = [
				ConfirmOrderCommand.Ticket.builder()
						.attendee(new AttendeeDto("attendee@dev.null", "Jan", "Nowak"))
						.ticketClassId(event.tickets.find { it.name == "TICKET X" }.id)
						.build(),
				ConfirmOrderCommand.Ticket.builder()
						.attendee(new AttendeeDto("attendee@dev.null", "Jan", "Nowak"))
						.ticketClassId(event.tickets.find { it.name == "TICKET Y" }.id)
						.build(),
				ConfirmOrderCommand.Ticket.builder()
						.attendee(new AttendeeDto("attendee1@dev.null", "Jan", "Nowak"))
						.ticketClassId(event.tickets.find { it.name == "TICKET Y" }.id)
						.build(),
				ConfirmOrderCommand.Ticket.builder()
						.attendee(new AttendeeDto("attendee1@dev.null", "Jan", "Nowak"))
						.ticketClassId(event.tickets.find { it.name == "TICKET Z" }.id)
						.build(),
				ConfirmOrderCommand.Ticket.builder()
						.attendee(new AttendeeDto("attendee2@dev.null", "Jan", "Nowak"))
						.ticketClassId(event.tickets.find { it.name == "TICKET Z" }.id)
						.build()
		]

		ApiError response = confirmOrder(ApiError, "127.0.0.1", null, ConfirmOrderCommand.builder()
				.customer(new ConfirmOrderCommand.Customer("email@dev.null", "email@dev.null", "Jan", "Nowak", LanguageCode.en))
				.tickets(tickets as List)
				.sessionId(UUID.randomUUID())
				.eventId(event.id)
				.build()
		)

		then:
		response.code == EventsErrorCode.ORDER_SESSION_NOT_FOUND.name()

		ordersSessionsRegistry.isEmpty()
		ordersRepository.findAll().count() == 0

		with(ticketsSalesSupport.getTicketsSalesInfo(event.id)) {
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET X" }.id }.pending == 0
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET X" }.id }.sold == 0
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET Y" }.id }.pending == 0
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET Y" }.id }.sold == 0
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET Z" }.id }.pending == 0
			getTickets().find { it.ticketClassId == event.tickets.find { it.name == "TICKET Z" }.id }.sold == 0
		}

	}

}
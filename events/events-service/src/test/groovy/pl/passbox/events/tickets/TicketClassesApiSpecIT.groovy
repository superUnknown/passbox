package pl.passbox.events.tickets

import org.springframework.test.context.TestPropertySource
import org.springframework.transaction.annotation.Transactional
import pl.passbox.api.events.configuration.EventsErrorCode
import pl.passbox.api.events.events.model.EventDto
import pl.passbox.api.events.events.model.types.EventCategory
import pl.passbox.api.events.events.model.types.EventStatus
import pl.passbox.api.events.events.model.types.RefundPolicy
import pl.passbox.api.events.orders.model.CheckoutOrderCommand
import pl.passbox.api.events.orders.model.OrderSessionDto
import pl.passbox.api.events.schedules.model.ScheduleDto
import pl.passbox.api.events.tickets.model.CreateTicketClassCommand
import pl.passbox.api.events.tickets.model.TicketClassDto
import pl.passbox.api.events.tickets.model.types.TicketClassStatus
import pl.passbox.api.payments.charges.model.FeePolicy
import pl.passbox.commons.configuration.ApiError
import pl.passbox.commons.configuration.ApplicationException
import pl.passbox.commons.identity.model.UserIdentity
import pl.passbox.events.AccountsApiMockConfiguration
import pl.passbox.events.EventsSpecIT
import pl.passbox.events.events.model.Event
import pl.passbox.events.tickets.model.TicketClass

import java.time.Instant
import java.time.LocalDateTime
import java.time.Year

@TestPropertySource(properties = [
		"events.tickets_specification.min_ticket_price=3.00"
])
class TicketClassesApiSpecIT extends EventsSpecIT {

	@Transactional
	def "Should create free ticket class for given event successfully"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()
		when:
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand())
		assert eventsRepository.findOne(event.id).get().tickets.size() == 1

		TicketClassDto response = createTicketClass(TicketClassDto, identity, event.id, CreateTicketClassCommand.Free.builder()
				.appliedDates([event.schedule.dates.first()] as Set)
				.description("test")
				.minPerOrder(1)
				.maxPerOrder(5)
				.saleStart(null)
				.quantity(null)
				.saleEnd(Instant.now().plusSeconds(60))
				.name("Normal")
				.build()
		)

		then:

		with(response as TicketClassDto.Free) {
			id != null
			status == TicketClassStatus.ACTIVE
			name == "Normal"
			description == "test"
			quantity == null
			minPerOrder == 1
			maxPerOrder == 5
			saleStart == null
			saleEnd != null
			appliedDates.size() == 1
			appliedDates.first().asDateTime() == event.schedule.dates.first().asDateTime()
			isCloseBeforeNow(created)
			createdBy == identity.userId
			modified == null
			modifiedBy == null
		}

		eventsRepository.findOne(event.id).get().tickets.size() == 2
		with(eventsRepository.findOne(event.id).get().tickets.find { it.id == response.id } as TicketClass.Free) {
			status == TicketClassStatus.ACTIVE
			name == response.name
			description == response.description
			quantity == response.quantity
			minPerOrder == response.minPerOrder
			maxPerOrder == response.maxPerOrder
			saleStart == response.saleStart
			saleEnd == response.saleEnd
			appliedDates.size() == response.appliedDates.size()
			appliedDates.first().id.ticket.id == id
			appliedDates.first().id.date.asDateTime() == response.appliedDates.first().asDateTime()
			created == response.created
			createdBy == response.createdBy
			modified == response.modified
			modifiedBy == response.modifiedBy
		}

	}

	@Transactional
	def "Should create paid ticket class for given event successfully"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()
		when:
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand())
		assert eventsRepository.findOne(event.id).get().tickets.size() == 1

		TicketClassDto response = createTicketClass(TicketClassDto, identity, event.id, CreateTicketClassCommand.Paid.builder()
				.appliedDates([event.schedule.dates.first()] as Set)
				.description("vip")
				.minPerOrder(null)
				.maxPerOrder(null)
				.saleStart(Instant.now().plusSeconds(60))
				.quantity(5)
				.price(BigDecimal.TEN)
				.saleEnd(null)
				.name("VIP")
				.build()
		)

		then:

		with(response as TicketClassDto.Paid) {
			id != null
			price == BigDecimal.TEN
			status == TicketClassStatus.ACTIVE
			name == "VIP"
			description == "vip"
			quantity == 5
			minPerOrder == null
			maxPerOrder == null
			saleStart != null
			saleEnd == null
			appliedDates.size() == 1
			appliedDates.first().asDateTime() == event.schedule.dates.first().asDateTime()
			isCloseBeforeNow(created)
			createdBy == identity.userId
			modified == null
			modifiedBy == null
		}

		eventsRepository.findOne(event.id).get().tickets.size() == 2
		with(eventsRepository.findOne(event.id).get().tickets.find { it.id == response.id } as TicketClass.Paid) {
			price == (response as TicketClassDto.Paid).price
			status == TicketClassStatus.ACTIVE
			name == response.name
			description == response.description
			quantity == response.quantity
			minPerOrder == response.minPerOrder
			maxPerOrder == response.maxPerOrder
			saleStart == response.saleStart
			saleEnd == response.saleEnd
			appliedDates.size() == response.appliedDates.size()
			appliedDates.first().id.ticket.id == id
			appliedDates.first().id.date.asDateTime() == response.appliedDates.first().asDateTime()
			created == response.created
			createdBy == response.createdBy
			modified == response.modified
			modifiedBy == response.modifiedBy
		}

	}

	@Transactional
	def "Should not create ticket class when applied date is not defined"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()
		when:
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand())
		assert eventsRepository.findOne(event.id).get().tickets.size() == 1

		ApiError response = createTicketClass(ApiError, identity, event.id, CreateTicketClassCommand.Paid.builder()
				.appliedDates([ScheduleDto.Date.fromDateTime(LocalDateTime.now().plusSeconds(60))] as Set)
				.description("vip")
				.minPerOrder(null)
				.maxPerOrder(null)
				.saleStart(Instant.now().plusSeconds(60))
				.quantity(5)
				.price(BigDecimal.TEN)
				.saleEnd(null)
				.name("VIP")
				.build()
		)

		then:

		response.code == EventsErrorCode.APPLIED_DATE_NOT_DEFINED.name()

		eventsRepository.findOne(event.id).get().tickets.size() == 1
		with(eventsRepository.findOne(event.id).get().tickets.first()) {
			id == event.tickets.first().id
		}

	}

	@Transactional
	def "Should not create ticket class when given event not belongs to account"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()
		when:
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand())
		assert eventsRepository.findOne(event.id).get().tickets.size() == 1

		ApiError response = createTicketClass(ApiError, UserIdentity.builder().accountId(UUID.randomUUID()).userId(UUID.randomUUID()).build(), event.id, CreateTicketClassCommand.Paid.builder()
				.appliedDates([event.schedule.dates.first()] as Set)
				.description("vip")
				.minPerOrder(null)
				.maxPerOrder(null)
				.saleStart(Instant.now().plusSeconds(60))
				.quantity(5)
				.price(BigDecimal.TEN)
				.saleEnd(null)
				.name("VIP")
				.build()
		)

		then:

		response.code == ApplicationException.CommonsErrorCode.UNAUTHORIZED_ACCESS.name()

		eventsRepository.findOne(event.id).get().tickets.size() == 1
		with(eventsRepository.findOne(event.id).get().tickets.first()) {
			id == event.tickets.first().id
		}

	}

	def "Should not create ticket class when given event doesn't exist"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()
		when:

		ApiError response = createTicketClass(ApiError, identity, UUID.randomUUID(), CreateTicketClassCommand.Paid.builder()
				.appliedDates([ScheduleDto.Date.fromDateTime(LocalDateTime.now().plusSeconds(60))] as Set)
				.description("vip")
				.minPerOrder(null)
				.maxPerOrder(null)
				.saleStart(Instant.now().plusSeconds(60))
				.quantity(5)
				.price(BigDecimal.TEN)
				.saleEnd(null)
				.name("VIP")
				.build()
		)

		then:
		response.code == EventsErrorCode.EVENT_NOT_FOUND.name()

	}

	@Transactional
	def "Should not create paid ticket class when given price is invalid"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()
		when:
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand())
		assert eventsRepository.findOne(event.id).get().tickets.size() == 1

		ApiError response = createTicketClass(ApiError, identity, event.id, CreateTicketClassCommand.Paid.builder()
				.appliedDates([event.schedule.dates.first()] as Set)
				.description("vip")
				.minPerOrder(null)
				.maxPerOrder(null)
				.saleStart(Instant.now().plusSeconds(60))
				.quantity(5)
				.price(BigDecimal.ONE)
				.saleEnd(null)
				.name("VIP")
				.build()
		)

		then:

		response.code == EventsErrorCode.TICKET_PRICE_INVALID.name()

		eventsRepository.findOne(event.id).get().tickets.size() == 1
		with(eventsRepository.findOne(event.id).get().tickets.first()) {
			id == event.tickets.first().id
		}

	}

	@Transactional
	def "Should apply schedule dates to ticket of event with multi-day schedule successfully when there are no pending order sessions"() {
		given:
		UserIdentity identity = UserIdentity.builder().accountId(AccountsApiMockConfiguration.ACCOUNT_ID).userId(UUID.randomUUID()).build()
		ScheduleDto.Date[] dates = [
				assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 27, 19, 30)),
				assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30)),
				assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 28, 19, 30))
		]
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand(
				"Beer fest",
				null,
				EventCategory.MUSIC,
				RefundPolicy.NO_REFUNDS,
				FeePolicy.INCLUDED_IN_PRICE,
				assembleEventLocationDto(),
				[assembleCreateFreeTicketCommand([dates[0], dates[2]], "Darmowy bilet", "bez piwa", null, null, null, null)],
				null,
				ScheduleDto.MultiDay.builder().dates([dates[0], dates[1], dates[2]] as Set).build()
		))

		assert ordersSessionsRegistry.isEmpty()
		assert event.tickets.first().appliedDates.size() == 2
		assert event.tickets.first().appliedDates.containsAll([dates[0], dates[2]])

		when:
		TicketClassDto response = applyTicketScheduleDates(TicketClassDto, identity, event.id, event.tickets.first().id, givenDates.collect {
			dates[it]
		})

		then:
		with(response as TicketClassDto.Free) {
			appliedDates.size() == givenDates.size()
			appliedDates.containsAll(givenDates.collect { dates[it] })
			isCloseBeforeNow(modified)
			modifiedBy == identity.userId
		}

		eventsRepository.findOne(event.id).get().tickets.size() == 1
		with(eventsRepository.findOne(event.id).get().tickets.find { it.id == response.id } as TicketClass.Free) {
			appliedDates.size() == givenDates.size()
			appliedDates.collect { it.date.asDateTime() }.containsAll(givenDates.collect { dates[it].asDateTime() })
			isCloseBeforeNow(modified)
			modifiedBy == identity.userId
		}

		ordersSessionsRegistry.isEmpty()

		where:
		givenDates << [
				[0, 1],
				[0],
				[2, 0, 1]
		]

	}

	@Transactional
	def "Should apply schedule dates to ticket of event with multi-day schedule successfully when there are pending order sessions"() {
		given:
		UserIdentity identity = UserIdentity.builder().accountId(AccountsApiMockConfiguration.ACCOUNT_ID).userId(UUID.randomUUID()).build()
		ScheduleDto.Date[] dates = [
				assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 27, 19, 30)),
				assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30)),
				assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 28, 19, 30))
		]
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand(
				"Beer fest",
				null,
				EventCategory.MUSIC,
				RefundPolicy.NO_REFUNDS,
				FeePolicy.INCLUDED_IN_PRICE,
				assembleEventLocationDto(),
				[assembleCreateFreeTicketCommand([dates[0], dates[2]])],
				null,
				ScheduleDto.MultiDay.builder().dates([dates[0], dates[1], dates[2]] as Set).build()
		))
		publishEvent(EventDto, identity, event.id)
		checkoutOrder(OrderSessionDto, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.first().id, 2)])
		checkoutOrder(OrderSessionDto, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.first().id, 3)])
		checkoutOrder(OrderSessionDto, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.first().id, 5)])

		assert ordersSessionsRegistry.size() == 3
		assert event.tickets.first().appliedDates.size() == 2
		assert event.tickets.first().appliedDates.containsAll([dates[0], dates[2]])

		when:
		TicketClassDto response = applyTicketScheduleDates(TicketClassDto, identity, event.id, event.tickets.first().id, givenDates.collect {
			dates[it]
		})

		then:
		with(response as TicketClassDto.Free) {
			appliedDates.size() == givenDates.size()
			appliedDates.containsAll(givenDates.collect { dates[it] })
			isCloseBeforeNow(modified)
			modifiedBy == identity.userId
		}

		eventsRepository.findOne(event.id).get().tickets.size() == 1
		with(eventsRepository.findOne(event.id).get().tickets.find { it.id == response.id } as TicketClass.Free) {
			appliedDates.size() == givenDates.size()
			appliedDates.collect { it.date.asDateTime() }.containsAll(givenDates.collect { dates[it].asDateTime() })
			isCloseBeforeNow(modified)
			modifiedBy == identity.userId
		}

		ordersSessionsRegistry.isEmpty()

		where:
		givenDates << [
				[0, 1],
				[0],
				[2, 0, 1]
		]

	}

	@Transactional
	def "Should not apply schedule dates to ticket of event with single-day schedule (there is no other date that could be applied)"() {
		given:
		UserIdentity identity = UserIdentity.builder().accountId(AccountsApiMockConfiguration.ACCOUNT_ID).userId(UUID.randomUUID()).build()
		ScheduleDto.Date date = assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 27, 19, 30))
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand(
				"Beer fest",
				null,
				EventCategory.MUSIC,
				RefundPolicy.NO_REFUNDS,
				FeePolicy.INCLUDED_IN_PRICE,
				assembleEventLocationDto(),
				[assembleCreateFreeTicketCommand([date])],
				null,
				ScheduleDto.SingleDay.builder().dates([date] as Set).build()
		))

		assert event.tickets.first().appliedDates.size() == 1
		assert event.tickets.first().appliedDates.containsAll([date])

		when:
		TicketClassDto response = applyTicketScheduleDates(TicketClassDto, identity, event.id, event.tickets.first().id, [date])

		then:
		with(response as TicketClassDto.Free) {
			appliedDates.size() == 1
			appliedDates.containsAll([date])
			modified == null
			modifiedBy == null
		}

		eventsRepository.findOne(event.id).get().tickets.size() == 1
		with(eventsRepository.findOne(event.id).get().tickets.find { it.id == response.id } as TicketClass.Free) {
			appliedDates.size() == 1
			appliedDates.collect { it.date.asDateTime() }.containsAll([date.asDateTime()])
			modified == null
			modifiedBy == null
		}

		ordersSessionsRegistry.isEmpty()

	}

	@Transactional
	def "Should not apply schedule dates to ticket of event with multi-day schedule successfully when given dates are equal to current"() {
		given:
		UserIdentity identity = UserIdentity.builder().accountId(AccountsApiMockConfiguration.ACCOUNT_ID).userId(UUID.randomUUID()).build()
		ScheduleDto.Date[] dates = [
				assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 27, 19, 30)),
				assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30)),
				assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 28, 19, 30))
		]
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand(
				"Beer fest",
				null,
				EventCategory.MUSIC,
				RefundPolicy.NO_REFUNDS,
				FeePolicy.INCLUDED_IN_PRICE,
				assembleEventLocationDto(),
				[assembleCreateFreeTicketCommand([dates[0], dates[2]])],
				null,
				ScheduleDto.MultiDay.builder().dates([dates[0], dates[1], dates[2]] as Set).build()
		))
		publishEvent(EventDto, identity, event.id)
		checkoutOrder(OrderSessionDto, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.first().id, 2)])
		checkoutOrder(OrderSessionDto, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.first().id, 3)])
		checkoutOrder(OrderSessionDto, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.first().id, 5)])

		assert ordersSessionsRegistry.size() == 3
		assert event.tickets.first().appliedDates.size() == 2
		assert event.tickets.first().appliedDates.containsAll([dates[0], dates[2]])

		when:
		TicketClassDto response = applyTicketScheduleDates(TicketClassDto, identity, event.id, event.tickets.first().id, givenDates.collect {
			dates[it]
		})

		then:
		with(response as TicketClassDto.Free) {
			appliedDates == event.tickets.first().appliedDates
			modified == null
			modifiedBy == null
		}

		eventsRepository.findOne(event.id).get().tickets.size() == 1
		with(eventsRepository.findOne(event.id).get().tickets.find { it.id == response.id } as TicketClass.Free) {
			appliedDates.collect { it.date.asDateTime() } == event.tickets.first().appliedDates.collect { it.asDateTime() }
			modified == null
			modifiedBy == null
		}

		ordersSessionsRegistry.size() == 3

		where:
		givenDates << [
				[0, 2],
				[2, 0]
		]

	}

	@Transactional
	def "Should not apply schedule dates to ticket when there are orders for this ticket"() {
		given:
		UserIdentity identity = UserIdentity.builder().accountId(AccountsApiMockConfiguration.ACCOUNT_ID).userId(UUID.randomUUID()).build()
		ScheduleDto.Date[] dates = [
				assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 27, 19, 30)),
				assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30)),
				assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 28, 19, 30))
		]
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand(
				"Beer fest",
				null,
				EventCategory.MUSIC,
				RefundPolicy.NO_REFUNDS,
				FeePolicy.INCLUDED_IN_PRICE,
				assembleEventLocationDto(),
				[assembleCreateFreeTicketCommand([dates[0], dates[2]])],
				null,
				ScheduleDto.MultiDay.builder().dates([dates[0], dates[1], dates[2]] as Set).build()
		))
		publishEvent(EventDto, identity, event.id)
		checkoutOrder(OrderSessionDto, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.first().id, 2)])
		checkoutOrder(OrderSessionDto, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.first().id, 3)])
		checkoutOrder(OrderSessionDto, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.first().id, 5)])
		checkoutAndConfirmOrder(event.id, [new CheckoutOrderCommand.Ticket(event.tickets.first().id, 2)])
		checkoutAndConfirmOrder(event.id, [new CheckoutOrderCommand.Ticket(event.tickets.first().id, 3)])

		assert ordersSessionsRegistry.size() == 3
		assert event.tickets.first().appliedDates.size() == 2
		assert event.tickets.first().appliedDates.containsAll([dates[0], dates[2]])

		when:
		ApiError response = applyTicketScheduleDates(ApiError, identity, event.id, event.tickets.first().id, [dates[0]])

		then:
		response.code == EventsErrorCode.ORDERS_FOR_TICKET_EXISTS.name()

		eventsRepository.findOne(event.id).get().tickets.size() == 1
		with(eventsRepository.findOne(event.id).get().tickets.find { it.id == event.tickets.first().id } as TicketClass.Free) {
			appliedDates.collect { it.date.asDateTime() } == event.tickets.first().appliedDates.collect { it.asDateTime() }
			modified == null
			modifiedBy == null
		}

		ordersSessionsRegistry.size() == 3

	}

	@Transactional
	def "Should not apply schedule dates to ticket when ticket doesn't exist"() {
		given:
		UserIdentity identity = UserIdentity.builder().accountId(AccountsApiMockConfiguration.ACCOUNT_ID).userId(UUID.randomUUID()).build()
		ScheduleDto.Date[] dates = [
				assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 27, 19, 30)),
				assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30)),
				assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 28, 19, 30))
		]
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand(
				"Beer fest",
				null,
				EventCategory.MUSIC,
				RefundPolicy.NO_REFUNDS,
				FeePolicy.INCLUDED_IN_PRICE,
				assembleEventLocationDto(),
				[assembleCreateFreeTicketCommand([dates[0], dates[2]])],
				null,
				ScheduleDto.MultiDay.builder().dates([dates[0], dates[1], dates[2]] as Set).build()
		))

		assert event.tickets.first().appliedDates.size() == 2
		assert event.tickets.first().appliedDates.containsAll([dates[0], dates[2]])

		when:
		ApiError response = applyTicketScheduleDates(ApiError, identity, event.id, UUID.randomUUID(), [dates[0]])

		then:
		response.code == EventsErrorCode.TICKET_CLASS_NOT_FOUND.name()

		eventsRepository.findOne(event.id).get().tickets.size() == 1
		with(eventsRepository.findOne(event.id).get().tickets.find { it.id == event.tickets.first().id } as TicketClass.Free) {
			appliedDates.collect { it.date.asDateTime() } == event.tickets.first().appliedDates.collect { it.asDateTime() }
			modified == null
			modifiedBy == null
		}

	}

	def "Should not apply schedule dates to ticket when event has illegal status"() {
		given:
		UserIdentity identity = UserIdentity.builder().accountId(AccountsApiMockConfiguration.ACCOUNT_ID).userId(UUID.randomUUID()).build()
		ScheduleDto.Date[] dates = [
				assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 27, 19, 30)),
				assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30)),
				assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 28, 19, 30))
		]
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand(
				"Beer fest",
				null,
				EventCategory.MUSIC,
				RefundPolicy.NO_REFUNDS,
				FeePolicy.INCLUDED_IN_PRICE,
				assembleEventLocationDto(),
				[assembleCreateFreeTicketCommand([dates[0], dates[2]])],
				null,
				ScheduleDto.MultiDay.builder().dates([dates[0], dates[1], dates[2]] as Set).build()
		))

		assert event.tickets.first().appliedDates.size() == 2
		assert event.tickets.first().appliedDates.containsAll([dates[0], dates[2]])

		when:
		with(eventsRepository.findOne(event.id).get()) { Event e ->
			e.setStatus(eventStatus)
			eventsRepository.save(e)
		}
		ApiError response = applyTicketScheduleDates(ApiError, identity, event.id, event.tickets.first().id, [dates[0]])

		then:
		response.code == code.name()

		where:
		eventStatus          | code
		EventStatus.STARTED  | EventsErrorCode.EVENT_STARTED
		EventStatus.FINISHED | EventsErrorCode.EVENT_FINISHED
		EventStatus.DELETED  | EventsErrorCode.EVENT_NOT_FOUND

	}

	@Transactional
	def "Should not apply schedule dates to ticket when event doesn't belongs to given account"() {
		given:
		UserIdentity identity = UserIdentity.builder().accountId(AccountsApiMockConfiguration.ACCOUNT_ID).userId(UUID.randomUUID()).build()
		ScheduleDto.Date[] dates = [
				assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 27, 19, 30)),
				assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30)),
				assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 28, 19, 30))
		]
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand(
				"Beer fest",
				null,
				EventCategory.MUSIC,
				RefundPolicy.NO_REFUNDS,
				FeePolicy.INCLUDED_IN_PRICE,
				assembleEventLocationDto(),
				[assembleCreateFreeTicketCommand([dates[0], dates[2]])],
				null,
				ScheduleDto.MultiDay.builder().dates([dates[0], dates[1], dates[2]] as Set).build()
		))

		assert event.tickets.first().appliedDates.size() == 2
		assert event.tickets.first().appliedDates.containsAll([dates[0], dates[2]])

		when:
		ApiError response = applyTicketScheduleDates(ApiError, UserIdentity.builder().accountId(UUID.randomUUID()).userId(UUID.randomUUID()).build(), event.id, event.tickets.first().id, [dates[0]])

		then:
		response.code == ApplicationException.CommonsErrorCode.UNAUTHORIZED_ACCESS.name()

		eventsRepository.findOne(event.id).get().tickets.size() == 1
		with(eventsRepository.findOne(event.id).get().tickets.find { it.id == event.tickets.first().id } as TicketClass.Free) {
			appliedDates.collect { it.date.asDateTime() } == event.tickets.first().appliedDates.collect { it.asDateTime() }
			modified == null
			modifiedBy == null
		}

	}

}
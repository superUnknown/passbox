package pl.passbox.events.events

import com.neovisionaries.i18n.CountryCode
import com.neovisionaries.i18n.CurrencyCode
import org.springframework.transaction.annotation.Transactional
import pl.passbox.api.events.configuration.EventsErrorCode
import pl.passbox.api.events.events.model.CreateEventCommand
import pl.passbox.api.events.events.model.EventDto
import pl.passbox.api.events.events.model.UpdateEventCommand
import pl.passbox.api.events.events.model.types.EventCategory
import pl.passbox.api.events.events.model.types.EventStatus
import pl.passbox.api.events.events.model.types.RefundPolicy
import pl.passbox.api.events.orders.model.CheckoutOrderCommand
import pl.passbox.api.events.orders.model.ConfirmOrderCommand
import pl.passbox.api.events.orders.model.OrderSessionDto
import pl.passbox.api.events.schedules.model.ScheduleDateStatus
import pl.passbox.api.events.schedules.model.ScheduleDto
import pl.passbox.api.events.schedules.model.ScheduleType
import pl.passbox.api.events.tickets.model.TicketClassDto
import pl.passbox.api.events.tickets.model.types.TicketClassStatus
import pl.passbox.api.events.tickets.model.types.TicketClassType
import pl.passbox.api.messages.model.EventLocationChangedNotificationEmail
import pl.passbox.api.messages.model.EventLocationChangedNotificationWebSocket
import pl.passbox.api.messages.model.EventScheduleChangedNotificationEmail
import pl.passbox.api.messages.model.EventScheduleChangedNotificationWebSocket
import pl.passbox.api.messages.model.OrderSessionInvalidatedWebSocket
import pl.passbox.api.messages.model.Type
import pl.passbox.api.payments.charges.model.FeePolicy
import pl.passbox.commons.configuration.ApiError
import pl.passbox.commons.configuration.ApplicationException
import pl.passbox.commons.identity.model.UserIdentity
import pl.passbox.events.AccountsApiMockConfiguration
import pl.passbox.events.EventsSpecIT
import pl.passbox.events.events.model.Event
import pl.passbox.events.schedules.model.Schedule
import pl.passbox.events.tickets.model.TicketClass

import java.time.Instant
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.Year
import java.util.stream.Collectors

class EventsApiSpecIT extends EventsSpecIT {

	def "Should not create event due to constraints validation errors on event properties"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()


		when:
		ApiError response = createEvent(ApiError, identity, CreateEventCommand.builder()
				.name(name)
				.totalCapacity(capacity)
				.category(category)
				.refundPolicy(refundPolicy)
				.feePolicy(feePolicy)
				.location(assembleEventLocationDto(locationName, locationCountry, locationCity, null, null, null, null))
				.schedule(ScheduleDto.SingleDay.builder().dates([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 45))] as Set).build())
				.tickets([assembleCreateFreeTicketCommand([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 39))], "Darmowy bilet", "bez piwa", 1, 5, null, null, 4)])
				.build()
		)

		then:
		response.code == ApplicationException.CommonsErrorCode.BAD_REQUEST.name()
		eventsRepository.findAll().count() == 0

		where:
		name    | capacity | category               | refundPolicy                     | feePolicy                   | locationName | locationCountry | locationCity
		""      | null     | EventCategory.MUSIC    | RefundPolicy.NO_REFUNDS          | FeePolicy.INCLUDED_IN_PRICE | "Browar"     | CountryCode.PL  | "Gdańsk"
		" "     | null     | EventCategory.MUSIC    | RefundPolicy.NO_REFUNDS          | FeePolicy.INCLUDED_IN_PRICE | "Browar"     | CountryCode.PL  | "Gdańsk"
		null    | null     | EventCategory.MUSIC    | RefundPolicy.ONE_DAY_UNTIL_EVENT | FeePolicy.INCLUDED_IN_PRICE | "Browar"     | CountryCode.PL  | "Gdańsk"
		"valid" | null     | null                   | RefundPolicy.NO_REFUNDS          | FeePolicy.INCLUDED_IN_PRICE | "Browar"     | CountryCode.PL  | "Gdańsk"
		"valid" | null     | EventCategory.BUSINESS | null                             | FeePolicy.INCLUDED_IN_PRICE | "Browar"     | CountryCode.PL  | "Gdańsk"
		"valid" | null     | EventCategory.BUSINESS | RefundPolicy.NO_REFUNDS          | null                        | "Browar"     | CountryCode.PL  | "Gdańsk"
		"valid" | null     | EventCategory.BUSINESS | RefundPolicy.NO_REFUNDS          | FeePolicy.APPENDED_TO_PRICE | ""           | CountryCode.PL  | "Gdańsk"
		"valid" | null     | EventCategory.BUSINESS | RefundPolicy.NO_REFUNDS          | FeePolicy.APPENDED_TO_PRICE | " "          | CountryCode.PL  | "Gdańsk"
		"valid" | null     | EventCategory.BUSINESS | RefundPolicy.NO_REFUNDS          | FeePolicy.APPENDED_TO_PRICE | null         | CountryCode.PL  | "Gdańsk"
		"valid" | null     | EventCategory.BUSINESS | RefundPolicy.NO_REFUNDS          | FeePolicy.APPENDED_TO_PRICE | "Browar"     | null            | "Gdańsk"
		"valid" | null     | EventCategory.BUSINESS | RefundPolicy.NO_REFUNDS          | FeePolicy.APPENDED_TO_PRICE | "Browar"     | CountryCode.PL  | ""
		"valid" | null     | EventCategory.BUSINESS | RefundPolicy.NO_REFUNDS          | FeePolicy.APPENDED_TO_PRICE | "Browar"     | CountryCode.PL  | " "
		"valid" | null     | EventCategory.BUSINESS | RefundPolicy.NO_REFUNDS          | FeePolicy.APPENDED_TO_PRICE | "Browar"     | CountryCode.PL  | null
		"valid" | 0        | EventCategory.BUSINESS | RefundPolicy.NO_REFUNDS          | FeePolicy.APPENDED_TO_PRICE | "Browar"     | CountryCode.PL  | "Gdańsk"
		"valid" | -1       | EventCategory.BUSINESS | RefundPolicy.NO_REFUNDS          | FeePolicy.APPENDED_TO_PRICE | "Browar"     | CountryCode.PL  | "Gdańsk"

	}

	def "Should not create event due to constraints validation errors on schedule properties"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()

		when:
		ApiError response = createEvent(ApiError, identity, CreateEventCommand.builder()
				.name("valid")
				.category(EventCategory.BUSINESS)
				.refundPolicy(RefundPolicy.NO_REFUNDS)
				.feePolicy(FeePolicy.APPENDED_TO_PRICE)
				.location(assembleEventLocationDto())
				.schedule(schedule)
				.tickets([assembleCreateFreeTicketCommand([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 39))], "Darmowy bilet", "bez piwa", 1, 5, null, null)])
				.build()
		)

		then:
		response.code == ApplicationException.CommonsErrorCode.BAD_REQUEST.name()
		eventsRepository.findAll().count() == 0

		where:
		schedule << [
				ScheduleDto.SingleDay.builder().build(),
				ScheduleDto.SingleDay.builder().dates([assembleScheduleDate(LocalDateTime.of(Year.now().value - 1, 1, 20, 17, 00, 00))] as Set).build(),
				ScheduleDto.SingleDay.builder().dates([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 20, 17, 00, 01)), assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 20, 18, 00, 00))] as Set).build(),
				ScheduleDto.SingleDay.builder().dates([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 19, 17, 00, 00)), assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 20, 17, 00, 00))] as Set).build(),
				ScheduleDto.MultiDay.builder().build(),
				ScheduleDto.MultiDay.builder().dates([assembleScheduleDate(LocalDateTime.of(Year.now().value - 1, 1, 20, 17, 00, 00))] as Set).build(),
				ScheduleDto.MultiDay.builder().dates([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 20, 17, 00, 00))] as Set).build(),
				ScheduleDto.MultiDay.builder().dates([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 20, 17, 00, 00)), assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 20, 17, 00, 55))] as Set).build(),
		]

	}

	def "Should not create event when ticket applied date is not defined"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()

		when:
		ApiError response = createEvent(ApiError, identity, CreateEventCommand.builder()
				.name("valid")
				.category(EventCategory.BUSINESS)
				.refundPolicy(RefundPolicy.NO_REFUNDS)
				.feePolicy(FeePolicy.APPENDED_TO_PRICE)
				.location(assembleEventLocationDto())
				.schedule(ScheduleDto.MultiDay.builder().dates([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 19, 12, 00, 00)), assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 20, 12, 00, 00))] as Set).build())
				.tickets([assembleCreateFreeTicketCommand([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 20, 17, 00, 01))], "Darmowy bilet", "bez piwa", 1, 5, null, null)])
				.build()
		)

		then:
		response.code == EventsErrorCode.APPLIED_DATE_NOT_DEFINED.name()
		eventsRepository.findAll().count() == 0

	}

	@Transactional
	def "Should create event successfully when single day schedule and one free ticket is defined"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()

		when:
		EventDto response = createEvent(EventDto, identity, CreateEventCommand.builder()
				.name("Beer fest")
				.description(eventDescription)
				.totalCapacity(50)
				.category(EventCategory.MUSIC)
				.refundPolicy(RefundPolicy.NO_REFUNDS)
				.feePolicy(FeePolicy.INCLUDED_IN_PRICE)
				.location(assembleEventLocationDto())
				.schedule(ScheduleDto.SingleDay.builder().dates([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 45))] as Set).build())
				.tickets([assembleCreateFreeTicketCommand([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 39))], "Darmowy bilet", "bez piwa", ticketMinPerOrder, ticketMaxPerOrder, ticketSaleStart, ticketSaleEnd, ticketQuantity)])
				.build()
		)

		then:
		response.id != null
		response.accountId == identity.accountId
		response.status == EventStatus.UNPUBLISHED
		response.name == "Beer fest"
		response.description == eventDescription
		response.totalCapacity == 50
		response.category == EventCategory.MUSIC
		response.refundPolicy == RefundPolicy.NO_REFUNDS
		response.feePolicy == FeePolicy.INCLUDED_IN_PRICE
		response.currency == CurrencyCode.PLN
		response.location == assembleEventLocationDto()
		isCloseBeforeNow(response.created)
		response.createdBy == identity.userId
		response.modified == null
		response.modifiedBy == null
		with(response.schedule as ScheduleDto.SingleDay) {
			dates.size() == 1
			dates.first().asDateTime() == LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 00)
			type == ScheduleType.SINGLE_DAY

		}
		response.tickets.size() == 1
		with(response.tickets.first() as TicketClassDto.Free) {
			id != null
			status == TicketClassStatus.ACTIVE
			name == "Darmowy bilet"
			description == "bez piwa"
			quantity == ticketQuantity
			minPerOrder == ticketMinPerOrder
			maxPerOrder == ticketMaxPerOrder
			saleStart == ticketSaleStart
			saleEnd == ticketSaleEnd
			appliedDates.size() == 1
			appliedDates.first().asDateTime() == LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 00)
			isCloseBeforeNow(created)
			createdBy == response.createdBy
			modified == null
			modifiedBy == null

		}

		with(eventsRepository.findOne(response.id).get()) {
			accountId == response.accountId
			status == response.status
			name == response.name
			description == response.description
			totalCapacity == response.totalCapacity
			category == response.category
			refundPolicy == response.refundPolicy
			feePolicy == response.feePolicy
			currency == response.currency
			location.name == response.location.name
			location.country == response.location.country
			location.city == response.location.city
			location.street == response.location.street
			location.house == response.location.house
			location.flat == response.location.flat
			location.postalCode == response.location.postalCode
			created == response.created
			createdBy == response.createdBy
			modified == response.modified
			modifiedBy == response.modifiedBy

		}

		with(eventsRepository.findOne(response.id).get().schedule as Schedule.SingleDay) {
			id != null
			type == response.schedule.type
			dates.size() == 1
			dates.first().asDateTime() == response.schedule.dates.first().asDateTime()
			dates.each { it.status == ScheduleDateStatus.ACTIVE }
		}

		eventsRepository.findOne(response.id).get().tickets.size() == 1
		with(eventsRepository.findOne(response.id).get().tickets.first() as TicketClass.Free) {
			status == TicketClassStatus.ACTIVE
			name == "Darmowy bilet"
			description == "bez piwa"
			quantity == ticketQuantity
			minPerOrder == ticketMinPerOrder
			maxPerOrder == ticketMaxPerOrder
			saleStart == ticketSaleStart
			saleEnd == ticketSaleEnd
			appliedDates.size() == 1
			appliedDates.first().id != null
			appliedDates.first().date.asDateTime() == response.schedule.getDates().first().asDateTime()
			isCloseBeforeNow(created)
			createdBy == response.createdBy
			modified == null
			modifiedBy == null
		}

		where:
		eventDescription | ticketQuantity | ticketMinPerOrder | ticketMaxPerOrder | ticketSaleStart | ticketSaleEnd
		null             | null           | null              | null              | null            | null
		"given"          | 1              | 5                 | 6                 | Instant.now()   | Instant.now().plusSeconds(5000)

	}

	@Transactional
	def "Should create event successfully when one multi day term, one free and one paid ticket is defined"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()

		when:
		EventDto response = createEvent(EventDto, identity, CreateEventCommand.builder()
				.name("Beer fest")
				.category(EventCategory.MUSIC)
				.refundPolicy(RefundPolicy.NO_REFUNDS)
				.feePolicy(FeePolicy.INCLUDED_IN_PRICE)
				.location(assembleEventLocationDto())
				.schedule(ScheduleDto.MultiDay.builder().dates(
				[
						assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 45)),
						assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 28, 15, 25, 45)),
						assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 29, 19, 30, 45))
				] as Set).build())
				.tickets([
				assembleCreateFreeTicketCommand([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 39))], "Darmowy bilet", "bez piwa", 1, 5, null, null),
				assembleCreatePaidTicketCommand([
						assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 39)),
						assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 28, 15, 25, 45)),
						assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 29, 19, 30, 45)),
				], "Płatny bilet", "z piwem", BigDecimal.ONE, null, 5, null, null)
		])
				.build()
		)

		then:
		response.id != null
		response.accountId == identity.accountId
		response.status == EventStatus.UNPUBLISHED
		response.name == "Beer fest"
		response.description == null
		response.totalCapacity == null
		response.category == EventCategory.MUSIC
		response.refundPolicy == RefundPolicy.NO_REFUNDS
		response.feePolicy == FeePolicy.INCLUDED_IN_PRICE
		response.currency == CurrencyCode.PLN
		response.location == assembleEventLocationDto()
		isCloseBeforeNow(response.created)
		response.createdBy == identity.userId
		response.modified == null
		response.modifiedBy == null
		with(response.schedule as ScheduleDto.MultiDay) {
			type == ScheduleType.MULTI_DAY
			dates.size() == 3
			dates.collect { it.asDateTime() }.sort { it }.containsAll([
					LocalDateTime.of(Year.now().value + 1, 1, 28, 15, 25, 00),
					LocalDateTime.of(Year.now().value + 1, 1, 29, 19, 30, 00),
					LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 00)
			])

		}

		response.tickets.size() == 2
		with(response.tickets.find { it.type == TicketClassType.FREE } as TicketClassDto.Free) {
			id != null
			status == TicketClassStatus.ACTIVE
			name == "Darmowy bilet"
			description == "bez piwa"
			quantity == null
			minPerOrder == 1
			maxPerOrder == 5
			saleStart == null
			saleEnd == null
			appliedDates.size() == 1
			appliedDates.first().asDateTime() == LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 00)
			isCloseBeforeNow(created)
			createdBy == response.createdBy
			modified == null
			modifiedBy == null

		}

		with(response.tickets.find { it.type == TicketClassType.PAID } as TicketClassDto.Paid) {
			id != null
			status == TicketClassStatus.ACTIVE
			name == "Płatny bilet"
			description == "z piwem"
			quantity == null
			price == BigDecimal.ONE
			minPerOrder == null
			maxPerOrder == 5
			saleStart == null
			saleEnd == null
			appliedDates.size() == 3
			appliedDates.sort { it.asDateTime() }[0].asDateTime() == LocalDateTime.of(Year.now().value + 1, 1, 28, 15, 25, 00)
			appliedDates.sort { it.asDateTime() }[1].asDateTime() == LocalDateTime.of(Year.now().value + 1, 1, 29, 19, 30, 00)
			appliedDates.sort { it.asDateTime() }[2].asDateTime() == LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 00)
			isCloseBeforeNow(created)
			createdBy == response.createdBy
			modified == null
			modifiedBy == null

		}

		with(eventsRepository.findOne(response.id).get()) {
			accountId == response.accountId
			status == response.status
			name == response.name
			description == response.description
			category == response.category
			totalCapacity == response.totalCapacity
			refundPolicy == response.refundPolicy
			feePolicy == response.feePolicy
			currency == response.currency
			location.name == response.location.name
			location.country == response.location.country
			location.city == response.location.city
			location.street == response.location.street
			location.house == response.location.house
			location.flat == response.location.flat
			location.postalCode == response.location.postalCode
			created == response.created
			createdBy == response.createdBy
			modified == response.modified
			modifiedBy == response.modifiedBy

		}

		with(eventsRepository.findOne(response.id).get().schedule as Schedule.MultiDay) {
			type == response.schedule.type
			dates.size() == 3
			dates.each { it.status == ScheduleDateStatus.ACTIVE }
			dates.collect { it.asDateTime() }.sort { it }.containsAll([
					LocalDateTime.of(Year.now().value + 1, 1, 28, 15, 25, 00),
					LocalDateTime.of(Year.now().value + 1, 1, 29, 19, 30, 00),
					LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 00)
			])
		}

		eventsRepository.findOne(response.id).get().tickets.size() == 2
		with(eventsRepository.findOne(response.id).get().tickets.find { it.type == TicketClassType.FREE } as TicketClass.Free) {
			status == TicketClassStatus.ACTIVE
			name == "Darmowy bilet"
			description == "bez piwa"
			minPerOrder == 1
			maxPerOrder == 5
			saleStart == null
			saleEnd == null
			quantity == null
			appliedDates.size() == 1
			appliedDates.first().id != null
			appliedDates.first().date.asDateTime() == LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 00)
			isCloseBeforeNow(created)
			createdBy == response.createdBy
			modified == null
			modifiedBy == null
		}

		with(eventsRepository.findOne(response.id).get().tickets.find { it.type == TicketClassType.PAID } as TicketClass.Paid) {
			status == TicketClassStatus.ACTIVE
			name == "Płatny bilet"
			description == "z piwem"
			price == BigDecimal.ONE
			minPerOrder == null
			maxPerOrder == 5
			saleStart == null
			saleEnd == null
			appliedDates.size() == 3
			appliedDates.sort { it.date.asDateTime() }[0].date.asDateTime() == LocalDateTime.of(Year.now().value + 1, 1, 28, 15, 25, 00)
			appliedDates.sort { it.date.asDateTime() }[1].date.asDateTime() == LocalDateTime.of(Year.now().value + 1, 1, 29, 19, 30, 00)
			appliedDates.sort { it.date.asDateTime() }[2].date.asDateTime() == LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 00)
			isCloseBeforeNow(created)
			createdBy == response.createdBy
			modified == null
			modifiedBy == null

		}

	}

	def "Should update event successfully when no tickets are given"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()
		when:
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand("Beer fest", null, EventCategory.MUSIC, RefundPolicy.NO_REFUNDS, FeePolicy.INCLUDED_IN_PRICE))
		publishEvent(EventDto, identity, event.id)
		checkoutOrder(OrderSessionDto, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.first().id, 2)])
		checkoutOrder(OrderSessionDto, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.first().id, 4)])

		EventDto response = updateEvent(EventDto, identity, event.id, "New fest", "desc", EventCategory.BUSINESS, RefundPolicy.ONE_DAY_UNTIL_EVENT, FeePolicy.APPENDED_TO_PRICE, event.schedule, 30)

		then:
		response.id != null
		response.accountId == identity.accountId
		response.status == EventStatus.PUBLISHED
		response.name == "New fest"
		response.description == "desc"
		response.totalCapacity == 30
		response.category == EventCategory.BUSINESS
		response.refundPolicy == RefundPolicy.ONE_DAY_UNTIL_EVENT
		response.feePolicy == FeePolicy.APPENDED_TO_PRICE
		response.currency == CurrencyCode.PLN
		response.location == assembleEventLocationDto()
		isCloseBeforeNow(response.created)
		response.createdBy == identity.userId
		isCloseBeforeNow(response.modified)
		response.modifiedBy == identity.userId
		with(response.schedule as ScheduleDto.SingleDay) {
			type == ScheduleType.SINGLE_DAY
			dates.size() == 1

		}

		response.tickets.size() == 1
		with(response.tickets.find { it.type == TicketClassType.FREE } as TicketClassDto.Free) {
			id != null
			status == TicketClassStatus.ACTIVE
			name == "Darmowy bilet"
			description == "bez piwa"
			minPerOrder == null
			maxPerOrder == null
			saleStart == null
			saleEnd == null
			appliedDates.size() == 1
			isCloseBeforeNow(created)
			createdBy == response.createdBy
			modified == null
			modifiedBy == null

		}

		with(eventsRepository.findOne(response.id).get()) {
			accountId == response.accountId
			status == response.status
			name == response.name
			description == response.description
			category == response.category
			totalCapacity == response.totalCapacity
			refundPolicy == response.refundPolicy
			feePolicy == response.feePolicy
			currency == response.currency
			location.name == response.location.name
			location.country == response.location.country
			location.city == response.location.city
			location.street == response.location.street
			location.house == response.location.house
			location.flat == response.location.flat
			location.postalCode == response.location.postalCode
			created == response.created
			createdBy == response.createdBy
			modified == response.modified
			modifiedBy == response.modifiedBy

		}
		messagesRegistry.isEmpty()

	}

	def "Should update event successfully when ticket is given"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()
		when:
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand(
				"Beer fest",
				null,
				EventCategory.MUSIC,
				RefundPolicy.NO_REFUNDS,
				FeePolicy.INCLUDED_IN_PRICE,
				assembleEventLocationDto(),
				[
						assembleCreateFreeTicketCommand([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 39))], "Darmowy bilet", "bez piwa", null, null, null, null),
						assembleCreatePaidTicketCommand([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 39))], "Płatny bilet", "z piwem", BigDecimal.TEN, 1, 10, Instant.now().minusSeconds(120), null),
				],
				15
		))
		publishEvent(EventDto, identity, event.id)
		checkoutOrder(OrderSessionDto, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.first().id, 2)])
		checkoutOrder(OrderSessionDto, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.last().id, 4)])

		EventDto response = updateEvent(
				EventDto,
				identity,
				event.id,
				"New fest",
				"desc",
				EventCategory.BUSINESS,
				RefundPolicy.ONE_DAY_UNTIL_EVENT,
				FeePolicy.APPENDED_TO_PRICE,
				event.schedule,
				null,
				[
						UpdateEventCommand.Ticket.builder()
								.ticketClassId(event.tickets.find { it.type == TicketClassType.PAID }.id)
								.ticket(assembleUpdatePaidTicketCommand("Płatny bilet updated", "z piwem", 45.99, null, 2, Instant.now().minusSeconds(120), Instant.now().plusSeconds(240)))
								.build()
				]
		)

		then:
		response.id != null
		response.accountId == identity.accountId
		response.status == EventStatus.PUBLISHED
		response.name == "New fest"
		response.description == "desc"
		response.totalCapacity == null
		response.category == EventCategory.BUSINESS
		response.refundPolicy == RefundPolicy.ONE_DAY_UNTIL_EVENT
		response.feePolicy == FeePolicy.APPENDED_TO_PRICE
		response.currency == CurrencyCode.PLN
		response.location == assembleEventLocationDto()
		isCloseBeforeNow(response.created)
		response.createdBy == identity.userId
		isCloseBeforeNow(response.modified)
		response.modifiedBy == identity.userId

		with(response.schedule as ScheduleDto.SingleDay) {
			type == ScheduleType.SINGLE_DAY
			dates.size() == 1
		}

		response.tickets.size() == 2
		with(response.tickets.find { it.type == TicketClassType.FREE } as TicketClassDto.Free) {
			id != null
			status == TicketClassStatus.ACTIVE
			name == "Darmowy bilet"
			description == "bez piwa"
			minPerOrder == null
			maxPerOrder == null
			saleStart == null
			saleEnd == null
			appliedDates.size() == 1
			isCloseBeforeNow(created)
			createdBy == response.createdBy
			modified == null
			modifiedBy == null

		}
		with(response.tickets.find { it.type == TicketClassType.PAID } as TicketClassDto.Paid) {
			id != null
			status == TicketClassStatus.ACTIVE
			name == "Płatny bilet updated"
			description == "z piwem"
			minPerOrder == null
			maxPerOrder == 2
			saleStart != null
			saleEnd != null
			price == 45.99
			appliedDates.size() == 1
			isCloseBeforeNow(created)
			createdBy == response.createdBy
			modified != null
			modifiedBy == identity.userId

		}

		with(eventsRepository.findOne(response.id).get()) {
			accountId == response.accountId
			status == response.status
			name == response.name
			description == response.description
			category == response.category
			totalCapacity == response.totalCapacity
			refundPolicy == response.refundPolicy
			feePolicy == response.feePolicy
			currency == response.currency
			location.name == response.location.name
			location.country == response.location.country
			location.city == response.location.city
			location.street == response.location.street
			location.house == response.location.house
			location.flat == response.location.flat
			location.postalCode == response.location.postalCode
			created == response.created
			createdBy == response.createdBy
			modified == response.modified
			modifiedBy == response.modifiedBy

		}

		with(ticketClassesRepository.findOne(response.tickets.find { it.type == TicketClassType.FREE }.id).get()) {
			modifiedBy == null
		}

		with(ticketClassesRepository.findOne(response.tickets.find { it.type == TicketClassType.PAID }.id).get()) {
			modifiedBy == identity.userId
		}
		messagesRegistry.isEmpty()

	}

	@Transactional
	def "Should update event successfully when location & schedule is updated and there are no orders"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand(
				"Beer fest",
				null,
				EventCategory.MUSIC,
				RefundPolicy.NO_REFUNDS,
				FeePolicy.INCLUDED_IN_PRICE,
				assembleEventLocationDto(),
				[
						assembleCreateFreeTicketCommand([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 39))], "Darmowy bilet", "bez piwa", null, null, null, null),
						assembleCreatePaidTicketCommand([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 39))], "Płatny bilet", "z piwem", BigDecimal.TEN, 1, 10, Instant.now().minusSeconds(120), null),
				],
				15
		))
		publishEvent(EventDto, identity, event.id)
		checkoutOrder(OrderSessionDto, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.first().id, 2)])
		checkoutOrder(OrderSessionDto, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.last().id, 4)])

		when:

		EventDto response = updateEvent(
				EventDto,
				identity,
				event.id,
				"New fest",
				"desc",
				EventCategory.BUSINESS,
				RefundPolicy.ONE_DAY_UNTIL_EVENT,
				FeePolicy.APPENDED_TO_PRICE,
				ScheduleDto.SingleDay.builder().dates([ScheduleDto.Date.builder().date(event.schedule.dates.first().date).time(LocalTime.of(11, 11)).build()] as Set).build(),
				null,
				[
						UpdateEventCommand.Ticket.builder()
								.ticketClassId(event.tickets.find { it.type == TicketClassType.PAID }.id)
								.ticket(assembleUpdatePaidTicketCommand("Płatny bilet updated", "z piwem", 45.99, null, 2, Instant.now().minusSeconds(120), Instant.now().plusSeconds(240)))
								.build()
				],
				assembleEventLocationDto("Browar Gdański II piętro", CountryCode.PL, "Gdynia")
		)

		then:
		response.id != null
		response.accountId == identity.accountId
		response.status == EventStatus.PUBLISHED
		response.name == "New fest"
		response.description == "desc"
		response.totalCapacity == null
		response.category == EventCategory.BUSINESS
		response.refundPolicy == RefundPolicy.ONE_DAY_UNTIL_EVENT
		response.feePolicy == FeePolicy.APPENDED_TO_PRICE
		response.currency == CurrencyCode.PLN
		response.location == assembleEventLocationDto("Browar Gdański II piętro", CountryCode.PL, "Gdynia")
		isCloseBeforeNow(response.created)
		response.createdBy == identity.userId
		isCloseBeforeNow(response.modified)
		response.modifiedBy == identity.userId

		with(response.schedule as ScheduleDto.SingleDay) {
			type == ScheduleType.SINGLE_DAY
			dates.size() == 1
			dates.first().asDateTime() == LocalDateTime.of(Year.now().value + 1, 1, 30, 11, 11, 00)
		}

		response.tickets.size() == 2
		with(response.tickets.find { it.type == TicketClassType.FREE } as TicketClassDto.Free) {
			id != null
			status == TicketClassStatus.ACTIVE
			name == "Darmowy bilet"
			description == "bez piwa"
			minPerOrder == null
			maxPerOrder == null
			saleStart == null
			saleEnd == null
			appliedDates.size() == 1
			isCloseBeforeNow(created)
			createdBy == response.createdBy
			modified == null
			modifiedBy == null

		}
		with(response.tickets.find { it.type == TicketClassType.PAID } as TicketClassDto.Paid) {
			id != null
			status == TicketClassStatus.ACTIVE
			name == "Płatny bilet updated"
			description == "z piwem"
			minPerOrder == null
			maxPerOrder == 2
			saleStart != null
			saleEnd != null
			price == 45.99
			appliedDates.size() == 1
			isCloseBeforeNow(created)
			createdBy == response.createdBy
			modified != null
			modifiedBy == identity.userId

		}

		with(eventsRepository.findOne(response.id).get()) {
			accountId == response.accountId
			status == response.status
			name == response.name
			description == response.description
			category == response.category
			totalCapacity == response.totalCapacity
			refundPolicy == response.refundPolicy
			feePolicy == response.feePolicy
			currency == response.currency
			location.name == response.location.name
			location.country == response.location.country
			location.city == response.location.city
			location.street == response.location.street
			location.house == response.location.house
			location.flat == response.location.flat
			location.postalCode == response.location.postalCode
			schedule.type == ScheduleType.SINGLE_DAY
			schedule.dates.first().asDateTime() == LocalDateTime.of(Year.now().value + 1, 1, 30, 11, 11, 00)
			created == response.created
			createdBy == response.createdBy
			modified == response.modified
			modifiedBy == response.modifiedBy

		}

		with(ticketClassesRepository.findOne(response.tickets.find { it.type == TicketClassType.FREE }.id).get()) {
			modifiedBy == null
		}

		with(ticketClassesRepository.findOne(response.tickets.find { it.type == TicketClassType.PAID }.id).get()) {
			modifiedBy == identity.userId
		}

		messagesRegistry.hasSize(2)

		messagesRegistry.getMessages(Type.EVENT_LOCATION_CHANGED_NOTIFICATION_WEB_SOCKET).size() == 1
		with(messagesRegistry.getMessages(Type.EVENT_LOCATION_CHANGED_NOTIFICATION_WEB_SOCKET).first() as EventLocationChangedNotificationWebSocket) {
			eventId == event.id
			location.name == "Browar Gdański II piętro"
			location.country == CountryCode.PL
			location.city == "Gdynia"
			location.street == "Długa"
			location.house == "15A"
			location.flat == "10"
			location.postalCode == "80-333"
		}

		messagesRegistry.getMessages(Type.EVENT_SCHEDULE_CHANGED_NOTIFICATION_WEB_SOCKET).size() == 1
		with(messagesRegistry.getMessages(Type.EVENT_SCHEDULE_CHANGED_NOTIFICATION_WEB_SOCKET).first() as EventScheduleChangedNotificationWebSocket) {
			eventId == event.id
			scheduleDates == response.schedule.dates.collect { it.asDateTime() }
		}

	}

	@Transactional
	def "Should update event successfully when location & schedule is updated and there are orders"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand(
				"Beer fest",
				null,
				EventCategory.MUSIC,
				RefundPolicy.NO_REFUNDS,
				FeePolicy.INCLUDED_IN_PRICE,
				assembleEventLocationDto(),
				[
						assembleCreateFreeTicketCommand([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 39))], "Darmowy bilet", "bez piwa", null, null, null, null),
						assembleCreatePaidTicketCommand([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 39))], "Płatny bilet", "z piwem", BigDecimal.TEN, 1, 10, Instant.now().minusSeconds(120), null),
				],
				15
		))
		publishEvent(EventDto, identity, event.id)
		checkoutAndConfirmOrder(event.id, [new CheckoutOrderCommand.Ticket(event.tickets.first().id, 2)])
		checkoutAndConfirmOrder(event.id, [new CheckoutOrderCommand.Ticket(event.tickets.last().id, 4)])
		checkoutAndConfirmOrder(event.id, [new CheckoutOrderCommand.Ticket(event.tickets.last().id, 4)], new ConfirmOrderCommand.Customer("customer2@dev.null", "customer2@dev.null", "Jan", "Nowak", null))

		when:

		EventDto response = updateEvent(
				EventDto,
				identity,
				event.id,
				"New fest",
				"desc",
				EventCategory.BUSINESS,
				RefundPolicy.ONE_DAY_UNTIL_EVENT,
				FeePolicy.APPENDED_TO_PRICE,
				ScheduleDto.SingleDay.builder().dates([ScheduleDto.Date.builder().date(event.schedule.dates.first().date).time(LocalTime.of(11, 11)).build()] as Set).build(),
				null,
				[
						UpdateEventCommand.Ticket.builder()
								.ticketClassId(event.tickets.find { it.type == TicketClassType.PAID }.id)
								.ticket(assembleUpdatePaidTicketCommand("Płatny bilet updated", "z piwem", 45.99, null, 2, Instant.now().minusSeconds(120), Instant.now().plusSeconds(240)))
								.build()
				],
				assembleEventLocationDto("Browar Gdański II piętro", CountryCode.PL, "Gdynia")
		)

		then:
		response.id != null
		response.accountId == identity.accountId
		response.status == EventStatus.PUBLISHED
		response.name == "New fest"
		response.description == "desc"
		response.totalCapacity == null
		response.category == EventCategory.BUSINESS
		response.refundPolicy == RefundPolicy.ONE_DAY_UNTIL_EVENT
		response.feePolicy == FeePolicy.APPENDED_TO_PRICE
		response.currency == CurrencyCode.PLN
		response.location == assembleEventLocationDto("Browar Gdański II piętro", CountryCode.PL, "Gdynia")
		isCloseBeforeNow(response.created)
		response.createdBy == identity.userId
		isCloseBeforeNow(response.modified)
		response.modifiedBy == identity.userId

		with(response.schedule as ScheduleDto.SingleDay) {
			type == ScheduleType.SINGLE_DAY
			dates.size() == 1
			dates.first().asDateTime() == LocalDateTime.of(Year.now().value + 1, 1, 30, 11, 11, 00)
		}

		response.tickets.size() == 2
		with(response.tickets.find { it.type == TicketClassType.FREE } as TicketClassDto.Free) {
			id != null
			status == TicketClassStatus.ACTIVE
			name == "Darmowy bilet"
			description == "bez piwa"
			minPerOrder == null
			maxPerOrder == null
			saleStart == null
			saleEnd == null
			appliedDates.size() == 1
			isCloseBeforeNow(created)
			createdBy == response.createdBy
			modified == null
			modifiedBy == null

		}
		with(response.tickets.find { it.type == TicketClassType.PAID } as TicketClassDto.Paid) {
			id != null
			status == TicketClassStatus.ACTIVE
			name == "Płatny bilet updated"
			description == "z piwem"
			minPerOrder == null
			maxPerOrder == 2
			saleStart != null
			saleEnd != null
			price == 45.99
			appliedDates.size() == 1
			isCloseBeforeNow(created)
			createdBy == response.createdBy
			modified != null
			modifiedBy == identity.userId

		}

		with(eventsRepository.findOne(response.id).get()) {
			accountId == response.accountId
			status == response.status
			name == response.name
			description == response.description
			category == response.category
			totalCapacity == response.totalCapacity
			refundPolicy == response.refundPolicy
			feePolicy == response.feePolicy
			currency == response.currency
			location.name == response.location.name
			location.country == response.location.country
			location.city == response.location.city
			location.street == response.location.street
			location.house == response.location.house
			location.flat == response.location.flat
			location.postalCode == response.location.postalCode
			schedule.type == ScheduleType.SINGLE_DAY
			schedule.dates.first().asDateTime() == LocalDateTime.of(Year.now().value + 1, 1, 30, 11, 11, 00)
			created == response.created
			createdBy == response.createdBy
			modified == response.modified
			modifiedBy == response.modifiedBy

		}

		with(ticketClassesRepository.findOne(response.tickets.find { it.type == TicketClassType.FREE }.id).get()) {
			modifiedBy == null
		}

		with(ticketClassesRepository.findOne(response.tickets.find { it.type == TicketClassType.PAID }.id).get()) {
			modifiedBy == identity.userId
		}

		messagesRegistry.hasSize(6)

		messagesRegistry.getMessages(Type.EVENT_LOCATION_CHANGED_NOTIFICATION_WEB_SOCKET).size() == 1
		with(messagesRegistry.getMessages(Type.EVENT_LOCATION_CHANGED_NOTIFICATION_WEB_SOCKET).first() as EventLocationChangedNotificationWebSocket) {
			eventId == event.id
			location.name == "Browar Gdański II piętro"
			location.country == CountryCode.PL
			location.city == "Gdynia"
			location.street == "Długa"
			location.house == "15A"
			location.flat == "10"
			location.postalCode == "80-333"
		}

		messagesRegistry.getMessages(Type.EVENT_SCHEDULE_CHANGED_NOTIFICATION_WEB_SOCKET).size() == 1
		with(messagesRegistry.getMessages(Type.EVENT_SCHEDULE_CHANGED_NOTIFICATION_WEB_SOCKET).first() as EventScheduleChangedNotificationWebSocket) {
			eventId == event.id
			scheduleDates == response.schedule.dates.collect { it.asDateTime() }
		}

		messagesRegistry.getMessages(Type.EVENT_LOCATION_CHANGED_NOTIFICATION_EMAIL).size() == 2
		messagesRegistry.getMessages(Type.EVENT_LOCATION_CHANGED_NOTIFICATION_EMAIL).each { EventLocationChangedNotificationEmail msg ->
			msg.eventName == event.name
			msg.customerFirstname ==~ "(customer@dev\\.null)|(customer2@dev\\.null)"
			msg.location.name == "Browar Gdański II piętro"
			msg.location.country == CountryCode.PL
			msg.location.city == "Gdynia"
			msg.location.street == "Długa"
			msg.location.house == "15A"
			msg.location.flat == "10"
			msg.location.postalCode == "80-333"
		}

		messagesRegistry.getMessages(Type.EVENT_SCHEDULE_CHANGED_NOTIFICATION_EMAIL).size() == 2
		messagesRegistry.getMessages(Type.EVENT_SCHEDULE_CHANGED_NOTIFICATION_EMAIL).each { EventScheduleChangedNotificationEmail msg ->
			msg.eventName == event.name
			msg.customerFirstname ==~ "(customer@dev\\.null)|(customer2@dev\\.null)"
			msg.scheduleDates == response.schedule.dates.collect { it.asDateTime() }
		}

	}

	def "Should not update event when given ticket quantity is smaller than already sold quantity"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()
		when:
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand(
				"Beer fest",
				null,
				EventCategory.MUSIC,
				RefundPolicy.NO_REFUNDS,
				FeePolicy.INCLUDED_IN_PRICE,
				assembleEventLocationDto(),
				[
						assembleCreateFreeTicketCommand([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 39))], "Darmowy bilet", "bez piwa", null, null, null, null),
						assembleCreatePaidTicketCommand([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 39))], "Płatny bilet", "z piwem", BigDecimal.TEN, 1, 10, Instant.now().minusSeconds(120), null),
				],
				15
		))
		publishEvent(EventDto, identity, event.id)
		checkoutOrder(OrderSessionDto, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.first().id, 2)])
		checkoutOrder(OrderSessionDto, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.last().id, 4)])

		ApiError response = updateEvent(
				ApiError,
				identity,
				event.id,
				"New fest",
				"desc",
				EventCategory.BUSINESS,
				RefundPolicy.ONE_DAY_UNTIL_EVENT,
				FeePolicy.APPENDED_TO_PRICE,
				event.schedule,
				null,
				[
						UpdateEventCommand.Ticket.builder()
								.ticketClassId(event.tickets.find { it.type == TicketClassType.PAID }.id)
								.ticket(assembleUpdatePaidTicketCommand("Płatny bilet updated", "z piwem", 45.99, null, 2, Instant.now().minusSeconds(120), Instant.now().plusSeconds(240), 2))
								.build()
				]
		)

		then:

		response.code == ApplicationException.CommonsErrorCode.BAD_REQUEST.name()

		with(eventsRepository.findOne(event.id).get()) {
			name == "Beer fest"
		}

		with(ticketClassesRepository.findOne(event.tickets.find { it.type == TicketClassType.FREE }.id).get()) {
			modifiedBy == null
		}

		with(ticketClassesRepository.findOne(event.tickets.find { it.type == TicketClassType.PAID }.id).get()) {
			modifiedBy == null
		}
		messagesRegistry.isEmpty()

	}

	def "Should not update event when given ticket price is invalid"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()
		when:
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand(
				"Beer fest",
				null,
				EventCategory.MUSIC,
				RefundPolicy.NO_REFUNDS,
				FeePolicy.INCLUDED_IN_PRICE,
				assembleEventLocationDto(),
				[
						assembleCreateFreeTicketCommand([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 39))], "Darmowy bilet", "bez piwa", null, null, null, null),
						assembleCreatePaidTicketCommand([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 39))], "Płatny bilet", "z piwem", BigDecimal.TEN, 1, null, Instant.now().minusSeconds(120), null),
				],
				15
		))
		publishEvent(EventDto, identity, event.id)
		checkoutOrder(OrderSessionDto, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.first().id, 2)])
		checkoutOrder(OrderSessionDto, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.last().id, 4)])

		ApiError response = updateEvent(
				ApiError,
				identity,
				event.id,
				"New fest",
				"desc",
				EventCategory.BUSINESS,
				RefundPolicy.ONE_DAY_UNTIL_EVENT,
				FeePolicy.APPENDED_TO_PRICE,
				event.schedule,
				null,
				[
						UpdateEventCommand.Ticket.builder()
								.ticketClassId(event.tickets.find { it.type == TicketClassType.PAID }.id)
								.ticket(assembleUpdatePaidTicketCommand("Płatny bilet updated", "z piwem", 0.11, null, 2, Instant.now().minusSeconds(120), Instant.now().plusSeconds(240), 10))
								.build()
				]
		)

		then:

		response.code == EventsErrorCode.TICKET_PRICE_INVALID.name()

		with(eventsRepository.findOne(event.id).get()) {
			name == "Beer fest"
		}

		with(ticketClassesRepository.findOne(event.tickets.find { it.type == TicketClassType.FREE }.id).get()) {
			modifiedBy == null
		}

		with(ticketClassesRepository.findOne(event.tickets.find { it.type == TicketClassType.PAID }.id).get()) {
			modifiedBy == null
		}
		messagesRegistry.isEmpty()

	}

	def "Should not update event when for given ticket is wrong type"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()
		when:
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand(
				"Beer fest",
				null,
				EventCategory.MUSIC,
				RefundPolicy.NO_REFUNDS,
				FeePolicy.INCLUDED_IN_PRICE,
				assembleEventLocationDto(),
				[
						assembleCreateFreeTicketCommand([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 39))], "Darmowy bilet", "bez piwa", null, null, null, null),
						assembleCreatePaidTicketCommand([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 39))], "Płatny bilet", "z piwem", BigDecimal.TEN, 1, null, Instant.now().minusSeconds(120), null),
				],
				15
		))
		publishEvent(EventDto, identity, event.id)
		checkoutOrder(OrderSessionDto, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.first().id, 2)])
		checkoutOrder(OrderSessionDto, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.last().id, 4)])

		ApiError response = updateEvent(
				ApiError,
				identity,
				event.id,
				"New fest",
				"desc",
				EventCategory.BUSINESS,
				RefundPolicy.ONE_DAY_UNTIL_EVENT,
				FeePolicy.APPENDED_TO_PRICE,
				event.schedule,
				null,
				[
						UpdateEventCommand.Ticket.builder()
								.ticketClassId(event.tickets.find { it.type == TicketClassType.FREE }.id)
								.ticket(assembleUpdatePaidTicketCommand("Płatny bilet updated", "z piwem", 0.99, null, 2, Instant.now().minusSeconds(120), Instant.now().plusSeconds(240), 10))
								.build()
				]
		)

		then:

		response.code == EventsErrorCode.TICKET_CLASS_NOT_FOUND.name()

		with(eventsRepository.findOne(event.id).get()) {
			name == "Beer fest"
		}

		with(ticketClassesRepository.findOne(event.tickets.find { it.type == TicketClassType.FREE }.id).get()) {
			modifiedBy == null
		}

		with(ticketClassesRepository.findOne(event.tickets.find { it.type == TicketClassType.PAID }.id).get()) {
			modifiedBy == null
		}
		messagesRegistry.isEmpty()

	}

	def "Should not update event when given ticket not exists"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()
		when:
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand(
				"Beer fest",
				null,
				EventCategory.MUSIC,
				RefundPolicy.NO_REFUNDS,
				FeePolicy.INCLUDED_IN_PRICE,
				assembleEventLocationDto(),
				[
						assembleCreateFreeTicketCommand([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 39))], "Darmowy bilet", "bez piwa", null, null, null, null),
						assembleCreatePaidTicketCommand([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 39))], "Płatny bilet", "z piwem", BigDecimal.TEN, 1, null, Instant.now().minusSeconds(120), null),
				],
				15
		))
		publishEvent(EventDto, identity, event.id)
		checkoutOrder(OrderSessionDto, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.first().id, 2)])
		checkoutOrder(OrderSessionDto, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.last().id, 4)])

		ApiError response = updateEvent(
				ApiError,
				identity,
				event.id,
				"New fest",
				"desc",
				EventCategory.BUSINESS,
				RefundPolicy.ONE_DAY_UNTIL_EVENT,
				FeePolicy.APPENDED_TO_PRICE,
				event.schedule,
				null,
				[
						UpdateEventCommand.Ticket.builder()
								.ticketClassId(UUID.randomUUID())
								.ticket(assembleUpdatePaidTicketCommand("Płatny bilet updated", "z piwem", 0.99, null, 2, Instant.now().minusSeconds(120), Instant.now().plusSeconds(240), 10))
								.build()
				]
		)

		then:

		response.code == EventsErrorCode.TICKET_CLASS_NOT_FOUND.name()

		with(eventsRepository.findOne(event.id).get()) {
			name == "Beer fest"
		}

		with(ticketClassesRepository.findOne(event.tickets.find { it.type == TicketClassType.FREE }.id).get()) {
			modifiedBy == null
		}

		with(ticketClassesRepository.findOne(event.tickets.find { it.type == TicketClassType.PAID }.id).get()) {
			modifiedBy == null
		}
		messagesRegistry.isEmpty()

	}

	def "Should not update event when given total capacity is lt currently sold"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()
		when:
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand("Beer fest", null, EventCategory.MUSIC, RefundPolicy.NO_REFUNDS, FeePolicy.INCLUDED_IN_PRICE))
		publishEvent(EventDto, identity, event.id)
		checkoutOrder(OrderSessionDto, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.first().id, 2)])
		checkoutOrder(OrderSessionDto, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.first().id, 4)])

		ApiError response = updateEvent(ApiError, identity, event.id, "New fest", "desc", EventCategory.BUSINESS, RefundPolicy.ONE_DAY_UNTIL_EVENT, FeePolicy.APPENDED_TO_PRICE, event.schedule, 5)

		then:

		response.code == ApplicationException.CommonsErrorCode.BAD_REQUEST.name()

		with(eventsRepository.findOne(event.id).get()) {
			name == "Beer fest"
			description == null
			category == EventCategory.MUSIC
			totalCapacity == null
			refundPolicy == RefundPolicy.NO_REFUNDS
			feePolicy == FeePolicy.INCLUDED_IN_PRICE
		}
		messagesRegistry.isEmpty()

	}

	def "Should not update event when not exists"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()
		when:
		ApiError response = updateEvent(ApiError, identity, UUID.randomUUID(), "New fest", "desc", EventCategory.BUSINESS, RefundPolicy.ONE_DAY_UNTIL_EVENT, FeePolicy.APPENDED_TO_PRICE, ScheduleDto.SingleDay.builder().dates([ScheduleDto.Date.fromDateTime(LocalDateTime.now())] as Set).build())

		then:
		response.code == EventsErrorCode.EVENT_NOT_FOUND.name()
		messagesRegistry.isEmpty()

	}

	def "Should not update event when is deleted"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()

		when:
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand("Beer fest", null, EventCategory.MUSIC, RefundPolicy.NO_REFUNDS, FeePolicy.INCLUDED_IN_PRICE))
		with(eventsRepository.findOne(event.id).get()) { Event e ->
			e.setStatus(EventStatus.DELETED)
			eventsRepository.save(e)
		}

		ApiError response = updateEvent(ApiError, identity, UUID.randomUUID(), "New fest", "desc", EventCategory.BUSINESS, RefundPolicy.ONE_DAY_UNTIL_EVENT, FeePolicy.APPENDED_TO_PRICE, event.schedule)

		then:
		response.code == EventsErrorCode.EVENT_NOT_FOUND.name()

		with(eventsRepository.findAll().collect(Collectors.toList()).find { (it as Event).id == event.id } as Event) {
			accountId == event.accountId
			status == EventStatus.DELETED
			name == event.name
			description == event.description
			category == event.category
			refundPolicy == event.refundPolicy
			feePolicy == event.feePolicy
			currency == event.currency
			location.name == event.location.name
			location.country == event.location.country
			location.city == event.location.city
			location.street == event.location.street
			location.house == event.location.house
			location.flat == event.location.flat
			location.postalCode == event.location.postalCode
			created == event.created
			createdBy == event.createdBy
			modified == event.modified
			modifiedBy == event.modifiedBy

		}
		messagesRegistry.isEmpty()

	}

	def "Should not update event when not belongs to given account"() {
		given:
		UserIdentity identity = UserIdentity.builder().accountId(AccountsApiMockConfiguration.ACCOUNT_ID).userId(UUID.randomUUID()).build()

		when:
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand("Beer fest", null, EventCategory.MUSIC, RefundPolicy.NO_REFUNDS, FeePolicy.INCLUDED_IN_PRICE))

		ApiError response = updateEvent(ApiError, UserIdentity.builder().accountId(UUID.randomUUID()).userId(UUID.randomUUID()).build(), event.id, "New fest", "desc", EventCategory.BUSINESS, RefundPolicy.ONE_DAY_UNTIL_EVENT, FeePolicy.APPENDED_TO_PRICE, event.schedule)

		then:
		response.code == ApplicationException.CommonsErrorCode.UNAUTHORIZED_ACCESS.name()

		with(eventsRepository.findAll().collect(Collectors.toList()).find { (it as Event).id == event.id } as Event) {
			accountId == event.accountId
			status == event.status
			name == event.name
			description == event.description
			category == event.category
			refundPolicy == event.refundPolicy
			feePolicy == event.feePolicy
			currency == event.currency
			location.name == event.location.name
			location.country == event.location.country
			location.city == event.location.city
			location.street == event.location.street
			location.house == event.location.house
			location.flat == event.location.flat
			location.postalCode == event.location.postalCode
			created == event.created
			createdBy == event.createdBy
			modified == event.modified
			modifiedBy == event.modifiedBy

		}
		messagesRegistry.isEmpty()

	}

	def "Should not update event when schedule has different days"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand(
				"Beer fest",
				null,
				EventCategory.MUSIC,
				RefundPolicy.NO_REFUNDS,
				FeePolicy.INCLUDED_IN_PRICE,
				assembleEventLocationDto(),
				[
						assembleCreateFreeTicketCommand([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 39))], "Darmowy bilet", "bez piwa", null, null, null, null),
						assembleCreatePaidTicketCommand([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 39))], "Płatny bilet", "z piwem", BigDecimal.TEN, 1, 10, Instant.now().minusSeconds(120), null),
				],
				15
		))

		when:

		ApiError response = updateEvent(
				ApiError,
				identity,
				event.id,
				"New fest",
				"desc",
				EventCategory.BUSINESS,
				RefundPolicy.ONE_DAY_UNTIL_EVENT,
				FeePolicy.APPENDED_TO_PRICE,
				ScheduleDto.SingleDay.builder().dates([ScheduleDto.Date.builder().date(event.schedule.dates.first().date.plusDays(1)).time(LocalTime.of(11, 11)).build()] as Set).build(),
				null,
				[
						UpdateEventCommand.Ticket.builder()
								.ticketClassId(event.tickets.find { it.type == TicketClassType.PAID }.id)
								.ticket(assembleUpdatePaidTicketCommand("Płatny bilet updated", "z piwem", 45.99, null, 2, Instant.now().minusSeconds(120), Instant.now().plusSeconds(240)))
								.build()
				],
				assembleEventLocationDto("Browar Gdański II piętro", CountryCode.PL, "Gdynia")
		)

		then:
		response.code == ApplicationException.CommonsErrorCode.BAD_REQUEST.name()
		response.message == "Schedule days cannot differ from current."
		messagesRegistry.isEmpty()

	}

	def "Should not update event when schedule has different type"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand(
				"Beer fest",
				null,
				EventCategory.MUSIC,
				RefundPolicy.NO_REFUNDS,
				FeePolicy.INCLUDED_IN_PRICE,
				assembleEventLocationDto(),
				[
						assembleCreateFreeTicketCommand([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 39))], "Darmowy bilet", "bez piwa", null, null, null, null),
						assembleCreatePaidTicketCommand([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 39))], "Płatny bilet", "z piwem", BigDecimal.TEN, 1, 10, Instant.now().minusSeconds(120), null),
				],
				15
		))

		when:

		ApiError response = updateEvent(
				ApiError,
				identity,
				event.id,
				"New fest",
				"desc",
				EventCategory.BUSINESS,
				RefundPolicy.ONE_DAY_UNTIL_EVENT,
				FeePolicy.APPENDED_TO_PRICE,
				ScheduleDto.MultiDay.builder().dates([ScheduleDto.Date.builder().date(event.schedule.dates.first().date.plusDays(1)).time(LocalTime.of(11, 11)).build(), ScheduleDto.Date.builder().date(event.schedule.dates.first().date.minusDays(1)).time(LocalTime.of(11, 11)).build()] as Set).build(),
				null,
				[
						UpdateEventCommand.Ticket.builder()
								.ticketClassId(event.tickets.find { it.type == TicketClassType.PAID }.id)
								.ticket(assembleUpdatePaidTicketCommand("Płatny bilet updated", "z piwem", 45.99, null, 2, Instant.now().minusSeconds(120), Instant.now().plusSeconds(240)))
								.build()
				],
				assembleEventLocationDto("Browar Gdański II piętro", CountryCode.PL, "Gdynia")
		)

		then:
		response.code == ApplicationException.CommonsErrorCode.BAD_REQUEST.name()
		response.message == "Schedule type cannot differ from current."
		messagesRegistry.isEmpty()

	}

	def "Should not update event when event has illegal status"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand(
				"Beer fest",
				null,
				EventCategory.MUSIC,
				RefundPolicy.NO_REFUNDS,
				FeePolicy.INCLUDED_IN_PRICE,
				assembleEventLocationDto(),
				[
						assembleCreateFreeTicketCommand([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 39))], "Darmowy bilet", "bez piwa", null, null, null, null),
						assembleCreatePaidTicketCommand([assembleScheduleDate(LocalDateTime.of(Year.now().value + 1, 1, 30, 19, 30, 39))], "Płatny bilet", "z piwem", BigDecimal.TEN, 1, 10, Instant.now().minusSeconds(120), null),
				],
				15
		))

		when:

		with(eventsRepository.findOne(event.id).get()) { Event e ->
			e.setStatus(eventStatus)
			eventsRepository.save(e)
		}

		ApiError response = updateEvent(
				ApiError,
				identity,
				event.id,
				"New fest",
				"desc",
				EventCategory.BUSINESS,
				RefundPolicy.ONE_DAY_UNTIL_EVENT,
				FeePolicy.APPENDED_TO_PRICE,
				ScheduleDto.SingleDay.builder().dates([ScheduleDto.Date.builder().date(event.schedule.dates.first().date).time(LocalTime.of(11, 11)).build()] as Set).build(),
				null,
				[
						UpdateEventCommand.Ticket.builder()
								.ticketClassId(event.tickets.find { it.type == TicketClassType.PAID }.id)
								.ticket(assembleUpdatePaidTicketCommand("Płatny bilet updated", "z piwem", 45.99, null, 2, Instant.now().minusSeconds(120), Instant.now().plusSeconds(240)))
								.build()
				],
				assembleEventLocationDto("Browar Gdański II piętro", CountryCode.PL, "Gdynia")
		)

		then:
		response.code == code.name()
		messagesRegistry.isEmpty()

		where:
		eventStatus          | code
		EventStatus.STARTED  | EventsErrorCode.EVENT_STARTED
		EventStatus.FINISHED | EventsErrorCode.EVENT_FINISHED
		EventStatus.DELETED  | EventsErrorCode.EVENT_NOT_FOUND

	}

	def "Should publish unpublished event successfully"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()
		when:
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand("Beer fest", null, EventCategory.MUSIC, RefundPolicy.NO_REFUNDS, FeePolicy.INCLUDED_IN_PRICE))
		EventDto response = publishEvent(EventDto, identity, event.id)

		then:
		response.id != null
		response.accountId == identity.accountId
		response.status == EventStatus.PUBLISHED
		response.name == "Beer fest"
		response.description == null
		response.category == EventCategory.MUSIC
		response.refundPolicy == RefundPolicy.NO_REFUNDS
		response.feePolicy == FeePolicy.INCLUDED_IN_PRICE
		response.currency == CurrencyCode.PLN
		response.location == assembleEventLocationDto()
		isCloseBeforeNow(response.created)
		response.createdBy == identity.userId
		isCloseBeforeNow(response.modified)
		response.modifiedBy == identity.userId
		response.tickets.size() == 1

		with(eventsRepository.findOne(response.id).get()) {
			accountId == response.accountId
			status == response.status
			name == response.name
			description == response.description
			category == response.category
			refundPolicy == response.refundPolicy
			feePolicy == response.feePolicy
			currency == response.currency
			location.name == response.location.name
			location.country == response.location.country
			location.city == response.location.city
			location.street == response.location.street
			location.house == response.location.house
			location.flat == response.location.flat
			location.postalCode == response.location.postalCode
			created == response.created
			createdBy == response.createdBy
			modified == response.modified
			modifiedBy == response.modifiedBy

		}

	}

	def "Should publish suspended event successfully"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()
		when:
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand("Beer fest", null, EventCategory.MUSIC, RefundPolicy.NO_REFUNDS, FeePolicy.INCLUDED_IN_PRICE))
		with(eventsRepository.findOne(event.id).get()) { Event e ->
			e.setStatus(EventStatus.SUSPENDED)
			eventsRepository.save(e)
		}
		EventDto response = publishEvent(EventDto, identity, event.id)

		then:
		response.id != null
		response.accountId == identity.accountId
		response.status == EventStatus.PUBLISHED
		response.name == "Beer fest"
		response.description == null
		response.category == EventCategory.MUSIC
		response.refundPolicy == RefundPolicy.NO_REFUNDS
		response.feePolicy == FeePolicy.INCLUDED_IN_PRICE
		response.currency == CurrencyCode.PLN
		response.location == assembleEventLocationDto()
		isCloseBeforeNow(response.created)
		response.createdBy == identity.userId
		isCloseBeforeNow(response.modified)
		response.modifiedBy == identity.userId
		response.tickets.size() == 1

		with(eventsRepository.findOne(response.id).get()) {
			accountId == response.accountId
			status == response.status
			name == response.name
			description == response.description
			category == response.category
			refundPolicy == response.refundPolicy
			feePolicy == response.feePolicy
			currency == response.currency
			location.name == response.location.name
			location.country == response.location.country
			location.city == response.location.city
			location.street == response.location.street
			location.house == response.location.house
			location.flat == response.location.flat
			location.postalCode == response.location.postalCode
			created == response.created
			createdBy == response.createdBy
			modified == response.modified
			modifiedBy == response.modifiedBy

		}

	}

	def "Should not publish event when event has wrong status"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()
		when:
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand("Beer fest", null, EventCategory.MUSIC, RefundPolicy.NO_REFUNDS, FeePolicy.INCLUDED_IN_PRICE))
		with(eventsRepository.findOne(event.id).get()) { Event e ->
			e.setStatus(eventStatus)
			eventsRepository.save(e)
		}
		ApiError response = publishEvent(ApiError, identity, event.id)

		then:
		response.code == code.name()

		where:
		eventStatus           | code
		EventStatus.PUBLISHED | EventsErrorCode.EVENT_PUBLISHED
		EventStatus.FINISHED  | EventsErrorCode.EVENT_FINISHED
		EventStatus.DELETED   | EventsErrorCode.EVENT_NOT_FOUND

	}

	def "Should not publish event when bank account number is not set"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.MISSING_BANK_ACCOUNT_NO_ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()
		when:
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand("Beer fest", null, EventCategory.MUSIC, RefundPolicy.NO_REFUNDS, FeePolicy.INCLUDED_IN_PRICE))
		ApiError response = publishEvent(ApiError, identity, event.id)

		then:
		response.code == EventsErrorCode.BANK_ACCOUNT_NUMBER_MISSING.name()
		with(eventsRepository.findOne(event.id).get()) {
			status == EventStatus.UNPUBLISHED
		}

	}

	def "Should not publish event when account not exists"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()
		when:
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand("Beer fest", null, EventCategory.MUSIC, RefundPolicy.NO_REFUNDS, FeePolicy.INCLUDED_IN_PRICE))
		ApiError response = publishEvent(ApiError, UserIdentity.builder().accountId(UUID.randomUUID()).userId(UUID.randomUUID()).build(), event.id)

		then:
		response.code == ApplicationException.CommonsErrorCode.UNAUTHORIZED_ACCESS.name()
		with(eventsRepository.findOne(event.id).get()) {
			status == EventStatus.UNPUBLISHED
		}

	}

	def "Should not publish event when not belongs to to given account"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()
		when:
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand("Beer fest", null, EventCategory.MUSIC, RefundPolicy.NO_REFUNDS, FeePolicy.INCLUDED_IN_PRICE))
		ApiError response = publishEvent(ApiError, UserIdentity.builder().accountId(UUID.randomUUID()).userId(UUID.randomUUID()).build(), event.id)

		then:

		response.code == ApplicationException.CommonsErrorCode.UNAUTHORIZED_ACCESS.name()
		with(eventsRepository.findOne(event.id).get()) {
			status == EventStatus.UNPUBLISHED
		}

	}

	def "Should not publish event when not exists"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()
		when:
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand("Beer fest", null, EventCategory.MUSIC, RefundPolicy.NO_REFUNDS, FeePolicy.INCLUDED_IN_PRICE))
		ApiError response = publishEvent(ApiError, identity, UUID.randomUUID())

		then:

		response.code == EventsErrorCode.EVENT_NOT_FOUND.name()
		with(eventsRepository.findOne(event.id).get()) {
			status == EventStatus.UNPUBLISHED
		}

	}

	def "Should suspend event successfully when there are no pending order sessions"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()
		when:
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand("Beer fest", null, EventCategory.MUSIC, RefundPolicy.NO_REFUNDS, FeePolicy.INCLUDED_IN_PRICE))
		publishEvent(EventDto, identity, event.id)
		EventDto response = suspendEvent(EventDto, identity, event.id)

		then:

		response.id == event.id
		response.status == EventStatus.SUSPENDED
		response.modifiedBy == identity.userId
		isCloseBeforeNow(response.modified)

		with(eventsRepository.findOne(response.id).get()) {
			status == response.status
			modified == response.modified
			modifiedBy == response.modifiedBy
		}

		ordersSessionsRegistry.isEmpty()
		messagesRegistry.isEmpty()

	}

	def "Should suspend event successfully when there are pending order sessions"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()
		when:
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand("Beer fest", null, EventCategory.MUSIC, RefundPolicy.NO_REFUNDS, FeePolicy.INCLUDED_IN_PRICE))
		publishEvent(EventDto, identity, event.id)

		assert ordersSessionsRegistry.isEmpty()

		OrderSessionDto session1 = checkoutOrder(OrderSessionDto, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.first().id, 5)])
		OrderSessionDto session2 = checkoutOrder(OrderSessionDto, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.first().id, 2)])
		OrderSessionDto session3 = checkoutOrder(OrderSessionDto, event.id, [new CheckoutOrderCommand.Ticket(event.tickets.first().id, 3)])

		assert ordersSessionsRegistry.size() == 3

		EventDto response = suspendEvent(EventDto, identity, event.id)

		then:

		response.id == event.id
		response.status == EventStatus.SUSPENDED
		response.modifiedBy == identity.userId
		isCloseBeforeNow(response.modified)

		with(eventsRepository.findOne(response.id).get()) {
			status == response.status
			modified == response.modified
			modifiedBy == response.modifiedBy
		}

		messagesRegistry.getMessages(Type.ORDER_SESSION_INVALIDATED).size() == 3
		with(messagesRegistry.getMessages(Type.ORDER_SESSION_INVALIDATED) as OrderSessionInvalidatedWebSocket[]) { OrderSessionInvalidatedWebSocket[] messages ->
			messages.each { it.reason == OrderSessionInvalidatedWebSocket.Reason.EVENT_SUSPENDED }
			messages.collect { it.sessionId }.sort { it }.containsAll([session1.id, session2.id, session3.id].sort { it })
		}

		Thread.sleep(2000)
		ordersSessionsRegistry.isEmpty()

	}

	def "Should not suspend event when not belongs to to given account"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()
		when:
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand("Beer fest", null, EventCategory.MUSIC, RefundPolicy.NO_REFUNDS, FeePolicy.INCLUDED_IN_PRICE))
		publishEvent(EventDto, identity, event.id)
		ApiError response = suspendEvent(ApiError, UserIdentity.builder().accountId(UUID.randomUUID()).userId(UUID.randomUUID()).build(), event.id)

		then:

		response.code == ApplicationException.CommonsErrorCode.UNAUTHORIZED_ACCESS.name()
		with(eventsRepository.findOne(event.id).get()) {
			status == EventStatus.PUBLISHED
		}

		ordersSessionsRegistry.isEmpty()
		messagesRegistry.isEmpty()

	}

	def "Should not suspend event when event has illegal status"() {
		given:
		UserIdentity identity = UserIdentity.builder()
				.accountId(AccountsApiMockConfiguration.ACCOUNT_ID)
				.userId(UUID.randomUUID())
				.build()
		when:
		EventDto event = createEvent(EventDto, identity, assembleValidCreateEventCommand("Beer fest", null, EventCategory.MUSIC, RefundPolicy.NO_REFUNDS, FeePolicy.INCLUDED_IN_PRICE))
		with(eventsRepository.findOne(event.id).get()) { Event e ->
			e.setStatus(eventStatus)
			eventsRepository.save(e)
		}
		ApiError response = suspendEvent(ApiError, identity, event.id)

		then:

		response.code == code.name()

		ordersSessionsRegistry.isEmpty()
		messagesRegistry.isEmpty()

		where:
		eventStatus             | code
		EventStatus.UNPUBLISHED | EventsErrorCode.EVENT_UNPUBLISHED
		EventStatus.SUSPENDED   | EventsErrorCode.EVENT_SUSPENDED
		EventStatus.FINISHED    | EventsErrorCode.EVENT_FINISHED
		EventStatus.DELETED     | EventsErrorCode.EVENT_NOT_FOUND

	}

}
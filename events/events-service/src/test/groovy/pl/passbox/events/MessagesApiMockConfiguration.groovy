package pl.passbox.events

import org.springframework.boot.test.context.TestComponent
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Primary
import pl.passbox.api.messages.MessagesApi
import pl.passbox.api.messages.model.SendMessageCommand
import pl.passbox.api.messages.model.Type

@TestConfiguration
class MessagesApiMockConfiguration {

	@Bean
	@Primary
	MessagesApi messagesApi(MessagesRegistry registry) {
		return new MessagesApi() {
			@Override
			<C extends SendMessageCommand> void send(List<C> commands) {
				registry.add(commands)
			}
		}
	}

	@TestComponent
	static class MessagesRegistry {

		private final Map<Type, List<? extends SendMessageCommand>> messages = Type.values().collectEntries {
			[(it): new ArrayList<? extends SendMessageCommand>()]
		}

		void add(List<SendMessageCommand> command) {
			println "[MESSAGES-API MOCK] sending message $command"
			command.each { messages.get(it.getType()).addAll(it) }

		}

		List<? extends SendMessageCommand> getMessages(Type type) {
			return messages.get(type)
		}

		void clean() {
			messages.each { it.value.clear() }
		}

		boolean hasSize(int size) {
			return size == messages.collect {it.getValue().size()}.sum() as int
		}

		boolean isEmpty() {
			return messages.find { !it.value.isEmpty() } == null
		}

	}

}
package pl.passbox.events

import lombok.Getter
import org.springframework.boot.test.context.TestComponent
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Primary
import pl.passbox.api.payments.charges.ChargesApi
import pl.passbox.api.payments.charges.model.ChargeDto
import pl.passbox.api.payments.charges.model.EstimateChargeCommand
import pl.passbox.api.payments.payments.PaymentsApi
import pl.passbox.api.payments.payments.model.RequestPaymentCommand
import pl.passbox.api.payments.payments.model.RequestPaymentResponseDto
import spock.mock.DetachedMockFactory

@TestConfiguration
class PaymentsApiMockConfiguration {

	private DetachedMockFactory factory = new DetachedMockFactory()

	@Bean
	@Primary
	ChargesApi chargesApi() {
		return new ChargesApi() {
			@Override
			ChargeDto estimate(EstimateChargeCommand cmd) {
				return ChargeDto.builder()
						.feePolicy(cmd.feePolicy)
						.currency(cmd.currency)
						.tariffId(UUID.randomUUID())
						.subtotal(BigDecimal.ZERO)
						.fee(BigDecimal.ZERO)
						.total(BigDecimal.ZERO)
						.products(
						cmd.tickets.collect {
							ChargeDto.Product.builder()
									.ticketClassId(it.ticketClassId)
									.ticketName(it.ticketName)
									.quantity(it.quantity)
									.unitPrice(it.unitPrice)
									.fee(BigDecimal.ZERO)
									.total(BigDecimal.ZERO)
									.build()
						})
						.build()
			}
		}
	}

	@Bean
	@Primary
	PaymentsApi paymentsApi(PaymentRequestsRegistry registry) {
		return new PaymentsApi() {
			@Override
			RequestPaymentResponseDto request(RequestPaymentCommand command) {
				registry.add(command)
				return RequestPaymentResponseDto.builder().paymentUrl("http://example.com/payment").build()
			}
		}
	}

	@Getter
	@TestComponent
	static class PaymentRequestsRegistry {

		private final LinkedList<RequestPaymentCommand> requests = []

		RequestPaymentCommand last() {
			return requests.last
		}

		void add(RequestPaymentCommand command) {
			requests.add(command)
		}

		void clear() {
			requests.clear()
		}

		boolean isEmpty() {
			return requests.isEmpty()
		}

	}

}
package pl.passbox.api.events.events.model.types;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RequiredFlag {
    YES(true),
    NO(false);

    private final boolean required;

}
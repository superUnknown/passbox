package pl.passbox.api.events.events.model;

import com.neovisionaries.i18n.CurrencyCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import pl.passbox.api.events.events.model.types.EventCategory;
import pl.passbox.api.events.events.model.types.EventStatus;
import pl.passbox.api.events.events.model.types.RefundPolicy;
import pl.passbox.api.events.schedules.model.ScheduleDto;
import pl.passbox.api.events.tickets.model.TicketClassDto;
import pl.passbox.api.payments.charges.model.FeePolicy;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EventDto {

    @NotNull
    private UUID id;

    @NotNull
    private UUID accountId;

    @NotNull
    private EventStatus status;

    @NotBlank
    private String name;

    private String description;

    @NotNull
    private EventCategory category;

    @NotNull
    private RefundPolicy refundPolicy;

    @NotNull
    private FeePolicy feePolicy;

    @NotNull
    private CurrencyCode currency;

    @Min(1)
    private Integer totalCapacity;

    @Valid
    @NotNull
    private EventLocationDto location;

    @Valid
    @NotNull
    private ScheduleDto schedule;

    @Valid
    @NotEmpty
    private List<TicketClassDto> tickets;

    @NotNull
    private Instant created;

    @NotNull
    private UUID createdBy;

    private Instant modified;

    private UUID modifiedBy;

}
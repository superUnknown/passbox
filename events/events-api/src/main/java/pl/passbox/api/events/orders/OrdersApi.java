package pl.passbox.api.events.orders;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.passbox.api.events.events.exceptions.EventNotFoundException;
import pl.passbox.api.events.events.exceptions.IllegalEventStatusException;
import pl.passbox.api.events.orders.exception.OrderSessionNotFoundException;
import pl.passbox.api.events.orders.model.CheckoutOrderCommand;
import pl.passbox.api.events.orders.model.ConfirmOrderCommand;
import pl.passbox.api.events.orders.model.ConfirmOrderResponseDto;
import pl.passbox.api.events.orders.model.OrderSessionDto;
import pl.passbox.api.events.schedules.exceptions.ScheduleNotFoundException;
import pl.passbox.api.events.tickets.exceptions.TicketClassNotFoundException;
import pl.passbox.api.events.tickets.exceptions.TicketNotAvailableException;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@FeignClient(OrdersApi.NAME)
@RequestMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface OrdersApi {

    String NAME = "orders-api";

    String SESSION_ID_VARIABLE = "sessionId";

    String CHECKOUT_ORDER_PATH = "/orders/checkout";

    String CONFIRM_ORDER_PATH = "/orders/confirm";

    String ORDER_SESSION_PATH = "/sessions/{" + SESSION_ID_VARIABLE + "}";

    @PostMapping(path = CHECKOUT_ORDER_PATH, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    OrderSessionDto checkout(@NotNull @Valid @RequestBody CheckoutOrderCommand command) throws IllegalEventStatusException, EventNotFoundException, ScheduleNotFoundException, TicketNotAvailableException, TicketClassNotFoundException;

    @PostMapping(path = CONFIRM_ORDER_PATH, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    ConfirmOrderResponseDto confirm(@NotNull @Valid @RequestBody ConfirmOrderCommand command) throws EventNotFoundException, IllegalEventStatusException, OrderSessionNotFoundException;

    @PutMapping(ORDER_SESSION_PATH)
    void refreshSession(@NotNull @PathVariable(SESSION_ID_VARIABLE) UUID sessionId) throws OrderSessionNotFoundException;

    @DeleteMapping(ORDER_SESSION_PATH)
    void removeSession(@NotNull @PathVariable(SESSION_ID_VARIABLE) UUID sessionId) throws OrderSessionNotFoundException;

}
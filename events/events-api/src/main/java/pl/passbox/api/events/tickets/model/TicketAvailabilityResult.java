package pl.passbox.api.events.tickets.model;

public enum TicketAvailabilityResult {
    TOTAL_EVENT_CAPACITY_REACHED,
    TOTAL_TICKET_QUANTITY_REACHED,
    TICKET_QUANTITY_OUT_OF_DEFINED_LIMITS,
    TICKET_SALE_NOT_STARTED,
    TICKET_SALE_ENDED,
    AVAILABLE
}
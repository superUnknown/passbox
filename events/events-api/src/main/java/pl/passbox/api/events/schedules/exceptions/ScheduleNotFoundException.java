package pl.passbox.api.events.schedules.exceptions;

import lombok.Getter;
import pl.passbox.api.events.configuration.EventsApplicationException;
import pl.passbox.api.events.configuration.EventsErrorCode;

import java.util.UUID;

@Getter
public class ScheduleNotFoundException extends EventsApplicationException {

    private final EventsErrorCode code = EventsErrorCode.SCHEDULE_NOT_FOUND;

    private ScheduleNotFoundException(String message) {
        super(message);
    }

    public static ScheduleNotFoundException byId(UUID scheduleId) {
        return new ScheduleNotFoundException(String.format("Schedule with id %s not found.", scheduleId));
    }

}
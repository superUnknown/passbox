package pl.passbox.api.events.orders.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TicketDto {

    @NotNull
    private UUID id;

    @NotNull
    private UUID ticketClassId;

    @NotBlank
    private String number;

    @Valid
    @NotNull
    private AttendeeDto attendee;

    @Valid
    private CheckIn checkIn;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CheckIn {

        @NotNull
        private Instant datetime;

        @NotNull
        private UUID checkedBy;

    }

}
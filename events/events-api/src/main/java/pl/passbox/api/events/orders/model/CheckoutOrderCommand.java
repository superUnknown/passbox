package pl.passbox.api.events.orders.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CheckoutOrderCommand {

    @NotNull
    private UUID eventId;

    @Valid
    @NotEmpty
    private List<Ticket> tickets;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Ticket {

        @NotNull
        private UUID ticketClassId;

        @Min(0)
        @NotNull
        private Integer quantity;

    }

}
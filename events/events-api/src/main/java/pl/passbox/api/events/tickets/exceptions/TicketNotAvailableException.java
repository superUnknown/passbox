package pl.passbox.api.events.tickets.exceptions;

import lombok.Getter;
import pl.passbox.api.events.configuration.EventsApplicationException;
import pl.passbox.api.events.configuration.EventsErrorCode;
import pl.passbox.api.events.tickets.model.TicketAvailabilityResult;

import java.util.UUID;

@Getter
public class TicketNotAvailableException extends EventsApplicationException {

    private final EventsErrorCode code = EventsErrorCode.TICKET_NOT_AVAILABLE;

    private TicketNotAvailableException(String message) {
        super(message);
    }

    public static TicketNotAvailableException byResult(UUID ticketClassId, TicketAvailabilityResult result) {
        return new TicketNotAvailableException(String.format("Ticket class with id %s is not available (%s).", ticketClassId, result));
    }

}
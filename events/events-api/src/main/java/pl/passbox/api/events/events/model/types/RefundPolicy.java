package pl.passbox.api.events.events.model.types;

public enum RefundPolicy {
    ONE_DAY_UNTIL_EVENT,
    TWO_DAYS_UNTIL_EVENT,
    THREE_DAYS_UNTIL_EVENT,
    WEEK_UNTIL_EVENT,
    MONTH_UNTIL_EVENT,
    NO_REFUNDS
}
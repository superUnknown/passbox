package pl.passbox.api.events.tickets.model.types;

public enum TicketStatus {
    /**
     * Parent Order for this ticket is not yet completed (is requested or pending).
     */
    REQUESTED,
    /**
     * Parent Order is completed and has been successfully paid; ticket can be checked in.
     */
    AVAILABLE,
    /**
     * Ticket is used - was checked at the gate.
     */
    CHECKED_IN,
    /**
     * Ticket was ordered but has been refunded (couldn't be checked before).
     */
    REFUNDED,
    /**
     * Ticket was cancelled during parent order cancellation.
     */
    CANCELED,
}
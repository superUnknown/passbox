package pl.passbox.api.events.configuration;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.passbox.commons.configuration.FeignApplicationErrorDecoder;
import pl.passbox.commons.configuration.FeignClientConfigurationSupport;
import pl.passbox.api.events.events.EventsApi;

@Configuration
@EnableConfigurationProperties(EventsApiConfiguration.Properties.class)
public class EventsApiConfiguration {

    @Bean
    public EventsApi eventsApi(EventsApiConfiguration.Properties properties, FeignApplicationErrorDecoder feignApplicationErrorDecoder) {
        return FeignClientConfigurationSupport.build(EventsApi.class, properties.getServiceUrl(), feignApplicationErrorDecoder);
    }

    @Bean
    public FeignApplicationErrorDecoder feignApplicationErrorDecoder() {
        return new FeignApplicationErrorDecoder(EventsErrorCode.values());
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @ConfigurationProperties("events.api")
    public static class Properties {

        @NotBlank
        private String serviceUrl;

    }

}

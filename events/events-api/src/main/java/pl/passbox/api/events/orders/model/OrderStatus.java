package pl.passbox.api.events.orders.model;

public enum OrderStatus {
    /**
     * Order has been fully completed by customer and was redirected to payment gateway for payment confirmation.
     */
    REQUESTED,
    /**
     * Payment confirmed by customer; waiting for finish payment.
     */
    PENDING,
    /**
     * Order successfully paid; payment transaction completed; tickets have been sent.
     */
    COMPLETED,
    /**
     * Order was requested, but canceled by customer during payment confirmation.
     */
    CANCELED,
    /**
     * Payment transaction has failed; order has not been paid.
     */
    FAILED
}
package pl.passbox.api.events.orders.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.passbox.api.payments.charges.model.FeePolicy;
import pl.passbox.api.events.events.model.types.RefundPolicy;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderDto {

    @NotNull
    private UUID id;

    @NotNull
    private UUID eventId;

    @NotNull
    private OrderStatus status;

    @NotNull
    private RefundPolicy refundPolicy;

    @NotNull
    private FeePolicy feePolicy;

    @NotNull
    private Instant started;

    @NotNull
    private Instant requested;

    private Instant completed;

    @Valid
    private CustomerDto customer;

    @Valid
    @Builder.Default
    private List<TicketDto> tickets = new ArrayList<>();

}
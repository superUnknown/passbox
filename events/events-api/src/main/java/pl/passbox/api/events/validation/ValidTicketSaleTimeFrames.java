package pl.passbox.api.events.validation;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.InvalidPropertyException;
import org.springframework.beans.PropertyAccessException;
import org.springframework.beans.PropertyAccessorFactory;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import javax.validation.ValidationException;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.time.Instant;


@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ValidTicketSaleTimeFrames.Validator.class)
@Documented
public @interface ValidTicketSaleTimeFrames {

    String message() default "Given ticket sale time frames are invalid.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    class Validator implements ConstraintValidator<ValidTicketSaleTimeFrames, Object> {

        private static final String SALE_START_PROPERTY = "saleStart";

        private static final String SALE_END_PROPERTY = "saleEnd";

        @Override
        public void initialize(ValidTicketSaleTimeFrames constraint) {
        }

        @Override
        public boolean isValid(Object o, ConstraintValidatorContext context) {
            if (o == null) {
                return true;
            }
            try {
                BeanWrapper wrapper = PropertyAccessorFactory.forBeanPropertyAccess(o);
                Instant saleStart = (Instant) wrapper.getPropertyValue(SALE_START_PROPERTY);
                Instant saleEnd = (Instant) wrapper.getPropertyValue(SALE_END_PROPERTY);
                return saleStart == null || saleEnd == null || saleStart.isBefore(saleEnd);
            } catch (InvalidPropertyException | PropertyAccessException e) {
                throw new ValidationException(e);
            }
        }

    }

}
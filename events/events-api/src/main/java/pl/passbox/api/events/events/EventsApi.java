package pl.passbox.api.events.events;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.passbox.api.accounts.accounts.exceptions.AccountNotFoundException;
import pl.passbox.api.accounts.accounts.exceptions.IllegalAccountStatusException;
import pl.passbox.api.events.events.exceptions.BankAccountNumberMissingException;
import pl.passbox.api.events.events.exceptions.EventNotFoundException;
import pl.passbox.api.events.events.exceptions.IllegalEventStatusException;
import pl.passbox.api.events.events.model.CreateEventCommand;
import pl.passbox.api.events.events.model.EventDto;
import pl.passbox.api.events.events.model.UpdateEventCommand;
import pl.passbox.api.events.tickets.exceptions.AppliedDateNotDefinedException;
import pl.passbox.api.events.tickets.exceptions.TicketClassNotFoundException;
import pl.passbox.api.events.tickets.exceptions.TicketPriceInvalidException;
import pl.passbox.commons.exception.UnauthorizedAccessException;
import pl.passbox.commons.identity.exception.UserIdentityMissingException;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@FeignClient(EventsApi.NAME)
@RequestMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface EventsApi {

    String NAME = "events-api";

    String EVENT_ID_VARIABLE = "eventId";

    String EVENTS_PATH = "/events";

    String EVENT_PATH = EVENTS_PATH + "/{" + EVENT_ID_VARIABLE + "}";

    String PUBLISH_EVENT_PATH = EVENT_PATH + "/publish";

    String SUSPEND_EVENT_PATH = EVENT_PATH + "/suspend";

    @PostMapping(path = EVENTS_PATH, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    EventDto create(@Valid @NotNull @RequestBody CreateEventCommand command) throws UserIdentityMissingException, AccountNotFoundException, AppliedDateNotDefinedException, TicketPriceInvalidException, EventNotFoundException, UnauthorizedAccessException;

    @PutMapping(path = EVENT_PATH, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    EventDto update(@NotNull @PathVariable(EVENT_ID_VARIABLE) UUID eventId, @Valid @NotNull @RequestBody UpdateEventCommand command) throws UserIdentityMissingException, EventNotFoundException, UnauthorizedAccessException, TicketPriceInvalidException, TicketClassNotFoundException, IllegalEventStatusException;

    @PatchMapping(PUBLISH_EVENT_PATH)
    EventDto publish(@NotNull @PathVariable(EVENT_ID_VARIABLE) UUID eventId) throws UserIdentityMissingException, EventNotFoundException, AccountNotFoundException, BankAccountNumberMissingException, UnauthorizedAccessException, IllegalAccountStatusException, IllegalEventStatusException;

    @PatchMapping(SUSPEND_EVENT_PATH)
    EventDto suspend(@NotNull @PathVariable(EVENT_ID_VARIABLE) UUID eventId) throws UserIdentityMissingException, EventNotFoundException, AccountNotFoundException, UnauthorizedAccessException, IllegalAccountStatusException, IllegalEventStatusException;

}
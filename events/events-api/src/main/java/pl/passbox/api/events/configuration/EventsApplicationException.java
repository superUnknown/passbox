package pl.passbox.api.events.configuration;

import pl.passbox.commons.configuration.ApplicationException;

public abstract class EventsApplicationException extends ApplicationException {

    public EventsApplicationException(String message) {
        super(message);
    }

    public EventsApplicationException(String message, Exception cause) {
        super(message, cause);
    }

    @Override
    public abstract EventsErrorCode getCode();

}
package pl.passbox.api.events.tickets.exceptions;

import lombok.Getter;
import pl.passbox.api.events.configuration.EventsApplicationException;
import pl.passbox.api.events.configuration.EventsErrorCode;

import java.util.UUID;

@Getter
public class OrdersForTicketExistsException extends EventsApplicationException {

    private final EventsErrorCode code = EventsErrorCode.ORDERS_FOR_TICKET_EXISTS;

    public OrdersForTicketExistsException(UUID ticketClassId) {
        super(String.format("Orders for ticket class with id %s exists.", ticketClassId));
    }

}
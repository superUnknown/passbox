package pl.passbox.api.events.schedules.model;

public enum ScheduleDateStatus {
    ACTIVE,
    DELETED
}
package pl.passbox.api.events.tickets.model.types;

public enum TicketClassStatus {
    ACTIVE,
    DELETED
}
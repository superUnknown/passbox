package pl.passbox.api.events.tickets.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import pl.passbox.api.events.schedules.model.ScheduleDto;
import pl.passbox.api.events.tickets.model.types.TicketClassStatus;
import pl.passbox.api.events.tickets.model.types.TicketClassType;
import pl.passbox.api.events.validation.ValidTicketOrderLimits;
import pl.passbox.api.events.validation.ValidTicketSaleTimeFrames;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Set;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ValidTicketOrderLimits
@ValidTicketSaleTimeFrames
@JsonSubTypes({
        @JsonSubTypes.Type(value = TicketClassDto.Free.class, name = "FREE"),
        @JsonSubTypes.Type(value = TicketClassDto.Paid.class, name = "PAID")
})
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
public abstract class TicketClassDto {

    @NotNull
    private UUID id;

    @NotNull
    private TicketClassStatus status;

    @NotBlank
    private String name;

    private String description;

    @Min(1)
    private Integer quantity;

    @Min(1)
    private Integer minPerOrder;

    @Min(1)
    private Integer maxPerOrder;

    private Instant saleStart;

    private Instant saleEnd;

    @Valid
    @NotEmpty
    private Set<ScheduleDto.Date> appliedDates;

    @NotNull
    private Instant created;

    @NotNull
    private UUID createdBy;

    private Instant modified;

    private UUID modifiedBy;

    @JsonIgnore
    public abstract TicketClassType getType();

    @Data
    @NoArgsConstructor
    @ToString(callSuper = true)
    public static class Free extends TicketClassDto {

        private final TicketClassType type = TicketClassType.FREE;

        @Builder
        public Free(UUID id, TicketClassStatus status, String name, String description, Integer quantity, Integer minPerOrder, Integer maxPerOrder, Instant saleStart, Instant saleEnd, Set<ScheduleDto.Date> appliedDates, Instant created, UUID createdBy, Instant modified, UUID modifiedBy) {
            super(id, status, name, description, quantity, minPerOrder, maxPerOrder, saleStart, saleEnd, appliedDates, created, createdBy, modified, modifiedBy);
        }

    }

    @Data
    @NoArgsConstructor
    @ToString(callSuper = true)
    public static class Paid extends TicketClassDto {

        private final TicketClassType type = TicketClassType.PAID;

        @NotNull
        @DecimalMin("0.01")
        private BigDecimal price;

        @Builder
        public Paid(UUID id, TicketClassStatus status, String name, String description, Integer quantity, Integer minPerOrder, Integer maxPerOrder, Instant saleStart, Instant saleEnd, Set<ScheduleDto.Date> appliedDates, Instant created, UUID createdBy, Instant modified, UUID modifiedBy, BigDecimal price) {
            super(id, status, name, description, quantity, minPerOrder, maxPerOrder, saleStart, saleEnd, appliedDates, created, createdBy, modified, modifiedBy);
            this.price = price;
        }

    }

}
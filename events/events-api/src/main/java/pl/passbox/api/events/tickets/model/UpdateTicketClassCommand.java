package pl.passbox.api.events.tickets.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;
import pl.passbox.api.events.tickets.model.types.TicketClassType;
import pl.passbox.api.events.validation.ValidTicketOrderLimits;
import pl.passbox.api.events.validation.ValidTicketSaleTimeFrames;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.Instant;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ValidTicketOrderLimits
@ValidTicketSaleTimeFrames
@JsonSubTypes({
        @JsonSubTypes.Type(value = UpdateTicketClassCommand.Free.class, name = "FREE"),
        @JsonSubTypes.Type(value = UpdateTicketClassCommand.Paid.class, name = "PAID")
})
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
public abstract class UpdateTicketClassCommand {

    @NotBlank
    private String name;

    private String description;

    @Min(1)
    private Integer quantity;

    @Min(1)
    private Integer minPerOrder;

    @Min(1)
    private Integer maxPerOrder;

    private Instant saleStart;

    private Instant saleEnd;

    @JsonIgnore
    public abstract TicketClassType getType();

    @Data
    @NoArgsConstructor
    @ToString(callSuper = true)
    public static class Free extends UpdateTicketClassCommand {

        private final TicketClassType type = TicketClassType.FREE;

        @Builder
        public Free(
                @JsonProperty("name") String name,
                @JsonProperty("description") String description,
                @JsonProperty("quantity") Integer quantity,
                @JsonProperty("minPerOrder") Integer minPerOrder,
                @JsonProperty("maxPerOrder") Integer maxPerOrder,
                @JsonProperty("saleStart") Instant saleStart,
                @JsonProperty("saleEnd") Instant saleEnd
        ) {
            super(name, description, quantity, minPerOrder, maxPerOrder, saleStart, saleEnd);
        }

    }

    @Data
    @NoArgsConstructor
    @ToString(callSuper = true)
    public static class Paid extends UpdateTicketClassCommand {

        private final TicketClassType type = TicketClassType.PAID;

        @NotNull
        @DecimalMin("0.01")
        private BigDecimal price;

        @Builder
        public Paid(
                @JsonProperty("name") String name,
                @JsonProperty("description") String description,
                @JsonProperty("quantity") Integer quantity,
                @JsonProperty("minPerOrder") Integer minPerOrder,
                @JsonProperty("maxPerOrder") Integer maxPerOrder,
                @JsonProperty("saleStart") Instant saleStart,
                @JsonProperty("saleEnd") Instant saleEnd,
                @JsonProperty("price") BigDecimal price
        ) {
            super(name, description, quantity, minPerOrder, maxPerOrder, saleStart, saleEnd);
            this.price = price;
        }

    }

}
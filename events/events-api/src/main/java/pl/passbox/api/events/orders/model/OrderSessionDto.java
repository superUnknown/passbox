package pl.passbox.api.events.orders.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
import pl.passbox.api.payments.charges.model.ChargeDto;
import pl.passbox.commons.utils.DatePatterns;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderSessionDto {

    @NotNull
    @Builder.Default
    private UUID id = UUID.randomUUID();

    @NotNull
    private UUID eventId;

    @NotNull
    @Builder.Default
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DatePatterns.DATE_TIME_PATTERN, timezone = DatePatterns.DATE_TIME_ZONE)
    private Instant started = Instant.now();

    @Valid
    @NotNull
    private ChargeDto charge;

    @Valid
    @NotEmpty
    private List<Ticket> tickets = new ArrayList<>();

    public Optional<Long> getTicketQuantity(UUID ticketClassId) {
        return tickets.stream().filter(t -> t.getTicketClassId().equals(ticketClassId)).map(t -> new Long(t.getQuantity())).findFirst();
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Ticket {

        @NotNull
        private UUID ticketClassId;

        @Min(1)
        @NotNull
        private Integer quantity;

    }


}
package pl.passbox.api.events.events.exceptions;

import lombok.Getter;
import pl.passbox.api.events.configuration.EventsApplicationException;
import pl.passbox.api.events.configuration.EventsErrorCode;

import java.util.UUID;

@Getter
public class EventNotFoundException extends EventsApplicationException {

    private final EventsErrorCode code = EventsErrorCode.EVENT_NOT_FOUND;

    private EventNotFoundException(String message) {
        super(message);
    }

    public static EventNotFoundException byId(UUID eventId) {
        return new EventNotFoundException(String.format("Event with id %s not found.", eventId));
    }

    public static EventNotFoundException byTicketClassId(UUID ticketClassId) {
        return new EventNotFoundException(String.format("Event for ticket class with id %s not found.", ticketClassId));
    }

}
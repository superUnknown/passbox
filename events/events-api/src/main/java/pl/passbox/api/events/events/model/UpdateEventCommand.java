package pl.passbox.api.events.events.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;
import pl.passbox.api.events.events.model.types.EventCategory;
import pl.passbox.api.events.events.model.types.RefundPolicy;
import pl.passbox.api.events.schedules.model.ScheduleDto;
import pl.passbox.api.events.tickets.model.UpdateTicketClassCommand;
import pl.passbox.api.payments.charges.model.FeePolicy;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UpdateEventCommand {

    @NotBlank
    private String name;

    private String description;

    @NotNull
    private EventCategory category;

    @NotNull
    private RefundPolicy refundPolicy;

    @NotNull
    private FeePolicy feePolicy;

    @Min(1)
    private Integer totalCapacity;

    @Valid
    @NotNull
    private EventLocationDto location;

    @Valid
    @NotNull
    private ScheduleDto schedule;

    @Valid
    @Builder.Default
    private List<Ticket> tickets = new ArrayList<>();

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Ticket {

        @NotNull
        private UUID ticketClassId;

        @Valid
        @NotNull
        private UpdateTicketClassCommand ticket;

    }

}
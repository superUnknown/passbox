package pl.passbox.api.events.tickets.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
import pl.passbox.api.events.schedules.model.ScheduleDto;

import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApplyScheduleDatesCommand {

    @NotEmpty
    private Set<ScheduleDto.Date> dates;

}
package pl.passbox.api.events.configuration;

import lombok.AllArgsConstructor;
import lombok.Getter;
import pl.passbox.api.events.events.exceptions.BankAccountNumberMissingException;
import pl.passbox.api.events.events.exceptions.EventNotFoundException;
import pl.passbox.api.events.events.exceptions.IllegalEventStatusException;
import pl.passbox.api.events.orders.exception.OrderSessionNotFoundException;
import pl.passbox.api.events.schedules.exceptions.ScheduleNotFoundException;
import pl.passbox.api.events.tickets.exceptions.AppliedDateNotDefinedException;
import pl.passbox.api.events.tickets.exceptions.OrdersForTicketExistsException;
import pl.passbox.api.events.tickets.exceptions.TicketClassNotFoundException;
import pl.passbox.api.events.tickets.exceptions.TicketNotAvailableException;
import pl.passbox.api.events.tickets.exceptions.TicketPriceInvalidException;
import pl.passbox.commons.configuration.ApplicationException;

@Getter
@AllArgsConstructor
public enum EventsErrorCode implements ApplicationException.ErrorCode {
    EVENT_NOT_FOUND(EventNotFoundException.class),
    ILLEGAL_EVENT_STATUS(IllegalEventStatusException.class),
    EVENT_UNPUBLISHED(IllegalEventStatusException.class),
    EVENT_PUBLISHED(IllegalEventStatusException.class),
    EVENT_SUSPENDED(IllegalEventStatusException.class),
    EVENT_FINISHED(IllegalEventStatusException.class),
    EVENT_DELETED(IllegalEventStatusException.class),
    EVENT_STARTED(IllegalEventStatusException.class),
    ORDER_SESSION_NOT_FOUND(OrderSessionNotFoundException.class),
    BANK_ACCOUNT_NUMBER_MISSING(BankAccountNumberMissingException.class),
    SCHEDULE_NOT_FOUND(ScheduleNotFoundException.class),
    TICKET_PRICE_INVALID(TicketPriceInvalidException.class),
    TICKET_CLASS_NOT_FOUND(TicketClassNotFoundException.class),
    TICKET_NOT_AVAILABLE(TicketNotAvailableException.class),
    ORDERS_FOR_TICKET_EXISTS(OrdersForTicketExistsException.class),
    APPLIED_DATE_NOT_DEFINED(AppliedDateNotDefinedException.class);

    private final Class<? extends EventsApplicationException> type;

}
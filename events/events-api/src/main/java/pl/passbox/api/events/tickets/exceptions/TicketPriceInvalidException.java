package pl.passbox.api.events.tickets.exceptions;

import lombok.Getter;
import pl.passbox.api.events.configuration.EventsApplicationException;
import pl.passbox.api.events.configuration.EventsErrorCode;

import java.math.BigDecimal;

@Getter
public class TicketPriceInvalidException extends EventsApplicationException {

    private final EventsErrorCode code = EventsErrorCode.TICKET_PRICE_INVALID;

    private TicketPriceInvalidException(String message) {
        super(message);
    }

    public static TicketPriceInvalidException gtMin(String ticketName, BigDecimal price, BigDecimal limit) {
        return new TicketPriceInvalidException(String.format("Ticket '%s' has smaller price (%s) then min permitted (%s).", ticketName, price, limit));
    }

}
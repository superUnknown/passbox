package pl.passbox.api.events.tickets;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.passbox.api.events.events.exceptions.EventNotFoundException;
import pl.passbox.api.events.events.exceptions.IllegalEventStatusException;
import pl.passbox.api.events.tickets.exceptions.AppliedDateNotDefinedException;
import pl.passbox.api.events.tickets.exceptions.OrdersForTicketExistsException;
import pl.passbox.api.events.tickets.exceptions.TicketClassNotFoundException;
import pl.passbox.api.events.tickets.exceptions.TicketPriceInvalidException;
import pl.passbox.api.events.tickets.model.ApplyScheduleDatesCommand;
import pl.passbox.api.events.tickets.model.CreateTicketClassCommand;
import pl.passbox.api.events.tickets.model.TicketClassDto;
import pl.passbox.commons.exception.UnauthorizedAccessException;
import pl.passbox.commons.identity.exception.UserIdentityMissingException;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@FeignClient(TicketClassesApi.NAME)
@RequestMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface TicketClassesApi {

    String NAME = "ticket-classes-api";

    String EVENT_ID_VARIABLE = "eventId";

    String TICKET_CLASS_ID_VARIABLE = "ticketClassId";

    String CREATE_TICKET_CLASS_PATH = "/events/{" + EVENT_ID_VARIABLE + "}/ticket-classes";

    String APPLY_SCHEDULE_DATES_PATH = "/events/{" + EVENT_ID_VARIABLE + "}/ticket-classes/{" + TICKET_CLASS_ID_VARIABLE + "}/dates";

    @PostMapping(path = CREATE_TICKET_CLASS_PATH, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    TicketClassDto create(@NotNull @PathVariable(EVENT_ID_VARIABLE) UUID eventId, @Valid @NotNull @RequestBody CreateTicketClassCommand command) throws UserIdentityMissingException, EventNotFoundException, TicketPriceInvalidException, UnauthorizedAccessException, AppliedDateNotDefinedException;

    @PutMapping(path = APPLY_SCHEDULE_DATES_PATH, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    TicketClassDto applyScheduleDates(
            @NotNull @PathVariable(EVENT_ID_VARIABLE) UUID eventId,
            @NotNull @PathVariable(TICKET_CLASS_ID_VARIABLE) UUID ticketClassId,
            @Valid @NotNull @RequestBody ApplyScheduleDatesCommand command
    ) throws UserIdentityMissingException, AppliedDateNotDefinedException, IllegalEventStatusException, TicketClassNotFoundException, EventNotFoundException, UnauthorizedAccessException, OrdersForTicketExistsException;

}
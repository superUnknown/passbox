package pl.passbox.api.events.events.model.types;

public enum CheckoutQuestionType {
    PHONE,
    EMAIL,
    TEXT,
    INTEGER,
    FLOAT,
    BOOLEAN
}
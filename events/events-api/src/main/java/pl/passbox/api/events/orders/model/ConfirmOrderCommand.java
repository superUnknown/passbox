package pl.passbox.api.events.orders.model;

import com.neovisionaries.i18n.LanguageCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import pl.passbox.commons.validation.EqualProperties;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ConfirmOrderCommand {

    @NotNull
    private UUID eventId;

    @NotNull
    private UUID sessionId;

    private String customerIp;

    @Valid
    @NotNull
    private Customer customer;

    @Valid
    @NotEmpty
    private List<Ticket> tickets;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @EqualProperties({"email", "email2"})
    public static class Customer {

        @Email
        @NotBlank
        private String email;

        @Email
        private String email2;

        @NotBlank
        private String firstname;

        @NotBlank
        private String lastname;

        private LanguageCode language;

    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Ticket {

        @NotNull
        private UUID ticketClassId;

        @Valid
        @NotNull
        private AttendeeDto attendee;

    }

}
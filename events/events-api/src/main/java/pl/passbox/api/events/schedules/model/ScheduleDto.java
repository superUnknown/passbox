package pl.passbox.api.events.schedules.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.validator.constraints.NotEmpty;
import pl.passbox.commons.utils.DatePatterns;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Set;

@Data
@NoArgsConstructor
@JsonSubTypes({
        @JsonSubTypes.Type(value = ScheduleDto.SingleDay.class, name = "SINGLE_DAY"),
        @JsonSubTypes.Type(value = ScheduleDto.MultiDay.class, name = "MULTI_DAY")
})
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
public abstract class ScheduleDto {

    @JsonIgnore
    public abstract ScheduleType getType();

    public abstract Set<Date> getDates();

    public abstract void setDates(Set<Date> dates);

    @Data
    @NoArgsConstructor
    @ToString(callSuper = true)
    public static class SingleDay extends ScheduleDto {

        private final ScheduleType type = ScheduleType.SINGLE_DAY;

        @Valid
        @NotEmpty
        @Size(min = 1, max = 1)
        private Set<Date> dates;

        @Builder
        public SingleDay(@JsonProperty("dates") Set<Date> dates) {
            this.dates = dates;
        }

    }

    @Data
    @NoArgsConstructor
    @ToString(callSuper = true)
    public static class MultiDay extends ScheduleDto {

        private final ScheduleType type = ScheduleType.MULTI_DAY;

        @Valid
        @NotEmpty
        @Size(min = 2)
        private Set<Date> dates;

        @Builder
        public MultiDay(@JsonProperty("dates") Set<Date> dates) {
            this.dates = dates;
        }

    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @EqualsAndHashCode(of = {"date", "time"})
    public static class Date {

        @NotNull
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DatePatterns.DATE_PATTERN)
        private LocalDate date;

        @NotNull
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = DatePatterns.TIME_PATTERN)
        private LocalTime time;

        @JsonIgnore
        public LocalDateTime asDateTime() {
            return date.atTime(time).truncatedTo(ChronoUnit.MINUTES);
        }

        public static Date fromDateTime(LocalDateTime dateTime) {
            return Date.builder()
                    .date(dateTime.toLocalDate())
                    .time(dateTime.toLocalTime())
                    .build();
        }

    }

}
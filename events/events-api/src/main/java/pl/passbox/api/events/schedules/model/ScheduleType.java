package pl.passbox.api.events.schedules.model;

public enum ScheduleType {
    SINGLE_DAY,
    MULTI_DAY
}
package pl.passbox.api.events.events.exceptions;

import lombok.Getter;
import pl.passbox.api.events.configuration.EventsApplicationException;
import pl.passbox.api.events.configuration.EventsErrorCode;

@Getter
public class BankAccountNumberMissingException extends EventsApplicationException {

    private final EventsErrorCode code = EventsErrorCode.BANK_ACCOUNT_NUMBER_MISSING;

    public BankAccountNumberMissingException() {
        super("Bank account number is not set.");
    }

}
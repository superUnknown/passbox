package pl.passbox.api.events.events.model.types;

public enum EventCategory {
    BUSINESS,
    MUSIC
}
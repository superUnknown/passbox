package pl.passbox.api.events.events.exceptions;

import lombok.Getter;
import pl.passbox.api.events.configuration.EventsApplicationException;
import pl.passbox.api.events.configuration.EventsErrorCode;
import pl.passbox.api.events.events.model.types.EventStatus;

import java.util.UUID;

@Getter
public class IllegalEventStatusException extends EventsApplicationException {

    private final EventsErrorCode code;

    private IllegalEventStatusException(String message, EventsErrorCode code) {
        super(message);
        this.code = code;
    }

    public static IllegalEventStatusException status(UUID eventId, EventStatus status) {
        switch (status) {
            case UNPUBLISHED:
                return unpublished(eventId);
            case PUBLISHED:
                return published(eventId);
            case SUSPENDED:
                return suspended(eventId);
            case STARTED:
                return started(eventId);
            case FINISHED:
                return finished(eventId);
            case DELETED:
                return deleted(eventId);
            default:
                return new IllegalEventStatusException(String.format("Event with id %s has illegal status to execute this operation.", eventId), EventsErrorCode.ILLEGAL_EVENT_STATUS);
        }
    }

    private static IllegalEventStatusException unpublished(UUID eventId) {
        return new IllegalEventStatusException(String.format("Event with id %s is unpublished.", eventId), EventsErrorCode.EVENT_UNPUBLISHED);
    }

    private static IllegalEventStatusException published(UUID eventId) {
        return new IllegalEventStatusException(String.format("Event with id %s is published.", eventId), EventsErrorCode.EVENT_PUBLISHED);
    }

    private static IllegalEventStatusException suspended(UUID eventId) {
        return new IllegalEventStatusException(String.format("Event with id %s is suspended.", eventId), EventsErrorCode.EVENT_SUSPENDED);
    }

    private static IllegalEventStatusException started(UUID eventId) {
        return new IllegalEventStatusException(String.format("Event with id %s has started.", eventId), EventsErrorCode.EVENT_STARTED);
    }

    private static IllegalEventStatusException finished(UUID eventId) {
        return new IllegalEventStatusException(String.format("Event with id %s is finished.", eventId), EventsErrorCode.EVENT_FINISHED);
    }

    private static IllegalEventStatusException deleted(UUID eventId) {
        return new IllegalEventStatusException(String.format("Event with id %s is deleted.", eventId), EventsErrorCode.EVENT_DELETED);
    }

}
package pl.passbox.api.events.orders.exception;

import lombok.Getter;
import pl.passbox.api.events.configuration.EventsApplicationException;
import pl.passbox.api.events.configuration.EventsErrorCode;

import java.util.UUID;

@Getter
public class OrderSessionNotFoundException extends EventsApplicationException {

    private final EventsErrorCode code = EventsErrorCode.ORDER_SESSION_NOT_FOUND;

    private OrderSessionNotFoundException(String message) {
        super(message);
    }

    public static OrderSessionNotFoundException byId(UUID sessionId) {
        return new OrderSessionNotFoundException(String.format("Order session with id %s not found.", sessionId));
    }

}
package pl.passbox.api.events.validation;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.InvalidPropertyException;
import org.springframework.beans.PropertyAccessException;
import org.springframework.beans.PropertyAccessorFactory;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import javax.validation.ValidationException;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Optional;


@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ValidTicketOrderLimits.Validator.class)
@Documented
public @interface ValidTicketOrderLimits {

    String message() default "Given min/max ticket order limits are invalid.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    class Validator implements ConstraintValidator<ValidTicketOrderLimits, Object> {

        private static final String MIN_PER_ORDER_PROPERTY = "minPerOrder";

        private static final String MAX_PER_ORDER_PROPERTY = "maxPerOrder";

        @Override
        public void initialize(ValidTicketOrderLimits constraint) {
        }

        @Override
        public boolean isValid(Object o, ConstraintValidatorContext context) {
            if (o == null) {
                return true;
            }
            try {
                BeanWrapper wrapper = PropertyAccessorFactory.forBeanPropertyAccess(o);
                Integer minPerOrder = (Integer) wrapper.getPropertyValue(MIN_PER_ORDER_PROPERTY);
                Integer maxPerOrder = (Integer) wrapper.getPropertyValue(MAX_PER_ORDER_PROPERTY);
                return Optional.ofNullable(minPerOrder).orElse(0) <= 0 || Optional.ofNullable(maxPerOrder).orElse(0) <= 0 || minPerOrder < maxPerOrder;
            } catch (InvalidPropertyException | PropertyAccessException e) {
                throw new ValidationException(e);
            }
        }

    }

}
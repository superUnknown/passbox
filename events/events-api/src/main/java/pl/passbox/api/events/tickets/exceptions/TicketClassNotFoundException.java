package pl.passbox.api.events.tickets.exceptions;

import lombok.Getter;
import pl.passbox.api.events.configuration.EventsApplicationException;
import pl.passbox.api.events.configuration.EventsErrorCode;
import pl.passbox.api.events.tickets.model.types.TicketClassType;

import java.util.UUID;

@Getter
public class TicketClassNotFoundException extends EventsApplicationException {

    private final EventsErrorCode code = EventsErrorCode.TICKET_CLASS_NOT_FOUND;

    private TicketClassNotFoundException(String message) {
        super(message);
    }

    public static TicketClassNotFoundException byId(UUID ticketClassId) {
        return new TicketClassNotFoundException(String.format("Ticket class with id %s not found.", ticketClassId));
    }

    public static TicketClassNotFoundException byIdAndType(UUID ticketClassId, TicketClassType type) {
        return new TicketClassNotFoundException(String.format("Ticket class of type %s with id %s not found.", type, ticketClassId));
    }

}
package pl.passbox.api.events.tickets.model.types;

public enum TicketClassType {
    FREE,
    PAID
}
package pl.passbox.api.events.tickets.exceptions;

import lombok.Getter;
import pl.passbox.api.events.configuration.EventsApplicationException;
import pl.passbox.api.events.configuration.EventsErrorCode;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
public class AppliedDateNotDefinedException extends EventsApplicationException {

    private final EventsErrorCode code = EventsErrorCode.APPLIED_DATE_NOT_DEFINED;

    public AppliedDateNotDefinedException(LocalDateTime dateTime, UUID ticketClassId) {
        super(String.format("Applied date '%s' for ticket with id %s is not defined.", dateTime, ticketClassId));
    }

}
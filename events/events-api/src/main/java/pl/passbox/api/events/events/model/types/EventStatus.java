package pl.passbox.api.events.events.model.types;

import com.google.common.collect.ImmutableList;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@AllArgsConstructor
public enum EventStatus {
    UNPUBLISHED,
    PUBLISHED,
    SUSPENDED,
    STARTED,
    FINISHED,
    DELETED;

    private static final List<EventStatus> PUBLISHABLE = ImmutableList.of(
            UNPUBLISHED,
            SUSPENDED
    );

    private static final List<EventStatus> UPDATEABLE = ImmutableList.of(
            UNPUBLISHED,
            PUBLISHED,
            SUSPENDED
    );

    public static boolean isPublishPossible(@NotNull EventStatus status) {
        return PUBLISHABLE.contains(status);
    }

    public static boolean isUpdatePossible(@NotNull EventStatus status) {
        return UPDATEABLE.contains(status);
    }

}
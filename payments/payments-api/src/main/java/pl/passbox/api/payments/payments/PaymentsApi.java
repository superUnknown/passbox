package pl.passbox.api.payments.payments;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.passbox.api.payments.payments.model.RequestPaymentCommand;
import pl.passbox.api.payments.payments.model.RequestPaymentResponseDto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@FeignClient(PaymentsApi.NAME)
@RequestMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface PaymentsApi {

    String NAME = "payments-api";

    String REQUEST_PAYMENT_PATH = "/payments";

    @PostMapping(REQUEST_PAYMENT_PATH)
    RequestPaymentResponseDto request(@NotNull @Valid @RequestBody RequestPaymentCommand command);

}
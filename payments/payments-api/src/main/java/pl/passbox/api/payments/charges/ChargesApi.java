package pl.passbox.api.payments.charges;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.passbox.api.payments.charges.model.ChargeDto;
import pl.passbox.api.payments.charges.model.EstimateChargeCommand;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@FeignClient(ChargesApi.NAME)
@RequestMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface ChargesApi {

    String NAME = "charges-api";

    String ESTIMATE_CHARGE_PATH = "/checkout";

    @PostMapping(ESTIMATE_CHARGE_PATH)
    ChargeDto estimate(@NotNull @Valid @RequestBody EstimateChargeCommand command);

}
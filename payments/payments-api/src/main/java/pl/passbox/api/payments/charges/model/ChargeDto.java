package pl.passbox.api.payments.charges.model;

import com.neovisionaries.i18n.CurrencyCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChargeDto {

    @NotNull
    private FeePolicy feePolicy;

    @NotNull
    private CurrencyCode currency;

    @NotNull
    private UUID tariffId;

    @NotNull
    @DecimalMin("0.00")
    private BigDecimal subtotal;

    @NotNull
    @DecimalMin("0.00")
    private BigDecimal fee;

    @NotNull
    @DecimalMin("0.00")
    private BigDecimal total;

    @NotNull
    @Builder.Default
    private Instant calculated = Instant.now();

    @Valid
    @NotEmpty
    private List<Product> products = new ArrayList<>();

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Product {

        @NotNull
        private UUID ticketClassId;

        @NotBlank
        private String ticketName;

        @Min(1)
        @NotNull
        private Integer quantity;

        @NotNull
        @DecimalMin("0.00")
        private BigDecimal unitPrice;

        @NotNull
        @DecimalMin("0.00")
        private BigDecimal total;

        @NotNull
        @DecimalMin("0.00")
        private BigDecimal fee;

    }

}
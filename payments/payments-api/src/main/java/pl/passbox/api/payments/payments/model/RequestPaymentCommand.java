package pl.passbox.api.payments.payments.model;

import com.neovisionaries.i18n.LanguageCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import pl.passbox.api.payments.charges.model.ChargeDto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RequestPaymentCommand {

    @NotNull
    private UUID orderId;

    @NotBlank
    private String customerIp;

    @Valid
    @NotNull
    private Customer customer;

    @Valid
    @NotNull
    private ChargeDto charge;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Customer {

        @Email
        @NotBlank
        private String email;

        @NotBlank
        private String firstname;

        @NotBlank
        private String lastname;

        @NotNull
        private LanguageCode language;

    }

}
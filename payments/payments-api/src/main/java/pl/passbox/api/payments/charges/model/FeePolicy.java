package pl.passbox.api.payments.charges.model;

public enum FeePolicy {
    INCLUDED_IN_PRICE,
    APPENDED_TO_PRICE
}
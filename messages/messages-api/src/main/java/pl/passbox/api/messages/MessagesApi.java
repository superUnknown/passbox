package pl.passbox.api.messages;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.passbox.api.messages.model.SendMessageCommand;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@FeignClient(MessagesApi.NAME)
@RequestMapping(value = MessagesApi.PATH, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface MessagesApi {

    String NAME = "messages-api";

    String PATH = "/messages";

    @PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    <C extends SendMessageCommand> void send(@Valid @NotNull @RequestBody List<C> commands);

}
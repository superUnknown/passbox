package pl.passbox.api.messages.model;

import com.google.common.collect.ImmutableMap;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@ToString(callSuper = true)
public class EventScheduleChangedNotificationEmail extends SendMessageCommand.Email {

    private static final String MESSAGE_CUSTOMER_FIRSTNAME_PROPERTY = "customer_firstname";

    private static final String MESSAGE_EVENT_SCHEDULE_DATES_PROPERTY = "schedule_dates";

    private static final String MESSAGE_EVENT_NAME_PROPERTY = "event_name";

    private final Type type = Type.EVENT_SCHEDULE_CHANGED_NOTIFICATION_EMAIL;

    @NotBlank
    private String eventName;

    @NotBlank
    private String customerFirstname;

    @Valid
    @NotNull
    private List<LocalDateTime> scheduleDates;

    @Builder
    public EventScheduleChangedNotificationEmail(Locale locale, String email, String eventName, String customerFirstname, List<LocalDateTime> scheduleDates) {
        super(locale, email);
        this.eventName = eventName;
        this.customerFirstname = customerFirstname;
        this.scheduleDates = scheduleDates;
    }

    @Override
    public Map<String, String> getTemplateParameters() {
        return ImmutableMap.<String, String>builder()
                .put(MESSAGE_EVENT_SCHEDULE_DATES_PROPERTY, scheduleDates.stream() // TODO
                        .map(LocalDateTime::toString)
                        .collect(Collectors.joining(","))
                )
                .put(MESSAGE_CUSTOMER_FIRSTNAME_PROPERTY, customerFirstname)
                .put(MESSAGE_EVENT_NAME_PROPERTY, eventName)
                .build();
    }

}
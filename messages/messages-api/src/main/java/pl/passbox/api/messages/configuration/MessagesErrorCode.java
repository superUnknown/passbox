package pl.passbox.api.messages.configuration;

import lombok.Getter;
import pl.passbox.commons.configuration.ApplicationException;

@Getter
public enum MessagesErrorCode implements ApplicationException.ErrorCode {
    ;

    private final Class<? extends MessagesApplicationException> type;

    MessagesErrorCode(Class<? extends MessagesApplicationException> type) {
        this.type = type;
    }

}
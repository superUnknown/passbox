package pl.passbox.api.messages.model;

import com.google.common.collect.ImmutableMap;
import com.neovisionaries.i18n.CountryCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Locale;
import java.util.Map;

@Data
@NoArgsConstructor
@ToString(callSuper = true)
public class EventLocationChangedNotificationEmail extends SendMessageCommand.Email {

    private static final String MESSAGE_CUSTOMER_FIRSTNAME_PROPERTY = "customer_firstname";

    private static final String MESSAGE_LOCATION_POSTAL_CODE_PROPERTY = "location_postal_ode";

    private static final String MESSAGE_LOCATION_COUNTRY_PROPERTY = "location_country";

    private static final String MESSAGE_LOCATION_STREET_PROPERTY = "location_street";

    private static final String MESSAGE_LOCATION_HOUSE_PROPERTY = "location_house";

    private static final String MESSAGE_LOCATION_NAME_PROPERTY = "location_name";

    private static final String MESSAGE_LOCATION_CITY_PROPERTY = "location_city";

    private static final String MESSAGE_EVENT_NAME_PROPERTY = "event_name";

    private final Type type = Type.EVENT_LOCATION_CHANGED_NOTIFICATION_EMAIL;

    @NotBlank
    private String eventName;

    @NotBlank
    private String customerFirstname;

    @Valid
    @NotNull
    private Location location;

    @Builder
    public EventLocationChangedNotificationEmail(Locale locale, String email, String eventName, String customerFirstname, Location location) {
        super(locale, email);
        this.eventName = eventName;
        this.customerFirstname = customerFirstname;
        this.location = location;
    }

    @Override
    public Map<String, String> getTemplateParameters() {
        return ImmutableMap.<String, String>builder()
                .put(MESSAGE_LOCATION_POSTAL_CODE_PROPERTY, location.postalCode)
                .put(MESSAGE_LOCATION_COUNTRY_PROPERTY, location.country.name())
                .put(MESSAGE_LOCATION_STREET_PROPERTY, location.street)
                .put(MESSAGE_LOCATION_HOUSE_PROPERTY, location.house)
                .put(MESSAGE_LOCATION_NAME_PROPERTY, location.name)
                .put(MESSAGE_LOCATION_CITY_PROPERTY, location.city)
                .put(MESSAGE_CUSTOMER_FIRSTNAME_PROPERTY, customerFirstname)
                .put(MESSAGE_EVENT_NAME_PROPERTY, eventName)
                .build();
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Location {

        @NotBlank
        private String name;

        @NotNull
        private CountryCode country;

        @NotBlank
        private String city;

        private String street;

        private String house;

        private String flat;

        private String postalCode;

    }

}
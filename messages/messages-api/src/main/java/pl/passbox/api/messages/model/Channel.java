package pl.passbox.api.messages.model;

public enum Channel {
    WEBSOCKET,
    EMAIL
}
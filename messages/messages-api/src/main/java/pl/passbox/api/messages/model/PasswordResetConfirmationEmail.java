package pl.passbox.api.messages.model;

import com.google.common.collect.ImmutableMap;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import java.util.Locale;
import java.util.Map;

@Data
@NoArgsConstructor
@ToString(callSuper = true)
public class PasswordResetConfirmationEmail extends SendMessageCommand.Email {

    private static final String MESSAGE_ACCOUNT_NAME_PROPERTY = "account_name";

    private static final String MESSAGE_FIRSTNAME_PROPERTY = "firstname";

    private static final String MESSAGE_LINK_PROPERTY = "link";

    private final Type type = Type.PASSWORD_RESET_CONFIRMATION;

    @NotBlank
    private String accountName;

    @NotBlank
    private String firstname;

    @NotBlank
    private String link;

    @Builder
    public PasswordResetConfirmationEmail(Locale locale, String email, String accountName, String firstname, String link) {
        super(locale, email);
        this.accountName = accountName;
        this.firstname = firstname;
        this.link = link;
    }

    @Override
    public Map<String, String> getTemplateParameters() {
        return ImmutableMap.of(
                MESSAGE_ACCOUNT_NAME_PROPERTY, accountName,
                MESSAGE_FIRSTNAME_PROPERTY, firstname,
                MESSAGE_LINK_PROPERTY, link
        );
    }

}
package pl.passbox.api.messages.model;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@NoArgsConstructor
@ToString(callSuper = true)
public class OrderSessionInvalidatedWebSocket extends SendMessageCommand.WebSocket {

    private final Type type = Type.ORDER_SESSION_INVALIDATED;

    @NotNull
    private UUID sessionId;

    @NotNull
    private Reason reason;

    @Builder
    public OrderSessionInvalidatedWebSocket(UUID sessionId, Reason reason) {
        this.sessionId = sessionId;
        this.reason = reason;
    }

    public enum Reason {
        EVENT_SUSPENDED
    }

}
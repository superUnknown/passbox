package pl.passbox.api.messages.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.util.Locale;
import java.util.Map;

@Data
@NoArgsConstructor
@JsonSubTypes({
        @JsonSubTypes.Type(value = RegistrationConfirmationEmail.class, name = "REGISTRATION_CONFIRMATION")
})
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
public abstract class SendMessageCommand {

    @JsonIgnore
    public abstract Channel getChannel();

    @JsonIgnore
    public abstract Type getType();

    @Data
    @NoArgsConstructor
    @ToString(callSuper = true)
    public static abstract class Email extends SendMessageCommand {

        private final Channel channel = Channel.EMAIL;

        @NotNull
        private Locale locale;

        @NotBlank
        @org.hibernate.validator.constraints.Email
        private String email;

        public Email(Locale locale, String email) {
            this.locale = locale;
            this.email = email;
        }

        @NotNull
        @JsonIgnore
        public abstract Map<String, String> getTemplateParameters();

    }

    @Data
    @NoArgsConstructor
    @ToString(callSuper = true)
    public static abstract class WebSocket extends SendMessageCommand {

        private final Channel channel = Channel.WEBSOCKET;

    }

}
package pl.passbox.api.messages.configuration;

import lombok.Getter;
import pl.passbox.commons.configuration.ApplicationException;

@Getter
public abstract class MessagesApplicationException extends ApplicationException {

    public MessagesApplicationException(String message) {
        super(message);
    }

    @Override
    public abstract MessagesErrorCode getCode();

}
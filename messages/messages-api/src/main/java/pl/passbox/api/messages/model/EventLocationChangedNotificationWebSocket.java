package pl.passbox.api.messages.model;

import com.neovisionaries.i18n.CountryCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@NoArgsConstructor
@ToString(callSuper = true)
public class EventLocationChangedNotificationWebSocket extends SendMessageCommand.WebSocket {

    private final Type type = Type.EVENT_LOCATION_CHANGED_NOTIFICATION_WEB_SOCKET;

    @NotNull
    private UUID eventId;

    @Valid
    @NotNull
    private Location location;

    @Builder
    public EventLocationChangedNotificationWebSocket(UUID eventId, Location location) {
        this.eventId = eventId;
        this.location = location;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Location {

        @NotBlank
        private String name;

        @NotNull
        private CountryCode country;

        @NotBlank
        private String city;

        private String street;

        private String house;

        private String flat;

        private String postalCode;

    }

}
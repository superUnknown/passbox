package pl.passbox.api.messages.model;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@ToString(callSuper = true)
public class EventScheduleChangedNotificationWebSocket extends SendMessageCommand.WebSocket {

    private final Type type = Type.EVENT_SCHEDULE_CHANGED_NOTIFICATION_WEB_SOCKET;

    @NotNull
    private UUID eventId;

    @Valid
    @NotNull
    private List<LocalDateTime> scheduleDates;

    @Builder
    public EventScheduleChangedNotificationWebSocket(UUID eventId, List<LocalDateTime> scheduleDates) {
        this.eventId = eventId;
        this.scheduleDates = scheduleDates;
    }

}
package pl.passbox.api.messages.configuration;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.passbox.api.messages.MessagesApi;
import pl.passbox.commons.configuration.FeignApplicationErrorDecoder;
import pl.passbox.commons.configuration.FeignClientConfigurationSupport;

@Configuration
@EnableConfigurationProperties(MessagesApiConfiguration.Properties.class)
public class MessagesApiConfiguration {

    @Bean
    public MessagesApi messagesApi(MessagesApiConfiguration.Properties properties, FeignApplicationErrorDecoder feignApplicationErrorDecoder) {
        return FeignClientConfigurationSupport.build(MessagesApi.class, properties.getServiceUrl(), feignApplicationErrorDecoder);
    }

    @Bean
    public FeignApplicationErrorDecoder feignApplicationErrorDecoder() {
        return new FeignApplicationErrorDecoder(MessagesErrorCode.values());
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @ConfigurationProperties("messages.api")
    public static class Properties {

        @NotBlank
        private String serviceUrl;

    }

}
package pl.passbox.commons.identity.model;

import com.google.common.base.Splitter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
@Slf4j
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserIdentity {

    private static final String USER_IDENTITY_PROPERTIES_DELIMITER = "|";

    private static final String AUTHORITIES_DELIMITER = ",";

    @NotNull
    private UUID userId;

    @NotNull
    private UUID accountId;

    @NotNull
    @Builder.Default
    private Set<Authority> authorities = Collections.emptySet();

    public String encode() {
        return userId + USER_IDENTITY_PROPERTIES_DELIMITER + accountId + USER_IDENTITY_PROPERTIES_DELIMITER + authorities.stream()
                .map(Authority::name)
                .collect(Collectors.joining(AUTHORITIES_DELIMITER));
    }

    public static UserIdentity decode(String value) {
        try {
            if (!StringUtils.isEmpty(value)) {
                List<String> parts = Splitter.on(USER_IDENTITY_PROPERTIES_DELIMITER).omitEmptyStrings().limit(3).splitToList(value);
                if (parts.size() >= 2) {
                    return UserIdentity.builder()
                            .authorities(parts.size() == 2 ? Collections.emptySet() : Stream.of(parts.get(2).split(AUTHORITIES_DELIMITER)).map(Authority::valueOf).collect(Collectors.toSet()))
                            .accountId(UUID.fromString(parts.get(1)))
                            .userId(UUID.fromString(parts.get(0)))
                            .build();
                }
            }
        } catch (Exception e) {
            log.error("Failed to decode user identity from value {} ({}).", value, e.getMessage(), e);
        }

        return null;
    }

}
package pl.passbox.commons.identity;

import lombok.extern.slf4j.Slf4j;
import pl.passbox.commons.configuration.ApplicationHeader;
import pl.passbox.commons.identity.exception.UserIdentityMissingException;
import pl.passbox.commons.identity.model.UserIdentity;
import pl.passbox.commons.utils.ApplicationHeaderResolver;

import javax.servlet.http.HttpServletRequest;

@Slf4j
public class UserIdentityResolver {

    public static UserIdentity resolve(HttpServletRequest request) throws UserIdentityMissingException {
        log.debug("Resolving user identify from request.");
        UserIdentity identity = ApplicationHeaderResolver.resolve(request, ApplicationHeader.USER_IDENTITY).map(UserIdentity::decode)
                .orElseThrow(UserIdentityMissingException::new);
        log.debug("User identify from request resolved: {}.", identity);
        return identity;
    }

}
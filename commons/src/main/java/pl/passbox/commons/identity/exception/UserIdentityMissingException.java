package pl.passbox.commons.identity.exception;

import lombok.Getter;
import pl.passbox.commons.configuration.ApplicationException;

@Getter
public class UserIdentityMissingException extends ApplicationException {

    private final ApplicationException.CommonsErrorCode code = ApplicationException.CommonsErrorCode.USER_IDENTITY_MISSING;

    public UserIdentityMissingException() {
        super("User identity is missing.");
    }

}
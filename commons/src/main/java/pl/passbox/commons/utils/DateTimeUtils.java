package pl.passbox.commons.utils;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class DateTimeUtils {

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter
            .ofPattern(DatePatterns.DATE_TIME_PATTERN)
            .withZone(ZoneId.of(DatePatterns.DATE_TIME_ZONE));

    public static String format(Instant instant) {
        return DATE_FORMATTER.format(instant);
    }

}
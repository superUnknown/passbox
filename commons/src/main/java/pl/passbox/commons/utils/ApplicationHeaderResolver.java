package pl.passbox.commons.utils;

import org.springframework.util.StringUtils;
import pl.passbox.commons.configuration.ApplicationHeader;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

public class ApplicationHeaderResolver {

    public static Optional<String> resolve(final HttpServletRequest request, final ApplicationHeader header) {
        return Optional.ofNullable(request.getHeader(header.getValue())).filter(h -> !StringUtils.isEmpty(h));
    }

}
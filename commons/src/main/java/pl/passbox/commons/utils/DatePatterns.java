package pl.passbox.commons.utils;

public class DatePatterns {

    public static final String DATE_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSSX";

    public static final String DATE_TIME_ZONE = "UTC";

    public static final String DATE_PATTERN = "yyyy-MM-dd";

    public static final String TIME_PATTERN = "HH:mm";

}
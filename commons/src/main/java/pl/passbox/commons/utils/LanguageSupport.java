package pl.passbox.commons.utils;

import com.google.common.collect.ImmutableList;
import com.neovisionaries.i18n.LanguageCode;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

public class LanguageSupport {

    private final static List<LanguageCode> SUPPORTED_LANGUAGES = ImmutableList.of(LanguageCode.pl, LanguageCode.en);

    private final static LanguageCode DEFAULT_LANGUAGE = LanguageCode.pl;

    public static LanguageCode resolve(LanguageCode language) {
        return Optional.ofNullable(language).filter(SUPPORTED_LANGUAGES::contains).orElse(DEFAULT_LANGUAGE);
    }

    public static LanguageCode resolve(LanguageCode primary, HttpServletRequest request) {
        return Optional.ofNullable(primary).filter(SUPPORTED_LANGUAGES::contains).orElse(resolve(request));
    }

    public static LanguageCode resolve(HttpServletRequest request) {
        return Optional.ofNullable(LanguageCode.getByLocale(request.getLocale()))
                .filter(SUPPORTED_LANGUAGES::contains).orElse(DEFAULT_LANGUAGE);
    }

}
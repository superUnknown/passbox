package pl.passbox.commons.exception;

import lombok.Getter;
import pl.passbox.commons.configuration.ApplicationException;

@Getter
public class UnauthorizedAccessException extends ApplicationException {

    private final ApplicationException.CommonsErrorCode code = CommonsErrorCode.UNAUTHORIZED_ACCESS;

    public UnauthorizedAccessException() {
        super("Unauthorized access.");
    }

}
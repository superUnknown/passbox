package pl.passbox.commons.locking.service;

import pl.passbox.commons.locking.model.Lock;
import pl.passbox.commons.locking.model.LockType;

import javax.validation.constraints.NotNull;

public interface DistributedLockService {

    Lock acquireLockOrWait(@NotNull LockType type, Object key);

}
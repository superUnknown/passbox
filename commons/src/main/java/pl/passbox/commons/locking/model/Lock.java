package pl.passbox.commons.locking.model;

import org.redisson.api.RLock;

import java.util.Optional;

public class Lock implements AutoCloseable {

    private final RLock lock;

    private volatile boolean stayLocked = false;

    public Lock(RLock lock) {
        this.lock = lock;
    }

    public void stayLocked() {
        this.stayLocked = true;
    }

    @Override
    public void close() {
        if (!stayLocked) {
            Optional.ofNullable(lock).ifPresent(java.util.concurrent.locks.Lock::unlock);
        }
    }

}
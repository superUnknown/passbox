package pl.passbox.commons.locking.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import pl.passbox.commons.configuration.DistributedLockConfiguration;
import pl.passbox.commons.locking.model.Lock;
import pl.passbox.commons.locking.model.LockType;

import java.util.concurrent.TimeUnit;

@Slf4j
@Service
@Validated
@AllArgsConstructor
public class DistributedLockServiceBean implements DistributedLockService {

    private static final String LOCK_FORMAT = "%s.locks.%s.%s";

    private final DistributedLockConfiguration.Properties properties;

    private final RedissonClient client;

    @Override
    public Lock acquireLockOrWait(LockType type, Object key) {
        try {
            log.debug("Acquiring lock of type {} on key {}.", type, key);
            RLock lock = getLock(type, key);
            if (!lock.tryLock(-1, properties.getLockTtlInSeconds(), TimeUnit.SECONDS)) {
                log.debug("Lock of type {} on key {} is currently locked. Waiting for release...", type, key);
                waitForRelease(lock);
            }
            log.debug("Lock of type {} on key {} acquired successfully.", type, key);
            return new Lock(lock);
        } catch (IllegalStateException | InterruptedException e) {
            log.error("Unexpected error occurred during acquiring lock of type {} on key {} ({}).", type, key, e.getMessage(), e);
            throw new RuntimeException(e.getMessage(), e);
        } catch (Exception e) {
            log.error("Unexpected error occurred during acquiring lock of type {} on key {} ({}).", type, key, e.getMessage(), e);
            return new Lock(null);
        }
    }

    private void waitForRelease(RLock lock) throws InterruptedException {
        long left = properties.getReleaseWaitingTimeInSeconds() * 1000;
        while (left > 0 && lock.isLocked()) {
            Thread.sleep(1000);
            left -= 1000;
        }
        if (lock.isLocked()) {
            throw new IllegalStateException("Waiting time for lock reached max.");
        }
    }

    private RLock getLock(LockType type, Object key) {
        return client.getLock(String.format(LOCK_FORMAT, properties.getPrefix(), type, key));
    }

}
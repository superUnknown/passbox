package pl.passbox.commons.configuration;


import lombok.Getter;
import pl.passbox.commons.exception.UnauthorizedAccessException;
import pl.passbox.commons.identity.exception.UserIdentityMissingException;

public abstract class ApplicationException extends Exception {

    public abstract ErrorCode getCode();

    public ApplicationException(String message) {
        super(message);
    }

    public ApplicationException(String message, Exception cause) {
        super(message, cause);
    }

    public interface ErrorCode {

        String name();

        Class<? extends Exception> getType();

    }

    @Getter
    public enum CommonsErrorCode implements ErrorCode {
        USER_IDENTITY_MISSING(UserIdentityMissingException.class),
        UNAUTHORIZED_ACCESS(UnauthorizedAccessException.class),
        INTERNAL_SERVER_ERROR(RuntimeException.class),
        BAD_REQUEST(IllegalArgumentException.class);

        private final Class<? extends Exception> type;

        CommonsErrorCode(Class<? extends Exception> type) {
            this.type = type;
        }

    }

}
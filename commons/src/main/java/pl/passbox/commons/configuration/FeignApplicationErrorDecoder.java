package pl.passbox.commons.configuration;

import feign.Response;
import feign.codec.ErrorDecoder;
import feign.jackson.JacksonDecoder;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FeignApplicationErrorDecoder implements ErrorDecoder {

    private final Map<String, Class<? extends Exception>> dict;

    public FeignApplicationErrorDecoder(ApplicationException.ErrorCode... codes) {
        this.dict = Stream.of(codes).collect(Collectors.toMap(ApplicationException.ErrorCode::name, ApplicationException.ErrorCode::getType));
    }

    @Override
    @SuppressWarnings("unchecked")
    public Exception decode(String method, Response response) {
        try {
            ApiError error = (ApiError) new JacksonDecoder().decode(response, ApiError.class);
            return dict.get(error.getCode()).getConstructor(String.class).newInstance(error.getMessage());
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

}
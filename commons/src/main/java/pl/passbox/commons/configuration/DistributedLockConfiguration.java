package pl.passbox.commons.configuration;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Configuration
@ComponentScan("pl.passbox.commons.locking.service")
@EnableConfigurationProperties(DistributedLockConfiguration.Properties.class)
public class DistributedLockConfiguration {

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @ConfigurationProperties("commons.distributed_lock")
    public static class Properties {

        @NotBlank
        private String prefix;

        @Min(1)
        @NotNull
        private Long lockTtlInSeconds;

        @Min(1)
        @NotNull
        private Long releaseWaitingTimeInSeconds;

    }

}
package pl.passbox.commons.configuration;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiError {

    @NotBlank
    private String code;

    @NotBlank
    private String message;

    public static ApiError fromApplicationException(ApplicationException e) {
        return ApiError.builder().message(e.getMessage()).code(e.getCode().name()).build();
    }

}
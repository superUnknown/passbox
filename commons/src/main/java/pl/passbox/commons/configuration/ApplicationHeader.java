package pl.passbox.commons.configuration;

import lombok.Getter;

@Getter
public enum ApplicationHeader {
    X_FORWARDED_FOR("x-forwarded-for-origin"),
    USER_IDENTITY("x-user-identity");

    private final String value;

    ApplicationHeader(String value) {
        this.value = value;
    }

}
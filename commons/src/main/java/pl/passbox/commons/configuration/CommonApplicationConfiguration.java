package pl.passbox.commons.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("pl.passbox.commons")
public class CommonApplicationConfiguration {
}
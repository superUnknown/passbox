package pl.passbox.commons.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.stream.Collectors;

@Slf4j
@ControllerAdvice
public class ApplicationExceptionHandler {

    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ApplicationException.class)
    public ApiError exceptionHandler(ApplicationException e) {
        ApiError error = ApiError.fromApplicationException(e);
        log.warn(error.toString());
        log.debug(error.toString(), e);
        return error;
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ApiError exceptionHandler(MethodArgumentNotValidException e) {
        ApiError error = ApiError.builder()
                .message(String.format("%s: %s", e.getMessage(), e.getBindingResult().getAllErrors().stream()
                        .map(DefaultMessageSourceResolvable::getDefaultMessage)
                        .collect(Collectors.joining(","))
                ))
                .code(ApplicationException.CommonsErrorCode.BAD_REQUEST.name())
                .build();
        log.warn(error.toString());
        log.debug(error.toString(), e);
        return error;
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ConstraintViolationException.class)
    public ApiError exceptionHandler(ConstraintViolationException e) {
        ApiError error = ApiError.builder()
                .message(String.format("%s: %s", e.getMessage(), e.getConstraintViolations().stream()
                        .map(ConstraintViolation::getMessage)
                        .collect(Collectors.joining(","))
                ))
                .code(ApplicationException.CommonsErrorCode.BAD_REQUEST.name())
                .build();
        log.warn(error.toString());
        log.debug(error.toString(), e);
        return error;
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({
            MissingServletRequestParameterException.class,
            HttpMediaTypeNotSupportedException.class,
            HttpMessageNotReadableException.class,
            IllegalArgumentException.class
    })
    public ApiError badRequest(Exception e) {
        ApiError error = ApiError.builder()
                .code(ApplicationException.CommonsErrorCode.BAD_REQUEST.name())
                .message(e.getMessage())
                .build();
        log.warn(error.toString());
        log.debug(error.toString(), e);
        return error;
    }

    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public ApiError exceptionHandler(Exception e) {
        ApiError error = ApiError.builder()
                .code(ApplicationException.CommonsErrorCode.INTERNAL_SERVER_ERROR.name())
                .message(e.getMessage())
                .build();
        log.error(error.toString(), e);
        return error;
    }

}
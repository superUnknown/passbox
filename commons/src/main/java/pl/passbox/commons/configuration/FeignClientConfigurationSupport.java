package pl.passbox.commons.configuration;

import feign.Feign;
import feign.Logger;
import feign.codec.Decoder;
import feign.codec.Encoder;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.okhttp.OkHttpClient;
import feign.slf4j.Slf4jLogger;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;

public class FeignClientConfigurationSupport {

    private static final OkHttpClient client = new OkHttpClient();

    private static final Encoder encoder = new JacksonEncoder();

    private static final Decoder decoder = new JacksonDecoder();

    private static final SpringMvcContract contract = new SpringMvcContract();

    public static <T> T build(Class<T> clazz, String url, FeignApplicationErrorDecoder errorDecoder) {
        return Feign.builder()
                .logger(new Slf4jLogger(clazz))
                .logLevel(Logger.Level.FULL)
                .errorDecoder(errorDecoder)
                .contract(contract)
                .encoder(encoder)
                .decoder(decoder)
                .client(client)
                .target(clazz, url);
    }

}

package pl.passbox.commons.validation;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.InvalidPropertyException;
import org.springframework.beans.PropertyAccessException;
import org.springframework.beans.PropertyAccessorFactory;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import javax.validation.ValidationException;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.util.Objects.isNull;


@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = EqualProperties.Validator.class)
@Documented
public @interface EqualProperties {

    String message() default "Given properties are not equal.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String[] value();

    @Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    @interface List {
        EqualProperties[] value();
    }

    class Validator implements ConstraintValidator<EqualProperties, Object> {

        private String[] properties;

        @Override
        public void initialize(EqualProperties constraint) {
            properties = constraint.value();
            if (properties.length < 2) {
                throw new ValidationException("You have to define at least 2 properties");
            }
        }

        @Override
        public boolean isValid(Object o, ConstraintValidatorContext context) {
            if (o == null) {
                return true;
            }

            try {
                context.disableDefaultConstraintViolation();
                boolean valid = true;
                BeanWrapper wrapper = PropertyAccessorFactory.forBeanPropertyAccess(o);
                Object original = wrapper.getPropertyValue(properties[0]);
                for (String property : properties) {
                    Object copy = wrapper.getPropertyValue(property);
                    if (isNull(copy)) {
                        continue;
                    }
                    if (!copy.equals(original)) {
                        valid = false;
                        context.buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate())
                                .addPropertyNode(property)
                                .addConstraintViolation();
                    }
                }
                return valid;
            } catch (InvalidPropertyException | PropertyAccessException e) {
                throw new ValidationException(e);
            }
        }

    }

}